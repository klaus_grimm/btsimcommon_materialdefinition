# - Try to find ASSIMP
# Once done, this will define
#
#  ASSIMP_FOUND        - system has ASSIMP 
#  ASSIMP_INCLUDE_DIRS - the ALSA include directories
#  ASSIMP_LIBRARIES    - link these to use ASSIMP
#
# See documentation on how to write CMake scripts at
# http://www.cmake.org/Wiki/CMake:How_To_Find_Libraries

include(LibFindMacros)

# This package has dependencies to other packages
#libfind_package(ASSIMP xyz)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(ASSIMP_PKGCONF assimp)

# Find the main include directory
find_path(ASSIMP_INCLUDE_DIR assimp/version.h
  $ENV{ASSIMP_ROOT_DIR}/include 
  ${ASSIMP_PKGCONF_INCLUDE_DIRS}
)

# Find the library directory
find_library(ASSIMP_LIBRARY
  NAMES assimp
  PATHS
  $ENV{ASSIMP_ROOT_DIR}/lib 
  ${ASSIMP_PKGCONF_LIBRARY_DIRS}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(ASSIMP_PROCESS_INCLUDES ASSIMP_INCLUDE_DIR)
set(ASSIMP_PROCESS_LIBS     ASSIMP_LIBRARY)

libfind_process(ASSIMP)

