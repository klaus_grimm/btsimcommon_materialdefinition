#include(Configuration)

set(CLHEP_FOUND FALSE)

find_program( CLHEP_CONFIG_EXECUTABLE clhep-config
  /usr/bin/
  /usr/local/bin
  $ENV{CLHEP_BASE_DIR}/bin
  )

if( CLHEP_CONFIG_EXECUTABLE )
  set( CLHEP_FOUND ON )
endif()

execute_process(
  COMMAND sh "${CLHEP_CONFIG_EXECUTABLE}" --prefix
  OUTPUT_VARIABLE CLHEP_home
  RESULT_VARIABLE RET
  ERROR_QUIET
  )
if( RET EQUAL 0 )
  string(STRIP "${CLHEP_home}" CLHEP_home )
  separate_arguments( CLHEP_home )
endif()

execute_process(
  COMMAND sh "${CLHEP_CONFIG_EXECUTABLE}" --version
  OUTPUT_VARIABLE CLHEP_native_version
  RESULT_VARIABLE RET
  ERROR_QUIET
  )
if( RET EQUAL 0 )
  string(STRIP "${CLHEP_native_version}" CLHEP_native_version )
  separate_arguments( CLHEP_native_version )
  string(REGEX REPLACE "[a-zA-Z]+([0-9]+[.][0-9]+[.][0-9]+.*)" "\\1" version ${CLHEP_native_version})
  set(CLHEP_native_version ${version})
endif()

#set(CLHEP_native_version ${CLHEP_config_version})
#set(CLHEP_home ${LCG_external}/clhep/${CLHEP_native_version}/${LCG_system})
set(CLHEP_VERSION "${CLHEP_native_version}")

# execute_process(
#   COMMAND sh "${CLHEP_CONFIG_EXECUTABLE}" --libs
#   OUTPUT_VARIABLE CLHEP_LIBRARIES
#   RESULT_VARIABLE RET
#   ERROR_QUIET
#   )
# if( RET EQUAL 0 )
#   string(STRIP "${CLHEP_LIBRARIES}" CLHEP_LIBRARIES )
#   separate_arguments( CLHEP_LIBRARIES )
# endif()

set(CLHEP_INCLUDE_DIRS ${CLHEP_home}/include)
set(CLHEP_LIBRARY_DIRS ${CLHEP_home}/lib)
set(CLHEP_LIBRARIES  
        CLHEP-Cast-${CLHEP_native_version}
        CLHEP-Evaluator-${CLHEP_native_version}
        CLHEP-Exceptions-${CLHEP_native_version}
        CLHEP-GenericFunctions-${CLHEP_native_version}
        CLHEP-Geometry-${CLHEP_native_version}
        CLHEP-Random-${CLHEP_native_version}
        CLHEP-RandomObjects-${CLHEP_native_version}
        CLHEP-RefCount-${CLHEP_native_version}
        CLHEP-Vector-${CLHEP_native_version}
        CLHEP-Matrix-${CLHEP_native_version} )

if(WIN32)
  set(CLHEP_environment PATH+=${CLHEP_home}/lib)
else()
  set(CLHEP_environment LD_LIBRARY_PATH+=${CLHEP_home}/lib )
endif()
 