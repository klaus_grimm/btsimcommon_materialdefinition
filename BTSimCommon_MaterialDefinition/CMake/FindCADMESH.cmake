# - Try to find CADMESH
# Once done, this will define
#
#  CADMESH_FOUND        - system has CADMESH 
#  CADMESH_INCLUDE_DIRS - the CADMESH include directories
#  CADMESH_LIBRARIES    - link these to use CADMESH
#
# See documentation on how to write CMake scripts at
# http://www.cmake.org/Wiki/CMake:How_To_Find_Libraries

include(LibFindMacros)

# This package has dependencies to other packages
libfind_package(CADMESH ASSIMP)

# Use pkg-config to get hints about paths
libfind_pkg_check_modules(CADMESH_PKGCONF cadmesh)

# Find the main include directory
find_path(CADMESH_INCLUDE_DIR CADMesh.hh
  $ENV{CADMESH_ROOT_DIR}/include 
  ${CADMESH_PKGCONF_INCLUDE_DIRS}
)

# Find the library directory
find_library(CADMESH_LIBRARY
  NAMES cadmesh
  PATHS
  $ENV{CADMESH_ROOT_DIR}/lib 
  ${CADMESH_PKGCONF_LIBRARY_DIRS}
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(CADMESH_PROCESS_INCLUDES CADMESH_INCLUDE_DIR)
set(CADMESH_PROCESS_LIBS     CADMESH_LIBRARY)

libfind_process(CADMESH)

