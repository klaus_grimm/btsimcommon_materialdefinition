//=============================================================================
///
///   \file  BTSimCommon_MaterialParameters.hh
///
///   \brief Implementation of the Material Parameter class
///
//=============================================================================
///
///   \author Klaus Hans Grimm  <klaus.grimm@gmail.com>
///
///   \date   18.03.2015
///
//=============================================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef BTSimCommon_MaterialParameters_h
#define BTSimCommon_MaterialParameters_h 1

//---------------
// User includes
//---------------

//-----------------
// Geant4 includes
//-----------------
#include "globals.hh"

#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"
#include "G4VUserDetectorConstruction.hh"

#include "G4Element.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4MaterialPropertiesTable.hh"


#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4LogicalSkinSurface.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4SurfaceProperty.hh"
#include "G4UImanager.hh"
#include "G4OpticalSurface.hh"
#include "G4UserLimits.hh"


#include "G4ios.hh"

//---------------
// ROOT includes
//---------------
#include "TMath.h"
#include "Math/Interpolator.h"
#include "TString.h"

//-----------------------
// C++ standard includes
//-----------------------
#include <iostream>     // std::cout
#include <fstream>      // contains ifstream
#include <string>
#include <sstream>
#include <limits>
#include <utility>      // std::pair, std::make_pair
#include <vector>

//===============
// Geant4 classes
//===============

class G4Element;
class G4Material;
class G4VPVParameterisation;
class G4UserLimits;
class G4OpticalSurface;
class G4NistManager;
class G4MaterialPropertiesTable;
class G4LogicalBorderSurface;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class BTSimCommon_MaterialParameters
{
  public:

              BTSimCommon_MaterialParameters();
             ~BTSimCommon_MaterialParameters();

  public:

     static BTSimCommon_MaterialParameters* GetInstance();

     G4Material*         GetMaterialDefinitionByName(G4String);
     G4OpticalSurface*   GetSurfaceObjectByName(G4String);

     void      SetOpticalAbsorptionLength_ZnS_Ag(G4double);
     G4double  GetOpticalAbsorptionLength_ZnS_Ag(G4double);

     void      SetIndexOfRefraction_MagicOpticalBond(G4double);
     G4double  GetIndexOfRefraction_MagicOpticalBond();

     void Initialize_EmissionSpectrum   (TString, G4MaterialPropertyVector* );

     void Initialize_EnergyResponse     (TString, G4double, G4MaterialPropertyVector*, G4MaterialPropertyVector* );
     void Initialize_AbsorptionLength   (TString, G4MaterialPropertyVector* );
     void Initialize_Reflectance        (TString, G4MaterialPropertyVector* );

     void Initialize_AbsorptionLengthThroughTransmission_Sellmeier ( TString                   filename_transmission,
    		                                                         G4bool                    bFlagCalculateInternalTransmission,
    		                                                         G4double                  fSellmeierCoeffcient[7],
    		                                                         G4double                  thickness_mm,
																	 G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength );



     void Initialize_AbsorptionLengthThroughTransmission_Cauchy ( TString                   filename_transmission,
     		                                                      G4bool                    bFlagCalculateInternalTransmission,
     		                                                      G4double                  fCauchyCoeffcient[3],
     		                                                      G4double                  thickness_mm,
																  G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength );

     void Initialize_IndexOfRefraction                   ( TString,                            // filename
    		                                               G4MaterialPropertyVector*,          // real      part index of refraction
														   G4MaterialPropertyVector*,          // imaginary part index of refraction
														   G4MaterialPropertyVector*,          // absolute       index of refraction
														   G4MaterialPropertyVector* );        // absorption length based in real/imag


     //--------------------------------------------------------------------------------------------------------------------------
     // World materials
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_Vacuum();
     G4Material* GetMaterial_Air();


     //--------------------------------------------------------------------------------------------------------------------------
     // Unsorted materials
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_CarbonFiber();
     G4Material* GetMaterial_Ceramic();
     G4Material* GetMaterial_Concrete();
     G4Material* GetMaterial_Enamel();
     G4Material* GetMaterial_Fiberglass_EGlass();
     G4Material* GetMaterial_Graphite_Electrodag5513();
     G4Material* GetMaterial_RockWool();
     G4Material* GetMaterial_Wood();


     //--------------------------------------------------------------------------------------------------------------------------
     // Metals and alloys
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_Aluminum();
     G4Material* GetMaterial_Al2007();
     G4Material* GetMaterial_Al6082();
     G4Material* GetMaterial_Al7075T6();

     G4Material* GetMaterial_Bronze_C38500();
     G4Material* GetMaterial_Copper();
     G4Material* GetMaterial_Dural();
     G4Material* GetMaterial_Gold();
     G4Material* GetMaterial_Iron();
     G4Material* GetMaterial_Lead();
     G4Material* GetMaterial_MUMetal();
     G4Material* GetMaterial_Nickel();
     G4Material* GetMaterial_Silver();

     G4Material* GetMaterial_StainlessSteel();
     G4Material* GetMaterial_StainlessSteel_304();
     G4Material* GetMaterial_StainlessSteel_316L();
     G4Material* GetMaterial_StainlessSteel_446();

     G4Material* GetMaterial_Tin();
     G4Material* GetMaterial_Titanium();
     G4Material* GetMaterial_Tungsten();


     //--------------------------------------------------------------------------------------------------------------------------
     // Drift chamber gases
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_ArCH4Gas_9010_858();
     G4Material* GetMaterial_ArCH4Gas_9307();

     G4Material* GetMaterial_ArCO2Gas_7030();
     G4Material* GetMaterial_ArCO2Gas_8020();

     G4Material* GetMaterial_CF4();

     G4Material* GetMaterial_EthylChloride();
     G4Material* GetMaterial_IsoHexane();
     G4Material* GetMaterial_TOLFGas();

     G4Material* GetMaterial_Helium3_100_4000();

     G4Material* GetMaterial_Krypton_55();
     G4Material* GetMaterial_KrCH4Gas_9307();
     G4Material* GetMaterial_KrCO2Gas_8020();

     G4Material* GetMaterial_P10Gas();

     G4Material* GetMaterial_XeCO2Gas_8020();
     G4Material* GetMaterial_XeCO2CF4Gas_702703();


     //--------------------------------------------------------------------------------------------------------------------------
     // Materials for neutron detection
     //--------------------------------------------------------------------------------------------------------------------------

     // Boron related
     G4Material* GetMaterial_Boron_Enriched();
     G4Material* GetMaterial_BoronCarbide_Absorber();

     // Gadolinium related
     G4Material* GetMaterial_Gadolinium();
     G4Material* GetMaterial_Gd155enriched();
     G4Material* GetMaterial_Gd157enriched();

     // Lithium related
     G4Element*  GetElement_Li6_Enriched();
     G4Material* GetMaterial_LiF();
     G4Material* GetMaterial_GS20();


     //--------------------------------------------------------------------------------------------------------------------------
     // Tissue equivalent materials
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_A150Plastics();
     G4Material* GetMaterial_ICRU_Phantom();


     //--------------------------------------------------------------------------------------------------------------------------
     // Materials for insulation
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_Delrin();
     G4Material* GetMaterial_FR4();
     G4Material* GetMaterial_Frialit();
     G4Material* GetMaterial_G10();
     G4Material* GetMaterial_Kapton();
     G4Material* GetMaterial_Macor();
     G4Material* GetMaterial_Noryl();
     G4Material* GetMaterial_PEEK();
     G4Material* GetMaterial_PVC();
     G4Material* GetMaterial_Teflon();


     //--------------------------------------------------------------------------------------------------------------------------
     // Materials for foils and wrappings
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_Havar();
     G4Material* GetMaterial_Hostaphan();
     G4Material* GetMaterial_MakrofolKG();
     G4Material* GetMaterial_Mylar();
     G4Material* GetMaterial_Tyvek();


     //--------------------------------------------------------------------------------------------------------------------------
     // Materials for glueing/bonding/coupling
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_Epoxy();
     G4Material* GetMaterial_EpoxyResin();
     G4Material* GetMaterial_MagicOpticalBond();
     G4Material* GetMaterial_MeltMount_1582();
     G4Material* GetMaterial_MeltMount_1704();
     G4Material* GetMaterial_OpticalCement();
     G4Material* GetMaterial_OpticalGrease();

     G4Material* GetMaterial_C19H20O4();
     G4Material* GetMaterial_C10H18O4();
     G4Material* GetMaterial_C9H22N2();
     G4Material* GetMaterial_Epotek_301_1();

     G4Material* GetMaterial_CAS_101_77_9();
     G4Material* GetMaterial_CAS_9003_36_5();
     G4Material* GetMaterial_CAS_10563_29_8();
     G4Material* GetMaterial_CAS_25068_38_6();

     G4Material* GetMaterial_Araldite_AW106();
     G4Material* GetMaterial_Hardener_HV953U();
     G4Material* GetMaterial_Epoxy_AW106_HV953U();

     G4Material* GetMaterial_Araldite_F();
     G4Material* GetMaterial_Hardener_HT972();
     G4Material* GetMaterial_Epoxy_F_HT972();

     //--------------------------------------------------------------------------------------------------------------------------
     // Materials for light transportation
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_Acrylic();
     G4Material* GetMaterial_FGL400();
     G4Material* GetMaterial_FusedSilica();
     G4Material* GetMaterial_Perspex_Opal069();
     G4Material* GetMaterial_Plexiglas_GS233();
     G4Material* GetMaterial_PMMA();


     //--------------------------------------------------------------------------------------------------------------------------
     // Scintillator materials
     //--------------------------------------------------------------------------------------------------------------------------

     // Organic scintillators
     G4Material* GetMaterial_BC400();
     G4Material* GetMaterial_BC408();
     G4Material* GetMaterial_BC91A();
     G4Material* GetMaterial_EJ280();

     // Inorganic scintillators
     G4Material* GetMaterial_BaF2();
     G4Material* GetMaterial_BaF2_Immediate();
     G4Material* GetMaterial_BaF2_Perfect();
     G4Material* GetMaterial_BaF2_PerfectFast();
     G4Material* GetMaterial_BaF2_Template();

     G4Material* GetMaterial_LaBr3_Ce();
     G4Material* GetMaterial_LYSO_Ce();
     G4Material* GetMaterial_NaI();
     G4Material* GetMaterial_ZnS_Ag();


     //--------------------------------------------------------------------------------------------------------------------------
     // PMT materials
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_BialkaliCathode();
     G4Material* GetMaterial_BorosilicateGlass();


     //--------------------------------------------------------------------------------------------------------------------------
     // SiPM materials
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* GetMaterial_BoronCarbide_Ceramics();
     G4Material* GetMaterial_Silicon();
     G4Material* GetMaterial_SiliconDioxide();


     //==========================================================================================================================

     G4OpticalSurface* GetOpticalSurface_BGOPolishedAir();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedLumirrorAir();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedLumirrorGlue();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedSpectraflectAir();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedSpectralonAir();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedTeflonAir();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedTioAir();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedTyvekAir();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedVM2000Air();
     G4OpticalSurface* GetOpticalSurface_BGOPolishedVM2000Glue();

     G4OpticalSurface* GetOpticalSurface_BGOGroundAir();
     G4OpticalSurface* GetOpticalSurface_BGOGroundLumirrorAir();
     G4OpticalSurface* GetOpticalSurface_BGOGroundLumirrorGlue();
     G4OpticalSurface* GetOpticalSurface_BGOGroundSpectraflectAir();
     G4OpticalSurface* GetOpticalSurface_BGOGroundSpectralonAir();
     G4OpticalSurface* GetOpticalSurface_BGOGroundTeflonAir();
     G4OpticalSurface* GetOpticalSurface_BGOGroundTioAir();
     G4OpticalSurface* GetOpticalSurface_BGOGroundTyvekAir();
     G4OpticalSurface* GetOpticalSurface_BGOGroundVM2000Air();
     G4OpticalSurface* GetOpticalSurface_BGOGroundVM2000Glue();

     G4OpticalSurface* GetOpticalSurface_PerfectSmooth();
     G4OpticalSurface* GetOpticalSurface_DiamondMilled();

     G4OpticalSurface* GetOpticalSurface_GenericGroundAir();
     G4OpticalSurface* GetOpticalSurface_GenericPolishedAir();
     G4OpticalSurface* GetOpticalSurface_GenericPolishedWhitePainted();
     G4OpticalSurface* GetOpticalSurface_GenericGroundWhitePainted();
     G4OpticalSurface* GetOpticalSurface_GenericPolishedBlackPainted();
     G4OpticalSurface* GetOpticalSurface_GenericGroundBlackPainted();

     G4OpticalSurface* GetOpticalSurface_AirGroundAluminum();
     G4OpticalSurface* GetOpticalSurface_AirGroundTitanium();
     G4OpticalSurface* GetOpticalSurface_AirGroundSilver();
     G4OpticalSurface* GetOpticalSurface_AirGroundGold();

     G4OpticalSurface* GetOpticalSurface_AirPolishedAluminum();
     G4OpticalSurface* GetOpticalSurface_AirPolishedTitanium();
     G4OpticalSurface* GetOpticalSurface_AirPolishedSilver();
     G4OpticalSurface* GetOpticalSurface_AirPolishedGold();


     G4OpticalSurface* GetOpticalSurface_PhotoCathode();
     G4OpticalSurface* GetOpticalSurface_BlackHole();

     //--------------------------------------------------------------------------------------------

  private:

     //----------------------------------------------------
     static BTSimCommon_MaterialParameters* pInstance;
     //----------------------------------------------------

     G4double              fIndexOfRefraction_MagicOpticalBond;
     G4double              fAbsorptionLength_ZnS_Ag;
     G4double              fAnthracene_LightYield;

     G4double              fdensity;
     std::vector<G4int>    vNatoms;
     std::vector<G4double> vFractionMass;
     std::vector<G4String> vElements;


     //--------------------------------------------------------------------------------------------------------------------------
     // World materials
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* pVacuum_MD;
     G4Material* pAir_MD;

     //--------------------------------------------------------------------------------------------------------------------------
     // Unsorted materials
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* pCarbonFiber_MD;
     G4Material* pCeramic_MD;
     G4Material* pConcrete_MD;
     G4Material* pEnamel_MD;
     G4Material* pFiberglass_EGlass_MD;
     G4Material* pGraphite_Electrodag5513_MD;
     G4Material* pRockWool_MD;
     G4Material* pWood_MD;


     //-----------------------------------------------------------------------------------------------------------------------------
     // Metals and alloys
     //-----------------------------------------------------------------------------------------------------------------------------
     G4Material* pAluminum_MD;                             G4MaterialPropertiesTable* pAluminum_MPT;
     G4Material* pAl2007_MD;
     G4Material* pAl6082_MD;
     G4Material* pAl7075T6_MD;                             G4MaterialPropertiesTable* pAl7075T6_MPT;

     G4Material* pBronze_C38500_MD;
     G4Material* pCopper_MD;                               G4MaterialPropertiesTable* pCopper_MPT;
     G4Material* pDural_MD;                                G4MaterialPropertiesTable* pDural_MPT;
     G4Material* pGold_MD;                                 G4MaterialPropertiesTable* pGold_MPT;
     G4Material* pIron_MD;
     G4Material* pLead_MD;
     G4Material* pMUMetal_MD;
     G4Material* pNickel_MD;
     G4Material* pSilver_MD;                               G4MaterialPropertiesTable* pSilver_MPT;

     G4Material* pStainlessSteel_MD;
     G4Material* pStainlessSteel_304_MD;
     G4Material* pStainlessSteel_316L_MD;
     G4Material* pStainlessSteel_446_MD;

     G4Material* pTin_MD;                                  G4MaterialPropertiesTable* pTin_MPT;
     G4Material* pTitanium_MD;                             G4MaterialPropertiesTable* pTitanium_MPT;
     G4Material* pTungsten_MD;                             G4MaterialPropertiesTable* pTungsten_MPT;

     //---------------------------------------------------------------------------------------------------------------------------
     // Drift chamber gases
     //---------------------------------------------------------------------------------------------------------------------------
     G4Material* pArCH4Gas_9010_858_MD;
     G4Material* pArCH4Gas_9307_MD;

     G4Material* pArCO2Gas_7030_MD;
     G4Material* pArCO2Gas_8020_MD;

     G4Material* pCF4_MD;

     G4Material* pEthylChloride_MD;
     G4Material* pIsoHexane_MD;
     G4Material* pTOLFGas_MD;

     G4Material* pHe3_100_4000_MD;

     G4Material* pKrCH4Gas_9307_MD;
     G4Material* pKrCO2Gas_8020_MD;
     G4Material* pKrypton_55_MD;

     G4Material* pP10Gas_MD;

     G4Material* pXeCO2Gas_8020_MD;
     G4Material* pXeCO2CF4Gas_702703_MD;



     //-----------------------------------------------------------------------------------------------------------------------------
     // Materials for neutron detection
     //-----------------------------------------------------------------------------------------------------------------------------

     // Boron related
     G4Isotope*  pB10_ID;
     G4Isotope*  pB11_ID;

     G4Material* pBoron_enriched_MD;
     G4Material* pBoronCarbide_Absorber_MD;

     // Gadolinium related
     G4Isotope*  pGd152_ID;
     G4Isotope*  pGd154_ID;
     G4Isotope*  pGd155_ID;
     G4Isotope*  pGd156_ID;
     G4Isotope*  pGd157_ID;
     G4Isotope*  pGd158_ID;
     G4Isotope*  pGd160_ID;

     G4Material* pGadolinium_MD;
     G4Material* pGd155_enriched_MD;
     G4Material* pGd157_enriched_MD;

     // Helium related
     G4Material* pHe3_MD;
     G4Element*  pHe3_ED;
     G4Isotope*  pHe3_ID;

     // Lithium related
     G4Isotope*  pLi6_ID;
     G4Isotope*  pLi7_ID;
     G4Element*  pLi6_Enriched_ED;

     G4Material* pLiF_MD;
     G4Material* pGS20_MD;                                 G4MaterialPropertiesTable* pGS20_MPT;

     //--------------------------------------------------------------------------------------------------------------------------
     // Tissue equivalent materials
     //--------------------------------------------------------------------------------------------------------------------------
     G4Material* pA150Plastics_MD;
     G4Material* pICRU_Phantom_MD;


     //-----------------------------------------------------------------------------------------------------------------------------
     // Materials for insulation
     //-----------------------------------------------------------------------------------------------------------------------------
     G4Material* pDelrin_MD;
     G4Material* pFR4_MD;
     G4Material* pFrialit_MD;
     G4Material* pG10_MD;
     G4Material* pKapton_MD;
     G4Material* pMacor_MD;
     G4Material* pNoryl_MD;
     G4Material* pPEEK_MD;
     G4Material* pPVC_MD;
     G4Material* pTeflon_MD;


     //-----------------------------------------------------------------------------------------------------------------------------
     // Materials for foils and wrappings
     //-----------------------------------------------------------------------------------------------------------------------------
     G4Material* pHavar_MD;
     G4Material* pHostaphan_MD;
     G4Material* pMakrofolKG_MD;
     G4Material* pMylar_MD;
     G4Material* pTyvek_MD;


     //-----------------------------------------------------------------------------------------------------------------------------
     // Materials for glueing/bonding/coupling
     //-----------------------------------------------------------------------------------------------------------------------------
     G4Material* pEpoxy_MD;
     G4Material* pEpoxyResin_MD;
     G4Material* pMagicOpticalBond_MD;                     G4MaterialPropertiesTable* pMagicOpticalBond_MPT;
     G4Material* pMeltMount_1582_MD;                       G4MaterialPropertiesTable* pMeltMount_1582_MPT;
     G4Material* pMeltMount_1704_MD;                       G4MaterialPropertiesTable* pMeltMount_1704_MPT;
     G4Material* pOpticalCement_MD;                        G4MaterialPropertiesTable* pOpticalCement_MPT;
     G4Material* pOpticalGrease_MD;                        G4MaterialPropertiesTable* pOpticalGrease_MPT;

     G4Material* pC19H20O4_MD;
     G4Material* pC10H18O4_MD;
     G4Material* pC9H22N2_MD;
     G4Material* pEpotek_301_1_MD;

     G4Material* pCAS_101_77_9_MD;
     G4Material* pCAS_9003_36_5_MD;
     G4Material* pCAS_10563_29_8_MD;
     G4Material* pCAS_25068_38_6_MD;

     G4Material* pAraldite_AW106_MD;
     G4Material* pHardener_HV953U_MD;
     G4Material* pEpoxy_AW106_HV953U_MD;

     G4Material* pAraldite_F_MD;
     G4Material* pHardener_HT972_MD;
     G4Material* pEpoxy_F_HT972_MD;

     //-----------------------------------------------------------------------------------------------------------------------------
     // Materials for optics
     //-----------------------------------------------------------------------------------------------------------------------------
     G4Material* pAcrylic_MD;
     G4Material* pFGL400_MD;                               G4MaterialPropertiesTable* pFGL400_MPT;
     G4Material* pFusedSilica_MD;                          G4MaterialPropertiesTable* pFusedSilica_MPT;
     G4Material* pPerspex_Opal069_MD;                      G4MaterialPropertiesTable* pPerspex_Opal069_MPT;
     G4Material* pPlexiglas_GS233_MD;                      G4MaterialPropertiesTable* pPlexiglas_GS233_MPT;
     G4Material* pPMMA_MD;


     //-----------------------------------------------------------------------------------------------------------------------------
     // Scintillator materials
     //-----------------------------------------------------------------------------------------------------------------------------

     // Organic scintillators
     G4Material* pBC400_MD;                                G4MaterialPropertiesTable* pBC400_MPT;
     G4Material* pBC408_MD;                                G4MaterialPropertiesTable* pBC408_MPT;
     G4Material* pBC91A_MD;                                G4MaterialPropertiesTable* pBC91A_MPT;
     G4Material* pEJ280_MD;                                G4MaterialPropertiesTable* pEJ280_MPT;

     // Inorganic scintillators
     G4Material* pBaF2_MD;                                 G4MaterialPropertiesTable* pBaF2_MPT;
     G4Material* pBaF2_Immediate_MD;                       G4MaterialPropertiesTable* pBaF2_Immediate_MPT;
     G4Material* pBaF2_Perfect_MD;                         G4MaterialPropertiesTable* pBaF2_Perfect_MPT;
     G4Material* pBaF2_PerfectFast_MD;                     G4MaterialPropertiesTable* pBaF2_PerfectFast_MPT;
     G4Material* pBaF2_Template_MD;                        G4MaterialPropertiesTable* pBaF2_Template_MPT;

     G4Material* pLaBr3_Ce_MD;                             G4MaterialPropertiesTable* pLaBr3_Ce_MPT;
     G4Material* pLYSO_MD;
     G4Material* pLYSO_Ce_MD;                              G4MaterialPropertiesTable* pLYSO_Ce_MPT;
     G4Material* pNaI_MD;                                  G4MaterialPropertiesTable* pNaI_MPT;
     G4Material* pZnS_Ag_MD;                               G4MaterialPropertiesTable* pZnS_Ag_MPT;


     //-----------------------------------------------------------------------------------------------------------------------------
     // PMT materials
     //-----------------------------------------------------------------------------------------------------------------------------
     G4Material* pBialkaliCathode_MD;                      G4MaterialPropertiesTable* pBialkaliCathode_MPT;
     G4Material* pBorosilicateGlass_MD;                    G4MaterialPropertiesTable* pBorosilicateGlass_MPT;


     //-----------------------------------------------------------------------------------------------------------------------------
     // SiPM materials
     //-----------------------------------------------------------------------------------------------------------------------------
     G4Material* pBoronCarbide_Ceramics_MD;
     G4Material* pSilicon_MD;                              G4MaterialPropertiesTable* pSilicon_MPT;
     G4Material* pSiliconDioxide_MD;



  private:

     G4MaterialPropertyVector* pMPV_DetectionEfficiency_Zero;

	 // Optical Surfaces ending with _OS

     G4OpticalSurface*   pBGOPolishedAir_OS;                // polished BGO surface facing  air only
     G4OpticalSurface*   pBGOPolishedLumirrorAir_OS;        // polished BGO surface wrapped with Lumirror foil
     G4OpticalSurface*   pBGOPolishedLumirrorGlue_OS;       // polished BGO surface glued   with Lumirror foil
     G4OpticalSurface*   pBGOPolishedSpectraflectAir_OS;    // polished BGO surface coated with TiO2 (Titanium Dioxide), but with reflectance of Spectraflect == BaSO4
	 G4OpticalSurface*   pBGOPolishedSpectralonAir_OS;      // polished BGO surface coated with TiO2 (Titanium Dioxide), but with reflectance of Spectralon   == PTFE
     G4OpticalSurface*   pBGOPolishedTeflonAir_OS;          // polished BGO surface wrapped with Teflon+Air
     G4OpticalSurface*   pBGOPolishedTioAir_OS;             // polished BGO surface coated with TiO2 (Titanium Dioxide)
     G4OpticalSurface*   pBGOPolishedTyvekAir_OS;           // polished BGO surface wrapped with Tyvek+Air
     G4OpticalSurface*   pBGOPolishedVM2000Air_OS;          // polished BGO surface wrapped with VM2000 foil
     G4OpticalSurface*   pBGOPolishedVM2000Glue_OS;         // polished BGO surface glued   with VM2000 foil

     G4OpticalSurface*   pBGOGroundAir_OS;                  // rough-cut BGO surface facing  air only
     G4OpticalSurface*   pBGOGroundLumirrorAir_OS;          // rough-cut BGO surface wrapped with Lumirror foil
     G4OpticalSurface*   pBGOGroundLumirrorGlue_OS;         // rough-cut BGO surface glued   with Lumirror foil
     G4OpticalSurface*   pBGOGroundSpectraflectAir_OS;      // rough-cut BGO surface coated with TiO2 (Titanium Dioxide), but with reflectance of Spectraflect == BaSO4
	 G4OpticalSurface*   pBGOGroundSpectralonAir_OS;        // rough-cut BGO surface coated with TiO2 (Titanium Dioxide), but with reflectance of Spectralon   == PTFE
     G4OpticalSurface*   pBGOGroundTeflonAir_OS;            // rough-cut BGO surface wrapped with Teflon+Air
     G4OpticalSurface*   pBGOGroundTioAir_OS;               // rough-cut BGO surface coated with TiO (TitaniumOxyid)
     G4OpticalSurface*   pBGOGroundTyvekAir_OS;             // rough-cut BGO surface wrapped with Tyvek+Air
     G4OpticalSurface*   pBGOGroundVM2000Air_OS;            // rough-cut BGO surface wrapped with VM2000 foil
     G4OpticalSurface*   pBGOGroundVM2000Glue_OS;           // rough-cut BGO surface glued   with VM2000 foil

     G4OpticalSurface*   pPerfectPolished_OS;               // perfect surface w/o any roughness (ideal case)
     G4OpticalSurface*   pDiamondMilled_OS;                 // diamond milled surface

     G4OpticalSurface*   pGenericGroundAir_OS;              // ground   generic surface facing air only
     G4OpticalSurface*   pGenericPolishedAir_OS;            // polished generic surface facing air only
     G4OpticalSurface*   pGenericPolishedWhitePainted_OS;   // polished generic surface, painted white
     G4OpticalSurface*   pGenericGroundWhitePainted_OS;     // ground   generic surface, painted white
     G4OpticalSurface*   pGenericPolishedBlackPainted_OS;   // polished generic surface, painted black
     G4OpticalSurface*   pGenericGroundBlackPainted_OS;     // ground   generic surface, painted black

     G4OpticalSurface*   pAirGroundAluminum_OS;             // air facing ground aluminum surface
     G4OpticalSurface*   pAirGroundTitanium_OS;             // air facing ground titanium surface
     G4OpticalSurface*   pAirGroundSilver_OS;               // air facing ground silver   surface
     G4OpticalSurface*   pAirGroundGold_OS;                 // air facing ground gold     surface

     G4OpticalSurface*   pAirPolishedAluminum_OS;           // air facing polished aluminum surface
     G4OpticalSurface*   pAirPolishedTitanium_OS;           // air facing polished titanium surface
     G4OpticalSurface*   pAirPolishedSilver_OS;             // air facing polished silver   surface
     G4OpticalSurface*   pAirPolishedGold_OS;               // air facing polished gold     surface

     G4OpticalSurface*   pPhotoCathode_OS;                  // optical surface between PMT entrance window and cathode
     G4OpticalSurface*   pBlackHole_OS;                     // perfect light absorber

     G4double GetIndexOfRefraction_SellmeierEquation (G4double, G4double, G4double, G4double, G4double, G4double, G4double, G4double);
     G4double GetIndexOfRefraction_CauchyEquation    (G4double, G4double, G4double, G4double);

     G4double GetDensityOfAir(G4double, G4double, G4double);

     void Initialize();
     void Update();

     void DefineIsotopes_Boron();
     void DefineIsotopes_Gadolinium();
     void DefineIsotopes_Helium();
     void DefineIsotopes_Lithium();


     G4NistManager*            pNistMaterialManager;

  private:

     // Path for material definition, e.g. light yield vs. wavelength
     TString                  sMaterialDefinition_FilePath;

     //============================================================================================
     //                        Coating/Wrapping alias Reflectance related
     //============================================================================================

     //-------------------------------------------
     // Lumirror related
     //-------------------------------------------
     G4MaterialPropertiesTable* pCoating_Lumirror_MPT;


     //------------------------------
     // Teflon related
     //------------------------------
     G4MaterialPropertiesTable* pCoating_Teflon_MPT;


     //------------------------------
     // Titanium Oxide (TiO2) related
     //------------------------------
     G4MaterialPropertiesTable* pCoating_TiO2_MPT;


     //---------------
     // Tyvek related
     //---------------
     G4MaterialPropertiesTable* pCoating_Tyvek_MPT;


     //-------------------------------------------
     // Spectralon related (PTFE)
     //-------------------------------------------
     G4MaterialPropertiesTable* pCoating_Spectralon_MPT;


     //-------------------------------------------
     // Spectraflect related (BaSO4)
     //-------------------------------------------
     G4MaterialPropertiesTable* pCoating_Spectraflect_MPT;


     //-------------------------------------------
     // VM2000 related (dielectric mirror from 3M)
     //-------------------------------------------
     G4MaterialPropertiesTable* pCoating_VM2000_MPT;



  protected:

     G4double GetLaBr3_Ce_RefractionIndex(G4double);

     G4double GetFusedSilica_AbsorptionLength(G4double);
     G4double GetFusedSilica_WindowTransmittance(G4double);

     G4double GetBorosilicateGlass_AbsorptionLength(G4double);

     G4double EnergyToLambdaConversion(G4double);
     G4double LambdaToEnergyConversion(G4double);

     std::vector<G4double>  ConvertVolumeToMassFraction          ( std::vector<G4double> vVolumeFraction , std::vector<G4double> vMolarWeight );
     std::vector<G4double>  ConvertPartialPressureToMassFraction ( std::vector<G4double> vPartialPressure, std::vector<G4double> vMolarWeight );

};

#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
