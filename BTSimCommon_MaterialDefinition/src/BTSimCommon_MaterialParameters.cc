//=============================================================================
///
///   \file  BTSimCommon_MaterialParameters.cc
///
///   \brief Implementation of the Material Parameter class
///
//=============================================================================
///
///   \author  Klaus Hans Grimm  <klaus.grimm@gmail.com>
///
///   \date    20.03.2015
///
//=============================================================================
//       Material List Overview: Check lines at 145+
//=============================================================================
//
// How to use it in your code:
//
//--------------------
// Write in your *.hh
//--------------------
//
// #include "BTSimCommon_MaterialParameters.hh"
//
// G4Material*          pMasterContainer_Material;
// G4Material*          pLB1236_Tube_Material;
// G4Material*          pLB1236_TubeCoating_Material;
// G4Material*          pLB1236_GasVolume_Material;
//
//-------------------
// Write in your *cc
//---------------------
//
//  //----------------
//  // In constructor
//  //----------------
//	pMaterial = nullptr;
//	pMaterial = BTSimCommon_MaterialParameters::GetInstance();
//
//  pMasterContainer_Material                 = pMaterial->GetMaterialDefinitionByName("Air");
//  pLB1236_Tube_Material                     = pMaterial->GetMaterialDefinitionByName("Al2007");
//  pLB1236_TubeCoating_Material              = pMaterial->GetMaterialDefinitionByName("Nickel");
//  pLB1236_GasVolume_Material                = pMaterial->GetMaterialDefinitionByName("ArCH4Gas_9010_858mbar");
//
//=====================================================================================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//---------------
// User includes
//---------------
#include "BTSimCommon_MaterialParameters.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BTSimCommon_MaterialParameters* BTSimCommon_MaterialParameters::pInstance = nullptr;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BTSimCommon_MaterialParameters* BTSimCommon_MaterialParameters::GetInstance()
{
	 if ( pInstance == nullptr )
	 {
		  pInstance = new BTSimCommon_MaterialParameters();
	 }

	 return pInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BTSimCommon_MaterialParameters::BTSimCommon_MaterialParameters()
{
	//-------------------------------------------------------------------------
	// This method creates a set of materials which will be used to build
    // the detector. It uses the predefined NIST-derived materials list
	// and adds. If you plan to change or add some property to on material
	// you need to create it as a copy of a NIST-derived to avoid conflict
	// with other subdetectors.
	//-------------------------------------------------------------------------

	pNistMaterialManager = nullptr;
	pNistMaterialManager = G4NistManager::Instance();

	//-------------------------------------------------------------------------
	// Use these if you want extensive output about materials definition
	//-------------------------------------------------------------------------

	//pNistMaterialManager->SetVerbose(1);
	//pNistMaterialManager->PrintElement("all");
	//pNistMaterialManager->ListMaterials("all");

	//-------------------------------------------------------------------------
	// Define data path
	//-------------------------------------------------------------------------

    sMaterialDefinition_FilePath = "./data/Material";   // subdir of build without "/" at the end

    //-------------------------
	// Initialize all pointers
    //------------------------
	this->Initialize();


	 //-----------------------------------------------------------------------
	 // Default value for the optical absorption length of ZnS:Ag
	 // Value can be changed by this->SetOpticalAbsorptionLength_ZnS_Ag(value)
	 //-----------------------------------------------------------------------
	 //
	 this->SetOpticalAbsorptionLength_ZnS_Ag(50.0*CLHEP::micrometer); // best estimate

	 //--------------------------------------------------------------------------------------------
	 // Default value for the index of refraction of the non-existing optical glue MagicOpticalBond
	 // Value can be changed by this->SetIndexOfRefraction_MagicOpticalBond(value)
	 //---------------------------------------------------------------------------------------------
	 //
	 this->SetIndexOfRefraction_MagicOpticalBond(1.58); // matches EJ-280

	 //-----------------------------------------------------------------------
	 // Some scintillator data sheets refer on this number
	 // - DETEC catalog, http://www.detec-rad.com, states 20.0/keV
	 // - Mark Chen from Queen's university states 17.4/keV in a presentation
	 //------------------------------------------------------------------------
	 //
	 fAnthracene_LightYield = 17.4/CLHEP::keV;

	 //------------------------------------------------------------------------------------------------------------------------------
	 // Do not account for border surface hits
	 //------------------------------------------------------------------------------------------------------------------------------
	 pMPV_DetectionEfficiency_Zero = nullptr;
	 pMPV_DetectionEfficiency_Zero = new G4MaterialPropertyVector();

	 pMPV_DetectionEfficiency_Zero ->InsertValues( this->LambdaToEnergyConversion(200.0*CLHEP::nanometer), 0.0 );
	 pMPV_DetectionEfficiency_Zero ->InsertValues( this->LambdaToEnergyConversion(800.0*CLHEP::nanometer), 0.0 );
	 //------------------------------------------------------------------------------------------------------------------------------


	 std::cout << "BTSimCommon_MaterialParameters() construction done!" << std::endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BTSimCommon_MaterialParameters::~BTSimCommon_MaterialParameters()
{

	std::cout << "Calling BTSimCommon_MaterialParameters::~BTSimCommon_MaterialParameters()" << std::endl;

	if (pInstance                 != nullptr )   { delete pInstance;                          pInstance = nullptr; }

	//==========================
	// Delete Material pointers
	//==========================

	//-----------------------------------------------------------------------------------------------------------------------------------------------
    // World materials
    //-----------------------------------------------------------------------------------------------------------------------------------------------
	if (pVacuum_MD                     != nullptr )   { delete pVacuum_MD;                         pVacuum_MD                    = nullptr; };
	if (pAir_MD                        != nullptr )   { delete pAir_MD;                            pAir_MD                       = nullptr; };

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// Unsorted materials
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if (pCarbonFiber_MD                != nullptr )	  { delete pCarbonFiber_MD;                    pCarbonFiber_MD               = nullptr; };
	if (pCeramic_MD                    != nullptr )	  { delete pCeramic_MD;                        pCeramic_MD                   = nullptr; };
	if (pConcrete_MD                   != nullptr )   { delete pConcrete_MD;                       pConcrete_MD                  = nullptr; };
	if (pEnamel_MD                     != nullptr )	  { delete pEnamel_MD;                         pEnamel_MD                    = nullptr; };
	if (pFiberglass_EGlass_MD          != nullptr )	  { delete pFiberglass_EGlass_MD;              pFiberglass_EGlass_MD         = nullptr; };
	if (pGraphite_Electrodag5513_MD    != nullptr )	  { delete pGraphite_Electrodag5513_MD;        pGraphite_Electrodag5513_MD   = nullptr; };
	if (pRockWool_MD                   != nullptr )   { delete pRockWool_MD;                       pRockWool_MD                  = nullptr; };
	if (pWood_MD                       != nullptr )   { delete pWood_MD;                           pWood_MD                      = nullptr; };


	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// Metals and alloys
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pAluminum_MD                  != nullptr )   { delete pAluminum_MD;                       pAluminum_MD                  = nullptr; };
	if ( pAl2007_MD                    != nullptr )   { delete pAl2007_MD;                         pAl2007_MD                    = nullptr; };
	if ( pAl6082_MD                    != nullptr )	  { delete pAl6082_MD;                         pAl6082_MD                    = nullptr; };
	if ( pAl7075T6_MD                  != nullptr )	  { delete pAl7075T6_MD;                       pAl7075T6_MD                  = nullptr; };

	if ( pBronze_C38500_MD             != nullptr )   { delete pBronze_C38500_MD;                  pBronze_C38500_MD             = nullptr; };
	if ( pCopper_MD                    != nullptr )   { delete pCopper_MD;                         pCopper_MD                    = nullptr; };
	if ( pDural_MD                     != nullptr )   { delete pDural_MD;                          pDural_MD                     = nullptr; };
	if ( pGold_MD                      != nullptr )   { delete pGold_MD;                           pGold_MD                      = nullptr; };
	if ( pIron_MD                      != nullptr )   { delete pIron_MD;                           pIron_MD                      = nullptr; };
	if ( pLead_MD                      != nullptr )   { delete pLead_MD;                           pLead_MD                      = nullptr; };
	if ( pMUMetal_MD                   != nullptr )   { delete pMUMetal_MD;                        pMUMetal_MD                   = nullptr; };
	if ( pNickel_MD                    != nullptr )   { delete pNickel_MD;                         pNickel_MD                    = nullptr; };
	if ( pSilver_MD                    != nullptr )   { delete pSilver_MD;                         pSilver_MD                    = nullptr; };

	if ( pStainlessSteel_MD            != nullptr )   { delete pStainlessSteel_MD;                 pStainlessSteel_MD            = nullptr; };
	if ( pStainlessSteel_304_MD        != nullptr )   { delete pStainlessSteel_304_MD;             pStainlessSteel_304_MD        = nullptr; };
	if ( pStainlessSteel_316L_MD       != nullptr )   { delete pStainlessSteel_316L_MD;            pStainlessSteel_316L_MD       = nullptr; };
	if ( pStainlessSteel_446_MD        != nullptr )   { delete pStainlessSteel_446_MD;             pStainlessSteel_446_MD        = nullptr; };

	if ( pTin_MD                       != nullptr )   { delete pTin_MD;                            pTin_MD                       = nullptr; };
	if ( pTitanium_MD                  != nullptr )   { delete pTitanium_MD;                       pTitanium_MD                  = nullptr; };
	if ( pTungsten_MD                  != nullptr )   { delete pTungsten_MD;                       pTungsten_MD                  = nullptr; };


    //-----------------------------------------------------------------------------------------------------------------------------------------------
    // Drift chamber gases
    //-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pArCH4Gas_9010_858_MD         != nullptr )   { delete pArCH4Gas_9010_858_MD;              pArCH4Gas_9010_858_MD         = nullptr; };
	if ( pArCH4Gas_9307_MD             != nullptr )	  { delete pArCH4Gas_9307_MD;                  pArCH4Gas_9307_MD             = nullptr; };
	if ( pArCO2Gas_7030_MD             != nullptr )   { delete pArCO2Gas_7030_MD;                  pArCO2Gas_7030_MD             = nullptr; };
	if ( pArCO2Gas_8020_MD             != nullptr )   { delete pArCO2Gas_8020_MD;                  pArCO2Gas_8020_MD             = nullptr; };

	if ( pCF4_MD                       != nullptr )   { delete pCF4_MD;                            pCF4_MD                       = nullptr; };

	if ( pEthylChloride_MD             != nullptr )   { delete pEthylChloride_MD;                  pEthylChloride_MD             = nullptr; };
	if ( pIsoHexane_MD                 != nullptr )   { delete pIsoHexane_MD;                      pIsoHexane_MD                 = nullptr; };
	if ( pTOLFGas_MD                   != nullptr )   { delete pTOLFGas_MD;                        pTOLFGas_MD                   = nullptr; };

	if ( pHe3_100_4000_MD              != nullptr )   { delete pHe3_100_4000_MD;                   pHe3_100_4000_MD              = nullptr; };

	if ( pKrCH4Gas_9307_MD             != nullptr )   { delete pKrCH4Gas_9307_MD;                  pKrCH4Gas_9307_MD             = nullptr; };
	if ( pKrCO2Gas_8020_MD             != nullptr )   { delete pKrCO2Gas_8020_MD;                  pKrCO2Gas_8020_MD             = nullptr; };
	if ( pKrypton_55_MD                != nullptr )   { delete pKrypton_55_MD;                     pKrypton_55_MD                = nullptr; };

	if ( pP10Gas_MD                    != nullptr )   { delete pP10Gas_MD;                         pP10Gas_MD                    = nullptr; };
	if ( pXeCO2Gas_8020_MD             != nullptr )   { delete pXeCO2Gas_8020_MD;                  pXeCO2Gas_8020_MD             = nullptr; };
	if ( pXeCO2CF4Gas_702703_MD        != nullptr )   { delete pXeCO2CF4Gas_702703_MD;             pXeCO2CF4Gas_702703_MD        = nullptr; };


	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// Materials for neutron detection
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pBoron_enriched_MD            != nullptr )   { delete pBoron_enriched_MD;                 pBoron_enriched_MD            = nullptr; };
	if ( pBoronCarbide_Absorber_MD     != nullptr )   { delete pBoronCarbide_Absorber_MD;          pBoronCarbide_Absorber_MD     = nullptr; };

	if ( pGadolinium_MD                != nullptr )   { delete pGadolinium_MD;                     pGadolinium_MD                = nullptr; };
	if ( pGd155_enriched_MD            != nullptr )   { delete pGd155_enriched_MD;                 pGd155_enriched_MD            = nullptr; };
	if ( pGd157_enriched_MD            != nullptr )   { delete pGd157_enriched_MD;                 pGd157_enriched_MD            = nullptr; };

	if ( pLiF_MD                       != nullptr )   { delete pLiF_MD;                            pLiF_MD                       = nullptr; };
	if ( pGS20_MD                      != nullptr )   { delete pGS20_MD;                           pGS20_MD                      = nullptr; };


    //-----------------------------------------------------------------------------------------------------------------------------------------------
    // Tissue equivalent materials
    //-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pA150Plastics_MD              != nullptr )   { delete pA150Plastics_MD;                   pA150Plastics_MD              = nullptr; };
	if ( pICRU_Phantom_MD              != nullptr )   { delete pICRU_Phantom_MD;                   pICRU_Phantom_MD              = nullptr; };

	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// Materials for insulation
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pDelrin_MD                    != nullptr )   { delete pDelrin_MD;                         pDelrin_MD                    = nullptr; };
	if ( pFR4_MD                       != nullptr )   { delete pFR4_MD;                            pFR4_MD                       = nullptr; };
	if ( pFrialit_MD                   != nullptr )   { delete pFrialit_MD;                        pFrialit_MD                   = nullptr; };
	if ( pG10_MD                       != nullptr )   { delete pG10_MD;                            pG10_MD                       = nullptr; };
	if ( pKapton_MD                    != nullptr )   { delete pKapton_MD;                         pKapton_MD                    = nullptr; };
	if ( pMacor_MD                     != nullptr )   { delete pMacor_MD;                          pMacor_MD                     = nullptr; };
	if ( pNoryl_MD                     != nullptr )   { delete pNoryl_MD;                          pNoryl_MD                     = nullptr; };
	if ( pPEEK_MD                      != nullptr )   { delete pPEEK_MD;                           pPEEK_MD                      = nullptr; };
	if ( pPVC_MD                       != nullptr )   { delete pPVC_MD;                            pPVC_MD                       = nullptr; };
	if ( pTeflon_MD                    != nullptr )   { delete pTeflon_MD;                         pTeflon_MD                    = nullptr; };


	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// Materials for foils and wrappings
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pHavar_MD                     != nullptr )   { delete pHavar_MD;                          pHavar_MD                     = nullptr; };
	if ( pHostaphan_MD                 != nullptr )   { delete pHostaphan_MD;                      pHostaphan_MD                 = nullptr; };
	if ( pMakrofolKG_MD                != nullptr )   { delete pMakrofolKG_MD;                     pMakrofolKG_MD                = nullptr; };
	if ( pMylar_MD                     != nullptr )   { delete pMylar_MD;                          pMylar_MD                     = nullptr; };
	if ( pTyvek_MD                     != nullptr )   { delete pTyvek_MD;                          pTyvek_MD                     = nullptr; };


	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// Materials for glueing/bonding/coupling
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pC19H20O4_MD                  != nullptr )   { delete pC19H20O4_MD;                       pC19H20O4_MD                  = nullptr; };
	if ( pC10H18O4_MD                  != nullptr )   { delete pC10H18O4_MD;                       pC10H18O4_MD                  = nullptr; };
	if ( pC9H22N2_MD                   != nullptr )   { delete pC9H22N2_MD;                        pC9H22N2_MD                   = nullptr; };
	if ( pEpotek_301_1_MD              != nullptr )   { delete pEpotek_301_1_MD;                   pEpotek_301_1_MD              = nullptr; };

	if ( pCAS_101_77_9_MD              != nullptr )   { delete pCAS_101_77_9_MD;                   pCAS_101_77_9_MD               = nullptr; };
	if ( pCAS_9003_36_5_MD             != nullptr )   { delete pCAS_9003_36_5_MD;                  pCAS_9003_36_5_MD             = nullptr; };
	if ( pCAS_10563_29_8_MD            != nullptr )   { delete pCAS_10563_29_8_MD;                 pCAS_10563_29_8_MD            = nullptr; };
	if ( pCAS_25068_38_6_MD            != nullptr )   { delete pCAS_25068_38_6_MD;                 pCAS_25068_38_6_MD            = nullptr; };

	if ( pAraldite_AW106_MD            != nullptr )   { delete pAraldite_AW106_MD;                 pAraldite_AW106_MD            = nullptr; };
	if ( pHardener_HV953U_MD           != nullptr )   { delete pHardener_HV953U_MD;                pHardener_HV953U_MD           = nullptr; };
	if ( pEpoxy_AW106_HV953U_MD        != nullptr )   { delete pEpoxy_AW106_HV953U_MD;             pEpoxy_AW106_HV953U_MD        = nullptr; };

	if ( pAraldite_F_MD                != nullptr )   { delete pAraldite_F_MD;                     pAraldite_F_MD                = nullptr; };
	if ( pHardener_HT972_MD            != nullptr )   { delete pHardener_HT972_MD;                 pHardener_HT972_MD            = nullptr; };
	if ( pEpoxy_F_HT972_MD             != nullptr )   { delete pEpoxy_F_HT972_MD;                  pEpoxy_F_HT972_MD             = nullptr; };

	if ( pEpoxy_MD                     != nullptr )   { delete pEpoxy_MD;                          pEpoxy_MD                     = nullptr; };
	if ( pEpoxyResin_MD                != nullptr )   { delete pEpoxyResin_MD;                     pEpoxyResin_MD                = nullptr; };
	if ( pMagicOpticalBond_MD          != nullptr )   { delete pMagicOpticalBond_MD;               pMagicOpticalBond_MD          = nullptr; };
	if ( pMeltMount_1582_MD            != nullptr )   { delete pMeltMount_1582_MD;                 pMeltMount_1582_MD            = nullptr; };
	if ( pMeltMount_1704_MD            != nullptr )	  { delete pMeltMount_1704_MD;                 pMeltMount_1704_MD            = nullptr; };
	if ( pOpticalCement_MD             != nullptr )	  { delete pOpticalCement_MD;                  pOpticalCement_MD             = nullptr; };
	if ( pOpticalCement_MPT            != nullptr )	  { delete pOpticalCement_MPT;                 pOpticalCement_MPT            = nullptr; };
	if ( pOpticalGrease_MD             != nullptr )	  { delete pOpticalGrease_MD;                  pOpticalGrease_MD             = nullptr; };
	if ( pOpticalGrease_MPT            != nullptr )	  { delete pOpticalGrease_MPT;                 pOpticalGrease_MPT            = nullptr; };


	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// Materials for light transportation
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pAcrylic_MD                   != nullptr )   { delete pAcrylic_MD;                        pAcrylic_MD                   = nullptr; };
	if ( pFGL400_MD                    != nullptr )   { delete pFGL400_MD;                         pFGL400_MD                    = nullptr; };
	if ( pFusedSilica_MD               != nullptr )   { delete pFusedSilica_MD;                    pFusedSilica_MD               = nullptr; };
	if ( pPerspex_Opal069_MD           != nullptr )   { delete pPerspex_Opal069_MD;                pPerspex_Opal069_MD           = nullptr; };
	if ( pPlexiglas_GS233_MD           != nullptr )	  { delete pPlexiglas_GS233_MD;                pPlexiglas_GS233_MD           = nullptr; };
	if ( pPMMA_MD                      != nullptr )	  { delete pPMMA_MD;                           pPMMA_MD                      = nullptr; };


	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// Scintillator materials
	//-----------------------------------------------------------------------------------------------------------------------------------------------

	// Organic scintillators
	if ( pBC400_MD                     != nullptr )   { delete pBC400_MD;                          pBC400_MD                     = nullptr; };
	if ( pBC408_MD                     != nullptr )   { delete pBC408_MD;                          pBC408_MD                     = nullptr; };
	if ( pBC91A_MD                     != nullptr )   { delete pBC91A_MD;                          pBC91A_MD                     = nullptr; };
	if ( pEJ280_MD                     != nullptr )   { delete pEJ280_MD;                          pEJ280_MD                     = nullptr; };

	// Inorganic scintillators
	if ( pBaF2_MD                      != nullptr )   { delete pBaF2_MD;                           pBaF2_MD                      = nullptr; };
	if ( pBaF2_Immediate_MD            != nullptr )   { delete pBaF2_Immediate_MD;                 pBaF2_Immediate_MD            = nullptr; };
	if ( pBaF2_Perfect_MD              != nullptr )   { delete pBaF2_Perfect_MD;                   pBaF2_Perfect_MD              = nullptr; };
	if ( pBaF2_PerfectFast_MD          != nullptr )   { delete pBaF2_PerfectFast_MD;               pBaF2_PerfectFast_MD          = nullptr; };
	if ( pBaF2_Template_MD             != nullptr )   { delete pBaF2_Template_MD;                  pBaF2_Template_MD             = nullptr; };

	if ( pLaBr3_Ce_MD                  != nullptr )   { delete pLaBr3_Ce_MD;                       pLaBr3_Ce_MD                  = nullptr; };
	if ( pLYSO_Ce_MD                   != nullptr )   { delete pLYSO_Ce_MD;                        pLYSO_Ce_MD                   = nullptr; };
	if ( pNaI_MD                       != nullptr )   { delete pNaI_MD;                            pNaI_MD                       = nullptr; };
	if ( pZnS_Ag_MD                    != nullptr )   { delete pZnS_Ag_MD;                         pZnS_Ag_MD                    = nullptr; };


	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// PMT materials
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pBialkaliCathode_MD           != nullptr )   { delete pBialkaliCathode_MD;                pBialkaliCathode_MD           = nullptr; };
	if ( pBorosilicateGlass_MD         != nullptr )   { delete pBorosilicateGlass_MD;              pBorosilicateGlass_MD         = nullptr; };


	//-----------------------------------------------------------------------------------------------------------------------------------------------
	// SiPM materials
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	if ( pBoronCarbide_Ceramics_MD     != nullptr )   { delete pBoronCarbide_Ceramics_MD;          pBoronCarbide_Ceramics_MD     = nullptr; };
	if ( pSilicon_MD                   != nullptr )   { delete pSilicon_MD;                        pSilicon_MD                   = nullptr; };
	if ( pSiliconDioxide_MD            != nullptr )   { delete pSiliconDioxide_MD;                 pSiliconDioxide_MD            = nullptr; };

	//==========================================================
	// Delete Material Property Table used for Wrapping/Coating
	//==========================================================
	if (pCoating_Tyvek_MPT             != nullptr )   { delete pCoating_Tyvek_MPT;                 pCoating_Tyvek_MPT             = nullptr; };


	//===============================================================================================================================================
	//===============================================================================================================================================
	//===============================================================================================================================================


	//=================================
	// Delete OpticalSurface  pointers
	//=================================

	if ( pBGOPolishedLumirrorAir_OS         != nullptr )   { delete pBGOPolishedLumirrorAir_OS;              pBGOPolishedLumirrorAir_OS         = nullptr; };
	if ( pBGOPolishedLumirrorGlue_OS        != nullptr )   { delete pBGOPolishedLumirrorGlue_OS;             pBGOPolishedLumirrorGlue_OS        = nullptr; };
	if ( pBGOPolishedAir_OS                 != nullptr )   { delete pBGOPolishedAir_OS;                      pBGOPolishedAir_OS                 = nullptr; };
	if ( pBGOPolishedTeflonAir_OS           != nullptr )   { delete pBGOPolishedTeflonAir_OS;                pBGOPolishedTeflonAir_OS           = nullptr; };
	if ( pBGOPolishedTioAir_OS              != nullptr )   { delete pBGOPolishedTioAir_OS;                   pBGOPolishedTioAir_OS              = nullptr; };
	if ( pBGOPolishedTyvekAir_OS            != nullptr )   { delete pBGOPolishedTyvekAir_OS;                 pBGOPolishedTyvekAir_OS            = nullptr; };
	if ( pBGOPolishedVM2000Air_OS           != nullptr )   { delete pBGOPolishedVM2000Air_OS;                pBGOPolishedVM2000Air_OS           = nullptr; };
	if ( pBGOPolishedVM2000Glue_OS          != nullptr )   { delete pBGOPolishedVM2000Glue_OS;               pBGOPolishedVM2000Glue_OS          = nullptr; };

	if ( pBGOGroundLumirrorAir_OS           != nullptr )   { delete pBGOGroundLumirrorAir_OS;                pBGOGroundLumirrorAir_OS           = nullptr; };
	if ( pBGOGroundLumirrorGlue_OS          != nullptr )   { delete pBGOGroundLumirrorGlue_OS;               pBGOGroundLumirrorGlue_OS          = nullptr; };
	if ( pBGOGroundAir_OS                   != nullptr )   { delete pBGOGroundAir_OS;                        pBGOGroundAir_OS                   = nullptr; };
	if ( pBGOGroundTeflonAir_OS             != nullptr )   { delete pBGOGroundTeflonAir_OS;                  pBGOGroundTeflonAir_OS             = nullptr; };
	if ( pBGOGroundTioAir_OS                != nullptr )   { delete pBGOGroundTioAir_OS;                     pBGOGroundTioAir_OS                = nullptr; };
	if ( pBGOGroundTyvekAir_OS              != nullptr )   { delete pBGOGroundTyvekAir_OS;                   pBGOGroundTyvekAir_OS              = nullptr; };
	if ( pBGOGroundVM2000Air_OS             != nullptr )   { delete pBGOGroundVM2000Air_OS;                  pBGOGroundVM2000Air_OS             = nullptr; };
	if ( pBGOGroundVM2000Glue_OS            != nullptr )   { delete pBGOGroundVM2000Glue_OS;                 pBGOGroundVM2000Glue_OS            = nullptr; };

	if ( pPerfectPolished_OS                != nullptr )   { delete pPerfectPolished_OS;                     pPerfectPolished_OS                = nullptr; };
	if ( pDiamondMilled_OS                  != nullptr )   { delete pDiamondMilled_OS;                       pDiamondMilled_OS                  = nullptr; };

	if ( pGenericGroundAir_OS               != nullptr )   { delete pGenericGroundAir_OS;                    pGenericGroundAir_OS               = nullptr; };
	if ( pGenericPolishedAir_OS             != nullptr )   { delete pGenericPolishedAir_OS;                  pGenericPolishedAir_OS             = nullptr; };
	if ( pGenericPolishedWhitePainted_OS    != nullptr )   { delete pGenericPolishedWhitePainted_OS;         pGenericPolishedWhitePainted_OS    = nullptr; };
	if ( pGenericGroundWhitePainted_OS      != nullptr )   { delete pGenericGroundWhitePainted_OS;           pGenericGroundWhitePainted_OS      = nullptr; };
	if ( pGenericPolishedBlackPainted_OS    != nullptr )   { delete pGenericPolishedBlackPainted_OS;         pGenericPolishedBlackPainted_OS    = nullptr; };
	if ( pGenericGroundBlackPainted_OS      != nullptr )   { delete pGenericGroundBlackPainted_OS;           pGenericGroundBlackPainted_OS      = nullptr; };

	if ( pAirGroundAluminum_OS              != nullptr )   { delete pAirGroundAluminum_OS;                   pAirGroundAluminum_OS              = nullptr; };
	if ( pAirGroundTitanium_OS              != nullptr )   { delete pAirGroundTitanium_OS;                   pAirGroundTitanium_OS              = nullptr; };
	if ( pAirGroundSilver_OS                != nullptr )   { delete pAirGroundSilver_OS;                     pAirGroundSilver_OS                = nullptr; };
	if ( pAirGroundGold_OS                  != nullptr )   { delete pAirGroundGold_OS;                       pAirGroundGold_OS                  = nullptr; };

	if ( pAirPolishedAluminum_OS            != nullptr )   { delete pAirPolishedAluminum_OS;                 pAirPolishedAluminum_OS            = nullptr; };
	if ( pAirPolishedTitanium_OS            != nullptr )   { delete pAirPolishedTitanium_OS;                 pAirPolishedTitanium_OS            = nullptr; };
	if ( pAirPolishedSilver_OS              != nullptr )   { delete pAirPolishedSilver_OS;                   pAirPolishedSilver_OS              = nullptr; };
	if ( pAirPolishedGold_OS                != nullptr )   { delete pAirPolishedGold_OS;                     pAirPolishedGold_OS                = nullptr; };

	if ( pPhotoCathode_OS                   != nullptr )   { delete pPhotoCathode_OS;                        pPhotoCathode_OS                   = nullptr; };
	if ( pBlackHole_OS                      != nullptr )   { delete pBlackHole_OS;                           pBlackHole_OS                      = nullptr; };


	std::cout << "Leaving BTSimCommon_MaterialParameters::~BTSimCommon_MaterialParameters()" << std::endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::Initialize()
{
	//----------------------
	// Initialize variables
	//----------------------

	//--------------------
	// Initilaize vectors
	//--------------------
	vNatoms.clear();
	vFractionMass.clear();
	vElements.clear();


	//===================================
	// Initialize OpticalSurface pointers
	//===================================

	pBGOPolishedLumirrorAir_OS          = nullptr;
	pBGOPolishedLumirrorGlue_OS 		= nullptr;
	pBGOPolishedAir_OS 					= nullptr;
	pBGOPolishedTeflonAir_OS 			= nullptr;
	pBGOPolishedTioAir_OS 				= nullptr;
	pBGOPolishedTyvekAir_OS 			= nullptr;
	pBGOPolishedVM2000Air_OS 			= nullptr;
	pBGOPolishedVM2000Glue_OS 			= nullptr;

	pBGOGroundLumirrorAir_OS 			= nullptr;
	pBGOGroundLumirrorGlue_OS 			= nullptr;
	pBGOGroundAir_OS 					= nullptr;
	pBGOGroundTeflonAir_OS 				= nullptr;
	pBGOGroundTioAir_OS 				= nullptr;
	pBGOGroundTyvekAir_OS 				= nullptr;
	pBGOGroundVM2000Air_OS 				= nullptr;
	pBGOGroundVM2000Glue_OS 			= nullptr;

	pPerfectPolished_OS                 = nullptr;
	pDiamondMilled_OS                   = nullptr;

	pGenericGroundAir_OS 				= nullptr;
	pGenericPolishedAir_OS 				= nullptr;
	pGenericPolishedWhitePainted_OS 	= nullptr;
	pGenericGroundWhitePainted_OS 		= nullptr;
	pGenericPolishedBlackPainted_OS 	= nullptr;
	pGenericGroundBlackPainted_OS 		= nullptr;

	pAirGroundAluminum_OS 				= nullptr;
	pAirGroundTitanium_OS 				= nullptr;
	pAirGroundSilver_OS 				= nullptr;
	pAirGroundGold_OS 					= nullptr;

	pAirPolishedAluminum_OS 			= nullptr;
	pAirPolishedTitanium_OS 			= nullptr;
	pAirPolishedSilver_OS 				= nullptr;
	pAirPolishedGold_OS 				= nullptr;

	pPhotoCathode_OS 					= nullptr;
	pBlackHole_OS 						= nullptr;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterialDefinitionByName(G4String sName)
{
  //-----------------------------------------------------------------------------------------------------------------------------
  // World materials
  //-----------------------------------------------------------------------------------------------------------------------------

       if (sName == "Vacuum")                    { this->GetMaterial_Vacuum();                     return pVacuum_MD;}
  else if (sName == "Air")                   	 { this->GetMaterial_Air();                        return pAir_MD;}

  //-----------------------------------------------------------------------------------------------------------------------------
  // Unsorted materials
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "CarbonFiber")               { this->GetMaterial_CarbonFiber();                return pCarbonFiber_MD;}
  else if (sName == "Ceramic")                   { this->GetMaterial_Ceramic();                    return pCeramic_MD;}
  else if (sName == "Concrete")                  { this->GetMaterial_Concrete();                   return pConcrete_MD;}
  else if (sName == "Enamel")                  	 { this->GetMaterial_Enamel();                     return pEnamel_MD;}
  else if (sName == "Fiberglass_EGlass")         { this->GetMaterial_Fiberglass_EGlass();          return pFiberglass_EGlass_MD;}
  else if (sName == "Graphite_Electrodag5513")   { this->GetMaterial_Graphite_Electrodag5513();    return pGraphite_Electrodag5513_MD;}
  else if (sName == "Wood")                  	 { this->GetMaterial_Wood();                       return pWood_MD;}
  else if (sName == "RockWool")                  { this->GetMaterial_RockWool();                   return pRockWool_MD;}



  //-----------------------------------------------------------------------------------------------------------------------------
  // Metals and alloys
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "Aluminum")                  { this->GetMaterial_Aluminum();              return pAluminum_MD;}
  else if (sName == "Al2007")              	     { this->GetMaterial_Al2007();                return pAl2007_MD;}
  else if (sName == "Al6082")              	     { this->GetMaterial_Al6082();                return pAl6082_MD;}
  else if (sName == "Al7075T6")              	 { this->GetMaterial_Al7075T6();              return pAl7075T6_MD;}
  else if (sName == "Bronze_C38500")             { this->GetMaterial_Bronze_C38500();         return pBronze_C38500_MD;}
  else if (sName == "Copper")                	 { this->GetMaterial_Copper();                return pCopper_MD;}
  else if (sName == "Dural")                 	 { this->GetMaterial_Dural();                 return pDural_MD;}
  else if (sName == "Gold")                  	 { this->GetMaterial_Gold();                  return pGold_MD;}
  else if (sName == "Iron")                  	 { this->GetMaterial_Iron();                  return pIron_MD;}
  else if (sName == "Lead")                  	 { this->GetMaterial_Lead();                  return pLead_MD;}
  else if (sName == "MUMetal")               	 { this->GetMaterial_MUMetal();               return pMUMetal_MD;}
  else if (sName == "Nickel")                  	 { this->GetMaterial_Nickel();                return pNickel_MD;}
  else if (sName == "Silver")                	 { this->GetMaterial_Silver();                return pSilver_MD;}
  else if (sName == "StainlessSteel")        	 { this->GetMaterial_StainlessSteel();        return pStainlessSteel_MD;}
  else if (sName == "StainlessSteel_304")    	 { this->GetMaterial_StainlessSteel_304();    return pStainlessSteel_304_MD; }
  else if (sName == "StainlessSteel_316L")   	 { this->GetMaterial_StainlessSteel_316L();   return pStainlessSteel_316L_MD;}
  else if (sName == "StainlessSteel_446")    	 { this->GetMaterial_StainlessSteel_446();    return pStainlessSteel_446_MD; }
  else if (sName == "Tin")                	     { this->GetMaterial_Tin();                   return pTin_MD;}
  else if (sName == "Titanium")              	 { this->GetMaterial_Titanium();              return pTitanium_MD;}
  else if (sName == "Tungsten")                	 { this->GetMaterial_Tungsten();              return pTungsten_MD;}


  //-----------------------------------------------------------------------------------------------------------------------------
  // Drift chamber gases
  //-----------------------------------------------------------------------------------------------------------------------------

  else if (sName == "ArCO2Gas_7030")         	 { this->GetMaterial_ArCO2Gas_7030();         return pArCO2Gas_7030_MD;}
  else if (sName == "ArCO2Gas_8020")         	 { this->GetMaterial_ArCO2Gas_8020();         return pArCO2Gas_8020_MD;}
  else if (sName == "ArCH4Gas_9307")         	 { this->GetMaterial_ArCH4Gas_9307();         return pArCH4Gas_9307_MD;}
  else if (sName == "ArCH4Gas_9010_858mbar")     { this->GetMaterial_ArCH4Gas_9010_858();     return pArCH4Gas_9010_858_MD;}

  else if (sName == "CF4")                       { this->GetMaterial_CF4();                   return pCF4_MD;}

  else if (sName == "EthylChloride")             { this->GetMaterial_EthylChloride();         return pEthylChloride_MD;}
  else if (sName == "IsoHexane")                 { this->GetMaterial_IsoHexane();             return pIsoHexane_MD;}
  else if (sName == "TOLFGas")                   { this->GetMaterial_TOLFGas();               return pTOLFGas_MD;}

  else if (sName == "Helium3_100_4000mbar")      { this->GetMaterial_Helium3_100_4000();      return pHe3_100_4000_MD;}

  else if (sName == "Krypton_55")                { this->GetMaterial_Krypton_55();            return pKrypton_55_MD;}
  else if (sName == "KrCH4Gas_9307")         	 { this->GetMaterial_KrCH4Gas_9307();         return pKrCH4Gas_9307_MD;}
  else if (sName == "KrCO2Gas_8020")         	 { this->GetMaterial_KrCO2Gas_8020();         return pKrCO2Gas_8020_MD;}

  else if (sName == "P10Gas")                	 { this->GetMaterial_P10Gas();                return pP10Gas_MD;}

  else if (sName == "XeCO2Gas_8020")         	 { this->GetMaterial_XeCO2Gas_8020();         return pXeCO2Gas_8020_MD;}
  else if (sName == "XeCO2CF4Gas_702703")        { this->GetMaterial_XeCO2CF4Gas_702703();    return pXeCO2CF4Gas_702703_MD;}


  //-----------------------------------------------------------------------------------------------------------------------------
  // Materials for neutron detection
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "BoronCarbide_Absorber")     { this->GetMaterial_BoronCarbide_Absorber(); return pBoronCarbide_Absorber_MD;}

  else if (sName == "Gadolinium")                { this->GetMaterial_Gadolinium();            return pGadolinium_MD;}
  else if (sName == "Gadolinium155_enriched")    { this->GetMaterial_Gd155enriched();         return pGd155_enriched_MD;}
  else if (sName == "Gadolinium157_enriched")    { this->GetMaterial_Gd157enriched();         return pGd157_enriched_MD;}

  else if (sName == "LiF")                 	     { this->GetMaterial_LiF();                   return pLiF_MD;}
  else if (sName == "GS20")                 	 { this->GetMaterial_GS20();                  return pGS20_MD;}


  //-----------------------------------------------------------------------------------------------------------------------------
  // Tissue equivalent materials
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "A150Plastics")              { this->GetMaterial_A150Plastics();          return pA150Plastics_MD;}
  else if (sName == "ICRU_Phantom")              { this->GetMaterial_ICRU_Phantom();          return pICRU_Phantom_MD;}


  //-----------------------------------------------------------------------------------------------------------------------------
  // Materials for insulation
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "Delrin")                  	 { this->GetMaterial_Delrin();                return pDelrin_MD;}
  else if (sName == "FR4")                	     { this->GetMaterial_FR4();                   return pFR4_MD;}
  else if (sName == "Frialit")                	 { this->GetMaterial_Frialit();               return pFrialit_MD;}
  else if (sName == "G10")                	     { this->GetMaterial_G10();                   return pG10_MD;}
  else if (sName == "Kapton")                	 { this->GetMaterial_Kapton();                return pKapton_MD;}
  else if (sName == "Macor")                	 { this->GetMaterial_Macor();                 return pMacor_MD;}
  else if (sName == "Noryl")                	 { this->GetMaterial_Noryl();                 return pNoryl_MD;}
  else if (sName == "PEEK")                	     { this->GetMaterial_PEEK();                  return pPEEK_MD;}
  else if (sName == "PVC")                       { this->GetMaterial_PVC();                   return pPVC_MD;}
  else if (sName == "Teflon")                	 { this->GetMaterial_Teflon();                return pTeflon_MD;}


  //-----------------------------------------------------------------------------------------------------------------------------
  // Materials for foils and wrappings
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "Havar")                 	 { this->GetMaterial_Havar();                 return pHavar_MD;}
  else if (sName == "Hostaphan")             	 { this->GetMaterial_Hostaphan();             return pHostaphan_MD;}
  else if (sName == "Makrofol_KG")             	 { this->GetMaterial_MakrofolKG();            return pMakrofolKG_MD;}
  else if (sName == "Mylar")                 	 { this->GetMaterial_Mylar();                 return pMylar_MD;}
  else if (sName == "Tyvek")                  	 { this->GetMaterial_Tyvek();                 return pTyvek_MD;}


  //-----------------------------------------------------------------------------------------------------------------------------
  // Materials for glueing/bonding/coupling
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "C19H20O4")            	     { this->GetMaterial_C19H20O4();              return pC19H20O4_MD;}
  else if (sName == "C10H18O4")            	     { this->GetMaterial_C10H18O4();              return pC10H18O4_MD;}
  else if (sName == "C9H22N2")            	     { this->GetMaterial_C9H22N2();               return pC9H22N2_MD;}
  else if (sName == "Epotek_301_1")            	 { this->GetMaterial_Epotek_301_1();          return pEpotek_301_1_MD;}

  else if (sName == "CAS_101_77_9")              { this->GetMaterial_CAS_101_77_9();          return pCAS_101_77_9_MD;}
  else if (sName == "CAS_9003_36_5")             { this->GetMaterial_CAS_9003_36_5();         return pCAS_9003_36_5_MD;}
  else if (sName == "CAS_10563_29_8")            { this->GetMaterial_CAS_10563_29_8();        return pCAS_10563_29_8_MD;}
  else if (sName == "CAS_25068_38_6")            { this->GetMaterial_CAS_25068_38_6();        return pCAS_25068_38_6_MD;}

  else if (sName == "Araldite_AW106")            { this->GetMaterial_Araldite_AW106();        return pAraldite_AW106_MD;}
  else if (sName == "Hardener_HV953U")           { this->GetMaterial_Hardener_HV953U();       return pHardener_HV953U_MD;}
  else if (sName == "Epoxy_AW106_HV953U")        { this->GetMaterial_Epoxy_AW106_HV953U();    return pEpoxy_AW106_HV953U_MD;}

  else if (sName == "Araldite_F")                { this->GetMaterial_Araldite_F();            return pAraldite_F_MD;}
  else if (sName == "Hardener_HT972")            { this->GetMaterial_Hardener_HT972();        return pHardener_HT972_MD;}
  else if (sName == "Epoxy_F_HT972")             { this->GetMaterial_Epoxy_F_HT972();         return pEpoxy_F_HT972_MD;}

  else if (sName == "Epoxy")            	     { this->GetMaterial_Epoxy();                 return pEpoxy_MD;}
  else if (sName == "EpoxyResin")            	 { this->GetMaterial_EpoxyResin();            return pEpoxyResin_MD;}
  else if (sName == "MagicOpticalBond")          { this->GetMaterial_MagicOpticalBond();      return pMagicOpticalBond_MD;}
  else if (sName == "MeltMount_1582")         	 { this->GetMaterial_MeltMount_1582();        return pMeltMount_1582_MD;}
  else if (sName == "MeltMount_1704")         	 { this->GetMaterial_MeltMount_1704();        return pMeltMount_1704_MD;}
  else if (sName == "OpticalCement")         	 { this->GetMaterial_OpticalCement();         return pOpticalCement_MD;}
  else if (sName == "OpticalGrease")         	 { this->GetMaterial_OpticalGrease();         return pOpticalGrease_MD;}



  //-----------------------------------------------------------------------------------------------------------------------------
  // Materials for light transportation
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "Acrylic")       	        { this->GetMaterial_Acrylic();               return pAcrylic_MD;}
  else if (sName == "FGL400")                	{ this->GetMaterial_FGL400();                return pFGL400_MD;}
  else if (sName == "FusedSilica")           	{ this->GetMaterial_FusedSilica();           return pFusedSilica_MD;}
  else if (sName == "Perspex_Opal069")       	{ this->GetMaterial_Perspex_Opal069();       return pPerspex_Opal069_MD;}
  else if (sName == "Plexiglas_GS233")       	{ this->GetMaterial_Plexiglas_GS233();       return pPlexiglas_GS233_MD;}
  else if (sName == "PMMA")       	            { this->GetMaterial_PMMA();                  return pPMMA_MD;}


  //-----------------------------------------------------------------------------------------------------------------------------
  // Scintillator materials
  //-----------------------------------------------------------------------------------------------------------------------------

  // Organic scintillators
  else if (sName == "BC400")                 	 { this->GetMaterial_BC400();                 return pBC400_MD;}
  else if (sName == "BC408")                 	 { this->GetMaterial_BC408();                 return pBC408_MD;}
  else if (sName == "BC91A")                 	 { this->GetMaterial_BC91A();                 return pBC91A_MD;}
  else if (sName == "EJ280")                 	 { this->GetMaterial_EJ280();                 return pEJ280_MD;}

  // Inorganic scintillators
  else if (sName == "BaF2")                  	 { this->GetMaterial_BaF2();                  return pBaF2_MD;}
  else if (sName == "BaF2_Immediate")        	 { this->GetMaterial_BaF2_Immediate();        return pBaF2_Immediate_MD;}
  else if (sName == "BaF2_Perfect")          	 { this->GetMaterial_BaF2_Perfect();          return pBaF2_Perfect_MD;}
  else if (sName == "BaF2_PerfectFast")      	 { this->GetMaterial_BaF2_PerfectFast();      return pBaF2_PerfectFast_MD;}

  else if (sName == "LaBr3_Ce")              	 { this->GetMaterial_LaBr3_Ce();              return pLaBr3_Ce_MD;}
  else if (sName == "LYSO_Ce")          	     { this->GetMaterial_LYSO_Ce();               return pLYSO_Ce_MD;}
  else if (sName == "NaI")                   	 { this->GetMaterial_NaI();                   return pNaI_MD;}
  else if (sName == "ZnS_Ag")                	 { this->GetMaterial_ZnS_Ag();                return pZnS_Ag_MD;}


  //-----------------------------------------------------------------------------------------------------------------------------
  // PMT materials
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "BialkaliCathode")       	 { this->GetMaterial_BialkaliCathode();       return pBialkaliCathode_MD;}
  else if (sName == "BorosilicateGlass")     	 { this->GetMaterial_BorosilicateGlass();     return pBorosilicateGlass_MD;}

  //-----------------------------------------------------------------------------------------------------------------------------
  // SiPM materials
  //-----------------------------------------------------------------------------------------------------------------------------
  else if (sName == "BoronCarbide_Ceramics")     { this->GetMaterial_BoronCarbide_Ceramics(); return pBoronCarbide_Ceramics_MD;}
  else if (sName == "Silicon")               	 { this->GetMaterial_Silicon();               return pSilicon_MD;}
  else if (sName == "SiliconDioxide")            { this->GetMaterial_SiliconDioxide();        return pSiliconDioxide_MD;}

  //---------------------------------------------------------------------------------------------------------------------------------------
  else { G4cout << "======>>> BTSimCommon_MaterialParameters::GetMaterialDefinitionByName() : Could not find object " << sName << G4endl;
 	     return 0;
       }
  //---------------------------------------------------------------------------------------------------------------------------------------

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#####################################################################################################################
//#####################################################################################################################
//                      World Materials
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Vacuum()
{
	  G4String sName = "Vacuum";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial( sName, false);

	  if (pMat == nullptr)
	  {

	  //==============================
	  //            Vacuum
	  //==============================

	  pVacuum_MD = nullptr;
	  pVacuum_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Galactic");
	  pVacuum_MD->SetName(sName);

	  //----------------------------------
	  // Define material properties table
	  //----------------------------------
	  G4MaterialPropertiesTable* pVacuum_MPT = nullptr;
	                             pVacuum_MPT = new G4MaterialPropertiesTable();

	  const G4int entries = 2;
	  G4double fVacuum_Energy[entries] = {0.01*CLHEP::eV, 100.*CLHEP::eV};

	  // REFRACTIVE INDEX
	  G4double fVacuum_Rindex[entries] = {1., 1.};
	  pVacuum_MPT->AddProperty("RINDEX", fVacuum_Energy, fVacuum_Rindex, entries);

	  // ABSORPTION LENGTH
	  G4double fVacuum_Abslen[entries] = {1.e8*CLHEP::m, 1.e8*CLHEP::m};
	  pVacuum_MPT->AddProperty("ABSLENGTH", fVacuum_Energy, fVacuum_Abslen, entries);

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pVacuum_MD->SetMaterialPropertiesTable(pVacuum_MPT);

	} // end if (pMat == nullptr)

	return pVacuum_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Air()
{
	  G4String sName    = "Air";
	  G4double fDensity = pNistMaterialManager->FindOrBuildMaterial("G4_AIR")->GetDensity();

	  std::cout << "NIST air density [mg/cm3] = " << fDensity/(CLHEP::mg/CLHEP::cm3) << std::endl;

	  //---------------------------------------------------------------------
	  // Use precision air  density formula by Brunzendorf, used by PTB BSS2
	  //---------------------------------------------------------------------
	  G4double fPTB_CurrentPressure     = 101325.00; // Pa
	  G4double fPTB_CurrentTemperature  =     25.00; // Grad Celsius
	  G4double fPTB_CurrentHumidity     =      0.65; // Relative humidity

	  //-----------------------------------------
	  // Calculate air density according to PTB
	  //------------------------------------------
	  G4double fDensity_PTBAir = this->GetDensityOfAir( fPTB_CurrentPressure,
			                                            fPTB_CurrentTemperature,
													    fPTB_CurrentHumidity );

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);


	  if (pMat == nullptr)
	  {
		//==============================
		//	           Air
		//==============================

		// Gas related: assume an ambient temperature of 20 degree
		const G4double fG4_CurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fG4_CurrentPressure    = 1.000*CLHEP::STP_Pressure;

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pAir_MD = nullptr;
	    pAir_MD = new G4Material( sName,
	    		                  fDensity_PTBAir,
								  1,
								  kStateGas,
								  fG4_CurrentTemperature,
								  fG4_CurrentPressure);

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pAir_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_AIR"), fFractionOfMass = 100.0*CLHEP::perCent);

	    //--------------------
		// Optical properties
	    //--------------------
		const G4int    nentries_Air = 2;
		const G4double rIndex       = 1. + 172. * 1e-6;

		G4double fAir_PhotonEnergy[nentries_Air] = {   0.1*CLHEP::eV ,  10.0*CLHEP::eV };
		G4double fAir_RINDEX      [nentries_Air] = {          rIndex ,  rIndex         };
		G4double fAir_ABSLENGTH   [nentries_Air] = { 100.0*CLHEP::m  , 100.0*CLHEP::m  }; // avoid infinite light-paths

		//----------------------------------
		// Define material properties table
		//----------------------------------
		G4MaterialPropertiesTable* pAir_MPT = nullptr;
		                           pAir_MPT = new G4MaterialPropertiesTable();

								   pAir_MPT->AddProperty("ABSLENGTH", fAir_PhotonEnergy, fAir_ABSLENGTH, nentries_Air);
	  	  	  	  	  	  	  	   pAir_MPT->AddProperty("RINDEX"   , fAir_PhotonEnergy, fAir_RINDEX   , nentries_Air);

	  	//----------------------------------------------
	    // Attach material properties table to material
	  	//----------------------------------------------
	  	pAir_MD->SetMaterialPropertiesTable(pAir_MPT);

	} // end if (pMat == nullptr)

	return pAir_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#####################################################################################################################
//#####################################################################################################################
//                      Unsorted Materials
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_CarbonFiber()
{
	  G4String sName    = "CarbonFiber";
	  G4double fDensity = 1.75*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=================================================================================================
	  // Carbon fiber 190 width + 65 microns scotch , overall 255 microns
	  // https://gemc.jlab.org/work/doxy/2.2/cpp__materials_8cc_source.html
	  //=================================================================================================
	  G4int ncomponents;

	  pCarbonFiber_MD = nullptr;
	  pCarbonFiber_MD = new G4Material( sName,
			                            fDensity,
										ncomponents = 2,
									    kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pCarbonFiber_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"), fFractionOfMass = 74.5*CLHEP::perCent);
	  pCarbonFiber_MD->AddMaterial( this->GetMaterial_Epoxy()                        , fFractionOfMass = 25.5*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pCarbonFiber_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Ceramic()
{
	  G4String sName    = "Ceramic";
	  G4double fDensity = 3.96*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=================================================================================================
	  //   Ceramic: 53% Aluminum, 47% Oxygen
	  // http://katrin.kit.edu/trac/ipedaq/browser/KatrinDetSim/src/KDSDetectorConstruction.cc?rev=2157
	  //=================================================================================================
	  G4int ncomponents;

	  pCeramic_MD = nullptr;
	  pCeramic_MD = new G4Material( sName,
			                        fDensity,
									ncomponents = 2,
									kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pCeramic_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass = 53.0*CLHEP::perCent);
	  pCeramic_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O"),  fFractionOfMass = 47.0*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pCeramic_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Concrete()
{
	  G4String sName    = "Concrete";
	  G4double fDensity = 2.30*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //         Concrete
	  //==============================
      //
	  // Stolen from: PhD Thesis Guoqing Zhang
	  //              Monte Carlo Simulation of Mixed Neutron-Gamma Radiation Fields and Dosimetry Devices
	  //              Karlsruhe Institute of Technology (KIT), Germany, December 2011

	  G4int ncomponents;

	  pConcrete_MD = nullptr;
	  pConcrete_MD = new G4Material( sName,
			                         fDensity,
									 ncomponents = 10,
									 kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_H"),  fFractionOfMass =   2.2100*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"),  fFractionOfMass =   0.2484*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O"),  fFractionOfMass =  57.4930*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Na"), fFractionOfMass =   1.5208*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mg"), fFractionOfMass =   0.1266*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass =   1.9953*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  30.4627*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_K"),  fFractionOfMass =   1.0045*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ca"), fFractionOfMass =   4.2951*CLHEP::perCent);
	  pConcrete_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass =   0.6436*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pConcrete_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Enamel()
{
	  G4String sName    = "Enamel";
	  G4double fDensity = 2.0*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=================================================================================
	  //   Emanel: Definition based on Behrens et al.
	  //           "Simulation of the radiation field of the Beta Secondary Standard BSS2"
	  //            http://iopscience.iop.org/1748-0221/8/02/P02019
	  //=================================================================================
      G4int ncomponents;

	  pEnamel_MD = nullptr;
	  pEnamel_MD = new G4Material( sName,
			                       fDensity,
								   ncomponents = 10,
								   kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_H") , fFractionOfMass =   1.77*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_B") , fFractionOfMass =   3.11*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C") , fFractionOfMass =   0.18*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O") , fFractionOfMass =  44.21*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_F") , fFractionOfMass =   2.93*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Na"), fFractionOfMass =   5.42*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass =   0.57*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  18.77*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ca"), fFractionOfMass =   0.61*CLHEP::perCent);
	  pEnamel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ba"), fFractionOfMass =  22.43*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pEnamel_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Fiberglass_EGlass()
{
	  G4String sName = "Fiberglass_EGlass";
	  G4double fDensity = 2.61*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=========================================================
	  //   Fiberglass EGlass material
	  //=========================================================
	  G4int ncomponents;

      pFiberglass_EGlass_MD = nullptr;
      pFiberglass_EGlass_MD = new G4Material( sName,
				                              fDensity,
											  ncomponents = 10,
								              kStateSolid);

      //----------------------------
      // Assembling the ingredients
      //----------------------------
      G4double fFractionOfMass;

      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_SILICON_DIOXIDE")  , fFractionOfMass =  54.0*CLHEP::perCent); // Si_O2
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_CALCIUM_OXIDE")    , fFractionOfMass =  19.0*CLHEP::perCent); // Ca_O
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_ALUMINUM_OXIDE")   , fFractionOfMass =  13.0*CLHEP::perCent); // Al2_O3
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE")  , fFractionOfMass =   2.5*CLHEP::perCent); // Mg_O
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_BORON_OXIDE")      , fFractionOfMass =   7.5*CLHEP::perCent); // B2_O3
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_TITANIUM_DIOXIDE") , fFractionOfMass =   0.8*CLHEP::perCent); // Ti_O2
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_SODIUM_MONOXIDE")  , fFractionOfMass =   1.0*CLHEP::perCent); // Na2_O
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_POTASSIUM_OXIDE")  , fFractionOfMass =   1.0*CLHEP::perCent); // K2_O
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_FERRIC_OXIDE")     , fFractionOfMass =   0.5*CLHEP::perCent); // Fe2_O3
      pFiberglass_EGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_F")                , fFractionOfMass =   0.7*CLHEP::perCent); // F2

	} // end if (pMat == nullptr)

	return pFiberglass_EGlass_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Graphite_Electrodag5513()
{
	  G4String sName    = "Graphite_Electrodag5513";
	  G4double fDensity = 2.2*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=================================================================================
	  //   Acheson Graphit: Electrodag 5513 (~Leitlack)
	  //   Zusammensetzung gemä0 Analysezertifikat vom Forschungszentrum Karlsruhe,
	  //   Chemische Analytik von 15.02.06 für die Innenbeschichtung vom TOL/F
	  //=================================================================================
	  G4int ncomponents;

	  pGraphite_Electrodag5513_MD = nullptr;
	  pGraphite_Electrodag5513_MD = new G4Material( sName,
			                                        fDensity,
													ncomponents = 8,
								                    kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pGraphite_Electrodag5513_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C") , fFractionOfMass =  98.59*CLHEP::perCent);
	  pGraphite_Electrodag5513_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =   0.84*CLHEP::perCent);
	  pGraphite_Electrodag5513_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass =   0.16*CLHEP::perCent);
	  pGraphite_Electrodag5513_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_S") , fFractionOfMass =   0.15*CLHEP::perCent);
	  pGraphite_Electrodag5513_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass =   0.14*CLHEP::perCent);
	  pGraphite_Electrodag5513_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mg"), fFractionOfMass =   0.05*CLHEP::perCent);
	  pGraphite_Electrodag5513_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ca"), fFractionOfMass =   0.04*CLHEP::perCent);
	  pGraphite_Electrodag5513_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_K") , fFractionOfMass =   0.03*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pGraphite_Electrodag5513_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_RockWool()
{
	G4String sName    = "RockWool";
	G4double fDensity = 2.7*CLHEP::gram/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===========================================
	  //           RockWool Insulation
	  //===========================================
	  //
	  // Material Definition taken from: www.med.navy.mil/.../ih-manmade-vitreous-fibers-technical-guide.pdf‎
	  G4int ncomponents;

	  pRockWool_MD = nullptr;
	  pRockWool_MD = new G4Material( sName,
			                         fDensity,
									 ncomponents = 8,
									 kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pRockWool_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_SILICON_DIOXIDE") , fFractionOfMass = 52.0*CLHEP::perCent); //  45   - 52   %
	  pRockWool_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_CALCIUM_OXIDE")   , fFractionOfMass = 11.0*CLHEP::perCent); //  10   - 12   %
	  pRockWool_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE") , fFractionOfMass = 12.0*CLHEP::perCent); //   8   - 15   %
	  pRockWool_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_ALUMINUM_OXIDE")  , fFractionOfMass = 12.0*CLHEP::perCent); //   8   - 13.5 %
	  pRockWool_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_POTASSIUM_OXIDE") , fFractionOfMass =  2.0*CLHEP::perCent); //   0.8 -  2.0 %
	  pRockWool_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_SODIUM_MONOXIDE") , fFractionOfMass =  3.0*CLHEP::perCent); //   0.8 -  3.3 %
	  pRockWool_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_TITANIUM_DIOXIDE"), fFractionOfMass =  2.0*CLHEP::perCent); //   1.5 -  2.7 %
	  pRockWool_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_FERRIC_OXIDE")    , fFractionOfMass =  6.0*CLHEP::perCent); //   5.5 -  6.5 %

	} // end if (pMat == nullptr)

	return pRockWool_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Wood()
{
	G4String sName    = "Wood";
	G4double fDensity = 0.9*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //    Wood ( -C2H4O-)
	  //==============================

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pWood_MD = nullptr;
		pWood_MD = new G4Material( sName,
				                   fDensity,
								   ncomponents = 3,
								   kStateSolid );
	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
		G4int natoms;

		pWood_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 2 );
		pWood_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 4 );
		pWood_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms = 1 );

	} // end if (pMat == nullptr)

	return pWood_MD;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#####################################################################################################################
//#####################################################################################################################
//                      Metals and Alloys
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Aluminum()
{
	  G4String sName = "Aluminum";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Aluminum
	  //==============================

	  pAluminum_MD = nullptr;
	  pAluminum_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Al");
	  pAluminum_MD->SetName(sName);

	  //--------------------------------------------------------------------------------------------------------------------
	  G4MaterialPropertyVector* pMPV_REALRINDEX       = nullptr; pMPV_REALRINDEX       = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX  = nullptr; pMPV_IMAGINARYRINDEX  = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX           = nullptr; pMPV_RINDEX           = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = nullptr; pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	  //--------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------------------------------------------------------------------
	  // Read index of refraction from file
	  //--------------------------------------------------------------------------------------------------------------------
	  this->Initialize_IndexOfRefraction("/Aluminum/Aluminum_IndexOfRefraction.dat",
			                              pMPV_REALRINDEX,
			                              pMPV_IMAGINARYRINDEX,
			                              pMPV_RINDEX,
			                              pMPV_RINDEX_ABSLENGTH);


	  std::cout << "Aluminum_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

	  //----------------------------------
	  // Define material properties table
	  //----------------------------------
	  pAluminum_MPT = nullptr;
	  pAluminum_MPT = new G4MaterialPropertiesTable();

	  //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	  pAluminum_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
	  pAluminum_MPT->AddProperty("REALRINDEX"                    , pMPV_REALRINDEX       );
	  pAluminum_MPT->AddProperty("IMAGINARYRINDEX"               , pMPV_IMAGINARYRINDEX  );
	  pAluminum_MPT->AddProperty("ABSLENGTH"                     , pMPV_RINDEX_ABSLENGTH );
	  //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pAluminum_MD->SetMaterialPropertiesTable(pAluminum_MPT);

	} // end if (pMat == nullptr)

	return pAluminum_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Al2007()
{
	  G4String sName    = "Al2007";
	  G4double fDensity = 2.81*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //    Aluminum AW-2007
	  //==============================
	  //
	  // Data based on chemical analysis report from Zänker&Dittrich
      G4int ncomponents;

	  pAl2007_MD = nullptr;
	  pAl2007_MD = new G4Material( sName,
			                       fDensity,
								   ncomponents = 13,
								   kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  0.80*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass =  0.80*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cu"), fFractionOfMass =  4.60*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mn"), fFractionOfMass =  1.00*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mg"), fFractionOfMass =  1.80*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass =  0.10*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ni"), fFractionOfMass =  0.20*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Zn"), fFractionOfMass =  0.80*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Bi"), fFractionOfMass =  0.20*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Pb"), fFractionOfMass =  1.50*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Sn"), fFractionOfMass =  0.20*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ti"), fFractionOfMass =  0.20*CLHEP::perCent);
	  pAl2007_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass = 87.80*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pAl2007_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Al6082()
{
	  G4String sName    = "Al6082";
	  G4double fDensity = 2.70*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //    Aluminum AW-6082
	  //==============================
      G4int ncomponents;

	  pAl6082_MD = nullptr;
	  pAl6082_MD = new G4Material( sName,
			                       fDensity,
								   ncomponents = 9,
								   kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  1.30*CLHEP::perCent);
	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass =  0.50*CLHEP::perCent);
	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cu"), fFractionOfMass =  0.10*CLHEP::perCent);
	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mn"), fFractionOfMass =  1.00*CLHEP::perCent);
	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mg"), fFractionOfMass =  1.20*CLHEP::perCent);
	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass =  0.25*CLHEP::perCent);
	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Zn"), fFractionOfMass =  0.20*CLHEP::perCent);
	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ti"), fFractionOfMass =  0.10*CLHEP::perCent);
	  pAl6082_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass = 95.35*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pAl6082_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Al7075T6()
{
	  G4String sName    = "Al7075T6";
	  G4double fDensity = 2.81*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //         Aluminum 7075-T6
	  //==============================
	  G4int ncomponents;

	  pAl7075T6_MD = nullptr;
	  pAl7075T6_MD = new G4Material( sName,
			                         fDensity,
									 ncomponents = 9,
									 kStateSolid);

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  0.40*CLHEP::perCent);
	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass =  0.50*CLHEP::perCent);
	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cu"), fFractionOfMass =  2.00*CLHEP::perCent);
	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mn"), fFractionOfMass =  0.30*CLHEP::perCent);
	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mg"), fFractionOfMass =  2.90*CLHEP::perCent);
	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass =  0.28*CLHEP::perCent);
	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Zn"), fFractionOfMass =  6.10*CLHEP::perCent);
	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ti"), fFractionOfMass =  0.20*CLHEP::perCent);
	  pAl7075T6_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass = 87.32*CLHEP::perCent);

	  //--------------------------------------------------------------------------------------------------------------------
	  //               Store file bases material properties in these vectors
	  //--------------------------------------------------------------------------------------------------------------------
	  G4MaterialPropertyVector* pMPV_REALRINDEX       = nullptr; pMPV_REALRINDEX       = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX  = nullptr; pMPV_IMAGINARYRINDEX  = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX           = nullptr; pMPV_RINDEX           = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = nullptr; pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	  //--------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------------------------------------------------------------------
	  // Read index of refraction from file
	  //--------------------------------------------------------------------------------------------------------------------
	  this->Initialize_IndexOfRefraction("/Aluminum/Aluminum_IndexOfRefraction.dat",
                                          pMPV_REALRINDEX,
                                          pMPV_IMAGINARYRINDEX,
                                          pMPV_RINDEX,
                                          pMPV_RINDEX_ABSLENGTH);

	  std::cout << "Al7075T6_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

	  //----------------------------------
	  // Define material properties table
	  //----------------------------------
	  pAl7075T6_MPT = nullptr;
	  pAl7075T6_MPT = new G4MaterialPropertiesTable();

	  //-------------------------------------------------------------------------------------------
	  pAl7075T6_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
	  pAl7075T6_MPT->AddProperty("REALRINDEX"                    , pMPV_REALRINDEX       );
	  pAl7075T6_MPT->AddProperty("IMAGINARYRINDEX"               , pMPV_IMAGINARYRINDEX  );
	  pAl7075T6_MPT->AddProperty("ABSLENGTH"                     , pMPV_RINDEX_ABSLENGTH );
	  //-------------------------------------------------------------------------------------------

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pAl7075T6_MD->SetMaterialPropertiesTable(pAl7075T6_MPT);

	} // end if (pMat == nullptr)

	return pAl7075T6_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Bronze_C38500()
{
	  G4String sName    = "Bronze_C38500";
      G4double fDensity = 8.47*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
      G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	    //============================================================
	    // Bronze Alloy: CuZn39Pb3 alias EN CW614W alias C38500 (ASTM)
	    //               Werkstoff-Nr.: 2.0401
	    //============================================================
		G4int ncomponents;

		pBronze_C38500_MD = nullptr;
		pBronze_C38500_MD = new G4Material( sName,
				                            fDensity,
											ncomponents = 7,
											kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4double fFractionOfMass;

		pBronze_C38500_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cu"), fFractionOfMass = 58.00*CLHEP::perCent);
		pBronze_C38500_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Zn"), fFractionOfMass = 37.50*CLHEP::perCent);
		pBronze_C38500_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Pb"), fFractionOfMass =  3.00*CLHEP::perCent);
		pBronze_C38500_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass =  0.50*CLHEP::perCent);
		pBronze_C38500_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ni"), fFractionOfMass =  0.50*CLHEP::perCent);
		pBronze_C38500_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Sn"), fFractionOfMass =  0.40*CLHEP::perCent);
		pBronze_C38500_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass =  0.10*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pBronze_C38500_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Copper()
{
	  G4String sName = "Copper";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Copper
	  //==============================

	  pCopper_MD = nullptr;
	  pCopper_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Cu");
	  pCopper_MD->SetName(sName);

	  //-------------------------------------------------------------------------------------------------------------------
	  //               Store file bases material properties in these vectors
	  //-------------------------------------------------------------------------------------------------------------------
	  G4MaterialPropertyVector* pMPV_REALRINDEX       = nullptr; pMPV_REALRINDEX       = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX  = nullptr; pMPV_IMAGINARYRINDEX  = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX           = nullptr; pMPV_RINDEX           = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = nullptr; pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	  //--------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------------------------------------------------------------------
	  // Read index of refraction from file
	  //--------------------------------------------------------------------------------------------------------------------
	  this->Initialize_IndexOfRefraction("/Copper/Copper_IndexOfRefraction.dat",
                                          pMPV_REALRINDEX,
                                          pMPV_IMAGINARYRINDEX,
                                          pMPV_RINDEX,
                                          pMPV_RINDEX_ABSLENGTH);

	  std::cout << "Copper_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

	  //----------------------------------
	  // Define material properties table
	  //----------------------------------
	  pCopper_MPT = nullptr;
	  pCopper_MPT = new G4MaterialPropertiesTable();

	  //-------------------------------------------------------------------------------------------
	  pCopper_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
	  pCopper_MPT->AddProperty("REALRINDEX"                    , pMPV_REALRINDEX       );
	  pCopper_MPT->AddProperty("IMAGINARYRINDEX"               , pMPV_IMAGINARYRINDEX  );
	  pCopper_MPT->AddProperty("ABSLENGTH"                     , pMPV_RINDEX_ABSLENGTH );
	  //-------------------------------------------------------------------------------------------

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pCopper_MD->SetMaterialPropertiesTable(pCopper_MPT);

	} // end if (pMat == nullptr)

	return pCopper_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Dural()
{
	  G4String sName    = "Dural";
	  G4double fDensity = 2.85*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //         Aluminum 7075-T6
	  //==============================
	  G4int ncomponents;

      pDural_MD = nullptr;
	  pDural_MD = new G4Material( sName,
			                      fDensity,
								  ncomponents = 8,
								  kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pDural_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mg"), fFractionOfMass =  1.10*CLHEP::perCent);
	  pDural_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass = 92.45*CLHEP::perCent);
	  pDural_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mn"), fFractionOfMass =  0.75*CLHEP::perCent);
	  pDural_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ni"), fFractionOfMass =  0.20*CLHEP::perCent);
	  pDural_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cu"), fFractionOfMass =  3.95*CLHEP::perCent);
	  pDural_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Sn"), fFractionOfMass =  0.20*CLHEP::perCent);
	  pDural_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Pb"), fFractionOfMass =  1.15*CLHEP::perCent);
	  pDural_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Bi"), fFractionOfMass =  0.20*CLHEP::perCent);

	  //--------------------------------------------------------------------------------------------------------------------
	  //               Store file bases material properties in these vectors
	  //--------------------------------------------------------------------------------------------------------------------
	  G4MaterialPropertyVector* pMPV_REALRINDEX       = nullptr; pMPV_REALRINDEX       = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX  = nullptr; pMPV_IMAGINARYRINDEX  = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX           = nullptr; pMPV_RINDEX           = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = nullptr; pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	  //--------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------------------------------------------------------------------
	  // Read index of refraction from file
	  //--------------------------------------------------------------------------------------------------------------------
	  this->Initialize_IndexOfRefraction("/Aluminum/Aluminum_IndexOfRefraction.dat",
			  	  	  	  	  	  	  	  pMPV_REALRINDEX,
										  pMPV_IMAGINARYRINDEX,
										  pMPV_RINDEX,
										  pMPV_RINDEX_ABSLENGTH);

	  std::cout << "Dural_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

	  //----------------------------------
	  // Define material properties table
	  ////--------------------------------
	  pDural_MPT = nullptr;
	  pDural_MPT = new G4MaterialPropertiesTable();

	  //--------------------------------------------------------------------------------------------------------------------
	  pDural_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
	  pDural_MPT->AddProperty("REALRINDEX"                    , pMPV_REALRINDEX       );
	  pDural_MPT->AddProperty("IMAGINARYRINDEX"               , pMPV_IMAGINARYRINDEX  );
	  pDural_MPT->AddProperty("ABSLENGTH"                     , pMPV_RINDEX_ABSLENGTH );
	  //--------------------------------------------------------------------------------------------------------------------

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pDural_MD->SetMaterialPropertiesTable(pDural_MPT);

	} // end if (pMat == nullptr)

	return pDural_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Gold()
{
	  G4String sName = "Gold";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Gold
	  //==============================

	  pGold_MD = nullptr;
	  pGold_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Au");
	  pGold_MD->SetName(sName);

	  //--------------------------------------------------------------------------------------------------------------------
	  //               Store file bases material properties in these vectors
	  //--------------------------------------------------------------------------------------------------------------------
	  G4MaterialPropertyVector* pMPV_REALRINDEX       = nullptr; pMPV_REALRINDEX       = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX  = nullptr; pMPV_IMAGINARYRINDEX  = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX           = nullptr; pMPV_RINDEX           = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = nullptr; pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	  //--------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------------------------------------------------------------------
	  // Read index of refraction from file
	  //--------------------------------------------------------------------------------------------------------------------
	  this->Initialize_IndexOfRefraction("/Gold/Gold_IndexOfRefraction.dat",
                                          pMPV_REALRINDEX,
										  pMPV_IMAGINARYRINDEX,
										  pMPV_RINDEX,
										  pMPV_RINDEX_ABSLENGTH);


	  std::cout << "Gold_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

	  //----------------------------------
	  // Define material properties table
	  //----------------------------------
	  pGold_MPT = nullptr;
	  pGold_MPT = new G4MaterialPropertiesTable();

	  //-------------------------------------------------------------------------------------------
	  pGold_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
	  pGold_MPT->AddProperty("REALRINDEX"                    , pMPV_REALRINDEX       );
	  pGold_MPT->AddProperty("IMAGINARYRINDEX"               , pMPV_IMAGINARYRINDEX  );
	  pGold_MPT->AddProperty("ABSLENGTH"                     , pMPV_RINDEX_ABSLENGTH );
	  //-------------------------------------------------------------------------------------------

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pGold_MD->SetMaterialPropertiesTable(pGold_MPT);

	} // end if (pMat == nullptr)

	return pGold_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Iron()
{
	G4String sName = "Iron";

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Iron
	  //==============================

	  pIron_MD = nullptr;
	  pIron_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Fe");
	  pIron_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pIron_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Lead()
{
	  G4String sName = "Lead";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Lead
	  //==============================

	  pLead_MD = nullptr;
	  pLead_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Pb");
	  pLead_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pLead_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_MUMetal()
{
	  G4String sName    = "MUMetal";
	  G4double fDensity = 8.25*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //         MUMetal
	  //==============================
	  // Definition taken from: https://jlabsvn.jlab.org/svnroot/halla/groups/g2p/HRSMC/src/HRSMaterial.cc
      G4int ncomponents;

	  pMUMetal_MD = nullptr;
	  pMUMetal_MD = new G4Material( sName,
			                        fDensity,
									ncomponents = 11,
									kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"),  fFractionOfMass =  0.015*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mn"), fFractionOfMass =  0.500*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_P"),  fFractionOfMass =  0.005*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_S"),  fFractionOfMass =  0.001*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  0.300*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass =  0.020*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ni"), fFractionOfMass = 80.200*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mo"), fFractionOfMass =  4.850*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass =  0.010*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Co"), fFractionOfMass =  0.020*CLHEP::perCent);
	  pMUMetal_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass = 14.079*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pMUMetal_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Nickel()
{
	G4String sName = "Nickel";

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Nickel
	  //==============================

	  pNickel_MD = nullptr;
	  pNickel_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Ni");
	  pNickel_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pNickel_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Silver()
{
	  G4String sName = "Silver";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Silver
	  //==============================

	  pSilver_MD = nullptr;
	  pSilver_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Ag");
	  pSilver_MD->SetName(sName);

	  //--------------------------------------------------------------------------------------------------------------------
	  //               Store file bases material properties in these vectors
	  //--------------------------------------------------------------------------------------------------------------------
	  G4MaterialPropertyVector* pMPV_REALRINDEX       = nullptr; pMPV_REALRINDEX       = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX  = nullptr; pMPV_IMAGINARYRINDEX  = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX           = nullptr; pMPV_RINDEX           = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = nullptr; pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	  //--------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------------------------------------------------------------------
	  // Read index of refraction from file
	  //--------------------------------------------------------------------------------------------------------------------
	  this->Initialize_IndexOfRefraction("/Silver/Silver_IndexOfRefraction.dat",
                                          pMPV_REALRINDEX,
										  pMPV_IMAGINARYRINDEX,
										  pMPV_RINDEX,
										  pMPV_RINDEX_ABSLENGTH);

	  std::cout << "Silver_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

	  //----------------------------------
	  // Define material properties table
	  //-----------------------------------
	  pSilver_MPT = nullptr;
	  pSilver_MPT = new G4MaterialPropertiesTable();

	  //-------------------------------------------------------------------------------------------
	  pSilver_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
	  pSilver_MPT->AddProperty("REALRINDEX"                    , pMPV_REALRINDEX       );
	  pSilver_MPT->AddProperty("IMAGINARYRINDEX"               , pMPV_IMAGINARYRINDEX  );
	  pSilver_MPT->AddProperty("ABSLENGTH"                     , pMPV_RINDEX_ABSLENGTH );
	  //-------------------------------------------------------------------------------------------

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pSilver_MD->SetMaterialPropertiesTable(pSilver_MPT);

	} // end if (pMat == nullptr)

	return pSilver_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_StainlessSteel()
{
	G4String sName    = "StainlessSteel";
	G4double fDensity = 7.88*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //    Generic Stainless Steel
	  //==============================
	  G4int ncomponents;

	  pStainlessSteel_MD = nullptr;
	  pStainlessSteel_MD = new G4Material( sName,
			                               fDensity,
										   ncomponents = 4,
										   kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pStainlessSteel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass = 71.5*CLHEP::perCent);
	  pStainlessSteel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass = 18.0*CLHEP::perCent);
	  pStainlessSteel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ni"), fFractionOfMass = 10.0*CLHEP::perCent);
	  pStainlessSteel_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  0.5*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pStainlessSteel_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_StainlessSteel_304()
{
	G4String sName    = "StainlessSteel_304";
	G4double fDensity = 7.93*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //    Stainless Steel 304
	  //==============================
      //
	  // Stolen from: https://jlabsvn.jlab.org/svnroot/halla/groups/g2p/HRSMC/src/HRSMaterial.cc

	  //---------------------
	  // Define new material
      //---------------------
	  G4int ncomponents;

	  pStainlessSteel_304_MD = nullptr;
	  pStainlessSteel_304_MD = new G4Material( sName,
			                                   fDensity,
											   ncomponents = 9,
											   kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass = 65.00*CLHEP::perCent);
	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass = 19.00*CLHEP::perCent);
	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ni"), fFractionOfMass = 10.00*CLHEP::perCent);
	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mn"), fFractionOfMass =  2.00*CLHEP::perCent);
	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  1.00*CLHEP::perCent);
	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mo"), fFractionOfMass =  2.84*CLHEP::perCent);
	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"),  fFractionOfMass =  0.08*CLHEP::perCent);
	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_P"),  fFractionOfMass =  0.05*CLHEP::perCent);
	  pStainlessSteel_304_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_S"),  fFractionOfMass =  0.03*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pStainlessSteel_304_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4Material* BTSimCommon_MaterialParameters::GetMaterial_StainlessSteel_316L()
{
	G4String sName    = "StainlessSteel_316L";
	G4double fDensity = 8.027*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	   //==============================
	   //    Stainless Steel 316L
	   //==============================
	   G4int ncomponents;

	   pStainlessSteel_316L_MD = nullptr;
	   pStainlessSteel_316L_MD = new G4Material( sName,
			                                     fDensity,
												 ncomponents = 5,
												 kStateSolid );

	   //----------------------------
	   // Assembling the ingredients
	   //----------------------------
	   G4double fFractionOfMass;

	   pStainlessSteel_316L_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass = 67.0*CLHEP::perCent);
	   pStainlessSteel_316L_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass = 18.0*CLHEP::perCent);
	   pStainlessSteel_316L_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ni"), fFractionOfMass = 11.0*CLHEP::perCent);
	   pStainlessSteel_316L_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  2.0*CLHEP::perCent);
	   pStainlessSteel_316L_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mn"), fFractionOfMass =  2.0*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pStainlessSteel_316L_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_StainlessSteel_446()
{
	G4String sName    = "StainlessSteel_446";
	G4double fDensity = 7.80*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //    Stainless Steel 446
	  //==============================
      //
	  // data taken from: http://www.metalcor.de/en/datenblatt/54/
	  G4int ncomponents;

	  pStainlessSteel_446_MD = nullptr;
	  pStainlessSteel_446_MD = new G4Material( sName,
			                                   fDensity,
											   ncomponents = 8,
											   kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pStainlessSteel_446_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass = 69.725 *CLHEP::perCent);
	  pStainlessSteel_446_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"),  fFractionOfMass =  0.12  *CLHEP::perCent);
	  pStainlessSteel_446_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  1.40  *CLHEP::perCent);
	  pStainlessSteel_446_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Mn"), fFractionOfMass =  1.00  *CLHEP::perCent);
	  pStainlessSteel_446_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_P"),  fFractionOfMass =  0.04  *CLHEP::perCent);
	  pStainlessSteel_446_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_S"),  fFractionOfMass =  0.015 *CLHEP::perCent);
	  pStainlessSteel_446_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass = 26.00  *CLHEP::perCent);
	  pStainlessSteel_446_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass =  1.70  *CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pStainlessSteel_446_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Tin()
{
	  G4String sName = "Tin";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Tin
	  //==============================

	  pTin_MD = nullptr;
	  pTin_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Sn");
	  pTin_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pTin_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Titanium()
{
	  G4String sName = "Titanium";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Titanium
	  //==============================

	  //---------------------
	  // Define new material
	  //---------------------
	  pTitanium_MD = nullptr;
	  pTitanium_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Ti");
	  pTitanium_MD->SetName(sName);

	  //--------------------------------------------------------------------------------------------------------------------
	  //               Store file bases material properties in these vectors
	  //--------------------------------------------------------------------------------------------------------------------
	  G4MaterialPropertyVector* pMPV_REALRINDEX       = nullptr; pMPV_REALRINDEX       = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX  = nullptr; pMPV_IMAGINARYRINDEX  = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX           = nullptr; pMPV_RINDEX           = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = nullptr; pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	  //--------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------------------------------------------------------------------
	  // Read index of refraction from file
	  //--------------------------------------------------------------------------------------------------------------------
	  this->Initialize_IndexOfRefraction("/Titanium/Titanium_IndexOfRefraction.dat",
              	  	  	  	  	  	  	  pMPV_REALRINDEX,
										  pMPV_IMAGINARYRINDEX,
										  pMPV_RINDEX,
										  pMPV_RINDEX_ABSLENGTH);

	  std::cout << "Titanium_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

	  //----------------------------------
	  // Define material properties table
	  //----------------------------------
	  pTitanium_MPT = nullptr;
	  pTitanium_MPT = new G4MaterialPropertiesTable();

	  //--------------------------------------------------------------------------------------------------------------------
	  pTitanium_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
	  pTitanium_MPT->AddProperty("REALRINDEX"                    , pMPV_REALRINDEX       );
	  pTitanium_MPT->AddProperty("IMAGINARYRINDEX"               , pMPV_IMAGINARYRINDEX  );
	  pTitanium_MPT->AddProperty("ABSLENGTH"                     , pMPV_RINDEX_ABSLENGTH );
	  //--------------------------------------------------------------------------------------------------------------------

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pTitanium_MD->SetMaterialPropertiesTable(pTitanium_MPT);

	} // end if (pMat == nullptr)

	return pTitanium_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Tungsten()
{
	  G4String sName = "Tungsten";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Tungsten
	  //==============================

	  pTungsten_MD = nullptr;
	  pTungsten_MD = pNistMaterialManager->FindOrBuildMaterial("G4_W");
	  pTungsten_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pTungsten_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#####################################################################################################################
//#####################################################################################################################
//                      Drift/Wire-Chamber Gases
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_ArCH4Gas_9010_858()
{
	G4String sName    = "ArCH4Gas_9010_858mbar";
	G4double fDensity = 1.6767*CLHEP::milligram/CLHEP::cm3; // at STP

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=====================================================================
		//      ArCH4 counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  90% Argon
		//  10% Methane (CH4)
		//
		// Environment:  858 mbar
		//                20 deg Celsius
		//
		//---------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = (858.0/1013.25) * CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * (CLHEP::STP_Temperature / fCurrentTemperature) * (fCurrentPressure/CLHEP::STP_Pressure);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pArCH4Gas_9010_858_MD = nullptr;
	    pArCH4Gas_9010_858_MD = new G4Material( sName,
	    		                                fCurrentDensity,
												ncomponents = 2,
											    kStateGas,
											    fCurrentTemperature,
											    fCurrentPressure );

	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 90.00*CLHEP::perCent ); vMolarWeight.push_back(39.95); // Argon
	    vVolumeFraction.push_back( 10.00*CLHEP::perCent ); vMolarWeight.push_back(16.04); // Methane

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pArCH4Gas_9010_858_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ar")     , fFractionOfMass = vMassFraction.at(0) );
	    pArCH4Gas_9010_858_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_METHANE"), fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pArCH4Gas_9010_858_MD->GetIonisation()->SetMeanEnergyPerIonPair( 26.4*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pArCH4Gas_9010_858_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_ArCH4Gas_9307()
{
	G4String sName    = "ArCH4Gas_9307";
	G4double fDensity = 1.709*CLHEP::milligram/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=====================================================================
		//      ArCH4 counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  93% Argon
		//   7% Methane (CH4)
		//
		// Environment:  1013.25 mbar (1 atm)
		//                 20    deg Celsius
		//
		//---------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = 1.000*CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * CLHEP::STP_Temperature / fCurrentTemperature;

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pArCH4Gas_9307_MD = nullptr;
	    pArCH4Gas_9307_MD = new G4Material( sName,
	    		                            fCurrentDensity,
											ncomponents = 2,
											kStateGas,
											fCurrentTemperature,
											fCurrentPressure);

	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 93.00*CLHEP::perCent ); vMolarWeight.push_back(39.95); // Argon
	    vVolumeFraction.push_back(  7.00*CLHEP::perCent ); vMolarWeight.push_back(16.04); // Methane

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pArCH4Gas_9307_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ar")     , fFractionOfMass = vMassFraction.at(0) );
	    pArCH4Gas_9307_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_METHANE"), fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pArCH4Gas_9307_MD->GetIonisation()->SetMeanEnergyPerIonPair( 26.4*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pArCH4Gas_9307_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_ArCO2Gas_7030()
{
	G4String sName    = "ArCO2Gas_7030";
	G4double fDensity = 1.8223*CLHEP::milligram/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

		//=====================================================================
		//     ArCO2 counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  70% Argon
		//  30% CO2
		//
		// Environment: 1013.25 mbar (1 atm)
		//                20    deg Celsius
		//
		//--------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = 1.000*CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * CLHEP::STP_Temperature / fCurrentTemperature;

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pArCO2Gas_7030_MD = nullptr;
	    pArCO2Gas_7030_MD = new G4Material( sName,
	    		                            fCurrentDensity,
											ncomponents = 2,
											kStateGas,
											fCurrentTemperature,
											fCurrentPressure );

	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 70.00*CLHEP::perCent ); vMolarWeight.push_back(39.95); // Argon
	    vVolumeFraction.push_back( 30.00*CLHEP::perCent ); vMolarWeight.push_back(44.01); // CO2

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pArCO2Gas_7030_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ar")            , fFractionOfMass = vMassFraction.at(0) );
	    pArCO2Gas_7030_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_CARBON_DIOXIDE"), fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pArCO2Gas_7030_MD->GetIonisation()->SetMeanEnergyPerIonPair( 26.4*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pArCO2Gas_7030_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_ArCO2Gas_8020()
{
	G4String sName    = "ArCO2Gas_8020";
	G4double fDensity = 1.8223*CLHEP::milligram/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

		//=====================================================================
		//     ArCO2 counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  80% Argon
		//  20% CO2
		//
		// Environment: 1013.25 mbar (1 atm)
		//                20    deg Celsius
		//
		//--------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = 1.000*CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * CLHEP::STP_Temperature / fCurrentTemperature;

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pArCO2Gas_8020_MD = nullptr;
	    pArCO2Gas_8020_MD = new G4Material( sName,
	    		                            fCurrentDensity,
											ncomponents = 2,
											kStateGas,
											fCurrentTemperature,
											fCurrentPressure );

	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 80.00*CLHEP::perCent ); vMolarWeight.push_back(39.95); // Argon
	    vVolumeFraction.push_back( 20.00*CLHEP::perCent ); vMolarWeight.push_back(44.01); // CO2

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pArCO2Gas_8020_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ar")            , fFractionOfMass = vMassFraction.at(0) );
	    pArCO2Gas_8020_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_CARBON_DIOXIDE"), fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pArCO2Gas_8020_MD->GetIonisation()->SetMeanEnergyPerIonPair( 26.4*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pArCO2Gas_8020_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_CF4()
{
	G4String sName    = "CF4";
	G4double fDensity = 3.72*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
		//==============================
		//    Tetrafluormethan CF4
		//==============================

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pCF4_MD = nullptr;
		pCF4_MD = new G4Material( sName,
				                  fDensity,
								  ncomponents = 2,
								  kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
		G4int natoms;

		pCF4_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 1 );
		pCF4_MD->AddElement( pNistMaterialManager->FindOrBuildElement("F"), natoms = 4 );

	} // end if (pMat == nullptr)

	return pCF4_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_EthylChloride()
{
	G4String sName    = "EthyChloride";
	G4double fDensity = 0.8898*CLHEP::g/CLHEP::cm3; // at 25 deg

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
		//==============================
		//    Ethyl Chloride C2_H5_Cl
		//==============================
		// Molar mass: 64.512 Kg/kmol
		//------------------------------

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pEthylChloride_MD = nullptr;
		pEthylChloride_MD = new G4Material( sName,
				                            fDensity,
											ncomponents = 3,
								            kStateGas );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
		G4int natoms;

		pEthylChloride_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C") , natoms = 2 );
		pEthylChloride_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H") , natoms = 5 );
		pEthylChloride_MD->AddElement( pNistMaterialManager->FindOrBuildElement("Cl"), natoms = 1 );

	} // end if (pMat == nullptr)

	return pEthylChloride_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_IsoHexane()
{
	G4String sName    = "IsoHexane";
	G4double fDensity = 0.65*CLHEP::g/CLHEP::cm3; // at 20 deg

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
		//==============================
		//   Isohexane C6_H14
		//==============================
		// Molar mass: 88.18 Kg/kmol
		//------------------------------

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pIsoHexane_MD = nullptr;
		pIsoHexane_MD = new G4Material( sName,
				                        fDensity,
										ncomponents = 2,
								        kStateLiquid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
		G4int natoms;

		pIsoHexane_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C") , natoms =  6 );
		pIsoHexane_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H") , natoms = 14 );

	} // end if (pMat == nullptr)

	return pIsoHexane_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Krypton_55()
{
	G4String sName        = "Krypton_55";
	G4double fDensity_STP = pNistMaterialManager->FindOrBuildMaterial("G4_Kr")->GetDensity();// at STP

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
                pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
		//=====================================================
		//     Krypton @ 5.5atm for building Kr-85 Beta source
		//=====================================================
		//
		// Environment: 5.5 atm, 20 deg Celsius
		//------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = 5.5*CLHEP::STP_Pressure;

		//---------------------
		// Define new material
		//---------------------
		pKrypton_55_MD = nullptr;
		pKrypton_55_MD = pNistMaterialManager->ConstructNewGasMaterial(  sName,
				                                                        "G4_Kr",
														                 fCurrentTemperature,
														                 fCurrentPressure );
	} // end if (pMat == nullptr)

	return pKrypton_55_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_KrCH4Gas_9307()
{
	G4String sName    = "KrCH4Gas_9307";
	G4double fDensity = 3.491*CLHEP::milligram/CLHEP::cm3; // at STP

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=====================================================================
		//      KrCH4 counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  93% Krypton
		//   7% Methane (CH4)
		//
		// Environment:  1013.25 mbar (1 atm)
		//                 20    deg Celsius
		//
		//---------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = (1013.25/1013.25) * CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * (CLHEP::STP_Temperature / fCurrentTemperature) * (fCurrentPressure/CLHEP::STP_Pressure);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pKrCH4Gas_9307_MD = nullptr;
	    pKrCH4Gas_9307_MD = new G4Material( sName,
	    		                            fCurrentDensity,
											ncomponents = 2,
											kStateGas,
											fCurrentTemperature,
											fCurrentPressure);

	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 93.00*CLHEP::perCent ); vMolarWeight.push_back(83.80); // Krypton
	    vVolumeFraction.push_back(  7.00*CLHEP::perCent ); vMolarWeight.push_back(16.04); // Methane

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pKrCH4Gas_9307_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Kr")     , fFractionOfMass = vMassFraction.at(0) );
	    pKrCH4Gas_9307_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_METHANE"), fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pKrCH4Gas_9307_MD->GetIonisation()->SetMeanEnergyPerIonPair( 24.0*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pKrCH4Gas_9307_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_KrCO2Gas_8020()
{
	G4String sName    = "KrCO2Gas_8020";
	G4double fDensity = 3.601*CLHEP::milligram/CLHEP::cm3; // at STP

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=====================================================================
		//      KrCO2 counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  80% Krypton
		//  20% CO2
		//
		// Environment:  1013.25 mbar (1 atm)
		//                 20    deg Celsius
		//
		//---------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = (1013.25/1013.25) * CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * (CLHEP::STP_Temperature / fCurrentTemperature) * (fCurrentPressure/CLHEP::STP_Pressure);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pKrCO2Gas_8020_MD = nullptr;
	    pKrCO2Gas_8020_MD = new G4Material( sName,
	    		                            fCurrentDensity,
											ncomponents = 2,
											kStateGas,
											fCurrentTemperature,
											fCurrentPressure);

	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 80.00*CLHEP::perCent ); vMolarWeight.push_back(83.80); // Krypton
	    vVolumeFraction.push_back( 20.00*CLHEP::perCent ); vMolarWeight.push_back(44.01); // CO2

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pKrCO2Gas_8020_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Kr")            , fFractionOfMass = vMassFraction.at(0) );
	    pKrCO2Gas_8020_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_CARBON_DIOXIDE"), fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pKrCO2Gas_8020_MD->GetIonisation()->SetMeanEnergyPerIonPair( 24.0*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pKrCO2Gas_8020_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_P10Gas()
{
	G4String sName    = "P10Gas";
	G4double fDensity = 1.557*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
		//=====================================================================
		//      P-10 counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  90% Argon
		//  10% Methane (CH4)
		//
		// Environment: 1013.25 mbar (1 atm)
		//                20    deg Celsius
		//
		//---------------------------------------------------------------------

		const G4double fCurrentTemperature  = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure     = 1.000*CLHEP::STP_Pressure;
		const G4double fCurrentDensity      = fDensity * CLHEP::STP_Temperature / fCurrentTemperature;

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pP10Gas_MD = nullptr;
	    pP10Gas_MD = new G4Material( sName,
	    		                     fCurrentDensity,
									 ncomponents = 2,
									 kStateGas,
									 fCurrentTemperature,
									 fCurrentPressure );


	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 90.00*CLHEP::perCent ); vMolarWeight.push_back(39.95); // Argon
	    vVolumeFraction.push_back( 10.00*CLHEP::perCent ); vMolarWeight.push_back(16.04); // Methane

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pP10Gas_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ar")     , fFractionOfMass = vMassFraction.at(0) );
	    pP10Gas_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_METHANE"), fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pP10Gas_MD->GetIonisation()->SetMeanEnergyPerIonPair( 26.4*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pP10Gas_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_TOLFGas()
{
	G4String sName    = "TOLFGas";
	G4double fDensity = 21.03*CLHEP::milligram/CLHEP::cm3;

	//-----------------------------------------------------------------------------------------------------------------
	// TOL/F gas mixture
	//    18.5 mbar ethyl chloride partial pressure of C2_H5_Cl (M = 64.51 g/mol)
	//    59.5 mbar isohexane      partial pressure of C6_H14   (M = 88.18 g/mol)
	//
	// => 78.0 mbar total pressure
	//-----------------------------------------------------------------------------------------------------------------

	//-----------------------------------------------------------------------------------------------------------------
	// Quick and dirty calculation of the density based on 100% Isohexan with 59.5 mbar (partial pressure) only !!!
	// (Contribution of ethyl chloride with 18.5 mbar partial pressure will be neglected)
	//
	// Ideal gas law:
	//
	// density = (p*M)/(R*T)
	//
	// p_(Isohexan) = 59.5 mbar                   partial pressure
	// M_(Isohexan) = 86.18 g/mol                 molar weight
	//            R = 83.145 (bar*cm3)/(mol*K)    gas constant
	//            T = 293.15 K (20 deg Celcius)   temperature
	//
	// density [g/cm3] = (0.0595 * 86.18) / (83.145*293.15) = 2.103 10^{-4} g/cm3
	//                 = 21.03 mg/cm3
	//
	//-----------------------------------------------------------------------------------------------------------------

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=====================================================================
		//      TOLF Gas
		//=====================================================================
		//
		// Gas mixture by partial pressure
		//
		//  18.5 mBar Ethyl Chloride (C2_H5_Cl, 64.512 g/mol)
		//  59.5 mBar Isohexane      (C6_H14  , 88.18  g/mol)
		//
		//
		// Environment:  78 mBar        (underpressure compared to 1013 mBar)
		//               20 deg Celsius
		//
		//---------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = (78.0/1013.25)*CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * (CLHEP::STP_Temperature / fCurrentTemperature) * (fCurrentPressure/CLHEP::STP_Pressure);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pTOLFGas_MD = nullptr;
	    pTOLFGas_MD = new G4Material( sName,
	    		                      fCurrentDensity,
									  ncomponents = 2,          // Number of elements or materials
									  kStateGas,
								      fCurrentTemperature,
									  fCurrentPressure
								    );

	    //-----------------------------------------------
	    // Input: Mixture by partial pressures (in mbar)
	    //-----------------------------------------------
	    std::vector<G4double> vPartialPressure; vPartialPressure .clear();
	    std::vector<G4double> vMolarWeight;     vMolarWeight     .clear();


	    vPartialPressure.push_back( 18.5 ); vMolarWeight.push_back( 64.51 ); // Ethyl Chloride C2_H5_Cl
	    vPartialPressure.push_back( 59.5 ); vMolarWeight.push_back( 88.18 ); //      Isohexane C6_H14


	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertPartialPressureToMassFraction(vPartialPressure, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pTOLFGas_MD->AddMaterial( this->GetMaterial_EthylChloride() , fFractionOfMass = vMassFraction.at(0) );
	    pTOLFGas_MD->AddMaterial( this->GetMaterial_IsoHexane()     , fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //----------------------------------------------------------------------------------------------
	    pTOLFGas_MD->GetIonisation()->SetMeanEnergyPerIonPair( 22.1*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pTOLFGas_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4Material* BTSimCommon_MaterialParameters::GetMaterial_XeCO2Gas_8020()
{
	G4String sName    = "XeCO2Gas_8020";
	G4double fDensity = 5.082*CLHEP::milligram/CLHEP::cm3; // at STP

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=====================================================================
		//      XeCO2 counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  80% Xenon
		//  20% CO2
		//
		// Environment:  1013.25 mbar (1 atm)
		//                 20    deg Celsius
		//
		//---------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = (1013.25/1013.25) * CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * (CLHEP::STP_Temperature / fCurrentTemperature) * (fCurrentPressure/CLHEP::STP_Pressure);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pXeCO2Gas_8020_MD = nullptr;
	    pXeCO2Gas_8020_MD = new G4Material( sName,
	    		                            fCurrentDensity,
											ncomponents = 2,
											kStateGas,
											fCurrentTemperature,
											fCurrentPressure);

	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 80.00*CLHEP::perCent ); vMolarWeight.push_back(131.29); // Xenon
	    vVolumeFraction.push_back( 20.00*CLHEP::perCent ); vMolarWeight.push_back( 44.01); // CO2

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pXeCO2Gas_8020_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Xe")            , fFractionOfMass = vMassFraction.at(0) );
	    pXeCO2Gas_8020_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_CARBON_DIOXIDE"), fFractionOfMass = vMassFraction.at(1) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pXeCO2Gas_8020_MD->GetIonisation()->SetMeanEnergyPerIonPair( 22.1*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pXeCO2Gas_8020_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_XeCO2CF4Gas_702703()
{
	G4String sName    = "XeCO2CF4Gas_702703";
	G4double fDensity = 4.76*CLHEP::milligram/CLHEP::cm3; // at STP

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=====================================================================
		//      XeCO2CF4G counting gas
		//=====================================================================
		//
		// Gas mixture by volume
		//
		//  70% Xenon
		//  27% CO2
		//   3% CF4
		//
		// Environment:  1013.25 mbar (1 atm)
		//                 20    deg Celsius
		//
		//---------------------------------------------------------------------

		const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;
		const G4double fCurrentPressure    = (1013.25/1013.25)*CLHEP::STP_Pressure;
		const G4double fCurrentDensity     = fDensity * (CLHEP::STP_Temperature / fCurrentTemperature) * (fCurrentPressure/CLHEP::STP_Pressure);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pXeCO2CF4Gas_702703_MD = nullptr;
	    pXeCO2CF4Gas_702703_MD = new G4Material( sName,
	    		                                 fCurrentDensity,
												 ncomponents = 3,         // Number of elements or materials
											     kStateGas,
											     fCurrentTemperature,
											     fCurrentPressure
										       );

	    //---------------------------------------
	    // Input: Mixture by fraction of volume
	    //---------------------------------------
	    std::vector<G4double> vVolumeFraction; vVolumeFraction .clear();
	    std::vector<G4double> vMolarWeight;    vMolarWeight    .clear();

	    vVolumeFraction.push_back( 70.00*CLHEP::perCent ); vMolarWeight.push_back(131.29); // Xenon
	    vVolumeFraction.push_back( 27.00*CLHEP::perCent ); vMolarWeight.push_back( 44.01); // CO2
	    vVolumeFraction.push_back(  3.00*CLHEP::perCent ); vMolarWeight.push_back( 88.00); // CF4

	    //---------------------------------------
	    // Output: Mixture by fraction of mass
	    //---------------------------------------
	    std::vector<G4double> vMassFraction = this->ConvertVolumeToMassFraction(vVolumeFraction, vMolarWeight);


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pXeCO2CF4Gas_702703_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Xe")            , fFractionOfMass = vMassFraction.at(0) );
	    pXeCO2CF4Gas_702703_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_CARBON_DIOXIDE"), fFractionOfMass = vMassFraction.at(1) );
	    pXeCO2CF4Gas_702703_MD->AddMaterial( this->GetMaterial_CF4()                                       , fFractionOfMass = vMassFraction.at(2) );

	    //----------------------------------------------------------------------------------------------
	    // Default ionization value (W value)
	    //----------------------------------------------------------------------------------------------
	    // Taken from:
	    //  "Monte Carlo estimation of average energy required to produce an ion pair in noble gases
	    //   by electrons with energies from 1 keV to 100 MeV"
	    //   Poskus, Andrius (Department of Solid State Electronics, Vilnius University
	    //----------------------------------------------------------------------------------------------
	    pXeCO2CF4Gas_702703_MD->GetIonisation()->SetMeanEnergyPerIonPair( 22.1*CLHEP::eV);

	} // end if (pMat == nullptr)

	return pXeCO2CF4Gas_702703_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....


//#####################################################################################################################
//#####################################################################################################################
//                      Tissue equivalent materials
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_A150Plastics()
{
	  G4String sName    = "A150Plastics";
	  G4double fDensity = 1.138*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//==============================
		//         A-150 Plastics
		//==============================
		G4int ncomponents;

		pA150Plastics_MD = nullptr;
		pA150Plastics_MD = new G4Material( sName,
				                           fDensity,
										   ncomponents = 6,
										   kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4double fFractionOfMass;

		pA150Plastics_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_H") , fFractionOfMass = 10.20*CLHEP::perCent);
		pA150Plastics_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C") , fFractionOfMass =  7.76*CLHEP::perCent);
		pA150Plastics_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_N") , fFractionOfMass =  3.50*CLHEP::perCent);
		pA150Plastics_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O") , fFractionOfMass =  5.20*CLHEP::perCent);
		pA150Plastics_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_F") , fFractionOfMass =  1.70*CLHEP::perCent);
		pA150Plastics_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ca"), fFractionOfMass =  1.80*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pA150Plastics_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_ICRU_Phantom()
{
	  G4String sName    = "ICRU_Phantom";
	  G4double fDensity = 1.00*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//==============================
		//         ICRU Phantom
		//==============================
		G4int ncomponents;

		pICRU_Phantom_MD = nullptr;
		pICRU_Phantom_MD = new G4Material( sName,
				                           fDensity,
										   ncomponents = 4,
										   kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4double fFractionOfMass;

		pICRU_Phantom_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O"), fFractionOfMass = 76.20*CLHEP::perCent);
		pICRU_Phantom_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"), fFractionOfMass = 11.10*CLHEP::perCent);
		pICRU_Phantom_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_H"), fFractionOfMass = 10.10*CLHEP::perCent);
		pICRU_Phantom_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_N"), fFractionOfMass =  2.60*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pICRU_Phantom_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#####################################################################################################################
//#####################################################################################################################
//                      Materials for Neutron Detection
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//=========================================================
//                      Boron related
//=========================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Boron_Enriched()
{
	G4String sName    = "Boron_enriched";
	G4double fDensity = 2.17*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat= G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //========================================================
	  //           Enriched Boron
	  //========================================================
	  //
	  // Check data sheet: 3M 10B Enriched  Boron Carbide

	  //--------------------------------------------------------
	  // Check if isotopes are already defined.
	  // Since they are defined all together it is sufficient
	  // to test for the existance of *one* isotope like B10
	  //--------------------------------------------------------

		G4Isotope* pIsotope = nullptr;
		           pIsotope = G4Isotope::GetIsotope("B10", false);


		if (pIsotope == nullptr) // no isotopes have been defined before
		{
			this->DefineIsotopes_Boron();
		}

		//----------------------------------------------------------------
		// Define element according to the isotope abundances/enrichments
		//----------------------------------------------------------------
		G4int nisotopes;

		G4Element* pBoron_enriched_ED = nullptr;
		           pBoron_enriched_ED = new G4Element("Boron_enriched",
		        		                              "Boron_enriched",
													   nisotopes = 2 );

		//---------------------------------------
		// Assembling the ingredients
		//---------------------------------------
		// Note: Isotopes are added by abundance
		//       and NOT by fraction of mass !!!
		//---------------------------------------
		G4double fRelativeAbundance;

		pBoron_enriched_ED->AddIsotope(pB10_ID, fRelativeAbundance = 96.0*CLHEP::perCent);
		pBoron_enriched_ED->AddIsotope(pB11_ID, fRelativeAbundance =  4.0*CLHEP::perCent);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pBoron_enriched_MD = nullptr;
        pBoron_enriched_MD = new G4Material( sName,
        		                             fDensity,
											 ncomponents = 1,
											 kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pBoron_enriched_MD->AddElement( pBoron_enriched_ED, fFractionOfMass = 100.0*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pBoron_enriched_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BoronCarbide_Absorber()
{
	  G4String sName    = "BoronCarbide_Absorber";
	  G4double fDensity = 2.52*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		  //==================================
		  //     Enriched BoronCarbide == B4C
		  //==================================
		  G4int ncomponents;

		  pBoronCarbide_Absorber_MD = nullptr;
		  pBoronCarbide_Absorber_MD = new G4Material( sName,
					                                  fDensity,
													  ncomponents = 2,
										              kStateSolid );

		  //----------------------------
		  // Assembling the ingredients
		  //----------------------------
		  G4double fFractionOfMass;

		  pBoronCarbide_Absorber_MD->AddMaterial( this->GetMaterial_Boron_Enriched()               , fFractionOfMass =  75.0*CLHEP::perCent); // 3M: 74% - 79%
		  pBoronCarbide_Absorber_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"), fFractionOfMass =  25.0*CLHEP::perCent); // 3M: 26% - 21%

	  } // end if (pMat == nullptr)

	  return pBoronCarbide_Absorber_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//=========================================================
//                      Gadolinium related
//=========================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Gadolinium()
{
	G4String sName    = "Gadolinium";
	G4double fDensity = 7.95*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===========================================
	  //           Gadolinium
	  //===========================================

	  //-------------------------------------------------------------------------------------------------------------------------
	  // Material Definition taken from: http://hypernews.slac.stanford.edu/HyperNews/geant4/get/phys-list/27.html?inline=-1
	  // Natural  abundance  taken from: https://isotopes.gov/catalog/product.php?element=Gadolinium
	  // Atomic mass 	     taken from: http://www.tracesciences.com/gd.htm
      //-------------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------
	  // Check if isotopes are already defined.
	  //--------------------------------------------------------
	  // Since they are defined all together it is
	  // sufficient to test for the existance of one isotope
	  //--------------------------------------------------------

		G4Isotope* pIsotope = nullptr;
		           pIsotope= G4Isotope::GetIsotope("Gd152", false);

		if (pIsotope == nullptr) // no isotopes have been defined before
		{
			this->DefineIsotopes_Gadolinium();
		}

		//----------------------------------------------------
		// Define element according to the isotope abundances
		//----------------------------------------------------
		G4int nisotopes;

		G4Element* pGd_ED = nullptr;
		           pGd_ED = new G4Element("Gadolinium",
				                          "Gd",
										   nisotopes = 7 );

	    //---------------------------------------
	    // Assembling the ingredients
	    //---------------------------------------
		// Note: Isotopes are added by abundance
		//       and NOT by fraction of mass !!!
		//---------------------------------------
		G4double fRelativeAbundance;

		pGd_ED->AddIsotope(pGd152_ID, fRelativeAbundance =  0.20*CLHEP::perCent);
		pGd_ED->AddIsotope(pGd154_ID, fRelativeAbundance =  2.15*CLHEP::perCent);
		pGd_ED->AddIsotope(pGd155_ID, fRelativeAbundance = 14.73*CLHEP::perCent);
		pGd_ED->AddIsotope(pGd156_ID, fRelativeAbundance = 20.47*CLHEP::perCent);
		pGd_ED->AddIsotope(pGd157_ID, fRelativeAbundance = 15.68*CLHEP::perCent);
		pGd_ED->AddIsotope(pGd158_ID, fRelativeAbundance = 24.87*CLHEP::perCent);
		pGd_ED->AddIsotope(pGd160_ID, fRelativeAbundance = 21.90*CLHEP::perCent);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pGadolinium_MD = nullptr;
        pGadolinium_MD = new G4Material( sName,
        		                         fDensity,
										 ncomponents = 1,
										 kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

        pGadolinium_MD->AddElement( pGd_ED, fFractionOfMass = 100.0*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pGadolinium_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Gd155enriched()
{
	G4String sName    = "Gadolinium155_enriched";
	G4double fDensity = 7.95*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat= G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===========================================
	  //           Gadolinium
	  //===========================================
	  //
	  // Material Definition taken from: http://hypernews.slac.stanford.edu/HyperNews/geant4/get/phys-list/27.html?inline=-1
	  // Natural  abundance  taken from: https://isotopes.gov/catalog/product.php?element=Gadolinium
	  // Atomic mass 	     taken from: http://www.tracesciences.com/gd.htm

	  //-------------------------------------------------------
	  // Check if isotopes are already defined.
	  // Since they are defined all together it is sufficient
	  // to test for the existance of *one* isotope like Gd152
	  //--------------------------------------------------------

		G4Isotope* pIsotope = nullptr;
		           pIsotope = G4Isotope::GetIsotope("Gd152", false);


		if (pIsotope == nullptr) // no isotopes have been defined before
		{
			this->DefineIsotopes_Gadolinium();
		}

		//----------------------------------------------------
		// Define element according to the isotope abundances
		//-----------------------------------------------------
		G4int nisotopes;

		G4Element* pGd155_enriched_ED = nullptr;
		           pGd155_enriched_ED = new G4Element("Gadolinium155_enriched",
		        		                              "Gd155_enriched",
													   nisotopes = 7);

		//---------------------------------------
		// Assembling the ingredients
		//---------------------------------------
		// Note: Isotopes are added by abundance
		//       and NOT by fraction of mass !!!
		//---------------------------------------
		G4double fRelativeAbundance;

		pGd155_enriched_ED->AddIsotope(pGd152_ID, fRelativeAbundance =  0.00108*CLHEP::perCent);
		pGd155_enriched_ED->AddIsotope(pGd154_ID, fRelativeAbundance =  0.98590*CLHEP::perCent);
		pGd155_enriched_ED->AddIsotope(pGd155_ID, fRelativeAbundance = 74.42330*CLHEP::perCent);
		pGd155_enriched_ED->AddIsotope(pGd156_ID, fRelativeAbundance = 17.56740*CLHEP::perCent);
		pGd155_enriched_ED->AddIsotope(pGd157_ID, fRelativeAbundance =  3.75130*CLHEP::perCent);
		pGd155_enriched_ED->AddIsotope(pGd158_ID, fRelativeAbundance =  2.53360*CLHEP::perCent);
		pGd155_enriched_ED->AddIsotope(pGd160_ID, fRelativeAbundance =  0.72780*CLHEP::perCent);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pGd155_enriched_MD = nullptr;
        pGd155_enriched_MD = new G4Material( sName,
        		                             fDensity,
											 ncomponents = 1,
											 kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pGd155_enriched_MD->AddElement( pGd155_enriched_ED, fFractionOfMass = 100.0*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pGd155_enriched_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Gd157enriched()
{
	G4String sName    = "Gadolinium157_enriched";
	G4double fDensity = 7.95*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===========================================
	  //           Gadolinium
	  //===========================================
	  //
	  // Material Definition taken from: http://hypernews.slac.stanford.edu/HyperNews/geant4/get/phys-list/27.html?inline=-1
	  // Natural  abundance  taken from: https://isotopes.gov/catalog/product.php?element=Gadolinium
	  // Atomic mass 	     taken from: http://www.tracesciences.com/gd.htm

	  // Check if isotopes are already defined.
	  // Since they are defined all together it is sufficient to test for the existance of one isotope

		G4Isotope* pIsotopeName = nullptr;
		           pIsotopeName = G4Isotope::GetIsotope("Gd152", false);

		if (pIsotopeName == nullptr) // no isotopes have been defined before
		{
			this->DefineIsotopes_Gadolinium();
		}

		//-----------------------------------------------------
		// Define element according to thze isotope abundances
		//-----------------------------------------------------
		G4int nisotopes;

		G4Element* pGd157_enriched_ED = nullptr;
		           pGd157_enriched_ED = new G4Element("Gadolinium157_enriched",
		        		                              "Gd157_enriched",
													   nisotopes = 7 );

	    //---------------------------------------
		// Assembling the ingredients
	    //---------------------------------------
		// Note: Isotopes are added by abundance
	    //       and NOT by fraction of mass !!!
		//---------------------------------------
		G4double fRelativeAbundance;

		pGd157_enriched_ED->AddIsotope(pGd152_ID, fRelativeAbundance =  0.00510*CLHEP::perCent);
		pGd157_enriched_ED->AddIsotope(pGd154_ID, fRelativeAbundance =  0.00753*CLHEP::perCent);
		pGd157_enriched_ED->AddIsotope(pGd155_ID, fRelativeAbundance =  1.35147*CLHEP::perCent);
		pGd157_enriched_ED->AddIsotope(pGd156_ID, fRelativeAbundance =  7.36270*CLHEP::perCent);
		pGd157_enriched_ED->AddIsotope(pGd157_ID, fRelativeAbundance = 69.66230*CLHEP::perCent);
		pGd157_enriched_ED->AddIsotope(pGd158_ID, fRelativeAbundance = 19.44310*CLHEP::perCent);
		pGd157_enriched_ED->AddIsotope(pGd160_ID, fRelativeAbundance =  2.10000*CLHEP::perCent);

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pGd157_enriched_MD = nullptr;
        pGd157_enriched_MD = new G4Material( sName,
        		                             fDensity,
											 ncomponents = 1,
											 kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pGd157_enriched_MD->AddElement( pGd157_enriched_ED, fFractionOfMass = 100.0*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pGd157_enriched_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//=========================================================
//                      Helium related
//=========================================================

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Helium3_100_4000()
{
	G4String sName = "Helium3_100_4000mbar";

    const G4double fAtomicMass         = 3.016*CLHEP::g/CLHEP::mole;
    const G4double fMolarConstant      = CLHEP::Avogadro*CLHEP::k_Boltzmann;           //from CLHEP

	const G4double fCurrentPressure    = 4.0* CLHEP::bar;
	const G4double fCurrentTemperature = CLHEP::STP_Temperature + 20.0*CLHEP::kelvin;  // 293 Kelvin

	      G4double fCurrentDensity     = (fAtomicMass*fCurrentPressure)/(fCurrentTemperature*fMolarConstant);


	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===========================================
	  //           Helium3
	  //===========================================

	  //--------------------------------------------------------
	  // Check if isotopes are already defined.
      // Since they are defined all together it is sufficient
	  // to test for the existance of *one* isotope like He3
      //--------------------------------------------------------

      G4Isotope* pIsotope = nullptr;
			     pIsotope = G4Isotope::GetIsotope("He3", false);


	  if (pIsotope == nullptr) // no isotopes have been defined before
	  {
		  this->DefineIsotopes_Helium();
	  }

	  //----------------------------------------------------------------
	  // Define element according to the isotope abundances/enrichments
	  //----------------------------------------------------------------
	  G4int nisotopes;

	  G4Element* pHe3_enriched_ED = nullptr;
			     pHe3_enriched_ED = new G4Element( "Helium3_enriched",
			        		                       "Helium3_enriched",
							                        nisotopes = 1 );

	  //---------------------------------------
	  // Assembling the ingredients
	  //---------------------------------------
	  // Note: Isotopes are added by abundance
	  //       and NOT by fraction of mass !!!
	  //---------------------------------------
	  G4double fRelativeAbundance;

      pHe3_enriched_ED->AddIsotope(pHe3_ID, fRelativeAbundance = 100.0*CLHEP::perCent);


	  //---------------------
      // Define new material
      //---------------------
	  G4int ncomponents;

	  pHe3_100_4000_MD = nullptr;
	  pHe3_100_4000_MD = new G4Material( sName,
	    		                         fCurrentDensity,
								         ncomponents = 1,
								         kStateGas,
							             fCurrentTemperature,
								         fCurrentPressure );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
      G4double fFractionOfMass;

	  pHe3_100_4000_MD->AddElement( pHe3_ED, fFractionOfMass = 100.0*CLHEP::perCent );


	} // end if (pMat == nullptr)

	return pHe3_100_4000_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



//=========================================================
//                      Lithium related
//=========================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Element* BTSimCommon_MaterialParameters::GetElement_Li6_Enriched()
{
	G4String sName    = "Lithium6_enriched";

	//--------------------------------------------------------------
	// Check whether element exists already in the element table
	//--------------------------------------------------------------
	G4Element* pElement = G4Element::GetElement(sName, false);

	if (pElement == nullptr)
	{
	  //===========================================
	  //          Lithium-6 enriched
	  //===========================================

	  // Check if isotopes are already defined.
	  // Since they are defined all together it is sufficient to test for the existance of one isotope

		G4Isotope* pIsotope = nullptr;
	               pIsotope = G4Isotope::GetIsotope("Li6", false);

		if (pIsotope == nullptr) // no isotopes have been defined before
		{
			this->DefineIsotopes_Lithium();
		}

		//----------------------------------------------------
		// Define element according to the isotope abundances
		//----------------------------------------------------
		G4int nisotopes;

		pLi6_Enriched_ED = nullptr;
		pLi6_Enriched_ED = new G4Element( "Li6_Enriched",
				                          "Li6_Enriched",
										   nisotopes = 2 );

	    //---------------------------------------
	    // Assembling the ingredients
	    //---------------------------------------
		// Note: Isotopes are added by abundance
		//       and NOT by fraction of mass !!!
		//---------------------------------------
		G4double fRelativeAbundance;

		pLi6_Enriched_ED->AddIsotope(pLi6_ID, fRelativeAbundance =  95.0*CLHEP::perCent);
		pLi6_Enriched_ED->AddIsotope(pLi7_ID, fRelativeAbundance =   5.0*CLHEP::perCent);

	} // end if (pElement == nullptr)

	return pLi6_Enriched_ED;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_LiF()
{
	G4String sName    = "LiF";
	G4double fDensity =  2.64*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===========================================
	  //           LiF
	  //===========================================

	  // Check if elements are already defined.
	  // Since they are defined all together it is sufficient to test for the existance of one isotope

		G4Element* pElement = nullptr;
		           pElement = G4Element::GetElement("Li6_Enriched", false);

		if (pElement == nullptr) // no element have been defined before
		{
			this->GetElement_Li6_Enriched();
		}

		//----------------------------------------------------
		// Define element according to the isotope abundances
		//----------------------------------------------------
		G4int ncomponents;

		pLiF_MD = nullptr;
        pLiF_MD = new G4Material( sName,
        		                  fDensity,
								  ncomponents = 2,
								  kStateSolid );


	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
        G4int natoms;

	    pLiF_MD->AddElement( pLi6_Enriched_ED                             , natoms = 1 );
	    pLiF_MD->AddElement( pNistMaterialManager->FindOrBuildElement("F"), natoms = 1 );

	} // end if (pMat == nullptr)

	return pLiF_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_GS20()
{
	G4String sName    = "GS20";
	G4double fDensity =  2.5*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=========================================================
	  //     Lithium glass scintillator GS20
	  //     (composition by mass fraction):
	  //
      //     56% SiO2, 4% MgO,  18% Al2O3, 18% Li2O, and 4% Ce2O3
	  //==========================================================

	  //-----------------------------------------------------------------------------------------------------------------------
	  // Values taken from:
	  // "Monte carlo simulation of a very high resolution thermal neutron detector composed of glass scintillator microfibers"
	  // Yushou Song et al. , arXiv:1606.03574v1 [physics.ins-det] 11 June 2016
	  //-----------------------------------------------------------------------------------------------------------------------


	  // Check if elements are already defined.
	  // Since they are defined all together it is sufficient to test for the existance of one isotope

		G4Material* pMatList = nullptr;
		            pMatList = G4Material::GetMaterial("LiF", false);

		if (pMatList == nullptr) // no LiF has been defined before
		{
			this->GetMaterial_LiF();
		}

		//-----------
		//   6LiO2
		//-----------
		G4Material* pLiOxide = nullptr;
                    pLiOxide = new G4Material("6LiO2",
                    		                   2.013*CLHEP::g/CLHEP::cm3,
											   2 );
        //----------------------------
        // Assembling the ingredients
        //----------------------------
        G4int natoms;

        pLiOxide->AddElement( pLi6_Enriched_ED                , natoms = 1 );
        pLiOxide->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms = 2 );

        //-----------------------------------------
        // Retrieve materials from NIST	definition
        //-----------------------------------------
        G4Material* pSiO2   = nullptr; pSiO2  = pNistMaterialManager->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
        G4Material* pMgO    = nullptr; pMgO   = pNistMaterialManager->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE");
        G4Material* pAl2O3  = nullptr; pAl2O3 = pNistMaterialManager->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");

        //-------
        // Ce2O3
        //-------
        vElements.clear();           vNatoms.clear();
        vElements.push_back("Ce");   vNatoms.push_back(2);
        vElements.push_back("O");    vNatoms.push_back(3);

        fDensity = 6.2*CLHEP::gram/CLHEP::cm3;

		//---------------------
		// Define new material
		//---------------------
        G4Material* pCe2O3 = nullptr;
                    pCe2O3 = pNistMaterialManager->ConstructNewMaterial( "Ce2O3",
                    		                                vElements,
															vNatoms,
															fDensity );

        //======
        // GS20
        //======
        fDensity = 2.5*CLHEP::gram/CLHEP::cm3;

		//---------------------
		// Define new material
		//---------------------
  	    G4int ncomponents;

        pGS20_MD = nullptr;
        pGS20_MD = new G4Material( sName,
        		                   fDensity,
								   ncomponents = 5,
								   kStateSolid );

        //----------------------------
        // Assembling the ingredients
        //----------------------------
        G4double fFractionOfMass;

        pGS20_MD->AddMaterial( pSiO2    , fFractionOfMass = 56*CLHEP::perCent);
        pGS20_MD->AddMaterial( pMgO     , fFractionOfMass =  4*CLHEP::perCent);
        pGS20_MD->AddMaterial( pAl2O3   , fFractionOfMass = 18*CLHEP::perCent);
        pGS20_MD->AddMaterial( pLiOxide , fFractionOfMass = 18*CLHEP::perCent);
        pGS20_MD->AddMaterial( pCe2O3   , fFractionOfMass =  4*CLHEP::perCent);


        //-------------------------------------------------------------------------------------------
        //               Store file bases material properties in these vectors
        //-------------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_EmissionSpectrum           = new G4MaterialPropertyVector();
		//---
		G4MaterialPropertyVector* pMPV_REALRINDEX                 = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX            = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH           = new G4MaterialPropertyVector();
		//---
		G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH           = new G4MaterialPropertyVector();
		//--
		G4MaterialPropertyVector* pMPV_ALPHASCINTILLATIONYIELD    = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_ELECTRONSCINTILLATIONYIELD = new G4MaterialPropertyVector();
		//-----------------------------------------------------------------------------------------

		//------------------------
		// Specify the Light Yield
		//------------------------
		G4double fGS20_LightYield = fAnthracene_LightYield*0.20; // Taken from arXiv:1606.03574v1 [physics.ins-det] 11 Jun 2016


		// Read emission spectrum from file
		this->Initialize_EmissionSpectrum("/GS20/GS20_EmissionSpectrum.dat",
										   pMPV_EmissionSpectrum );

		std::cout << "GS20_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;


		// Read energy response of alpha and beta radiation from file
		// => Light yield is a function of kinetic energy and depends on particle type
		this->Initialize_EnergyResponse( "/GS20/GS20_EnergyResponse.dat",
                                          fGS20_LightYield,
						                  pMPV_ALPHASCINTILLATIONYIELD,
						                  pMPV_ELECTRONSCINTILLATIONYIELD);

		std::cout << "GS20_EnergyResponse entries = " << pMPV_ELECTRONSCINTILLATIONYIELD->GetVectorLength() << std::endl;

		// Read absorption length from file
		this->Initialize_AbsorptionLength("/GS20/GS20_AbsorptionLength.dat",
				                           pMPV_ABSORPTIONLENGTH );

		std::cout << "GS20_AbsorptionLength entries = " << pMPV_ABSORPTIONLENGTH->GetVectorLength() << std::endl;


		// Read index of refraction from file
		this->Initialize_IndexOfRefraction("/GS20/GS20_IndexOfRefraction.dat",
				 	 	                    pMPV_REALRINDEX,
											pMPV_IMAGINARYRINDEX,
											pMPV_RINDEX,
											pMPV_RINDEX_ABSLENGTH);

		std::cout << "GS20_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pGS20_MPT = nullptr;
		pGS20_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pGS20_MPT->AddProperty("FASTCOMPONENT"                 , pMPV_EmissionSpectrum );
		pGS20_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
		pGS20_MPT->AddProperty("ABSLENGTH"                     , pMPV_ABSORPTIONLENGTH );

		// Non-linear light yield depending on particle energy
		pGS20_MPT->AddProperty("ALPHASCINTILLATIONYIELD"        , pMPV_ALPHASCINTILLATIONYIELD    );
		pGS20_MPT->AddProperty("ELECTRONSCINTILLATIONYIELD"     , pMPV_ELECTRONSCINTILLATIONYIELD );
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pGS20_MPT->AddConstProperty("SCINTILLATIONYIELD"       , fGS20_LightYield   ); // saint-gobain
		pGS20_MPT->AddConstProperty("RESOLUTIONSCALE"          ,   1.0              ); // Resolution scale: sigma = resolution_scale * sqrt(E * scintillation_yield)
		pGS20_MPT->AddConstProperty("FASTTIMECONSTANT"         ,  50.0*CLHEP::ns    ); // saint-gobain
	    pGS20_MPT->AddConstProperty("FASTSCINTILLATIONRISETIME",   1.0*CLHEP::ns    );
		pGS20_MPT->AddConstProperty("YIELDRATIO"               ,   1.0              ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pGS20_MD->SetMaterialPropertiesTable(pGS20_MPT);

	} // end if (pMat == nullptr)

	return pGS20_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#####################################################################################################################
//#####################################################################################################################
//                      Materials for Insulation
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Delrin()
{
	  G4String sName = "Delrin";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial( sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Delrin
	  //==============================

	  pDelrin_MD = nullptr;
	  pDelrin_MD = pNistMaterialManager->FindOrBuildMaterial("G4_POLYOXYMETHYLENE");
	  pDelrin_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pDelrin_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_FR4()
{
	  G4String sName = "FR4";
	  G4double fDensity = 1.86*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=========================================================
	  //   FR4 printed circuit material
	  //=========================================================
      G4int ncomponents;

      pFR4_MD = nullptr;
      pFR4_MD = new G4Material( sName,
				                fDensity,
								ncomponents = 2,
								kStateSolid);

      //----------------------------
      // Assembling the ingredients
      //----------------------------
      G4double fFractionOfMass;

      pFR4_MD->AddMaterial( this->GetMaterial_SiliconDioxide(), fFractionOfMass =  52.8*CLHEP::perCent);
      pFR4_MD->AddMaterial( this->GetMaterial_Epoxy()         , fFractionOfMass =  47.2*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pFR4_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Frialit()
{
	  G4String sName = "Frialit";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=========================================================
	  //   Isolator Frialit F99,7 alias reines Al2O3 von FRIATEC
	  //=========================================================

	  pFrialit_MD = nullptr;
	  pFrialit_MD = pNistMaterialManager->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
	  pFrialit_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pFrialit_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_G10()
{
	  G4String sName = "G10";
	  G4double fDensity = 1.70*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=========================================================
	  //   G10 printed circuit material
	  //=========================================================
	  G4int ncomponents;

      pG10_MD = nullptr;
      pG10_MD = new G4Material( sName,
				                fDensity,
								ncomponents = 4,
								kStateSolid);

      //----------------------------
      // Assembling the ingredients
      //----------------------------
      G4int natoms;

      pG10_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), natoms = 1 );
      pG10_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O") , natoms = 2 );
      pG10_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C") , natoms = 3 );
      pG10_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_H") , natoms = 3 );

	} // end if (pMat == nullptr)

	return pG10_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Kapton()
{
	  G4String sName = "Kapton";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Kapton
	  //==============================

	  pKapton_MD = nullptr;
	  pKapton_MD = pNistMaterialManager->FindOrBuildMaterial("G4_KAPTON");
	  pKapton_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pKapton_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Macor()
{
	G4String sName    = "Macor";
	G4double fDensity = 2.52*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//==============================
		//    Macor
		//==============================
		G4int ncomponents;

		pMacor_MD = nullptr;
		pMacor_MD = new G4Material( sName,
				                    fDensity,
									ncomponents = 5,
									kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
		G4double fFractionOfMass;

		pMacor_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_SILICON_DIOXIDE"), fFractionOfMass =  47.00*CLHEP::perCent); // Si_O2
		pMacor_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_ALUMINUM_OXIDE") , fFractionOfMass =  17.00*CLHEP::perCent); // Al2_O3
		pMacor_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_MAGNESIUM_OXIDE"), fFractionOfMass =  18.00*CLHEP::perCent); // Mg_O
		pMacor_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_POTASSIUM_OXIDE"), fFractionOfMass =  10.50*CLHEP::perCent); // K2_O
		pMacor_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_BORON_OXIDE")    , fFractionOfMass =   7.50*CLHEP::perCent); // B2_O3

	} // end if (pMat == nullptr)

	return pMacor_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Noryl()
{
	G4String sName    = "Noryl";
	G4double fDensity = 1.06*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	    //==============================
	    //    Noryl
	    //==============================
		G4int ncomponents;

		pNoryl_MD = nullptr;
		pNoryl_MD = new G4Material( sName,
				                    fDensity,
									ncomponents = 3,
									kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
		G4double fFractionOfMass;

		pNoryl_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"), fFractionOfMass =  47.06*CLHEP::perCent);
		pNoryl_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_H"), fFractionOfMass =  47.06*CLHEP::perCent);
		pNoryl_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O"), fFractionOfMass =   5.88*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pNoryl_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_PEEK()
{
	G4String sName    = "PEEK";
	G4double fDensity = 1.31*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//==============================
		//    PEEK C19_H12_O3
		//==============================
		G4int ncomponents;

		pPEEK_MD = nullptr;
		pPEEK_MD = new G4Material( sName,
				                   fDensity,
								   ncomponents = 3,
								   kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
		G4int natoms = 0;

		pPEEK_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"), natoms = 19 );
		pPEEK_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_H"), natoms = 12 );
		pPEEK_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O"), natoms =  3 );


	} // end if (pMat == nullptr)

	return pPEEK_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_PVC()
{
	  G4String sName = "PVC";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            PVC
	  //==============================

	  pPVC_MD = nullptr;
	  pPVC_MD = pNistMaterialManager->FindOrBuildMaterial("G4_POLYVINYL_CHLORIDE");
	  pPVC_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pPVC_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Teflon()
{
	  G4String sName = "Teflon";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Teflon
	  //==============================

	  pTeflon_MD = nullptr;
	  pTeflon_MD = pNistMaterialManager->FindOrBuildMaterial("G4_TEFLON");
	  pTeflon_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pTeflon_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#####################################################################################################################
//#####################################################################################################################
//                      Materials for Foils and/or Wrappings
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Havar()
{
	  G4String sName    = "Havar";
	  G4double fDensity = 8.30*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //         Havar
	  //==============================
	  // Definition taken from: http://geant4.web.cern.ch/geant4/UserDocumentation/Doxygen/examples_doc/html_TestEm5/html/DetectorConstruction_8cc_source.html

	  G4int ncomponents;

	  pHavar_MD = nullptr;
	  pHavar_MD = new G4Material( sName,
			                      fDensity,
								  ncomponents = 5,
								  kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pHavar_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cr"), fFractionOfMass = 17.85*CLHEP::perCent);
	  pHavar_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Fe"), fFractionOfMass = 18.22*CLHEP::perCent);
	  pHavar_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Co"), fFractionOfMass = 44.52*CLHEP::perCent);
	  pHavar_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ni"), fFractionOfMass = 13.10*CLHEP::perCent);
	  pHavar_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_W") , fFractionOfMass =  6.31*CLHEP::perCent);

	} // end if (pMat == nullptr)

	return pHavar_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Hostaphan()
{
	  G4String sName    = "Hostaphan";
	  G4double fDensity = 1.38*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===========================================================================
	  //   Hostaphan: (C_{10}H_{8}O_{4})_{n} alias Polyethylene Terephthalate (PET)
	  //===========================================================================
      G4int ncomponents;

	  pHostaphan_MD = nullptr;
	  pHostaphan_MD = new G4Material( sName,
			                          fDensity,
									  ncomponents = 3,
									  kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pHostaphan_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"),  natoms = 10 );
	  pHostaphan_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"),  natoms =  8 );
	  pHostaphan_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"),  natoms =  4 );

	} // end if (pMat == nullptr)

	return pHostaphan_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_MakrofolKG()
{
	  G4String sName = "Makrofol_KG";
	  G4double fDensity = 1.20*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //       Makrofol KG from Bayer
	  //==============================

		pMakrofolKG_MD = nullptr;
		pMakrofolKG_MD = pNistMaterialManager->FindOrBuildMaterial("G4_POLYCARBONATE");
		pMakrofolKG_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pMakrofolKG_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Mylar()
{
	G4String sName = "Mylar";

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	 //==============================
	 //           Mylar
	 //==============================

	 pMylar_MD = nullptr;
	 pMylar_MD = pNistMaterialManager->FindOrBuildMaterial("G4_MYLAR");
	 pMylar_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pMylar_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Tyvek()
{
	G4String sName    = "Tyvek";
	G4double fDensity = 0.935*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	    //==============================
	    //    Tyvek ( -CH2-CH2-)
	    //==============================
		G4int ncomponents;

		pTyvek_MD = nullptr;
		pTyvek_MD = new G4Material( sName,
				                    fDensity,
									ncomponents = 2,
									kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
		G4int natoms;

		pTyvek_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_C"), natoms = 1 );
		pTyvek_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_H"), natoms = 2 );

	} // end if (pMat == nullptr)

	return pTyvek_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//#####################################################################################################################
//#####################################################################################################################
//                      Materials for Glueing/Bonding/Coupling
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_CAS_101_77_9()
{
	G4String sName    = "CAS_101_77_9";
	G4double fDensity = 1.15*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //====================================================================
	  //    4,4'-diaminodiphenylmethane (C13_H14_N2)
	  //    CAS number: 101-77-9
	  //--------------------------------------------------------------------
      // http://www.molbase.com/en/search.html?search_keyword=101-77-9
	  //====================================================================
      G4int ncomponents;

	  pCAS_101_77_9_MD = nullptr;
	  pCAS_101_77_9_MD = new G4Material( sName,
			                             fDensity,
								         ncomponents = 3,
								         kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pCAS_101_77_9_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 13 );
	  pCAS_101_77_9_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 14 );
	  pCAS_101_77_9_MD->AddElement( pNistMaterialManager->FindOrBuildElement("N"), natoms =  2 );

	} // end if (pMat == nullptr)

	return pCAS_101_77_9_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_CAS_9003_36_5()
{
	G4String sName    = "CAS_9003_36_5";
	G4double fDensity = 1.18*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //====================================================================
	  //    FORMALDEHYDE (C10_H13_Cl_O3)x
	  //    CAS number: 9003-36-5
	  //--------------------------------------------------------------------
      // http://www.molbase.com/en/search.html?search_keyword=9003-36-5
	  //====================================================================
      G4int ncomponents;

	  pCAS_9003_36_5_MD = nullptr;
	  pCAS_9003_36_5_MD = new G4Material( sName,
			                              fDensity,
								          ncomponents = 4,
								          kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pCAS_9003_36_5_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C") , natoms = 10 );
	  pCAS_9003_36_5_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H") , natoms = 13 );
	  pCAS_9003_36_5_MD->AddElement( pNistMaterialManager->FindOrBuildElement("Cl"), natoms =  1 );
	  pCAS_9003_36_5_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O") , natoms =  3 );

	} // end if (pMat == nullptr)

	return pCAS_9003_36_5_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_CAS_10563_29_8()
{
	G4String sName    = "CAS_10563_29_8";
	G4double fDensity = 0.94*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //====================================================================
	  //    N,N-Dimethyldipropylenetriamine (C8_H21_N3)
	  //    CAS number: 10563-29-8
	  //--------------------------------------------------------------------
      // http://www.molbase.com/en/cas-10563-29-8.html
	  //====================================================================
      G4int ncomponents;

	  pCAS_10563_29_8_MD = nullptr;
	  pCAS_10563_29_8_MD = new G4Material( sName,
			                               fDensity,
								           ncomponents = 3,
								           kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pCAS_10563_29_8_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C") , natoms =  8 );
	  pCAS_10563_29_8_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H") , natoms = 21 );
	  pCAS_10563_29_8_MD->AddElement( pNistMaterialManager->FindOrBuildElement("N") , natoms =  3 );

	} // end if (pMat == nullptr)

	return pCAS_10563_29_8_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_CAS_25068_38_6()
{
	G4String sName    = "CAS_25068_38_6";
	G4double fDensity = 1.18*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //====================================================================
	  //    BISPHENOL A DIGLYCIDYL ETHER RESIN (C54_H60_O9)
	  //    CAS number: 25068-38-6
	  //--------------------------------------------------------------------
      // http://www.molbase.com/en/search.html?search_keyword=25068-38-6
	  //====================================================================
      G4int ncomponents;

	  pCAS_25068_38_6_MD = nullptr;
	  pCAS_25068_38_6_MD = new G4Material( sName,
			                               fDensity,
								           ncomponents = 3,
								           kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pCAS_25068_38_6_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 54 );
	  pCAS_25068_38_6_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 60 );
	  pCAS_25068_38_6_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms =  9 );

	} // end if (pMat == nullptr)

	return pCAS_25068_38_6_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Araldite_AW106()
{
	G4String sName    = "Araldite_AW106";
	G4double fDensity = 1.19*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

	  //====================================================================
	  //   Epoxy Resin Araldite AW106
	  //--------------------------------------------------------------------
      // Huntsman safety data sheet for AW 106
	  //====================================================================
	  G4int ncomponents;

	  pAraldite_AW106_MD = nullptr;
	  pAraldite_AW106_MD = new G4Material( sName,
			                               fDensity,
								           ncomponents = 2,
								           kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pAraldite_AW106_MD->AddMaterial( this->GetMaterial_CAS_25068_38_6(), fFractionOfMass = 80.00*CLHEP::perCent );
	  pAraldite_AW106_MD->AddMaterial( this->GetMaterial_CAS_9003_36_5() , fFractionOfMass = 20.00*CLHEP::perCent );

	} // end if (pMat == nullptr)

	return pAraldite_AW106_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Araldite_F()
{
	G4String sName    = "Araldite_F";
	G4double fDensity = 1.18*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

	  //====================================================================
	  //   Epoxy Resin Araldite F
	  //--------------------------------------------------------------------
      // Huntsman safety data sheet
	  //====================================================================
	  G4int ncomponents;

	  pAraldite_F_MD = nullptr;
	  pAraldite_F_MD = new G4Material( sName,
			                           fDensity,
								       ncomponents = 1,
								       kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pAraldite_F_MD->AddMaterial( this->GetMaterial_CAS_25068_38_6(), fFractionOfMass = 100.00*CLHEP::perCent );

	} // end if (pMat == nullptr)

	return pAraldite_F_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Hardener_HT972()
{
	G4String sName    = "Hardener_HT972";
	G4double fDensity = 1.15*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

	  //====================================================================
	  //  Epoxy Hardener HT 972
	  //--------------------------------------------------------------------
      //  Ciba safety data sheet for Hardener HT 972
	  //====================================================================
	  G4int ncomponents;

	  pHardener_HT972_MD = nullptr;
	  pHardener_HT972_MD = new G4Material( sName,
			                               fDensity,
								           ncomponents = 1,
								           kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pHardener_HT972_MD->AddMaterial( this->GetMaterial_CAS_101_77_9(), fFractionOfMass = 100.00*CLHEP::perCent );

	} // end if (pMat == nullptr)

	return pHardener_HT972_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Hardener_HV953U()
{
	G4String sName    = "Hardener_HV953U";
	G4double fDensity = 0.94*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

	  //====================================================================
	  //   Epoxy Hardener HV 953U
	  //--------------------------------------------------------------------
      // Huntsman safety data sheet for Hardener HV 953U
	  //====================================================================
	  G4int ncomponents;

	  pHardener_HV953U_MD = nullptr;
	  pHardener_HV953U_MD = new G4Material( sName,
			                                fDensity,
								            ncomponents = 1,
								            kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pHardener_HV953U_MD->AddMaterial( this->GetMaterial_CAS_10563_29_8(), fFractionOfMass = 100.00*CLHEP::perCent );

	} // end if (pMat == nullptr)

	return pHardener_HV953U_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Epoxy_AW106_HV953U()
{
	G4String sName    = "Epoxy_AW106_HV953U";
	G4double fDensity = 1.20*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

	  //====================================================================
	  //   Epoxy Mixture: Resin Araldite AW106 + Hardener HV 953U
	  //--------------------------------------------------------------------
      // Huntsman safety data sheet for AW 106 +  HV 953U
	  //====================================================================
	  G4int ncomponents;

	  pEpoxy_AW106_HV953U_MD = nullptr;
	  pEpoxy_AW106_HV953U_MD = new G4Material( sName,
			                                   fDensity,
								               ncomponents = 2,
								               kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pEpoxy_AW106_HV953U_MD->AddMaterial( this->GetMaterial_Araldite_AW106()  , fFractionOfMass = (100.0/180.0)*CLHEP::perCent );
	  pEpoxy_AW106_HV953U_MD->AddMaterial( this->GetMaterial_Hardener_HV953U() , fFractionOfMass = ( 80.0/180.0)*CLHEP::perCent );

	} // end if (pMat == nullptr)

	return pEpoxy_AW106_HV953U_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Epoxy_F_HT972()
{
	G4String sName    = "Epoxy_F_HT972";
	G4double fDensity = 1.20*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

	  //====================================================================
	  //  Epoxy Mixture: Resin Araldite F + Hardener HT 972
	  //--------------------------------------------------------------------
      //  Huntsman safety data sheet
	  //====================================================================
	  G4int ncomponents;

	  pEpoxy_F_HT972_MD = nullptr;
	  pEpoxy_F_HT972_MD = new G4Material( sName,
			                              fDensity,
								          ncomponents = 2,
								          kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pEpoxy_F_HT972_MD->AddMaterial( this->GetMaterial_Araldite_F()     , fFractionOfMass = (100.0/127.0)*CLHEP::perCent );
	  pEpoxy_F_HT972_MD->AddMaterial( this->GetMaterial_Hardener_HT972() , fFractionOfMass = ( 27.0/127.0)*CLHEP::perCent );

	} // end if (pMat == nullptr)

	return pEpoxy_AW106_HV953U_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_C19H20O4()
{
	G4String sName    = "C19H20O4";
	G4double fDensity = 1.16*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //====================================================================
	  //   Diglycidyl Ether of Bisphenol A (C19_H20_O4)
	  //--------------------------------------------------------------------
      // DEVELOPMENT OF A THGEM IMAGING DETECTOR WITH DELAY LINE READOUT
      // BY ANDREI R. HANU, B.Sc.
	  // Master thesis, McMaster University, 2013
	  //====================================================================
      G4int ncomponents;

	  pC19H20O4_MD = nullptr;
	  pC19H20O4_MD = new G4Material( sName,
			                         fDensity,
								     ncomponents = 3,
								     kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pC19H20O4_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 19 );
	  pC19H20O4_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 20 );
	  pC19H20O4_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms =  4 );

	} // end if (pMat == nullptr)

	return pC19H20O4_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_C10H18O4()
{
	G4String sName    = "C10H18O4";
	G4double fDensity = 1.10*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //====================================================================
	  //   1,4-Butanediol Diglycidyl (C10_H18_O4)
	  //--------------------------------------------------------------------
      // DEVELOPMENT OF A THGEM IMAGING DETECTOR WITH DELAY LINE READOUT
      // BY ANDREI R. HANU, B.Sc.
	  // Master thesis, McMaster University, 2013
	  //====================================================================
	  G4int ncomponents;

	  pC10H18O4_MD = nullptr;
	  pC10H18O4_MD = new G4Material( sName,
			                         fDensity,
									 ncomponents = 3,
								     kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pC10H18O4_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 10 );
	  pC10H18O4_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 18 );
	  pC10H18O4_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms =  4 );

	} // end if (pMat == nullptr)

	return pC10H18O4_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_C9H22N2()
{
	G4String sName    = "C9H22N2";
	G4double fDensity =  0.865*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //====================================================================
	  //   1,4-Hexanediamine 2,2,4-trimethyl (C9_H22_N2)
	  //--------------------------------------------------------------------
      // DEVELOPMENT OF A THGEM IMAGING DETECTOR WITH DELAY LINE READOUT
      // BY ANDREI R. HANU, B.Sc.
	  // Master thesis, McMaster University, 2013
	  //====================================================================
	  G4int ncomponents;

	  pC9H22N2_MD = nullptr;
	  pC9H22N2_MD = new G4Material( sName,
			                         fDensity,
								     ncomponents = 3,
								     kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pC9H22N2_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms =  9 );
	  pC9H22N2_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 22 );
	  pC9H22N2_MD->AddElement( pNistMaterialManager->FindOrBuildElement("N"), natoms =  2 );

	} // end if (pMat == nullptr)

	return pC9H22N2_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Epotek_301_1()
{
	G4String sName    = "Epotek_301_1";
	G4double fDensity = 1.19*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

	  //====================================================================
	  //   Epoxy Resin Epotek 301-1
	  //--------------------------------------------------------------------
      // DEVELOPMENT OF A THGEM IMAGING DETECTOR WITH DELAY LINE READOUT
      // BY ANDREI R. HANU, B.Sc.
	  // Master thesis, McMaster University, 2013
	  //====================================================================
	  G4int ncomponents;

	  pEpotek_301_1_MD = nullptr;
	  pEpotek_301_1_MD = new G4Material( sName,
			                             fDensity,
								         ncomponents = 3,
								         kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pEpotek_301_1_MD->AddMaterial( this->GetMaterial_C19H20O4(), fFractionOfMass = 56.00*CLHEP::perCent );
	  pEpotek_301_1_MD->AddMaterial( this->GetMaterial_C10H18O4(), fFractionOfMass = 24.00*CLHEP::perCent );
	  pEpotek_301_1_MD->AddMaterial( this->GetMaterial_C9H22N2() , fFractionOfMass = 20.85*CLHEP::perCent );


	} // end if (pMat == nullptr)

	return pEpotek_301_1_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Epoxy()
{
	G4String sName    = "Epoxy";
	G4double fDensity = 1.16*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //====================================================================
	  //       Epoxy
	  //--------------------------------------------------------------------
      // https://gemc.jlab.org/work/doxy/2.2/cpp__materials_8cc_source.html
	  //====================================================================
	  G4int ncomponents;

	  pEpoxy_MD = nullptr;
	  pEpoxy_MD = new G4Material( sName,
			                      fDensity,
								  ncomponents = 4,
								  kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pEpoxy_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 15 );
	  pEpoxy_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 32 );
	  pEpoxy_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms =  4 );
	  pEpoxy_MD->AddElement( pNistMaterialManager->FindOrBuildElement("N"), natoms =  2 );

	} // end if (pMat == nullptr)

	return pEpoxy_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_EpoxyResin()
{
	G4String sName    = "EpoxyResin";
	G4double fDensity = 1.268*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //       Epoxy-Resin: C11H12O3
	  //==============================
      G4int ncomponents;

	  pEpoxyResin_MD = nullptr;
	  pEpoxyResin_MD = new G4Material( sName,
			                           fDensity,
									   ncomponents = 3,
									   kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4int natoms;

	  pEpoxyResin_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 11);
	  pEpoxyResin_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 12);
	  pEpoxyResin_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms =  3);

	} // end if (pMat == nullptr)

	return pEpoxyResin_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_MagicOpticalBond()
{
	  G4String sName    = "MagicOpticalBond";
	  G4double fDensity = 2.462*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {

		//---------------------------------------------------------------------------------------
		// non-existing MagicOpticalBond : contains polystyrene
		//---------------------------------------------------------------------------------------
		G4int ncomponents;

		pMagicOpticalBond_MD = nullptr;
		pMagicOpticalBond_MD = new G4Material( sName,
				                               fDensity,
											   ncomponents = 1,
											   kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4double fFractionOfMass;

		pMagicOpticalBond_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_POLYSTYRENE"), fFractionOfMass = 100*CLHEP::perCent );

	    //-----------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //-----------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH  = nullptr;
		                          pMPV_ABSORPTIONLENGTH  = new G4MaterialPropertyVector();

		// Read absorption length from file
		this->Initialize_AbsorptionLength("/OpticalGlue/MagicOpticalBond/MagicOpticalBond_AbsorptionLength.dat",
				                           pMPV_ABSORPTIONLENGTH );

		std::cout << "MagicOpticalBond_AbsorptionLength entries = " << pMPV_ABSORPTIONLENGTH->GetVectorLength() << std::endl;


		//----------------------------------------------------------------------------------------------------------------
		// The index of refraction is defined via the Setter function this->GetIndexOfRefraction_MagicOpticalBond(value)
		// This value is assigned for the entire optical range (crude approximation: wrong, but OK here (narrow wavelength range))
		//----------------------------------------------------------------------------------------------------------------

		const G4int                                    nentries_IndexOfRefraction  = 2;
		G4double fMagicOpticalBond_PhotonEnergy       [nentries_IndexOfRefraction] = { 1.2*CLHEP::eV                                 , 6.5*CLHEP::eV                                 }; //optical range
		G4double fMagicOpticalBond_IndexOfRefraction  [nentries_IndexOfRefraction] = { this->GetIndexOfRefraction_MagicOpticalBond() , this->GetIndexOfRefraction_MagicOpticalBond() };


		//---------------------------------
		// Define material properties table
		//---------------------------------
		pMagicOpticalBond_MPT = nullptr;
		pMagicOpticalBond_MPT = new G4MaterialPropertiesTable();

		pMagicOpticalBond_MPT->AddProperty("RINDEX"    , fMagicOpticalBond_PhotonEnergy, fMagicOpticalBond_IndexOfRefraction, nentries_IndexOfRefraction )->SetSpline(true);
		pMagicOpticalBond_MPT->AddProperty("ABSLENGTH" , pMPV_ABSORPTIONLENGTH );

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pMagicOpticalBond_MD->SetMaterialPropertiesTable(pMagicOpticalBond_MPT);

	}
	else  // // end if (pMat == nullptr)
	{
		//--------------------------------------------------------------------------------------------------------------------------------
		// When you are *here* then MagicOpticalBond has been previously constructed. In this case we only update the index of refraction
		//--------------------------------------------------------------------------------------------------------------------------------

		pMagicOpticalBond_MD->GetMaterialPropertiesTable()->RemoveProperty("RINDEX");

		//-------------------------------------------------------------------------------------------------------------------------
		// The index of refraction is defined via the Setter function this->GetIndexOfRefraction_MagicOpticalBond(value)
		// This value is assigned for the entire optical range (crude approximation: wrong, but OK here (narrow wavelength range))
		//-------------------------------------------------------------------------------------------------------------------------

		const G4int                                    nentries_IndexOfRefraction  = 2;
		G4double fMagicOpticalBond_PhotonEnergy       [nentries_IndexOfRefraction] = { 1.2*CLHEP::eV                                 ,  6.5*CLHEP::eV                                }; //optical range
		G4double fMagicOpticalBond_IndexOfRefraction  [nentries_IndexOfRefraction] = { this->GetIndexOfRefraction_MagicOpticalBond() , this->GetIndexOfRefraction_MagicOpticalBond() };

		//----------------
		// apply settings
		//-----------------
		pMagicOpticalBond_MD->GetMaterialPropertiesTable()->AddProperty( "RINDEX",
				                                                          fMagicOpticalBond_PhotonEnergy,
																		  fMagicOpticalBond_IndexOfRefraction,
																		  nentries_IndexOfRefraction )->SetSpline(true);

	} // end if (pMat == nullptr)

	return pMagicOpticalBond_MD;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_MeltMount_1582()
{
	  G4String sName    = "MeltMount_1582";
	  G4double fDensity = 2.462*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		//---------------------------------------------------------------------------------------
		// MeltMount : contains polystyrene
		//---------------------------------------------------------------------------------------

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pMeltMount_1582_MD = nullptr;
		pMeltMount_1582_MD = new G4Material( sName,
				                             fDensity,
											 ncomponents = 1,
											 kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4double fFractionOfMass;

		pMeltMount_1582_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_POLYSTYRENE"), fFractionOfMass = 100*CLHEP::perCent );


	    //------------------------------------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //------------------------------------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REALRINDEX                           = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX                      = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                               = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength = new G4MaterialPropertyVector();
		//------------------------------------------------------------------------------------------------------------------

        std::cout << "MeltMount_1582 : reading IndexOfRefraction file " << std::endl;

        //------------------------------------------------------------------------------------------------------------------
		// Read index of refraction from file
        //------------------------------------------------------------------------------------------------------------------
		this->Initialize_IndexOfRefraction("/MeltMount/MeltMount_1582_IndexOfRefraction.dat",
				                            pMPV_REALRINDEX,
				                            pMPV_IMAGINARYRINDEX,
				                            pMPV_RINDEX,
				                            pMPV_RINDEX_ABSLENGTH);

		std::cout << "MeltMount_1582_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;


		//------------------------------------------------------------------------------------------------------------------
		// Read in optical Transmission file of MeltMount_1582 and extract absorption length
		//------------------------------------------------------------------------------------------------------------------
		// -> Here the reflection losses are *not* taken into account !!
		//------------------------------------------------------------------------------------------------------------------
        // Use Cauchy equation stated on data sheet
		//------------------------------------------------------------------------------------------------------------------

		  G4double fMeltMount_1582_Cauchy_A  =   1.558144;
	      G4double fMeltMount_1582_Cauchy_B1 =   750182.8;
	      G4double fMeltMount_1582_Cauchy_B2 =   2.71695e12;

	      G4double fMeltMount_1582_CauchyCoefficients[3] = { fMeltMount_1582_Cauchy_A,
	    		                                             fMeltMount_1582_Cauchy_B1,
															 fMeltMount_1582_Cauchy_B2 };



		// Set the flag for calculating the internal transmission based on total transmission and index of refraction (via Sellmeier Coefficients)
		//
		G4bool bCalcutaleInteralTransmission = true; // tabulated values are already corrected for surface reflection losses

//		G4bool bCalcutaleInteralTransmission = false; // tabulated values are already corrected for surface reflection losses


		G4double fMeltMount_1582_Thickness    =  3.0*CLHEP::millimeter;       // Transmission has been measured for *this* thickness

		this->Initialize_AbsorptionLengthThroughTransmission_Cauchy( "/MeltMount/MeltMount_1582_ExternalTransmission.dat",
		    		                                                  bCalcutaleInteralTransmission,         // true
																	  fMeltMount_1582_CauchyCoefficients,
																	  fMeltMount_1582_Thickness,
																	  pMPV_OpticalTransmission_AbsorptionLength );

		std::cout << "MeltMount_1582_AbsorptionLength entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;

		//---------------------------------
		// Define material properties table
		//---------------------------------
		pMeltMount_1582_MPT = nullptr;
		pMeltMount_1582_MPT = new G4MaterialPropertiesTable();

		pMeltMount_1582_MPT->AddProperty("RINDEX"     , pMPV_RINDEX );
		pMeltMount_1582_MPT->AddProperty("ABSLENGTH"  , pMPV_OpticalTransmission_AbsorptionLength );


		//----------------------------------------------
		// Attach material properties table to material
		//-----------------------------------------------
		pMeltMount_1582_MD->SetMaterialPropertiesTable(pMeltMount_1582_MPT);

	} // end  if (pMat == nullptr)

	return pMeltMount_1582_MD;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_MeltMount_1704()
{
	  G4String sName    = "MeltMount_1704";
	  G4double fDensity = 2.462*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {

		//---------------------------------------------------------------------------------------
		// MeltMount : contains polystyrene
		//---------------------------------------------------------------------------------------

		//---------------------
		// Define new material
	    //---------------------
		G4int ncomponents;

		pMeltMount_1704_MD = nullptr;
		pMeltMount_1704_MD = new G4Material( sName,
				                             fDensity,
											 ncomponents = 1,
											 kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4double fFractionOfMass;

		pMeltMount_1704_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_POLYSTYRENE"), fFractionOfMass = 100*CLHEP::perCent );

	    //---------------------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //---------------------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REALRINDEX                           = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX                      = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                               = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength = new G4MaterialPropertyVector();
		//---------------------------------------------------------------------------------------------------

		//------------------------------------------------------------------------------------------------------------------
		// Read index of refraction from file
		//------------------------------------------------------------------------------------------------------------------
		this->Initialize_IndexOfRefraction("/MeltMount/MeltMount_1704_IndexOfRefraction.dat",
				                            pMPV_REALRINDEX,
				                            pMPV_IMAGINARYRINDEX,
				                            pMPV_RINDEX,
				                            pMPV_RINDEX_ABSLENGTH);

		std::cout << "MeltMount_1704_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;


		//--------------------------------------------------------------------------------------------------------------------
		// Read in optical Transmission file of MeltMount_1704 and extract absorption length
		//--------------------------------------------------------------------------------------------------------------------
		// -> Here the reflection losses are *not* taken into account !!
		//---------------------------------------------------------------------------------------------------------------------

		// Use Cauchy equation stated on data sheet

		G4double fMeltMount_1704_Cauchy_A  =   1.663869;
		G4double fMeltMount_1704_Cauchy_B1 =   1284123;
		G4double fMeltMount_1704_Cauchy_B2 =   3.804718e12;


		G4double fMeltMount_1704_CauchyCoefficients[3] = { fMeltMount_1704_Cauchy_A,
		    		                                       fMeltMount_1704_Cauchy_B1,
														   fMeltMount_1704_Cauchy_B2 };


		// Set the flag for calculating the internal transmission based on total transmission and index of refraction (via Sellmeier Coefficients)
		//
		G4bool bCalcutaleInteralTransmission = true; // tabulated values are already corrected for surface reflection losses

		G4double fMeltMount_1704_Thickness    =  3.0*CLHEP::millimeter;       // Transmission has been measured for *this* thickness

		this->Initialize_AbsorptionLengthThroughTransmission_Cauchy( "/MeltMount/MeltMount_1704_ExternalTransmission.dat",
		    		                                                  bCalcutaleInteralTransmission,         // true
																	  fMeltMount_1704_CauchyCoefficients,
																	  fMeltMount_1704_Thickness,
																	  pMPV_OpticalTransmission_AbsorptionLength );

		std::cout << "MeltMount_1704_AbsorptionLength entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pMeltMount_1704_MPT = nullptr;
		pMeltMount_1704_MPT = new G4MaterialPropertiesTable();

		pMeltMount_1704_MPT->AddProperty("RINDEX"     , pMPV_RINDEX );
		pMeltMount_1704_MPT->AddProperty("ABSLENGTH"  , pMPV_OpticalTransmission_AbsorptionLength );

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pMeltMount_1704_MD->SetMaterialPropertiesTable(pMeltMount_1704_MPT);

	} // end  if (mat == nullptr)

	return pMeltMount_1704_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_OpticalCement()
{
	  G4String sName    = "OpticalCement";
	  G4double fDensity = 1.05*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	 //=====================================
	 //          Optical Cement
	 //=====================================

	 //================================================================
	 //              EPOTEK 301-2 optical glue
	 //================================================================
	 //
     // - Used for DIRC, refraction index see DIRC notes #140
     // - Properties see http://personalpages.to.infn.it/~tosello/EngMeet/ITSmat/SDD/Epotek-301-1.html
	 //
     //=================================================================

     //---------------------
	 // Define new material
     //---------------------
     G4int ncomponents;

	 pOpticalCement_MD = nullptr;
	 pOpticalCement_MD = new G4Material( sName,
			                             fDensity,
										 ncomponents = 1,
										 kStateSolid );

	 //----------------------------
	 // Assembling the ingredients
	 //----------------------------
	 G4double fFractionOfMass;

	 pOpticalCement_MD->AddMaterial(pNistMaterialManager->FindOrBuildMaterial("G4_POLYSTYRENE"), fFractionOfMass = 100*CLHEP::perCent);

	 //------------------------------------
	 // Defining optical range of interest
	 //------------------------------------
	 Double_t fOpticalCement_LambdaMin       = 180*CLHEP::nanometer;
	 Double_t fOpticalCement_LambdaMax       = 800*CLHEP::nanometer;
	 Double_t fOpticalCement_PhotonEnergyMin = this->LambdaToEnergyConversion(fOpticalCement_LambdaMax);
	 Double_t fOpticalCement_PhotonEnergyMax = this->LambdaToEnergyConversion(fOpticalCement_LambdaMin);

	 // optical properties of optical cement
	 const G4int nentries_OpticalCement = 2;

	 G4double fOpticalCement_PhotonEnergy [nentries_OpticalCement] = { fOpticalCement_PhotonEnergyMin , fOpticalCement_PhotonEnergyMax };
	 G4double fOpticalCement_RINDEX       [nentries_OpticalCement] = { 1.59          , 1.59         };
	 G4double fOpticalCement_ABSLENGTH    [nentries_OpticalCement] = { 7.0*CLHEP::m  , 7.0*CLHEP::m };

	 //----------------------------------
	 // Define material properties table
	 //----------------------------------
	 pOpticalCement_MPT = nullptr;
	 pOpticalCement_MPT = new G4MaterialPropertiesTable();

	 pOpticalCement_MPT->AddProperty("RINDEX"   , fOpticalCement_PhotonEnergy, fOpticalCement_RINDEX   , nentries_OpticalCement)->SetSpline(true);
	 pOpticalCement_MPT->AddProperty("ABSLENGTH", fOpticalCement_PhotonEnergy, fOpticalCement_ABSLENGTH, nentries_OpticalCement)->SetSpline(true);

	 //----------------------------------------------
	 // Attach material properties table to material
	 //----------------------------------------------
	 pOpticalCement_MD->SetMaterialPropertiesTable(pOpticalCement_MPT);

	} // end if (pMat == nullptr)

	return pOpticalCement_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_OpticalGrease()
{
	  G4String sName    = "OpticalGrease";
	  G4double fDensity = 0.97*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

	     //================================================================
	     //                 Polydimethylsiloxane (Grease)
	     //================================================================
	     const G4int numentriespolydimethylsiloxane = 3;

	     //--------------------------------------
	     // Optical properties of optical grease
	     //--------------------------------------
	     G4double fPolydimethylsiloxane_Energy[numentriespolydimethylsiloxane] = {  1.2*CLHEP::eV ,  3.1*CLHEP::eV ,  6.5*CLHEP::eV };
	     G4double fPolydimethylsiloxane_Absorp[numentriespolydimethylsiloxane] = { 10.0*CLHEP::cm , 10.0*CLHEP::cm , 10.0*CLHEP::cm };
	     G4double fPolydimethylsiloxane_Rindex[numentriespolydimethylsiloxane] = {  1.4           ,  1.4           ,  1.4           };

	     //--------------------
	     // Define Composition
	     //--------------------
	     vElements.clear();            vNatoms.clear();
	     vElements.push_back("Si");    vNatoms.push_back(1);
	     vElements.push_back("O");     vNatoms.push_back(1);
	     vElements.push_back("H");     vNatoms.push_back(2);
	     vElements.push_back("H");     vNatoms.push_back(6);


	     //---------------------
	     // Define new material
		 //---------------------
	     pNistMaterialManager->ConstructNewMaterial("Polydimethylsiloxane",
	    		                        vElements,
										vNatoms,
										fDensity,
										true );
	     //---------------
	     // Clear vectors
	     //---------------
	     vElements.clear();
	     vNatoms.clear();

	     //---------------------
         // Define new material
	     //---------------------
		 G4int ncomponents;

	     pOpticalGrease_MD = nullptr;
	     pOpticalGrease_MD = new G4Material( sName,
	    		                             fDensity,
											 ncomponents = 1,
											 kStateLiquid );

	     //----------------------------
	     // Assembling the ingredients
	     //----------------------------
	     G4double fFractionOfMass;

	     pOpticalGrease_MD->AddMaterial(pNistMaterialManager->FindOrBuildMaterial("Polydimethylsiloxane"), fFractionOfMass = 100*CLHEP::perCent);

	     //------------------------------------------
	     // Define property table for optical grease
	     //------------------------------------------
	     pOpticalGrease_MPT = nullptr;
	     pOpticalGrease_MPT = new G4MaterialPropertiesTable();

	     pOpticalGrease_MPT->AddProperty("ABSLENGTH", fPolydimethylsiloxane_Energy, fPolydimethylsiloxane_Absorp, numentriespolydimethylsiloxane)->SetSpline(true);
	     pOpticalGrease_MPT->AddProperty("RINDEX"   , fPolydimethylsiloxane_Energy, fPolydimethylsiloxane_Rindex, numentriespolydimethylsiloxane)->SetSpline(true);

	     //----------------------------------------------
	     // Attach material properties table to material
	     //----------------------------------------------
	     pOpticalGrease_MD->SetMaterialPropertiesTable(pOpticalGrease_MPT);

	} // end if (pMat == nullptr)

	return pOpticalGrease_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//#####################################################################################################################
//#####################################################################################################################
//                      Materials for Light Transportation
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Acrylic()
{
	G4String sName    = "Acrylic";
	G4double fDensity = 1.05*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//================================================================
		//                     Acrylic alias Plexiglas
		//================================================================
		//
		// Wikipedia: Acrylonitrile  C3H3N   0.81 g/cm3
		//
		//----------------------------------------------------------------

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pAcrylic_MD = nullptr;
		pAcrylic_MD = new G4Material( sName,
				                      fDensity,
									  ncomponents = 3,
									  kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4int natoms;

		pAcrylic_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 3 );
		pAcrylic_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 3 );
		pAcrylic_MD->AddElement( pNistMaterialManager->FindOrBuildElement("N"), natoms = 1 );

	} // end if (pMat == nullptr)

	return pAcrylic_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_FGL400()
{
	G4String sName    = "FGL400";
	G4double fDensity = 2.23*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===============================================================
	  //    Assuming Schott Glass N-KF9 used for colored filter FGL400
	  //===============================================================

	  //------------------------------------------------------------------------------------------------------------------
	  // Defining optical range of interest
	  //------------------------------------------------------------------------------------------------------------------
  	  G4double fFGL400_LambdaMin       = 180*CLHEP::nanometer;
  	  G4double fFGL400_LambdaMax       = 800*CLHEP::nanometer;
  	  G4double fFGL400_PhotonEnergyMin = this->LambdaToEnergyConversion(fFGL400_LambdaMax);
  	  G4double fFGL400_PhotonEnergyMax = this->LambdaToEnergyConversion(fFGL400_LambdaMin);

	  // Number of bins for material properties table
	  const G4int NEntries_FGL400 = 100;

      // Index of Refraction: use Sellmeier dispersion formula
      // Assuming Schott Glass N-KF9 because it matches the indices of refraction listed in data sheet of FGL400

      G4double fFGL400_Thickness    =   2.0*CLHEP::millimeter;       // Transmission has been measured for *this* thickness of colored glass filter

//      // N-KF9 dispersion constants: GEANT4 crashes !!!
//      G4double fFGL400_Sellmeier_B1 =   1.19286778;
//      G4double fFGL400_Sellmeier_B2 =   0.0893346571;
//      G4double fFGL400_Sellmeier_B3 =   0.920819805;
//      G4double fFGL400_Sellmeier_C1 =   0.00839154696;
//      G4double fFGL400_Sellmeier_C2 =   0.0404010786;
//      G4double fFGL400_Sellmeier_C3 = 112.572446;

      //--------------------------
      // BK7 dispersion constants
      //--------------------------
      G4double fFGL400_Sellmeier_A  =   1.0;
      G4double fFGL400_Sellmeier_B1 =   1.03961212;
      G4double fFGL400_Sellmeier_B2 =   0.231792344;
      G4double fFGL400_Sellmeier_B3 =   1.01046945;
      G4double fFGL400_Sellmeier_C1 =   TMath::Sqrt(0.00600069867);
      G4double fFGL400_Sellmeier_C2 =   TMath::Sqrt(0.0200179144);
      G4double fFGL400_Sellmeier_C3 =   TMath::Sqrt(103.560653);

      G4double fFGL400_SellmeierCoefficients[7] = { fFGL400_Sellmeier_A,
    		                                        fFGL400_Sellmeier_B1, fFGL400_Sellmeier_B2, fFGL400_Sellmeier_B3,
    		                                        fFGL400_Sellmeier_C1, fFGL400_Sellmeier_C2, fFGL400_Sellmeier_C3 };

      // now loop over wavelength and calculate/lookup the index of refraction and absorption length
	  // and store the results into vectors

      //-----------------------------------------------------------------------------------------------------
      //               Store file bases material properties in these vectors
      //-----------------------------------------------------------------------------------------------------
      G4MaterialPropertyVector* pMPV_RINDEX                               = new G4MaterialPropertyVector();
      G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength = new G4MaterialPropertyVector();
      //-----------------------------------------------------------------------------------------------------

      G4double fCurrent_PhotonEnergy = 0.0;
      G4double fCurrent_WaveLength   = 0.0;

      for (G4int i=0; i<NEntries_FGL400; i++)
      {
    	  fCurrent_PhotonEnergy = fFGL400_PhotonEnergyMin + (fFGL400_PhotonEnergyMax - fFGL400_PhotonEnergyMin) / (NEntries_FGL400 - 1) * i ;
    	  fCurrent_WaveLength   = this->EnergyToLambdaConversion( fCurrent_PhotonEnergy ); // [nm]

    	  pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
    			                                                                                         fFGL400_Sellmeier_A,
    			                                                                                         fFGL400_Sellmeier_B1, fFGL400_Sellmeier_B2, fFGL400_Sellmeier_B3,
    			                                                                                         fFGL400_Sellmeier_C1, fFGL400_Sellmeier_C2, fFGL400_Sellmeier_C3 ));
      } // end for

      //--------------------------------------------------------------------------------------------------------------------
      // Read in optical Transmission file of FGL400 and extract absorption length
      //--------------------------------------------------------------------------------------------------------------------
      // Beware; The file only contains the total transmission including the reflection off the front and back surface.
      //         What we actually need is the internal transmission which is about ~4% higher than the total transmission
      //
      //---------------------------------------------------------------------------------------------------------------------

      // Set the flag for calculating the internal transmission based on total transmission and index of refraction (via Sellmeier Coefficients)
      //
      G4bool bCalcutaleInteralTransmission = true;

      this->Initialize_AbsorptionLengthThroughTransmission_Sellmeier( "/FGL/FGL400_Transmission.dat",
    		                                                           bCalcutaleInteralTransmission,
    		                                                           fFGL400_SellmeierCoefficients,
    		                                                           fFGL400_Thickness,
																	   pMPV_OpticalTransmission_AbsorptionLength );

      std::cout << "FGL400_AbsorptionLength entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;

      //----------------------------------------------------------------------------------------------------------------------------
	  // Don't know the chemical composition of FGL400. Assuming Schott glass BK7 as a starter
	  // Stolen from: http://hypernews.slac.stanford.edu/HyperNews/geant4/get/AUX/2013/03/11/12.39-85121-chDetectorConstruction.cc
      //----------------------------------------------------------------------------------------------------------------------------

      //---------------------
      // Define new material
      //---------------------
	  G4int ncomponents;

      pFGL400_MD = nullptr;
	  pFGL400_MD = new G4Material( sName,
			                       fDensity,
								   ncomponents = 6,
								   kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pFGL400_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_B"),  fFractionOfMass =   4.0062*CLHEP::perCent);
	  pFGL400_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O"),  fFractionOfMass =  53.9562*CLHEP::perCent);
	  pFGL400_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Na"), fFractionOfMass =   2.8191*CLHEP::perCent);
	  pFGL400_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass =   1.1644*CLHEP::perCent);
	  pFGL400_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  37.7220*CLHEP::perCent);
	  pFGL400_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_K"),  fFractionOfMass =   0.3321*CLHEP::perCent);

	  //----------------------------------
      // Define material properties table
	  //----------------------------------
	  pFGL400_MPT = nullptr;
	  pFGL400_MPT = new G4MaterialPropertiesTable();

	  pFGL400_MPT->AddProperty("RINDEX"    , pMPV_RINDEX );
	  pFGL400_MPT->AddProperty("ABSLENGTH" , pMPV_OpticalTransmission_AbsorptionLength );

	  //----------------------------------------------
	  // Attach material properties table to material
	  //-----------------------------------------------
	  pFGL400_MD->SetMaterialPropertiesTable(pFGL400_MPT);

	} // end if (pMat == nullptr)

	return pFGL400_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_FusedSilica()
{
	G4String sName    = "FusedSilica";
	G4double fDensity = 2.201*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//================================================================
		//           PMT Window: Fused Silica alias Fused Quartz
		//================================================================

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pFusedSilica_MD = nullptr;
		pFusedSilica_MD = new G4Material( sName,
				                          fDensity,
										  ncomponents = 1,
										  kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4double fFractionOfMass;

		pFusedSilica_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_SILICON_DIOXIDE"), fFractionOfMass = 100.0*CLHEP::perCent);

	       // fused quartz properties stolen from
	       // http://sergiant.web.cern.ch/sergiant/NA62FW/html/dd/db2/CedarMaterialParameters_8cc_source.html
	       const G4int NEntries_FusedSilica = 100;

	       // Quartz has a large transmission range. limits for a possible Cherenkov photon spectrum is set for:
	       G4double fFusedSilica_LambdaMin         = 180.0*CLHEP::nanometer;
	       G4double fFusedSilica_LambdaMax         = 800.0*CLHEP::nanometer;
	       G4double fFusedSilica_PhotonEnergyMin   = this->LambdaToEnergyConversion(fFusedSilica_LambdaMax);
	       G4double fFusedSilica_PhotonEnergyMax   = this->LambdaToEnergyConversion(fFusedSilica_LambdaMin);


	       // Fused Silica dispersion constants
	       // taken from: http://refractiveindex.info/?shelf=main&book=SiO2&page=Malitson
	       //
	       G4double fFusedSilica_Sellmeier_A  =   1.0;
	       G4double fFusedSilica_Sellmeier_B1 =   0.6961663;
	       G4double fFusedSilica_Sellmeier_B2 =   0.4079426;
	       G4double fFusedSilica_Sellmeier_B3 =   0.8974794;
	       G4double fFusedSilica_Sellmeier_C1 =   0.0684043;
	       G4double fFusedSilica_Sellmeier_C2 =   0.1162414;
	       G4double fFusedSilica_Sellmeier_C3 =   9.8961610;

	       G4double fFusedSilica_SellmeierCoefficients[7] = { fFusedSilica_Sellmeier_A,
	     		                                              fFusedSilica_Sellmeier_B1, fFusedSilica_Sellmeier_B2, fFusedSilica_Sellmeier_B3,
	     		                                              fFusedSilica_Sellmeier_C1, fFusedSilica_Sellmeier_C2, fFusedSilica_Sellmeier_C3 };


	       // now loop over wavelength and calculate/lookup the index of refraction and absorption length
		   // and store the results into vectors

	       //--------------------------------------------------------------------------------------
	       //               Store file bases material properties in these vectors
	       //--------------------------------------------------------------------------------------
	       G4MaterialPropertyVector* pMPV_RINDEX           = new G4MaterialPropertyVector();
	       G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	       //--------------------------------------------------------------------------------------

	       G4double fCurrent_PhotonEnergy = 0.0;
	       G4double fCurrent_WaveLength   = 0.0;

	       for (G4int i=0; i<NEntries_FusedSilica; i++)
	       {
	    	   fCurrent_PhotonEnergy = fFusedSilica_PhotonEnergyMin + (fFusedSilica_PhotonEnergyMax - fFusedSilica_PhotonEnergyMin) / (NEntries_FusedSilica - 1) * i ;
	    	   fCurrent_WaveLength   = this->LambdaToEnergyConversion(fCurrent_PhotonEnergy) ;

	    	   pMPV_RINDEX_ABSLENGTH ->InsertValues( fCurrent_PhotonEnergy, this->GetFusedSilica_AbsorptionLength(fCurrent_WaveLength) );
	    	   pMPV_RINDEX           ->InsertValues( fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
	     	    			                                                                                              fFusedSilica_Sellmeier_A,
	     	    			                                                                                              fFusedSilica_Sellmeier_B1, fFusedSilica_Sellmeier_B2, fFusedSilica_Sellmeier_B3,
	     	    			                                                                                              fFusedSilica_Sellmeier_C1, fFusedSilica_Sellmeier_C2, fFusedSilica_Sellmeier_C3 ));
	       }

	       //----------------------------------
	       // Define material properties table
	       //----------------------------------

	       pFusedSilica_MPT = nullptr;
	       pFusedSilica_MPT = new G4MaterialPropertiesTable();

	       pFusedSilica_MPT->AddProperty("RINDEX"    , pMPV_RINDEX           );
	       pFusedSilica_MPT->AddProperty("ABSLENGTH" , pMPV_RINDEX_ABSLENGTH );

	       //----------------------------------------------
	       // Attach material properties table to material
	       //----------------------------------------------
	       pFusedSilica_MD->SetMaterialPropertiesTable(pFusedSilica_MPT);

	} // end if (pMat == nullptr)

	return pFusedSilica_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Perspex_Opal069()
{
	G4String sName    = "Perspex_Opal069";
	G4double fDensity = 1.05*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{

		//==============================================================================================================
		// Poly(methyl methacrylate) (PMMA),
		// also known as acrylic or acrylic glass as well as by the trade names Plexiglas, Acrylite, Lucite, and Perspex
		//
		// Chemical formula : (C5_O2_H8)n
	    //==============================================================================================================
		// Data sheet: https://www.plexiglas-shop.com/pdfs/en/211-1_PLEXIGLAS_GS_XT_en.pdf
		//==============================================================================================================

		//----------------------------------------------------------------------
		// Acrylic glass: diffuse white with a transmission of 9%@3mm thickness
		//
		// Brand name: Perspex OPAL 069
		// -> material used for diffuser disk in LB6390 in front of PMT
		//---------------------------------------------------------------------

		// What is the composition of the new material
		// Code stolen from: http://geant4.cern.ch/support/source/geant4/examples/advanced/air_shower/src/UltraDetectorConstruction.cc

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pPerspex_Opal069_MD = nullptr;
		pPerspex_Opal069_MD = new G4Material( sName,
				                              fDensity,
											  ncomponents = 3,
											  kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4int natoms;

		pPerspex_Opal069_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 5);
		pPerspex_Opal069_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms = 2);
		pPerspex_Opal069_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 8);


		// setup
		const G4int NEntries_Perspex_Opal069   = 100;

		//------------------------------------
		// Defining optical range of interest
		//------------------------------------
		G4double fPerspex_Opal069_LambdaMin       = 200*CLHEP::nanometer;
		G4double fPerspex_Opal069_LambdaMax       = 800*CLHEP::nanometer;
		G4double fPerspex_Opal069_PhotonEnergyMin = this->LambdaToEnergyConversion(fPerspex_Opal069_LambdaMax);
		G4double fPerspex_Opal069_PhotonEnergyMax = this->LambdaToEnergyConversion(fPerspex_Opal069_LambdaMin);


//	     // Acrylic glass dispersion constants (regarding wavelength given in micrometers)
//	     // See http://refractiveindex.info/?shelf=organic&book=poly%28methyl_methacrylate%29&page=Szczurowski
//	      G4double fAcrylic_Sellmeier_A  =   1.0;
//	      G4double fAcrylic_Sellmeier_B1 =   0.99654;
//	      G4double fAcrylic_Sellmeier_B2 =   0.18964;
//	      G4double fAcrylic_Sellmeier_B3 =   0.00411;
//	      G4double fAcrylic_Sellmeier_C1 =   TMath::Sqrt(0.00787);
//	      G4double fAcrylic_Sellmeier_C2 =   TMath::Sqrt(0.02191);
//	      G4double fAcrylic_Sellmeier_C3 =   TMath::Sqrt(3.85727);
//
//	      // Acrylic glass dispersion constants (regarding wavelength given in micrometers)
//	      // See https://refractiveindex.info/?shelf=organic&book=poly%28methyl_methacrylate%29&page=Sultanova
//	      G4double fAcrylic_Sellmeier_A  =   1.0;
//	      G4double fAcrylic_Sellmeier_B1 =   1.1819;
//	      G4double fAcrylic_Sellmeier_B2 =   0.0;
//	      G4double fAcrylic_Sellmeier_B3 =   0.0;
//	      G4double fAcrylic_Sellmeier_C1 =   TMath::Sqrt(0.011313);
//	      G4double fAcrylic_Sellmeier_C2 =   0.0;
//	      G4double fAcrylic_Sellmeier_C3 =   0.0;

		// Acrylic glass dispersion constants (regarding wavelength given in micrometers)
		// See https://refractiveindex.info/?shelf=organic&book=poly%28methyl_methacrylate%29&page=Bodurov
		G4double fAcrylic_Sellmeier_A  =   1.0;
		G4double fAcrylic_Sellmeier_B1 =   1.1937;
		G4double fAcrylic_Sellmeier_B2 =   0.0;
		G4double fAcrylic_Sellmeier_B3 =   0.0;
		G4double fAcrylic_Sellmeier_C1 =   0.0959316;
		G4double fAcrylic_Sellmeier_C2 =   0.0;
		G4double fAcrylic_Sellmeier_C3 =   0.0;

		G4double fAcrylic_SellmeierCoefficients[7] = { fAcrylic_Sellmeier_A,
			    		                               fAcrylic_Sellmeier_B1, fAcrylic_Sellmeier_B2, fAcrylic_Sellmeier_B3,
			    		                               fAcrylic_Sellmeier_C1, fAcrylic_Sellmeier_C2, fAcrylic_Sellmeier_C3 };


		// now loop over wavelength and calculate/lookup the index of refraction and absorption length
		// and store the results into vectors

	    //---------------------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //---------------------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_RINDEX                               = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength = new G4MaterialPropertyVector();
		//---------------------------------------------------------------------------------------------------

		G4double fCurrent_PhotonEnergy = 0.0;
		G4double fCurrent_WaveLength   = 0.0;

		for (G4int i=0; i<NEntries_Perspex_Opal069; i++)
		{
			fCurrent_PhotonEnergy  = fPerspex_Opal069_PhotonEnergyMin + (fPerspex_Opal069_PhotonEnergyMax - fPerspex_Opal069_PhotonEnergyMin) / (NEntries_Perspex_Opal069 - 1) * i ;
			fCurrent_WaveLength    = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy) ; //nm

			pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
	     	    			                                                                               fAcrylic_Sellmeier_A,
	     	    			                                                                               fAcrylic_Sellmeier_B1, fAcrylic_Sellmeier_B2, fAcrylic_Sellmeier_B3,
	     	    			                                                                               fAcrylic_Sellmeier_C1, fAcrylic_Sellmeier_C2, fAcrylic_Sellmeier_C3) );
		} // end for

		//--------------------------------------------------------------------------------------------------------------------
		// Read in optical Transmission file and extract absorption length
		//--------------------------------------------------------------------------------------------------------------------
		// Beware; The file only contains the total transmission including the reflection off the front and back surface.
		//         What we actually need is the internal transmission which is about ~8% higher than the total transmission
		//
		//---------------------------------------------------------------------------------------------------------------------

		// Set the flag for calculating the internal transmission based on total transmission and index of refraction (via Sellmeier Coefficients)
		//
		G4bool   bCalcutaleInteralTransmission = true;

		G4double fPerspex_Opal069_Thickness = 3.0*CLHEP::millimeter; // thickness of Perspex_Opal069 sheet for this optical transmission measurement

		this->Initialize_AbsorptionLengthThroughTransmission_Sellmeier( "/Perspex/Perspex_Opal069_ExternalTransmission.dat",
				                                                         bCalcutaleInteralTransmission,
															             fAcrylic_SellmeierCoefficients,
															             fPerspex_Opal069_Thickness,
															             pMPV_OpticalTransmission_AbsorptionLength );

		std::cout << "Perspex_Opal069_AbsorptionLength entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pPerspex_Opal069_MPT = nullptr;
		pPerspex_Opal069_MPT = new G4MaterialPropertiesTable();

		pPerspex_Opal069_MPT->AddProperty("RINDEX"    , pMPV_RINDEX );
		pPerspex_Opal069_MPT->AddProperty("ABSLENGTH" , pMPV_OpticalTransmission_AbsorptionLength );

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pPerspex_Opal069_MD->SetMaterialPropertiesTable(pPerspex_Opal069_MPT);

	} // end if (pMat == nullptr)

	return pPerspex_Opal069_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Plexiglas_GS233()
{
	G4String sName    = "Plexiglas_GS233";
	G4double fDensity = 1.19*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//==============================================================================================================
		// Poly(methyl methacrylate) (PMMA),
		// also known as acrylic or acrylic glass as well as by the trade names Plexiglas, Acrylite, Lucite, and Perspex
		//
		// Chemical formula : (C5_O2_H8)n
	    //==============================================================================================================
		// Data sheet: https://www.plexiglas-shop.com/pdfs/en/211-1_PLEXIGLAS_GS_XT_en.pdf
		//==============================================================================================================

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pPlexiglas_GS233_MD = nullptr;
		pPlexiglas_GS233_MD = new G4Material( sName,
	    		                              fDensity,
											  ncomponents = 3,
											  kStateSolid );

	     //----------------------------
	     // Assembling the ingredients
	     //----------------------------
	     G4int natoms;

	     pPlexiglas_GS233_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 5);
	     pPlexiglas_GS233_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms = 2);
	     pPlexiglas_GS233_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 8);

	     // setup
	     const G4int NEntries_Plexiglas   = 1000;

	     //------------------------------------
	     // Defining optical range of interest
	     //------------------------------------
	     G4double fPlexiglas_LambdaMin       = 200.0*CLHEP::nanometer;
	     G4double fPlexiglas_LambdaMax       = 800.0*CLHEP::nanometer;
	     G4double fPlexiglas_PhotonEnergyMin = this->LambdaToEnergyConversion(fPlexiglas_LambdaMax);
	     G4double fPlexiglas_PhotonEnergyMax = this->LambdaToEnergyConversion(fPlexiglas_LambdaMin);


//	     // Acrylic glass dispersion constants (regarding wavelength given in micrometers)
//	     // See http://refractiveindex.info/?shelf=organic&book=poly%28methyl_methacrylate%29&page=Szczurowski
//	      G4double fAcrylic_Sellmeier_A  =   1.0;
//	      G4double fAcrylic_Sellmeier_B1 =   0.99654;
//	      G4double fAcrylic_Sellmeier_B2 =   0.18964;
//	      G4double fAcrylic_Sellmeier_B3 =   0.00411;
//	      G4double fAcrylic_Sellmeier_C1 =   TMath::Sqrt(0.00787);
//	      G4double fAcrylic_Sellmeier_C2 =   TMath::Sqrt(0.02191);
//	      G4double fAcrylic_Sellmeier_C3 =   TMath::Sqrt(3.85727);
//
//	      // Acrylic glass dispersion constants (regarding wavelength given in micrometers)
//	      // See https://refractiveindex.info/?shelf=organic&book=poly%28methyl_methacrylate%29&page=Sultanova
//	      G4double fAcrylic_Sellmeier_A  =   1.0;
//	      G4double fAcrylic_Sellmeier_B1 =   1.1819;
//	      G4double fAcrylic_Sellmeier_B2 =   0.0;
//	      G4double fAcrylic_Sellmeier_B3 =   0.0;
//	      G4double fAcrylic_Sellmeier_C1 =   TMath::Sqrt(0.011313);
//	      G4double fAcrylic_Sellmeier_C2 =   0.0;
//	      G4double fAcrylic_Sellmeier_C3 =   0.0;
//
	     // Acrylic glass dispersion constants (regarding wavelength given in micrometers)
	     // See https://refractiveindex.info/?shelf=organic&book=poly%28methyl_methacrylate%29&page=Bodurov
	     G4double fAcrylic_Sellmeier_A  =   1.0;
	     G4double fAcrylic_Sellmeier_B1 =   1.1937;
	     G4double fAcrylic_Sellmeier_B2 =   0.0;
	     G4double fAcrylic_Sellmeier_B3 =   0.0;
	     G4double fAcrylic_Sellmeier_C1 =   0.0959316;
	     G4double fAcrylic_Sellmeier_C2 =   0.0;
	     G4double fAcrylic_Sellmeier_C3 =   0.0;
//
//
//	      // Acrylic glass dispersion constants (regarding wavelength given in micrometers)
//	      // See http://arxiv.org/abs/1605.07011v1
//	      G4double fAcrylic_Sellmeier_A  =   1.0;
//	      G4double fAcrylic_Sellmeier_B1 =   0.4963;
//	      G4double fAcrylic_Sellmeier_B2 =   0.6965;
//	      G4double fAcrylic_Sellmeier_B3 =   0.3223;
//	      G4double fAcrylic_Sellmeier_C1 =   0.07180; // in um
//	      G4double fAcrylic_Sellmeier_C2 =   0.1174;  // in um
//	      G4double fAcrylic_Sellmeier_C3 =   9.237;   // in um

//	      // Acrylic glass dispersion constants (regarding wavelength given in micrometers)
//	      // See https://orbi.ulg.ac.be/bitstream/2268/91398/1/R2.pdf
//	      // -> Choosing PMMA #4 made from Evonik == Plexiglas
//	      // ===> No hangups, but no hits im PMT ...
//	      G4double fAcrylic_Sellmeier_A  =   1.0;
//	      G4double fAcrylic_Sellmeier_B1 =   0.1838458;
//	      G4double fAcrylic_Sellmeier_B2 =   0.0998312;
//	      G4double fAcrylic_Sellmeier_B3 =   6664.339;
//	      G4double fAcrylic_Sellmeier_C1 =   TMath::Sqrt(28.27502000); // in um
//	      G4double fAcrylic_Sellmeier_C2 =   TMath::Sqrt( 0.01127337); // in um
//	      G4double fAcrylic_Sellmeier_C3 =   TMath::Sqrt( 0.01127703); // in um

	     G4double fAcrylic_SellmeierCoefficients[7] = { fAcrylic_Sellmeier_A,
	    		                                        fAcrylic_Sellmeier_B1, fAcrylic_Sellmeier_B2, fAcrylic_Sellmeier_B3,
	    		                                        fAcrylic_Sellmeier_C1, fAcrylic_Sellmeier_C2, fAcrylic_Sellmeier_C3 };


	      //------------------------------------------------------------------------------------------------------
	      //               Store file bases material properties in these vectors
	      //------------------------------------------------------------------------------------------------------
	      G4MaterialPropertyVector* pMPV_RINDEX                               = new G4MaterialPropertyVector();
	      G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength = new G4MaterialPropertyVector();
	      //------------------------------------------------------------------------------------------------------

	      G4double fCurrent_PhotonEnergy       = 0.0;
	      G4double fCurrent_WaveLength         = 0.0;
	      G4double fCurrent_IndexOfRefraction  = 0.0;

	      // now loop over wavelength and calculate/lookup the index of refraction and absorption length
	      // and store the results into vectors
	      //

	      G4double bParam[4] = {1760.7010,-1.3687,2.4388e-3,-1.5178e-6} ;

	      for (G4int i=0; i<NEntries_Plexiglas; i++)
	       {
	    	   fCurrent_PhotonEnergy = fPlexiglas_PhotonEnergyMin + i*(fPlexiglas_PhotonEnergyMax - fPlexiglas_PhotonEnergyMin) / float(NEntries_Plexiglas - 1); // eV
	    	   fCurrent_WaveLength   = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy); //nm

//	    	   pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
//		     	    			                                                                              fAcrylic_Sellmeier_A,
//		     	    			                                                                              fAcrylic_Sellmeier_B1, fAcrylic_Sellmeier_B2, fAcrylic_Sellmeier_B3,
//		     	    			                                                                              fAcrylic_Sellmeier_C1, fAcrylic_Sellmeier_C2, fAcrylic_Sellmeier_C3) );

	    	   //----------------------------------------------------------------------------------------------------------------------
	    	   // Parameterization for refractive index of High Grade PMMA
	    	   // taken from: http://geant4.cern.ch/support/source/geant4/examples/advanced/air_shower/src/UltraDetectorConstruction.cc
	    	   //----------------------------------------------------------------------------------------------------------------------

	    	   // reset value
	    	   fCurrent_IndexOfRefraction = 0.0;

	    	   for (G4int jj=0 ; jj<4 ; jj++)
	    	   {
	    		   fCurrent_IndexOfRefraction +=  (bParam[jj]/1000.0)*std::pow(fCurrent_WaveLength/CLHEP::nanometer,jj) ;
	    	   }

	    	   pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, fCurrent_IndexOfRefraction);

	    	   std::cout << "Plexiglas_GS233 Index of Refaction = " << fCurrent_IndexOfRefraction << " @" << fCurrent_WaveLength/CLHEP::nanometer << " [nm] " << std::endl;

	       } // end for (G4int i=0; i<NEntries_Plexiglas; i++)


	       //--------------------------------------------------------------------------------------------------------------------
	       // Read in optical Transmission file and extract absorption length
	       //--------------------------------------------------------------------------------------------------------------------
	       // Beware; The file only contains the total transmission including the reflection off the front and back surface.
	       //         What we actually need is the internal transmission which is about ~8% higher than the total transmission
	       //
	       //---------------------------------------------------------------------------------------------------------------------

	       // Set the flag for calculating the internal transmission based on total transmission and index of refraction (via Sellmeier Coefficients)
	       //
	       G4bool   bCalcutaleInteralTransmission = true;
//	       G4bool   bCalcutaleInteralTransmission = false; // for test purpose

	       G4double fPlexiglas_Thickness = 3.0*CLHEP::millimeter; // thickness of plexiglas sheet for this optical transmission measurement

	      this->Initialize_AbsorptionLengthThroughTransmission_Sellmeier( "/Plexiglas/Plexiglas_GS_ExternalTransmission.dat", // hangup during run
//        this->Initialize_AbsorptionLengthThroughTransmission_Sellmeier( "/Plexiglas/PMMA_ExternalTransmission.dat",         // works
	       		                                                            bCalcutaleInteralTransmission,
	       		                                                            fAcrylic_SellmeierCoefficients,
	       		                                                            fPlexiglas_Thickness,
																			pMPV_OpticalTransmission_AbsorptionLength );

		   std::cout << "Plexiglas_GS233AbsorptionLength entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;

		   //----------------------------------
           // Define material properties table
		   //----------------------------------
		   pPlexiglas_GS233_MPT = nullptr;
	       pPlexiglas_GS233_MPT = new G4MaterialPropertiesTable();

	       pPlexiglas_GS233_MPT->AddProperty("RINDEX"    , pMPV_RINDEX );
	       pPlexiglas_GS233_MPT->AddProperty("ABSLENGTH" , pMPV_OpticalTransmission_AbsorptionLength );

	       //----------------------------------------------
           // Attach material properties table to material
	       //----------------------------------------------
	       pPlexiglas_GS233_MD->SetMaterialPropertiesTable(pPlexiglas_GS233_MPT);

	} // end if (pMat == nullptr)

	return pPlexiglas_GS233_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_PMMA()
{
	G4String sName    = "PMMA";
	G4double fDensity = 1.19*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//================================================================
		//                     PMMA (C5_O2_H8)
		//================================================================

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pPMMA_MD = nullptr;
		pPMMA_MD = new G4Material( sName,
				                   fDensity,
								   ncomponents = 3,
								   kStateSolid );

		//----------------------------
		// Assembling the ingredients
		//----------------------------
		G4int natoms;

		pPMMA_MD->AddElement( pNistMaterialManager->FindOrBuildElement("C"), natoms = 5 );
		pPMMA_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O"), natoms = 2 );
		pPMMA_MD->AddElement( pNistMaterialManager->FindOrBuildElement("H"), natoms = 8 );

	} // end if (pMat == nullptr)

	return pPMMA_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//#####################################################################################################################
//#####################################################################################################################
//                      Materials for Scintillators
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//=========================================================
//                      Organic Scintillators
//=========================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BC400()
{
	  G4String sName    = "BC400";
	  G4double fDensity = 1.032*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		  //=======================================================
		  //     Plastic Scintillator Bicron BC-400
		  //     alias Eljen EJ-212 or Saint-Gobain NE-102A
		  //     -> Base material: Polyvinyltoluene (PVT)
		  //=======================================================

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pBC400_MD = nullptr;
	    pBC400_MD = new G4Material( sName,
	    		                    fDensity,
									ncomponents = 1,
									kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pBC400_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE"), fFractionOfMass = 100*CLHEP::perCent);


	    //-------------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //-------------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_EmissionSpectrum                     = new G4MaterialPropertyVector();
		//---
		G4MaterialPropertyVector* pMPV_REALRINDEX                           = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX                      = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                               = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH                     = new G4MaterialPropertyVector();
		//---
		G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH                     = new G4MaterialPropertyVector();
//		G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength = new G4MaterialPropertyVector();
		//--
		G4MaterialPropertyVector* pMPV_ALPHASCINTILLATIONYIELD              = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_ELECTRONSCINTILLATIONYIELD           = new G4MaterialPropertyVector();
		//-----------------------------------------------------------------------------------------

        //------------------------
		// Specify the Light Yield
        //------------------------
		G4double fBC400_LightYield = fAnthracene_LightYield*0.65; // Taken from Saint Gobain (11.3 photonen/keV)


	 	//-------------------------------------------------------------------------
	 	// Refractive index for Polyvinyltoluene (PVT)
	 	//-------------------------------------------------------------------------
	 	// Formula used for refractive index n where n^2 = (nI)^2 + A/(lambda^2 -
	 	// lambda_0^2).  nI = 1.53319, A = 20,690nm^2, lambda_0 = 196.6nm.  nI was
	 	// fitted such that lambda = 425nm => n = 1.58 as is reported in the EJ254
	 	// data sheet. Other constants were extracted as is from the following
	 	// reference.
	 	//
	 	// Reference: Journal of Colloid and Interface Science, Volume 118, Issue 2,
	 	// August 1987, Pages 314–325, Measurement of the complex refractive index
	 	// of polyvinyltoluene in the UV, visible, and near IR: Application to the
	 	// size determination of PVT latices. T. Depireux, F. Dumont, A. Watillon.
	 	//-------------------------------------------------------------------------

	     G4double fPVT_Sellmeier_A  =   1.53319;
	     G4double fPVT_Sellmeier_B1 =   0.02069;
	     G4double fPVT_Sellmeier_B2 =   0.0;
	     G4double fPVT_Sellmeier_B3 =   0.0;
	     G4double fPVT_Sellmeier_C1 =   0.1966;
	     G4double fPVT_Sellmeier_C2 =   0.0;
	     G4double fPVT_Sellmeier_C3 =   0.0;

	     G4double fPVT_SellmeierCoefficients[7] = { fPVT_Sellmeier_A,
		    		                                fPVT_Sellmeier_B1, fPVT_Sellmeier_B2, fPVT_Sellmeier_B3,
		    		                                fPVT_Sellmeier_C1, fPVT_Sellmeier_C2, fPVT_Sellmeier_C3 };

	    //------------------------------------------------------------------------------------------------------------------
		// Read emission spectrum from file
	    //------------------------------------------------------------------------------------------------------------------
		this->Initialize_EmissionSpectrum("/BC400/BC400_EmissionSpectrum.dat",
				                           pMPV_EmissionSpectrum );

		std::cout << "BC400_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;

		//------------------------------------------------------------------------------------------------------------------
		// Read energy response of alpha and beta radiation from file
		// => Light yield is a function of kinetic energy and depends on particle type
		//------------------------------------------------------------------------------------------------------------------
		this->Initialize_EnergyResponse( "/BC400/BC400_EnergyResponse.dat",
                                          fBC400_LightYield,
					                      pMPV_ALPHASCINTILLATIONYIELD,
					                      pMPV_ELECTRONSCINTILLATIONYIELD);

		std::cout << "BC400_EnergyResponse entries = " << pMPV_ELECTRONSCINTILLATIONYIELD->GetVectorLength() << std::endl;

		//------------------------------------------------------------------------------------------------------------------
		// Read absorption length from file
		//------------------------------------------------------------------------------------------------------------------
		this->Initialize_AbsorptionLength("/BC400/BC400_AbsorptionLength.dat",
				                           pMPV_ABSORPTIONLENGTH );

		std::cout << "BC400_AbsorptionLength entries = " << pMPV_ABSORPTIONLENGTH->GetVectorLength() << std::endl;


		//------------------------------------------------------------------------------------------------------------------
		// Read index of refraction from file
		//------------------------------------------------------------------------------------------------------------------
		this->Initialize_IndexOfRefraction("/BC400/BC400_IndexOfRefraction.dat",
                                            pMPV_REALRINDEX,
                                            pMPV_IMAGINARYRINDEX,
                                            pMPV_RINDEX,
                                            pMPV_RINDEX_ABSLENGTH);

		std::cout << "BC400_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;


        //-----------------------------
		// Retrieve index of refraction
		//-----------------------------

		const G4int BC400_IndexOfRefraction_NumberOfEntries   = 1000;

		//------------------------------------
		// Defining optical range of interest
		//------------------------------------
		G4double fBC400_IndexOfRefraction_LambdaMin       = 200.0*CLHEP::nanometer;
		G4double fBC400_IndexOfRefraction_LambdaMax       = 800.0*CLHEP::nanometer;
		G4double fBC400_IndexOfRefraction_PhotonEnergyMin = this->LambdaToEnergyConversion(fBC400_IndexOfRefraction_LambdaMax);
		G4double fBC400_IndexOfRefraction_PhotonEnergyMax = this->LambdaToEnergyConversion(fBC400_IndexOfRefraction_LambdaMin);


		// now loop over wavelength and calculate/lookup the index of refraction
		// and store the results into vectors

		G4double fCurrent_PhotonEnergy = 0.0;
		G4double fCurrent_WaveLength   = 0.0;

		for (G4int i=0; i<BC400_IndexOfRefraction_NumberOfEntries; i++)
		{
			fCurrent_PhotonEnergy  = fBC400_IndexOfRefraction_PhotonEnergyMin + (fBC400_IndexOfRefraction_PhotonEnergyMax - fBC400_IndexOfRefraction_PhotonEnergyMin) / (BC400_IndexOfRefraction_NumberOfEntries - 1) * i;
			fCurrent_WaveLength    = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy);

//			pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
//                                                                                                           fPVT_Sellmeier_A,
//                                                                                                           fPVT_Sellmeier_B1, fPVT_Sellmeier_B2, fPVT_Sellmeier_B3,
//                                                                                                           fPVT_Sellmeier_C1, fPVT_Sellmeier_C2, fPVT_Sellmeier_C3) );
		} // end for


	    //---------------------------------
		// Define material properties table
	    //---------------------------------
		pBC400_MPT = nullptr;
		pBC400_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pBC400_MPT->AddProperty("FASTCOMPONENT"                 , pMPV_EmissionSpectrum );
		pBC400_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
		pBC400_MPT->AddProperty("ABSLENGTH"                     , pMPV_ABSORPTIONLENGTH );
//		pBC400_MPT->AddProperty("ABSLENGTH"                     , pMPV_OpticalTransmission_AbsorptionLength );

		// Non-linear light yield depending on particle energy
		pBC400_MPT->AddProperty("ALPHASCINTILLATIONYIELD"       , pMPV_ALPHASCINTILLATIONYIELD    );
		pBC400_MPT->AddProperty("ELECTRONSCINTILLATIONYIELD"    , pMPV_ELECTRONSCINTILLATIONYIELD );
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pBC400_MPT->AddConstProperty("SCINTILLATIONYIELD"       , fBC400_LightYield ); // saint-gobain
		pBC400_MPT->AddConstProperty("RESOLUTIONSCALE"          ,  1.0              ); // Resolution scale: sigma = resolution_scale * sqrt(E * scintillation_yield)
		pBC400_MPT->AddConstProperty("FASTTIMECONSTANT"         ,  2.4*CLHEP::ns    ); // saint-gobain
//	    pBC400_MPT->AddConstProperty("FASTSCINTILLATIONRISETIME",  0.9*CLHEP::ns    );
		pBC400_MPT->AddConstProperty("YIELDRATIO"               ,  1.0              ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pBC400_MD->SetMaterialPropertiesTable(pBC400_MPT);

	} // end if (pMat == nullptr)

	return pBC400_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BC408()
{
	  G4String sName    = "BC408";
	  G4double fDensity = 1.032*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		  //=======================================================
		  //     Plastic Scintillator Bicron BC-408
		  //     alias Eljen EJ-200 or Saint-Gobain Pilot F
		  //     -> Base material: Polyvinyltoluene (PVT)
		  //=======================================================

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pBC408_MD = nullptr;
	    pBC408_MD = new G4Material( sName,
	    		                    fDensity,
									ncomponents = 1,
									kStateSolid) ;

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pBC408_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE"), fFractionOfMass = 100*CLHEP::perCent);

	    //---------------------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //---------------------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_EmissionSpectrum                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                               = new G4MaterialPropertyVector();
//		G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength = new G4MaterialPropertyVector();
		//--
		G4MaterialPropertyVector* pMPV_ALPHASCINTILLATIONYIELD              = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_ELECTRONSCINTILLATIONYIELD           = new G4MaterialPropertyVector();
		//-----------------------------------------------------------------------------------------

        //------------------------
		// Specify the Light Yield
        //------------------------
		G4double fBC408_LightYield = fAnthracene_LightYield*0.64; // Taken from Saint Gobain, 11.14 photonen/keV

	 	//-------------------------------------------------------------------------
	 	// Refractive index for Polyvinyltoluene
	 	//-------------------------------------------------------------------------
	 	// Formula used for refractive index n where n^2 = (nI)^2 + A/(lambda^2 -
	 	// lambda_0^2).  nI = 1.53319, A = 20,690nm^2, lambda_0 = 196.6nm.  nI waspLYSO_Ce_MD  =
	 	// fitted such that lambda = 425nm => n = 1.58 as is reported in the EJ254
	 	// data sheet. Other constants were extracted as is from the following
	 	// reference.
	 	//
	 	// Reference: Journal of Colloid and Interface Science, Volume 118, Issue 2,
	 	// August 1987, Pages 314–325, Measurement of the complex refractive index
	 	// of polyvinyltoluene in the UV, visible, and near IR: Application to the
	 	// size determination of PVT latices. T. Depireux, F. Dumont, A. Watillon.
	 	//-------------------------------------------------------------------------

	     G4double fPVT_Sellmeier_A  =   1.53319;
	     G4double fPVT_Sellmeier_B1 =   0.02069;
	     G4double fPVT_Sellmeier_B2 =   0.0;
	     G4double fPVT_Sellmeier_B3 =   0.0;
	     G4double fPVT_Sellmeier_C1 =   0.1966;
	     G4double fPVT_Sellmeier_C2 =   0.0;
	     G4double fPVT_Sellmeier_C3 =   0.0;

	     G4double fPVT_SellmeierCoefficients[7] = { fPVT_Sellmeier_A,
		    		                                fPVT_Sellmeier_B1, fPVT_Sellmeier_B2, fPVT_Sellmeier_B3,
		    		                                fPVT_Sellmeier_C1, fPVT_Sellmeier_C2, fPVT_Sellmeier_C3 };


		//------------------------------------------------------------------------------------------------------------------
		// Read emission spectrum from file
	    //------------------------------------------------------------------------------------------------------------------
		this->Initialize_EmissionSpectrum("/BC408/BC408_EmissionSpectrum.dat",
				                           pMPV_EmissionSpectrum );

		std::cout << "BC408_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;


		//----------------------------------------------------------------------------
		// Read energy response of alpha and beta radiation from file
		// => Light yield is a function of kinetic energy and depends on particle type
		//----------------------------------------------------------------------------
		this->Initialize_EnergyResponse( "/BC408/BC408_EnergyResponse.dat",
                                          fBC408_LightYield,
                                          pMPV_ALPHASCINTILLATIONYIELD,
                                          pMPV_ELECTRONSCINTILLATIONYIELD);

		std::cout << "BC408_EnergyResponse entries = " << pMPV_ELECTRONSCINTILLATIONYIELD->GetVectorLength() << std::endl;

		//-----------------------------------------------------------------------------------------------------------------
//		// Read absorption length from file
		//------------------------------------------------------------------------------------------------------------------
//		this->Initialize_AbsorptionLength("/BC408/BC408_AbsorptionLength.dat",
//				                           pMPV_ABSORPTIONLENGTH );
//
//		std::cout << "BC408_AbsorptionLength entries = " << pMPV_ABSORPTIONLENGTH->GetVectorLength() << std::endl;

		// Set the flag for calculating the internal transmission based on total transmission and index of refraction (via Sellmeier Coefficients)
		//
		G4bool   bCalcutaleInteralTransmission = true;                  // tabulated values are already corrected for surface reflection losses

		G4double fBC408_Thickness              = 60.0*CLHEP::millimeter;  // Transmission has been measured for *this* thickness

		this->Initialize_AbsorptionLengthThroughTransmission_Sellmeier( "/BC408/BC408_ExternalTransmission.dat",
		    		                                                     bCalcutaleInteralTransmission,         // true
		    		                                                     fPVT_SellmeierCoefficients,            // PVT values are used
		    		                                                     fBC408_Thickness,
															             pMPV_OpticalTransmission_AbsorptionLength );

		std::cout << "BC408_OpticalTransmission_AbsorptionLength entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;


        //-----------------------------
		// Retrieve index of refraction
		//-----------------------------

		const G4int BC408_IndexOfRefraction_NumberOfEntries   = 100;

		//------------------------------------
		// Defining optical range of interest
		//------------------------------------
		G4double fBC408_IndexOfRefraction_LambdaMin       = 200.0*CLHEP::nanometer;
		G4double fBC408_IndexOfRefraction_LambdaMax       = 800.0*CLHEP::nanometer;
		G4double fBC408_IndexOfRefraction_PhotonEnergyMin = this->LambdaToEnergyConversion(fBC408_IndexOfRefraction_LambdaMax);
		G4double fBC408_IndexOfRefraction_PhotonEnergyMax = this->LambdaToEnergyConversion(fBC408_IndexOfRefraction_LambdaMin);


		//--------------------------------------------------------------------------
        // now loop over wavelength and calculate/lookup the index of refraction
		// and store the results into vectors
		//--------------------------------------------------------------------------

	     G4double fCurrent_PhotonEnergy = 0.0;
	     G4double fCurrent_WaveLength   = 0.0;

	       for (G4int i=0; i<BC408_IndexOfRefraction_NumberOfEntries; i++)
	       {
	    	   fCurrent_PhotonEnergy  = fBC408_IndexOfRefraction_PhotonEnergyMin + (fBC408_IndexOfRefraction_PhotonEnergyMax - fBC408_IndexOfRefraction_PhotonEnergyMin) / (BC408_IndexOfRefraction_NumberOfEntries - 1) * i;
	    	   fCurrent_WaveLength    = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy);

	    	   pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
                                                                                                              fPVT_Sellmeier_A,
                                                                                                              fPVT_Sellmeier_B1, fPVT_Sellmeier_B2, fPVT_Sellmeier_B3,
                                                                                                              fPVT_Sellmeier_C1, fPVT_Sellmeier_C2, fPVT_Sellmeier_C3) );
	       } // end for



	    //---------------------------------
		// Define material properties table
	    //---------------------------------
	    pBC408_MPT = nullptr;
		pBC408_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pBC408_MPT->AddProperty("FASTCOMPONENT"                 , pMPV_EmissionSpectrum );
		pBC408_MPT->AddProperty("SLOWCOMPONENT"                 , pMPV_EmissionSpectrum );
		pBC408_MPT->AddProperty("RINDEX"                        , pMPV_RINDEX           );
//		pBC408_MPT->AddProperty("ABSLENGTH"                     , pMPV_ABSORPTIONLENGTH );
		pBC408_MPT->AddProperty("ABSLENGTH"                     , pMPV_OpticalTransmission_AbsorptionLength );
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// Non-linear light yield depending on particle energy
		pBC408_MPT->AddProperty("ALPHASCINTILLATIONYIELD"       , pMPV_ALPHASCINTILLATIONYIELD    );
		pBC408_MPT->AddProperty("ELECTRONSCINTILLATIONYIELD"    , pMPV_ELECTRONSCINTILLATIONYIELD );
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// based on paper: SONG Yu-Shou et al. "Monte Carlo simulation of the property of a scintillation bar in the multi-neutron correlation spectrometer"
		//                 Chinese Physics C, Vol. 33, No.10, Oct. 2009
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pBC408_MPT->AddConstProperty("SCINTILLATIONYIELD"       , fBC408_LightYield );  // saint-gobain
		pBC408_MPT->AddConstProperty("RESOLUTIONSCALE"          ,   1.0             );  // Resolution scale: sigma = resolution_scale * sqrt(E * scintillation_yield)
//		pBC408_MPT->AddConstProperty("FASTTIMECONSTANT"         ,   2.1*CLHEP::ns   );pLYSO_Ce_MD  =
//	    pBC408_MPT->AddConstProperty("SLOWTIMECONSTANT"         ,  14.2*CLHEP::ns   );
//	    pBC408_MPT->AddConstProperty("FASTSCINTILLATIONRISETIME",   0.9*CLHEP::ns   );
//	    pBC408_MPT->AddConstProperty("SLOWSCINTILLATIONRISETIME",   0.9*CLHEP::ns   );
//		pBC408_MPT->AddConstProperty("YIELDRATIO"               ,   0.7874          );  // relative strength of fast component as a fraction of total scintillation yield
//
		pBC408_MPT->AddConstProperty("FASTTIMECONSTANT"         ,   2.1*CLHEP::ns   );
		pBC408_MPT->AddConstProperty("FASTSCINTILLATIONRISETIME",   0.9*CLHEP::ns   );
		pBC408_MPT->AddConstProperty("YIELDRATIO"               ,   1.0             );  // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pBC408_MD->SetMaterialPropertiesTable(pBC408_MPT);

	} // end if (pMat == nullptr)

	return pBC408_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BC91A()
{
	  G4String sName    = "BC91A";
	  G4double fDensity = 1.05*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
	    //===================================
	    //    Bicron BC 91A (WLS)
	    //===================================

		//-------------------------------
	    //Define WLS fiber core material
		//-------------------------------
		G4int ncomponents;

		pBC91A_MD = nullptr;
	    pBC91A_MD = new G4Material( sName,
	    		                    fDensity,
									ncomponents = 1,
									kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

		pBC91A_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_POLYSTYRENE"), fFractionOfMass = 100*CLHEP::perCent);

		//---------------------------------
	    // Add optical properties to fiber
		//---------------------------------
	    const G4int NUMENTRIES1 = 2;

	    G4double PhotonEnergy_WLS_REFINDEX_core[NUMENTRIES1] = { 3.5*CLHEP::eV, 2.0*CLHEP::eV};

	    //Data: http://www.detectors.saint-gobain.com/fibers.aspx
	    G4double WLS_REFINDEX_core[NUMENTRIES1] = { 1.6          , 1.6         };
	    G4double WLS_ABSORB_core  [NUMENTRIES1] = { 7.0*CLHEP::m , 7.0*CLHEP::m};

	    // Data from "Simulation of optical processes in GEANT4"
	    // http://www-zeuthen.desy.de/lcdet/Feb_05_WS/talks/rd_lcdet_sim.pdf

	    const G4int NUMENTRIES2 = 24;

	    //Data for Bcf91a
	    G4double PhotonEnergy_WLS_EM_core[NUMENTRIES2] ={
	                       3.50*CLHEP::eV, 2.67*CLHEP::eV, 2.66*CLHEP::eV, 2.64*CLHEP::eV, 2.63*CLHEP::eV, 2.61*CLHEP::eV,
	                       2.58*CLHEP::eV, 2.56*CLHEP::eV, 2.55*CLHEP::eV, 2.53*CLHEP::eV, 2.50*CLHEP::eV, 2.48*CLHEP::eV,
	                       2.46*CLHEP::eV, 2.45*CLHEP::eV, 2.44*CLHEP::eV, 2.43*CLHEP::eV, 2.41*CLHEP::eV, 2.37*CLHEP::eV,
	                       2.33*CLHEP::eV, 2.25*CLHEP::eV, 2.24*CLHEP::eV, 2.19*CLHEP::eV, 2.15*CLHEP::eV, 2.00*CLHEP::eV };

	    //Data for Bcf91a
	    G4double WLS_EMISSION_core[NUMENTRIES2] ={
	                       0, 0.02, 0.09, 0.20, 0.29,0.40, 0.59, 0.70, 0.80,
	                       0.89,1.00, 0.96, 0.88, 0.79, 0.69,0.59, 0.50, 0.40,
	                       0.31, 0.22,0.19, 0.10, 0.06, 0};


	    const G4int NUMENTRIES3 = 42;

	    //Data for Bcf91a
	    G4double PhotonEnergy_WLS_ABS_core[NUMENTRIES3] ={
	                       3.500*CLHEP::eV, 3.477*CLHEP::eV, 3.340*CLHEP::eV, 3.321*CLHEP::eV, 3.291*CLHEP::eV,
	                       3.214*CLHEP::eV, 3.162*CLHEP::eV, 3.129*CLHEP::eV, 3.091*CLHEP::eV, 3.086*CLHEP::eV,
	                       3.049*CLHEP::eV, 3.008*CLHEP::eV, 2.982*CLHEP::eV, 2.958*CLHEP::eV, 2.928*CLHEP::eV,
	                       2.905*CLHEP::eV, 2.895*CLHEP::eV, 2.890*CLHEP::eV, 2.858*CLHEP::eV, 2.813*CLHEP::eV,
	                       2.774*CLHEP::eV, 2.765*CLHEP::eV, 2.752*CLHEP::eV, 2.748*CLHEP::eV, 2.739*CLHEP::eV,
	                       2.735*CLHEP::eV, 2.731*CLHEP::eV, 2.723*CLHEP::eV, 2.719*CLHEP::eV, 2.698*CLHEP::eV,
	                       2.674*CLHEP::eV, 2.626*CLHEP::eV, 2.610*CLHEP::eV, 2.583*CLHEP::eV, 2.556*CLHEP::eV,
	                       2.530*CLHEP::eV, 2.505*CLHEP::eV, 2.480*CLHEP::eV, 2.455*CLHEP::eV, 2.431*CLHEP::eV,
	                       2.407*CLHEP::eV, 2.000*CLHEP::eV };

	    //Data for Bcf91a
	    G4double WLS_ABSLENGTH_core[NUMENTRIES3] ={
	                       0.28*CLHEP::cm,   0.28*CLHEP::cm,  0.26*CLHEP::cm,   0.2*CLHEP::cm,    0.16*CLHEP::cm,
	                       0.12*CLHEP::cm,   0.08*CLHEP::cm,  0.06*CLHEP::cm,   0.02*CLHEP::cm,   0.02*CLHEP::cm,
	                       0.06*CLHEP::cm,   0.08*CLHEP::cm,  0.10*CLHEP::cm,   0.12*CLHEP::cm,   0.12*CLHEP::cm,
	                       0.16*CLHEP::cm,   0.16*CLHEP::cm,  0.18*CLHEP::cm,   0.18*CLHEP::cm,   0.20*CLHEP::cm,
	                       0.21*CLHEP::cm,   0.22*CLHEP::cm,  0.22*CLHEP::cm,   0.23*CLHEP::cm,   0.23*CLHEP::cm,
	                       0.24*CLHEP::cm,   0.25*CLHEP::cm,  0.25*CLHEP::cm,   0.26*CLHEP::cm,   0.27*CLHEP::cm,
	                       0.30*CLHEP::cm,   2.69*CLHEP::cm,  3.49*CLHEP::cm,   3.99*CLHEP::cm,   5.00*CLHEP::cm,
	                      11.60*CLHEP::cm,  21.60*CLHEP::cm, 33.10*CLHEP::cm, 175.00*CLHEP::cm, 393.00*CLHEP::cm,
	                     617.00*CLHEP::cm, 794.00*CLHEP::cm };


		//----------------------------------
		// Define material properties table
		//----------------------------------
	    pBC91A_MPT = nullptr;
	    pBC91A_MPT = new G4MaterialPropertiesTable();

	    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	    pBC91A_MPT->AddProperty("RINDEX"      ,PhotonEnergy_WLS_REFINDEX_core, WLS_REFINDEX_core  ,NUMENTRIES1)->SetSpline(true);
	    pBC91A_MPT->AddProperty("ABSLENGTH"   ,PhotonEnergy_WLS_REFINDEX_core, WLS_ABSORB_core    ,NUMENTRIES1)->SetSpline(true);
	    pBC91A_MPT->AddProperty("WLSCOMPONENT",PhotonEnergy_WLS_EM_core      , WLS_EMISSION_core  ,NUMENTRIES2)->SetSpline(true);
	    pBC91A_MPT->AddProperty("WLSABSLENGTH",PhotonEnergy_WLS_ABS_core     , WLS_ABSLENGTH_core ,NUMENTRIES3)->SetSpline(true);
	    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	    pBC91A_MPT->AddConstProperty("WLSTIMECONSTANT", 2.7*CLHEP::ns); // http://www.detectors.saint-gobain.com/uploadedFiles/SGdetectors/Documents/Brochures/Organics-Brochure.pdf
	    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	    //----------------------------------------------
	    // Attach material properties table to material
	    //----------------------------------------------
	    pBC91A_MD->SetMaterialPropertiesTable(pBC91A_MPT);

	} // end if (pMat == nullptr)

	return pBC91A_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_EJ280()
{
	  G4String sName    = "EJ280";
      G4double fDensity = 1.023*CLHEP::g/CLHEP::cm3;

      //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
      //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		  //==============================================================
		  //     Wavelength Shifting Plastics Eljen EJ-280
		  //     shifts blue light into green light
		  //     -> Base material: Polyvinyltoluene (PVT) == C_9H_10)
		  //==============================================================

		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pEJ280_MD = nullptr;
	    pEJ280_MD = new G4Material( sName,
	    		                    fDensity,
									ncomponents = 1,
									kStateSolid );

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

	    pEJ280_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE"), fFractionOfMass = 100*CLHEP::perCent);


	    //-----------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //-----------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_EmissionSpectrum                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                               = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength = new G4MaterialPropertyVector();

		//------------------------------------------------------------------------------------------------------------------
		// Read emission spectrum from file
		//------------------------------------------------------------------------------------------------------------------
		this->Initialize_EmissionSpectrum("/EJ280/EJ280_EmissionSpectrum.dat",
										   pMPV_EmissionSpectrum );

		std::cout << "EJ280_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;


		//--------------------------------------------------------------------------------------------------------------------
		// Read in optical Transmission file of EJ-280 and extract absorption length
		//--------------------------------------------------------------------------------------------------------------------
		// -> Here the reflection losses are already taken into account !!
		//---------------------------------------------------------------------------------------------------------------------

	 	//-------------------------------------------------------------------------
	 	// Refractive index for Polyvinyltoluene
	 	//-------------------------------------------------------------------------
	 	// Formula used for refractive index n where n^2 = (nI)^2 + A/(lambda^2 -
	 	// lambda_0^2).  nI = 1.53319, A = 20,690nm^2, lambda_0 = 196.6nm.  nI was
	 	// fitted such that lambda = 425nm => n = 1.58 as is reported in the EJ254
	 	// data sheet. Other constants were extracted as is from the following
	 	// reference.
	 	//
	 	// Reference: Journal of Colloid and Interface Science, Volume 118, Issue 2,
	 	// August 1987, Pages 314–325, Measurement of the complex refractive index
	 	// of polyvinyltoluene in the UV, visible, and near IR: Application to the
	 	// size determination of PVT latices. T. Depireux, F. Dumont, A. Watillon.
	 	//-------------------------------------------------------------------------

	     G4double fPVT_Sellmeier_A  =   1.53319;
	     G4double fPVT_Sellmeier_B1 =   0.02069;
	     G4double fPVT_Sellmeier_B2 =   0.0;
	     G4double fPVT_Sellmeier_B3 =   0.0;
	     G4double fPVT_Sellmeier_C1 =   0.1966;
	     G4double fPVT_Sellmeier_C2 =   0.0;
	     G4double fPVT_Sellmeier_C3 =   0.0;

	     G4double fPVT_SellmeierCoefficients[7] = { fPVT_Sellmeier_A,
		    		                                fPVT_Sellmeier_B1, fPVT_Sellmeier_B2, fPVT_Sellmeier_B3,
		    		                                fPVT_Sellmeier_C1, fPVT_Sellmeier_C2, fPVT_Sellmeier_C3 };


		// Set the flag for calculating the internal transmission based on total transmission and index of refraction (via Sellmeier Coefficients)
		//
		G4bool   bCalcutaleInteralTransmission = false;                  // tabulated values are already corrected for surface reflection losses

		G4double fEJ280_Thickness              = 0.7*CLHEP::millimeter;  // Transmission has been measured for *this* thickness

		this->Initialize_AbsorptionLengthThroughTransmission_Sellmeier( "/EJ280/EJ280_InternalTransmission.dat",
		    		                                                     bCalcutaleInteralTransmission,         // false
		    		                                                     fPVT_SellmeierCoefficients,            // PVT values are used
		    		                                                     fEJ280_Thickness,
															             pMPV_OpticalTransmission_AbsorptionLength );

		std::cout << "EJ280_AbsorptionLength entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;


         //-----------------------------
	     // Retrieve index of refraction
		 //-----------------------------

	     const G4int EJ280_IndexOfRefraction_NumberOfEntries   = 100;

	     // defining optical range of interest
	     G4double fEJ280_IndexOfRefraction_LambdaMin       = 200.0*CLHEP::nanometer;
	     G4double fEJ280_IndexOfRefraction_LambdaMax       = 800.0*CLHEP::nanometer;
	     G4double fEJ280_IndexOfRefraction_PhotonEnergyMin = this->LambdaToEnergyConversion(fEJ280_IndexOfRefraction_LambdaMax);
	     G4double fEJ280_IndexOfRefraction_PhotonEnergyMax = this->LambdaToEnergyConversion(fEJ280_IndexOfRefraction_LambdaMin);


          // now loop over wavelength and calculate/lookup the index of refraction
	      // and store the results into vectors

	     G4double fCurrent_PhotonEnergy = 0.0;
	     G4double fCurrent_WaveLength   = 0.0;

	       for (G4int i=0; i<EJ280_IndexOfRefraction_NumberOfEntries; i++)
	       {
	    	   fCurrent_PhotonEnergy  = fEJ280_IndexOfRefraction_PhotonEnergyMin + (fEJ280_IndexOfRefraction_PhotonEnergyMax - fEJ280_IndexOfRefraction_PhotonEnergyMin) / (EJ280_IndexOfRefraction_NumberOfEntries - 1) * i;
               fCurrent_WaveLength    = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy);

               pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
                                                                                                              fPVT_Sellmeier_A,
                                                                                                              fPVT_Sellmeier_B1, fPVT_Sellmeier_B2, fPVT_Sellmeier_B3,
                                                                                                              fPVT_Sellmeier_C1, fPVT_Sellmeier_C2, fPVT_Sellmeier_C3) );
	       }

	    //----------------------------------
		// Define material properties table
	    //----------------------------------
	    pEJ280_MPT = nullptr;
		pEJ280_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pEJ280_MPT->AddProperty("RINDEX"          , pMPV_RINDEX           );
		pEJ280_MPT->AddProperty("WLSCOMPONENT"    , pMPV_EmissionSpectrum );
		pEJ280_MPT->AddProperty("WLSABSLENGTH"    , pMPV_OpticalTransmission_AbsorptionLength );

		pEJ280_MPT->AddConstProperty("WLSTIMECONSTANT", 8.5*CLHEP::ns );
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pEJ280_MD->SetMaterialPropertiesTable(pEJ280_MPT);

	} // end if (pMat == nullptr)

	return pEJ280_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//=========================================================
//                      Inorganic Scintillators
//=========================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BaF2_Template()
{
	  G4String sName    = "BaF2_Template";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//--------------
		// BaF2 Template
		//--------------

		const G4int fBaF2_MaterialPropertiesNEntries = 10;

		G4double fBaF2_PhotonEnergy     [fBaF2_MaterialPropertiesNEntries] = { 1.2*CLHEP::eV, 3.99*CLHEP::eV, 4.00*CLHEP::eV, 4.01*CLHEP::eV, 4.02*CLHEP::eV, 5.63*CLHEP::eV, 5.64*CLHEP::eV, 6.36*CLHEP::eV, 6.37*CLHEP::eV, 6.5*CLHEP::eV }; // Saint-Gobain
		G4double fBaF2_FastComponent    [fBaF2_MaterialPropertiesNEntries] = { 0.0,    0.0,     0.0,     0.0,     0.0,     0.0,     0.67,    0.33,    0.0,     0.0    }; // Saint-Gobain
		G4double fBaF2_SlowComponent    [fBaF2_MaterialPropertiesNEntries] = { 0.0,    0.0,     1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0    }; // Saint-Gobain
		G4double fBaF2_RefractionIndex  [fBaF2_MaterialPropertiesNEntries] = { 1.50,   1.50,    1.50,    1.50,    1.50,    1.54,    1.54,    1.54,    1.54,    1.54   }; // Saint-Gobain
		G4double fBaF2_AbsorptionLength [fBaF2_MaterialPropertiesNEntries] = { 34*CLHEP::cm,  34*CLHEP::cm,   34*CLHEP::cm,   34*CLHEP::cm,   34*CLHEP::cm,   34*CLHEP::cm,   34*CLHEP::cm,   34*CLHEP::cm,   34*CLHEP::cm,   34*CLHEP::cm  }; // Ma1992-2

		//---------------------
		// Define new material
		//---------------------
		pBaF2_Template_MD = nullptr;
		pBaF2_Template_MD = pNistMaterialManager->FindOrBuildMaterial("G4_BARIUM_FLUORIDE");
		pBaF2_Template_MD->SetName(sName);

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pBaF2_Template_MPT = nullptr;
		pBaF2_Template_MPT = new G4MaterialPropertiesTable();

		pBaF2_Template_MPT->AddProperty("FASTCOMPONENT"             , fBaF2_PhotonEnergy, fBaF2_FastComponent   , fBaF2_MaterialPropertiesNEntries)->SetSpline(true);
		pBaF2_Template_MPT->AddProperty("SLOWCOMPONENT"             , fBaF2_PhotonEnergy, fBaF2_SlowComponent   , fBaF2_MaterialPropertiesNEntries)->SetSpline(true);
		pBaF2_Template_MPT->AddProperty("RINDEX"                    , fBaF2_PhotonEnergy, fBaF2_RefractionIndex , fBaF2_MaterialPropertiesNEntries)->SetSpline(true);
		pBaF2_Template_MPT->AddProperty("ABSLENGTH"                 , fBaF2_PhotonEnergy, fBaF2_AbsorptionLength, fBaF2_MaterialPropertiesNEntries)->SetSpline(true);

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pBaF2_Template_MD->SetMaterialPropertiesTable(pBaF2_Template_MPT);

	} // end if (pMat == nullptr)

	return pBaF2_Template_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BaF2()
{
	  G4String sName    = "BaF2";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//----------------
		// Use a template
		//----------------
		this->GetMaterial_BaF2_Template();

		//---------------------
		// Define new material
		//---------------------
		pBaF2_MD = nullptr;
		pBaF2_MD = new G4Material( sName,
				                   pBaF2_Template_MD->GetDensity(),
								   pBaF2_Template_MD,
								   kStateSolid );

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pBaF2_MPT = nullptr;
		pBaF2_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pBaF2_MPT->AddConstProperty("SCINTILLATIONYIELD"   ,  11.8/CLHEP::keV  ); // saint-gobain
		pBaF2_MPT->AddConstProperty("RESOLUTIONSCALE"      ,   1.0             ); // photon fluctuation = ResolutionScale * sqrt(MeanNumberOfPhotons)
//		pBaF2_MPT->AddConstProperty("FASTTIMECONSTANT"     ,   0.7*CLHEP::ns   ); // saint-gobain
//		pBaF2_MPT->AddConstProperty("SLOWTIMECONSTANT"     , 630.0*CLHEP::ns   ); // saint-gobain
//		pBaF2_MPT->AddConstProperty("YIELDRATIO"           ,   0.15            ); // relative strength of fast component as a fraction of total scintillation yield

		pBaF2_MPT->AddConstProperty("FASTTIMECONSTANT"     ,  630.0*CLHEP::ns  );
		pBaF2_MPT->AddConstProperty("YIELDRATIO"           ,    1.0            ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pBaF2_MD->SetMaterialPropertiesTable(pBaF2_MPT);

	} // end if (pMat == nullptr)

	return pBaF2_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BaF2_Immediate()
{
	  G4String sName    = "BaF2_Immediate";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=====================
		//    Immediate BaF2
		//=====================

		//----------------
		// Use a template
		//----------------
		this->GetMaterial_BaF2_Template();

		//---------------------
		// Define new material
		//---------------------
		pBaF2_Immediate_MD = nullptr;
		pBaF2_Immediate_MD = new G4Material( sName,
				                             pBaF2_Template_MD->GetDensity(),
											 pBaF2_Template_MD,
											 kStateSolid );

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pBaF2_Immediate_MPT = nullptr;
		pBaF2_Immediate_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pBaF2_Immediate_MPT->AddConstProperty("SCINTILLATIONYIELD" ,  11.8/CLHEP::keV     );  // saint-gobain
		pBaF2_Immediate_MPT->AddConstProperty("RESOLUTIONSCALE"    ,   1.0                );  // photon fluctuation = ResolutionScale * sqrt(MeanNumberOfPhotons)
		pBaF2_Immediate_MPT->AddConstProperty("FASTTIMECONSTANT"   ,   0.000001*CLHEP::ns );  // immediate
		pBaF2_Immediate_MPT->AddConstProperty("SLOWTIMECONSTANT"   ,   0.000001*CLHEP::ns );  // immediate
		pBaF2_Immediate_MPT->AddConstProperty("YIELDRATIO"         ,   0.15               ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pBaF2_Immediate_MD->SetMaterialPropertiesTable(pBaF2_Immediate_MPT);

	} // end if (pMat == nullptr)

	return pBaF2_Immediate_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BaF2_Perfect()
{
	  G4String sName   = "BaF2_Perfect";

	  // Check whether material exists already in the materials table
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//=======================
		//      Perfect BaF2
		//=======================

		//----------------
		// Use a template
		//----------------
		this->GetMaterial_BaF2_Template();

		//---------------------
		// Define new material
		//---------------------
		pBaF2_Perfect_MD = nullptr;
		pBaF2_Perfect_MD = new G4Material( sName,
				                           pBaF2_Template_MD->GetDensity(),
										   pBaF2_Template_MD,
										   kStateSolid );

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pBaF2_Perfect_MPT = nullptr;
		pBaF2_Perfect_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pBaF2_Perfect_MPT->AddConstProperty("SCINTILLATIONYIELD"  , 100.0/CLHEP::keV     ); // perfect
		pBaF2_Perfect_MPT->AddConstProperty("RESOLUTIONSCALE"     ,   1.0                ); // photon fluctuation = ResolutionScale * sqrt(MeanNumberOfPhotons)
		pBaF2_Perfect_MPT->AddConstProperty("FASTTIMECONSTANT"    ,   0.000001*CLHEP::ns ); // perfect
		pBaF2_Perfect_MPT->AddConstProperty("SLOWTIMECONSTANT"    ,   0.000001*CLHEP::ns ); // perfect
		pBaF2_Perfect_MPT->AddConstProperty("YIELDRATIO"          ,   0.15               ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pBaF2_Perfect_MD ->SetMaterialPropertiesTable(pBaF2_Perfect_MPT);

	} // end if (pMat == nullptr)

	return pBaF2_Perfect_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BaF2_PerfectFast()
{
	  G4String sName   = "BaF2_PerfectFast";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
		//====================
		//  PerfectFast BaF2
		//====================

		//----------------
		// Use a template
		//----------------
		this->GetMaterial_BaF2_Template();

		//---------------------
		// Define new material
		//---------------------
		pBaF2_PerfectFast_MD = nullptr;
		pBaF2_PerfectFast_MD = new G4Material( sName,
				                               pBaF2_Template_MD->GetDensity(),
											   pBaF2_Template_MD,
											   kStateSolid );

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pBaF2_PerfectFast_MPT = nullptr;
		pBaF2_PerfectFast_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pBaF2_PerfectFast_MPT->AddConstProperty("SCINTILLATIONYIELD" , 100.0/CLHEP::keV     ); // perfect
		pBaF2_PerfectFast_MPT->AddConstProperty("RESOLUTIONSCALE"    ,   1.0                ); // photon fluctuation = ResolutionScale * sqrt(MeanNumberOfPhotons)
		pBaF2_PerfectFast_MPT->AddConstProperty("FASTTIMECONSTANT"   ,   0.000001*CLHEP::ns ); // perfect
		pBaF2_PerfectFast_MPT->AddConstProperty("YIELDRATIO"         ,   1.0                ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pBaF2_PerfectFast_MD->SetMaterialPropertiesTable(pBaF2_PerfectFast_MPT);

	} // end if (pMat == nullptr)

	return pBaF2_PerfectFast_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_LaBr3_Ce()
{
	  G4String sName    = "LaBr3_Ce";
	  G4double fDensity = 5.29*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		//======
		// LaBr
		//======

		//-----------------
		// Define material
		//-----------------
		G4int ncomponents;

		pLaBr3_Ce_MD = nullptr;
		pLaBr3_Ce_MD = new G4Material( sName,
				                       fDensity,
									   ncomponents = 3,
									   kStateSolid) ;

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

		pLaBr3_Ce_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_La"), fFractionOfMass = 36.62*CLHEP::perCent);
		pLaBr3_Ce_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Br"), fFractionOfMass = 63.19*CLHEP::perCent);
		pLaBr3_Ce_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ce"), fFractionOfMass =  0.19*CLHEP::perCent); // loef2001


	    //-----------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //-----------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_EmissionSpectrum           = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH           = new G4MaterialPropertyVector();
		//--
		G4MaterialPropertyVector* pMPV_ALPHASCINTILLATIONYIELD    = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_ELECTRONSCINTILLATIONYIELD = new G4MaterialPropertyVector();
		//-----------------------------------------------------------------------------------------

		G4double fLaBr3_Ce_LightYield = 61.0/CLHEP::keV; // Taken from Saint Gobain

	    //------------------------------------------------------------------------------------------------------------------
		// Read emission spectrum from file
	    //------------------------------------------------------------------------------------------------------------------
		this->Initialize_EmissionSpectrum("/LaBr3/LaBr3_Ce_EmissionSpectrum.dat",
										   pMPV_EmissionSpectrum );

		std::cout << "LaBr3_Ce_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;


	    //------------------------------------------------------------------------------------------------------------------
		// Read energy response of alpha and beta radiation from file
		// => Light yield is a function of kinetic energy and depends on particle type
	    //------------------------------------------------------------------------------------------------------------------
		this->Initialize_EnergyResponse("/LaBr3/LaBr3_Ce_EnergyResponse.dat",
				                         fLaBr3_Ce_LightYield,
						                 pMPV_ALPHASCINTILLATIONYIELD,
						                 pMPV_ELECTRONSCINTILLATIONYIELD);

		std::cout << "LaBr3_Ce_EnergyResponse entries = " << pMPV_ELECTRONSCINTILLATIONYIELD->GetVectorLength() << std::endl;

	    //------------------------------------------------------------------------------------------------------------------
		// Read optical absorption length from file
	    //------------------------------------------------------------------------------------------------------------------
		this->Initialize_AbsorptionLength("/LaBr3/LaBr3_Ce_AbsorptionLength.dat",
				                           pMPV_ABSORPTIONLENGTH );

		std::cout << "LaBr3_Ce_AbsorptionLength entries = " << pMPV_ABSORPTIONLENGTH->GetVectorLength() << std::endl;



	     // setup
		     const G4int NEntries_LaBr3_Ce   = 100;

		     //------------------------------------
		     // Defining optical range of interest
		     //------------------------------------
		     G4double fLaBr3_Ce_LambdaMin       = 200.0*CLHEP::nanometer;
		     G4double fLaBr3_Ce_LambdaMax       = 800.0*CLHEP::nanometer;
		     G4double fLaBr3_Ce_PhotonEnergyMin = this->LambdaToEnergyConversion(fLaBr3_Ce_LambdaMax);
		     G4double fLaBr3_Ce_PhotonEnergyMax = this->LambdaToEnergyConversion(fLaBr3_Ce_LambdaMin);

		     // now loop over wavelength and calculate/lookup the index of refraction and absorption length
		     // and store the results into vectors

		     G4double fCurrent_PhotonEnergy = 0.0;
		     G4double fCurrent_WaveLength   = 0.0;

		       for (G4int i=0; i<NEntries_LaBr3_Ce; i++)
		       {
		    	   fCurrent_PhotonEnergy = fLaBr3_Ce_PhotonEnergyMin + (fLaBr3_Ce_PhotonEnergyMax - fLaBr3_Ce_PhotonEnergyMin) / (NEntries_LaBr3_Ce - 1) * i;
		    	   fCurrent_WaveLength   = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy);

		    	   pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetLaBr3_Ce_RefractionIndex(fCurrent_WaveLength) );

		       }

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pLaBr3_Ce_MPT = nullptr;
		pLaBr3_Ce_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pLaBr3_Ce_MPT->AddProperty("FASTCOMPONENT"               , pMPV_EmissionSpectrum );
		pLaBr3_Ce_MPT->AddProperty("RINDEX"                      , pMPV_RINDEX           );
		pLaBr3_Ce_MPT->AddProperty("ABSLENGTH"                   , pMPV_ABSORPTIONLENGTH );

		// Non-linear light yield depending on particle energy
		pLaBr3_Ce_MPT->AddProperty("ALPHASCINTILLATIONYIELD"     , pMPV_ALPHASCINTILLATIONYIELD    );
		pLaBr3_Ce_MPT->AddProperty("ELECTRONSCINTILLATIONYIELD"  , pMPV_ELECTRONSCINTILLATIONYIELD );

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pLaBr3_Ce_MPT->AddConstProperty("SCINTILLATIONYIELD"     , fLaBr3_Ce_LightYield ); // saint-gobain
		pLaBr3_Ce_MPT->AddConstProperty("RESOLUTIONSCALE"        ,  1.0                 ); // photon fluctuation = ResolutionScale * sqrt(MeanNumberOfPhotons)
		pLaBr3_Ce_MPT->AddConstProperty("FASTTIMECONSTANT"       , 16.0*CLHEP::ns       ); // saint-gobain
		pLaBr3_Ce_MPT->AddConstProperty("YIELDRATIO"             ,  1.0                 ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pLaBr3_Ce_MD->SetMaterialPropertiesTable(pLaBr3_Ce_MPT);

	} // end if (pMat == nullptr)

	return pLaBr3_Ce_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_LYSO_Ce()
{
	  G4String sName    = "LYSO_Ce";
	  G4double fDensity = 7.40*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		//---------------------------------------------------------------------------------------
		// LYSO : Linear Formula:  Lu_{1.8}Y_{0.2}Si_{1}O_{5}(Ce) == Lu_{9}Y_{1}Si_{5}O_{25}(Ce)
		//---------------------------------------------------------------------------------------

		//----------------------------------
		// Assemble LYSO without any dopant
		//----------------------------------
		G4int ncomponents;

		pLYSO_MD = nullptr;
		pLYSO_MD = new G4Material( "LYSO",
				                    fDensity,
									ncomponents = 4,
									kStateSolid );

		//-------------------------------------------------------------------------------------------------------------------------
		//	LYSO being a Lutetium-based scintillator means that it contains the naturally occurring radioactive isotope Lu-176
		//	with a natural abundance of 2.59 %. It is a β-emitter with a half-life t1/2 =3.56 × 1010 y and 99.66 % of
		//	the time it decays to the 597 keV excited state of 176H f , [26]. From this state it decays through a cascade of three
		//	prompt gamma rays with the following energies (in order of the cascade): 307 keV, 202 keV and 88 keV.
		//-------------------------------------------------------------------------------------------------------------------------


		//-------------------------------------------------
		// Assembling the ingredients: LYSO without dopant
		//-------------------------------------------------
		G4int  natoms;
		G4bool bIsotopes;

		pLYSO_MD->AddElement( pNistMaterialManager->FindOrBuildElement("Lu", bIsotopes=true) , natoms =  9 );
		pLYSO_MD->AddElement( pNistMaterialManager->FindOrBuildElement("Y",  bIsotopes=true) , natoms =  1 );
		pLYSO_MD->AddElement( pNistMaterialManager->FindOrBuildElement("Si", bIsotopes=true) , natoms =  5 );
		pLYSO_MD->AddElement( pNistMaterialManager->FindOrBuildElement("O",  bIsotopes=true) , natoms = 25 );

		//-----------------
		// Define material
		//-----------------
		pLYSO_Ce_MD = nullptr;
		pLYSO_Ce_MD = new G4Material( sName,
				                      fDensity,
									  ncomponents = 2,
									  kStateSolid );

		//----------------------------------
		// Assemble LYSO including a dopant
		//----------------------------------
	    G4double fFractionOfMass;

		pLYSO_Ce_MD->AddMaterial( pLYSO_MD                                                          , fFractionOfMass = 99.8*CLHEP::perCent);
		pLYSO_Ce_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ce", bIsotopes=true), fFractionOfMass =  0.2*CLHEP::perCent); // Level of Ce dopant

	    //-----------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //-----------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_EmissionSpectrum           = new G4MaterialPropertyVector();
		//---
		G4MaterialPropertyVector* pMPV_REALRINDEX                 = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX            = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH           = new G4MaterialPropertyVector();
		//---
		G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH           = new G4MaterialPropertyVector();
		//--
		G4MaterialPropertyVector* pMPV_ALPHASCINTILLATIONYIELD    = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_ELECTRONSCINTILLATIONYIELD = new G4MaterialPropertyVector();
		//-----------------------------------------------------------------------------------------

		//------------------------------------------------------------------------------------------------------------------
		// Read emission spectrum from file
		//------------------------------------------------------------------------------------------------------------------
		this->Initialize_EmissionSpectrum("/LYSO/LYSO_Ce_EmissionSpectrum.dat",
										   pMPV_EmissionSpectrum );

		std::cout << "LYSO_Ce_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;


		//---------------------------------------------------------------------------------------------
	    // LYSO light yield and RESOLUTIONSCALE taken from:
		//
		// "Optimum performance investigation of LYSO crystal pixels: A comparison between GATE simulation and experimental data"
	    //  arXiv:1309.3736v1 [physics.ins-det] 15 Sep 2013
		//
		//----------------------------------------------------------------------------------------------
		//
		G4double fLYSO_Ce_LightYield = 26.0/CLHEP::keV;

	    //------------------------------------------------------------------------------------------------------------------
		// Read energy response of alpha and beta radiation from file
		// => Light yield is a function of kinetic energy and depends on particle type
	    //------------------------------------------------------------------------------------------------------------------
		this->Initialize_EnergyResponse("/LYSO/LYSO_Ce_EnergyResponse.dat",
				                         fLYSO_Ce_LightYield,
										 pMPV_ALPHASCINTILLATIONYIELD,
										 pMPV_ELECTRONSCINTILLATIONYIELD);

		std::cout << "LYSO_Ce_EnergyResponse entries = " << pMPV_ELECTRONSCINTILLATIONYIELD->GetVectorLength() << std::endl;

		//------------------------------------------------------------------------------------------------------------------
		// Read absorption length from file
		//------------------------------------------------------------------------------------------------------------------
		this->Initialize_AbsorptionLength("/LYSO/LYSO_Ce_AbsorptionLength.dat",
				                           pMPV_ABSORPTIONLENGTH );

		std::cout << "LYSO_Ce_AbsorptionLength entries = " << pMPV_ABSORPTIONLENGTH->GetVectorLength() << std::endl;


		//------------------------------------------------------------------------------------------------------------------
		// Read index of refraction from file
		//------------------------------------------------------------------------------------------------------------------
		this->Initialize_IndexOfRefraction("/LYSO/LYSO_Ce_IndexOfRefraction.dat",
				                            pMPV_REALRINDEX,
				                            pMPV_IMAGINARYRINDEX,
				                            pMPV_RINDEX,
				                            pMPV_RINDEX_ABSLENGTH);

		std::cout << "LYSO_Ce_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pLYSO_Ce_MPT = nullptr;
		pLYSO_Ce_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pLYSO_Ce_MPT->AddProperty("FASTCOMPONENT"                , pMPV_EmissionSpectrum );
		pLYSO_Ce_MPT->AddProperty("RINDEX"                       , pMPV_RINDEX           );
		pLYSO_Ce_MPT->AddProperty("ABSLENGTH"                    , pMPV_ABSORPTIONLENGTH );

		// Non-linear light yield depending on particle energy
		pLYSO_Ce_MPT->AddProperty("ALPHASCINTILLATIONYIELD"      , pMPV_ALPHASCINTILLATIONYIELD    );
		pLYSO_Ce_MPT->AddProperty("ELECTRONSCINTILLATIONYIELD"   , pMPV_ELECTRONSCINTILLATIONYIELD );

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pLYSO_Ce_MPT->AddConstProperty("SCINTILLATIONYIELD"      , fLYSO_Ce_LightYield  );
		pLYSO_Ce_MPT->AddConstProperty("RESOLUTIONSCALE"         ,  1.0                 ); // photon fluctuation = ResolutionScale * sqrt(MeanNumberOfPhotons)
//		pLYSO_Ce_MPT->AddConstProperty("RESOLUTIONSCALE"         ,  6.8                 ); // photon fluctuation = ResolutionScale * sqrt(MeanNumberOfPhotons)
		pLYSO_Ce_MPT->AddConstProperty("FASTTIMECONSTANT"        , 40.0*CLHEP::ns       );
		pLYSO_Ce_MPT->AddConstProperty("YIELDRATIO"              ,  1.0                 ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pLYSO_Ce_MD->SetMaterialPropertiesTable(pLYSO_Ce_MPT);

	} // end  if (pMat == nullptr)

	return pLYSO_Ce_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_NaI()
{
	  G4String sName    = "NaI";
	  G4double fDensity = 3.67*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		  //=============
		  //   NaI(Tl)
		  //=============

		  //-----------------
		  // Define material
		  //-----------------
		  G4int ncomponents;

		  pNaI_MD = nullptr;
		  pNaI_MD = new G4Material( sName,
				                    fDensity,
									ncomponents = 3,
									kStateSolid );

		  //----------------------------
		  // Assembling the ingredients
		  //----------------------------
		  G4double fFractionOfMass;

		  pNaI_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Na"), fFractionOfMass = 15.32*CLHEP::perCent);
		  pNaI_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_I" ), fFractionOfMass = 84.60*CLHEP::perCent);
		  pNaI_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Tl"), fFractionOfMass =  0.08*CLHEP::perCent); // trefilova2002


		  //-----------------------------------------------------------------------------------------
		  //               Store file bases material properties in these vectors
		  //-----------------------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_EmissionSpectrum = new G4MaterialPropertyVector();
		  G4MaterialPropertyVector* pMPV_RINDEX           = new G4MaterialPropertyVector();
		  G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH = new G4MaterialPropertyVector();
		  //--
		  G4MaterialPropertyVector* pMPV_ALPHASCINTILLATIONYIELD    = new G4MaterialPropertyVector();
		  G4MaterialPropertyVector* pMPV_ELECTRONSCINTILLATIONYIELD = new G4MaterialPropertyVector();
		  //-----------------------------------------------------------------------------------------

		  G4double fNaI_LightYield = 38.0/CLHEP::keV; // Taken from Saint Gobain

		  //------------------------------------------------------------------------------------------------------------------
		  // Read emission spectrum from file
		  //------------------------------------------------------------------------------------------------------------------
		  this->Initialize_EmissionSpectrum("/NaI/NaI_EmissionSpectrum.dat",
											 pMPV_EmissionSpectrum );

		  std::cout << "NaI_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;

		  //------------------------------------------------------------------------------------------------------------------
		  // Read energy response of alpha and beta radiation from file
		  // => Light yield is a function of kinetic energy and depends on particle type
		  //------------------------------------------------------------------------------------------------------------------
		  this->Initialize_EnergyResponse("/NaI/NaI_EnergyResponse.dat",
				                           fNaI_LightYield,
						                   pMPV_ALPHASCINTILLATIONYIELD,
						                   pMPV_ELECTRONSCINTILLATIONYIELD);

		  std::cout << "NaI_EnergyResponse entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;

		  //------------------------------------------------------------------------------------------------------------------
		  // Read optical absorption length from file
		  //------------------------------------------------------------------------------------------------------------------
		  this->Initialize_AbsorptionLength("/NaI/NaI_AbsorptionLength.dat",
				                             pMPV_ABSORPTIONLENGTH );

		  std::cout << "NaI_AbsorptionLength entries = " <<  pMPV_ABSORPTIONLENGTH->GetVectorLength()  << std::endl;


	    // setup
		const G4int NEntries_NaI   = 100;

		//------------------------------------
		// Defining optical range of interest
		//------------------------------------
		G4double fNaI_LambdaMin       = 200.0*CLHEP::nanometer;
		G4double fNaI_LambdaMax       = 800.0*CLHEP::nanometer;
		G4double fNaI_PhotonEnergyMin = this->LambdaToEnergyConversion(fNaI_LambdaMax);
		G4double fNaI_PhotonEnergyMax = this->LambdaToEnergyConversion(fNaI_LambdaMin);

		// NaI:Tl dispersion constants (regarding wavelength given in micrometers)
		// See http://refractiveindex.info/?shelf=main&book=NaI&page=Li
		G4double fNaI_Sellmeier_A  =   1.478;
		G4double fNaI_Sellmeier_B1 =   1.532;
		G4double fNaI_Sellmeier_B2 =   4.27;
		G4double fNaI_Sellmeier_B3 =   0.0;
		G4double fNaI_Sellmeier_C1 =   0.17;
		G4double fNaI_Sellmeier_C2 =  86.21;
		G4double fNaI_Sellmeier_C3 =   0.0;

		G4double fNaI_SellmeierCoefficients[7] = { fNaI_Sellmeier_A,
	    		                                   fNaI_Sellmeier_B1, fNaI_Sellmeier_B2, fNaI_Sellmeier_B3,
	    		                                   fNaI_Sellmeier_C1, fNaI_Sellmeier_C2, fNaI_Sellmeier_C3 };

		// now loop over wavelength and calculate/lookup the index of refraction and absorption length
		// and store the results into vectors

		G4double fCurrent_PhotonEnergy = 0.0;
		G4double fCurrent_WaveLength   = 0.0;

		for (G4int i=0; i<NEntries_NaI; i++)
		{
			fCurrent_PhotonEnergy     = fNaI_PhotonEnergyMin + (fNaI_PhotonEnergyMax - fNaI_PhotonEnergyMin) / (NEntries_NaI - 1) * i;
			fCurrent_WaveLength       = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy); //nm

			pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
	    			                                                                                       fNaI_Sellmeier_A,
																					                       fNaI_Sellmeier_B1, fNaI_Sellmeier_B2, fNaI_Sellmeier_B3,
																					                       fNaI_Sellmeier_C1, fNaI_Sellmeier_C2, fNaI_Sellmeier_C3 ));
		} // end for


		//----------------------------------
		// Define material properties table
		//----------------------------------
		pNaI_MPT = nullptr;
		pNaI_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pNaI_MPT->AddProperty("FASTCOMPONENT"                  , pMPV_EmissionSpectrum );
		pNaI_MPT->AddProperty("SLOWCOMPONENT"                  , pMPV_EmissionSpectrum );
		pNaI_MPT->AddProperty("RINDEX"                         , pMPV_RINDEX           );
		pNaI_MPT->AddProperty("ABSLENGTH"                      , pMPV_ABSORPTIONLENGTH );

	//  pNaI_MPT->AddProperty("ELECTRONSCINTILLATIONYIELD"     , vNaI_EnergyResponse_Energy, fNaI_LightYieldAbsolute_Beta , fNaI_LightYieldAbsolute_Beta.size() )->SetSpline(true);
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pNaI_MPT->AddConstProperty("SCINTILLATIONYIELD"        , fNaI_LightYield  ); // saint-gobain
		pNaI_MPT->AddConstProperty("RESOLUTIONSCALE"           ,   4.0            ); // photon fluctuation = ResolutionScale * sqrt(MeanNumberOfPhotons)
//		pNaI_MPT->AddConstProperty("FASTTIMECONSTANT"          , 245.0*CLHEP::ns  ); // saint-gobain
//		pNaI_MPT->AddConstProperty("SLOWTIMECONSTANT"          , 245.0*CLHEP::ns  ); // saint-gobain
//		pNaI_MPT->AddConstProperty("SLOWSCINTILLATIONRISETIME" ,  27.0*CLHEP::ns  ); // weber2000
//		pNaI_MPT->AddConstProperty("YIELDRATIO"                ,   0.5            ); // relative strength of fast component as a fraction of total scintillation yield
//
		pNaI_MPT->AddConstProperty("FASTTIMECONSTANT"          , 245.0*CLHEP::ns  ); // saint-gobain
		pNaI_MPT->AddConstProperty("YIELDRATIO"                ,   1.0            ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//---------------------------------------------------------------------------------------------------
		// RESOLUTIONSCALE taken from
		// http://www.ipnl.in2p3.fr/perso/stezowski/SToGS/shtmldoc/SToGS__MaterialConsultant_8cc_source.html
		//---------------------------------------------------------------------------------------------------

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pNaI_MD->SetMaterialPropertiesTable(pNaI_MPT);

	} // end if (pMat == nullptr)

	return pNaI_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_ZnS_Ag()
{
	  G4String sName    = "ZnS_Ag";
	  G4double fDensity = 4.09*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
	    //============
	    //   ZnS(Ag)
	    //============

		//================================================================================
		//          common Zns:Ag (P22B-X alias GL47/N-C2), 8um median particle size
		// See http://www.phosphor-technology.com/products/crt.htm
		// see IdentNr. 45713AA
		//================================================================================


		//-----------------------------------------------------------------------------------------
		//               Store file bases material properties in these vectors
		//-----------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_EmissionSpectrum           = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_RINDEX                     = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH           = new G4MaterialPropertyVector();
		//--
		G4MaterialPropertyVector* pMPV_ALPHASCINTILLATIONYIELD    = new G4MaterialPropertyVector();
		G4MaterialPropertyVector* pMPV_ELECTRONSCINTILLATIONYIELD = new G4MaterialPropertyVector();
		//-----------------------------------------------------------------------------------------

		G4double fZnS_Ag_LightYield = 38.0/CLHEP::keV * 1.30; // ZnS data sheet: 130% relative light yield compared to NaI = 49.4 photons/keV

	    //------------------------------------------------------------------------------------------------------------------
		// Read emission spectrum from file
	    //------------------------------------------------------------------------------------------------------------------
		this->Initialize_EmissionSpectrum("/ZnS_Ag/ZnS_Ag_EmissionSpectrum.dat",
				                           pMPV_EmissionSpectrum );

		std::cout << "ZnS_Ag_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;

	    //------------------------------------------------------------------------------------------------------------------
		// Read energy response of alpha and beta radiation from file
		// => Light yield is a function of kinetic energy and depends on particle type
	    //------------------------------------------------------------------------------------------------------------------
		this->Initialize_EnergyResponse("/ZnS_Ag/ZnS_Ag_EnergyResponse.dat",
                                         fZnS_Ag_LightYield,
					                     pMPV_ALPHASCINTILLATIONYIELD,
					                     pMPV_ELECTRONSCINTILLATIONYIELD);

		std::cout << "ZnS_Ag_EnergyResponse entries = " << pMPV_ELECTRONSCINTILLATIONYIELD->GetVectorLength() << std::endl;

		//----------------------------------------------------------------------------------------------------------------
		// The optical absorption length is defined via the Setter function this->SetOpticalAbsorptionLength_ZnS_Ag(value)
		// This value is assigned for the entire optical range (crude approximation)
		//----------------------------------------------------------------------------------------------------------------

		//------------------------------------
		// Defining optical range of interest
		//------------------------------------
		G4double fZnS_Ag_LambdaMin       = 180.0*CLHEP::nanometer;
		G4double fZnS_Ag_LambdaMax       = 800.0*CLHEP::nanometer;
		G4double fZnS_Ag_PhotonEnergyMin = this->LambdaToEnergyConversion(fZnS_Ag_LambdaMax);
		G4double fZnS_Ag_PhotonEnergyMax = this->LambdaToEnergyConversion(fZnS_Ag_LambdaMin);

		// Number of bins for material properties table
		const UInt_t nentries_ZnS_Ag = 1000;

		// ZnS:Ag dispersion constants (regarding wavelength given in micrometers)
		// http://refractiveindex.info/?shelf=main&book=ZnS&page=Debenham
		G4double fZnS_Ag_Sellmeier_A  =      8.393;
		G4double fZnS_Ag_Sellmeier_B1 =      0.14383;
		G4double fZnS_Ag_Sellmeier_B2 =   4430.99;
		G4double fZnS_Ag_Sellmeier_B3 =      0.0;
		G4double fZnS_Ag_Sellmeier_C1 =      0.2421;
		G4double fZnS_Ag_Sellmeier_C2 =     36.71;
		G4double fZnS_Ag_Sellmeier_C3 =      0.0;

		G4double fZnS_Ag_SellmeierCoefficients[7] = { fZnS_Ag_Sellmeier_A,
	    		                                      fZnS_Ag_Sellmeier_B1, fZnS_Ag_Sellmeier_B2, fZnS_Ag_Sellmeier_B3,
	    		                                      fZnS_Ag_Sellmeier_C1, fZnS_Ag_Sellmeier_C2, fZnS_Ag_Sellmeier_C3 };

		// now loop over wavelength and calculate/lookup the index of refraction and absorption length
		// and store the results into vectors

		G4double fCurrent_PhotonEnergy       = 0.0;
		G4double fCurrent_WaveLength         = 0.0;

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		for (UInt_t i=0; i<nentries_ZnS_Ag; i++)
		{
			fCurrent_PhotonEnergy = fZnS_Ag_PhotonEnergyMin + (fZnS_Ag_PhotonEnergyMax - fZnS_Ag_PhotonEnergyMin) / (nentries_ZnS_Ag - 1) * i;
			fCurrent_WaveLength   = this->LambdaToEnergyConversion(fCurrent_PhotonEnergy);

			pMPV_RINDEX->InsertValues(fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
		    			                                                                                   fZnS_Ag_Sellmeier_A,
																						                   fZnS_Ag_Sellmeier_B1, fZnS_Ag_Sellmeier_B2, fZnS_Ag_Sellmeier_B3,
																						                   fZnS_Ag_Sellmeier_C1, fZnS_Ag_Sellmeier_C2, fZnS_Ag_Sellmeier_C3 ));

			pMPV_ABSORPTIONLENGTH ->InsertValues(fCurrent_PhotonEnergy, this->GetOpticalAbsorptionLength_ZnS_Ag (fCurrent_WaveLength) );

		} // end for
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//-----------------
		// Define material
		//-----------------
		G4int ncomponents;

		pZnS_Ag_MD = nullptr;
		pZnS_Ag_MD = new G4Material( sName,
				                     fDensity,
									 ncomponents = 3,
									 kStateSolid );

		//-------------------------------------------------------------------
		// Taken from http://webmineral.com/data/Matraite.shtml#.UuIh6LQwe70
		//-------------------------------------------------------------------

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

		pZnS_Ag_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Zn"), fFractionOfMass = 67.00*CLHEP::perCent);
		pZnS_Ag_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_S") , fFractionOfMass = 32.90*CLHEP::perCent);
		pZnS_Ag_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Ag"), fFractionOfMass =  0.10*CLHEP::perCent);

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pZnS_Ag_MPT = nullptr;
		pZnS_Ag_MPT = new G4MaterialPropertiesTable();

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		pZnS_Ag_MPT->AddProperty("FASTCOMPONENT"                , pMPV_EmissionSpectrum );
	//	pZnS_Ag_MPT->AddProperty("SlOWCOMPONENT"                , pMPV_EmissionSpectrum );
		pZnS_Ag_MPT->AddProperty("RINDEX"                       , pMPV_RINDEX           );
		pZnS_Ag_MPT->AddProperty("ABSLENGTH"                    , pMPV_ABSORPTIONLENGTH );
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		// Non-linear light yield depending on particle energy
		pZnS_Ag_MPT->AddProperty("ALPHASCINTILLATIONYIELD"      , pMPV_ALPHASCINTILLATIONYIELD    );
		pZnS_Ag_MPT->AddProperty("ELECTRONSCINTILLATIONYIELD"   , pMPV_ELECTRONSCINTILLATIONYIELD );

//		pZnS_Ag_MPT->AddConstProperty("ELECTRONYIELDRATIO"      ,  0.5  ); // relative strength of fast component as a fraction of total scintillation yield
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//---------------------------------------------------------------------------------------------------------------------------------
		// Decay components are taken from:
		// "An Investigation into the Growth and Characterisation of Thin Film Radioluminescent Phosphors for Neutron Diffration Analysis"
		//  Thomas Miller, Ph.D. thesis May 2010. Nottingham Trent University
		//---------------------------------------------------------------------------------------------------------------------------------

		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	    pZnS_Ag_MPT->AddConstProperty("SCINTILLATIONYIELD"      , fZnS_Ag_LightYield );
	    pZnS_Ag_MPT->AddConstProperty("RESOLUTIONSCALE"         ,     1.0            );
	    pZnS_Ag_MPT->AddConstProperty("FASTTIMECONSTANT"        , 37.1*CLHEP::microsecond  );
//	    pZnS_Ag_MPT->AddConstProperty("FASTTIMECONSTANT"        ,   866.0*CLHEP::nanosecond  );    // whole variety found in literature: 866.0ns @440nm taken here. Intensity: ~ 2%
//	    pZnS_Ag_MPT->AddConstProperty("SLOWTIMECONSTANT"        , 37100.0*CLHEP::nanosecond  );    // whole variety found in literature:  37.1us @540mn taken here. Intensity: ~98%

//	    pZnS_Ag_MPT->AddConstProperty("YIELDRATIO"              ,   0.5             );   // relative strength of fast component as a fraction of total scintillation yield
	                                                                                      // 1.0 == fast component only
	    pZnS_Ag_MPT->AddConstProperty("YIELDRATIO"              ,   1.0             );   // relative strength of fast component as a fraction of total scintillation yield
	                                                                                      // 1.0 == fast component only
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	    //----------------------------------------------
	    // Attach material properties table to material
	    //----------------------------------------------
	    pZnS_Ag_MD->SetMaterialPropertiesTable(pZnS_Ag_MPT);

	}
	else // end if (pMat == nullptr)
	{
		//-------------------------------------------------------------------------------------------------------------------------
		// When you are here then ZnS_Ag has been previously constructed. In this case we only update the optical absorption length
		//-------------------------------------------------------------------------------------------------------------------------

	    pZnS_Ag_MD->GetMaterialPropertiesTable()->RemoveProperty("ABSLENGTH");

		//----------------------------------------------------------------------------------------------------------------
		// The optical absorption length is defined via the Setter function this->SetOpticalAbsorptionLength_ZnS_Ag(value)
		// This value is assigned for the entire optical range (crude approximation)
		//----------------------------------------------------------------------------------------------------------------
		// defining optical range of interest
		G4double fZnS_Ag_LambdaMin       = 180.0*CLHEP::nanometer;
		G4double fZnS_Ag_LambdaMax       = 800.0*CLHEP::nanometer;
		G4double fZnS_Ag_PhotonEnergyMin = this->LambdaToEnergyConversion(fZnS_Ag_LambdaMax);
		G4double fZnS_Ag_PhotonEnergyMax = this->LambdaToEnergyConversion(fZnS_Ag_LambdaMin);

		// Number of bins for material properties table
		const UInt_t nentries_ZnS_Ag = 1000;

		// now loop over wavelength and calculate/lookup the index of refraction and absorption length
		// and store the results into vectors

	    //-----------------------------------------------------------------------------------------
	    //               Store file bases material properties in these vectors
	    //-----------------------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_ABSORPTIONLENGTH_NEW = new G4MaterialPropertyVector();
		//-----------------------------------------------------------------------------------------

		G4double fCurrent_PhotonEnergy = 0.0;
		G4double fCurrent_WaveLength   = 0.0;

		for (UInt_t i=0; i<nentries_ZnS_Ag; i++)
		{
			fCurrent_PhotonEnergy = fZnS_Ag_PhotonEnergyMin + (fZnS_Ag_PhotonEnergyMax - fZnS_Ag_PhotonEnergyMin) / (nentries_ZnS_Ag - 1) * i;
			fCurrent_WaveLength   = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy);

			pMPV_ABSORPTIONLENGTH_NEW ->InsertValues(fCurrent_PhotonEnergy, this->GetOpticalAbsorptionLength_ZnS_Ag(fCurrent_WaveLength) );

		} // end for

		pZnS_Ag_MD->GetMaterialPropertiesTable()->AddProperty("ABSLENGTH", pMPV_ABSORPTIONLENGTH_NEW );

	} // end if (mat == nullptr)

	  return pZnS_Ag_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....



//#####################################################################################################################
//#####################################################################################################################
//                      Materials for Vacuum Photomultipliers
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BialkaliCathode()
{
	  G4String sName    = "BialkaliCathode";
	  G4double fDensity = 4.28*CLHEP::g/CLHEP::cm3;

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	  if (pMat == nullptr)
	  {
		//=======================================================
		//              Bialkali Cathode (dummy)
		//=======================================================

		// S20 index of refraction taken from "Optics Communications, issue 180, 2000. p89–102"
		//
		// http://www.photek.com/pdf/Experimental-data-on-the-reflection-and-transmission-spectral-response-of-photocathodes.pdf
		//
		// => PMT operating in transmission mode: cathode index of refraction ca. 3.1

		//------------------------------------
		// Defining optical range of interest
		//------------------------------------
		G4double fBialkaliCathode_LambdaMin       = 180*CLHEP::nanometer;
		G4double fBialkaliCathode_LambdaMax       = 800*CLHEP::nanometer;
		G4double fBialkaliCathode_PhotonEnergyMin = this->LambdaToEnergyConversion(fBialkaliCathode_LambdaMax);
		G4double fBialkaliCathode_PhotonEnergyMax = this->LambdaToEnergyConversion(fBialkaliCathode_LambdaMin);

	  	// Number of bins for material properties table
		const G4int nentries_BialkaliCathode = 2;

		//------------------------------------
		// Optical properties of Photocathode
		//------------------------------------
		G4double fBialkaliCathode_PhotonEnergy [nentries_BialkaliCathode] = { fBialkaliCathode_PhotonEnergyMin, fBialkaliCathode_PhotonEnergyMax   };
//	    G4double fBialkaliCathode_RINDEX       [nentries_BialkaliCathode] = { 3.1      ,  3.1      }; // based on above mentioned paper
		G4double fBialkaliCathode_RINDEX       [nentries_BialkaliCathode] = { 1.57     ,  1.57     }; // matched to optical cement
		G4double fBialkaliCathode_ABSLENGTH    [nentries_BialkaliCathode] = { 1.e-6*CLHEP::mm ,  1.e-6*CLHEP::mm }; // absorb all


		//---------------------
		// Define new material
		//---------------------
		G4int ncomponents;

		pBialkaliCathode_MD = nullptr;
		pBialkaliCathode_MD = new G4Material( sName,
				                              fDensity,
											  ncomponents = 3,
											  kStateSolid) ;

	    //----------------------------
	    // Assembling the ingredients
	    //----------------------------
	    G4double fFractionOfMass;

		pBialkaliCathode_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_K"),  fFractionOfMass = 13.3*CLHEP::perCent);
		pBialkaliCathode_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Cs"), fFractionOfMass = 45.2*CLHEP::perCent);
		pBialkaliCathode_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Sb"), fFractionOfMass = 41.5*CLHEP::perCent);

		//----------------------------------
		// Define material properties table
		//----------------------------------
		pBialkaliCathode_MPT = nullptr;
		pBialkaliCathode_MPT = new G4MaterialPropertiesTable();

		pBialkaliCathode_MPT->AddProperty("RINDEX"    , fBialkaliCathode_PhotonEnergy, fBialkaliCathode_RINDEX   , nentries_BialkaliCathode)->SetSpline(true);
		pBialkaliCathode_MPT->AddProperty("ABSLENGTH" , fBialkaliCathode_PhotonEnergy, fBialkaliCathode_ABSLENGTH, nentries_BialkaliCathode)->SetSpline(true);

		//----------------------------------------------
		// Attach material properties table to material
		//----------------------------------------------
		pBialkaliCathode_MD->SetMaterialPropertiesTable(pBialkaliCathode_MPT);

	} // end if (pMat == nullptr)

	return pBialkaliCathode_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BorosilicateGlass()
{
	G4String sName    = "BorosilicateGlass";
	G4double fDensity = 2.23*CLHEP::g/CLHEP::cm3;

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //=======================================================
	  //              Borosilicate Glass
	  //=======================================================

      //------------------------------------
	  // Defining optical range of interest
	  //------------------------------------
	  G4double fBorosilicateGlass_LambdaMin       = 200.0*CLHEP::nanometer;
	  G4double fBorosilicateGlass_LambdaMax       = 800.0*CLHEP::nanometer;
	  G4double fBorosilicateGlass_PhotonEnergyMin = this->LambdaToEnergyConversion(fBorosilicateGlass_LambdaMax);
	  G4double fBorosilicateGlass_PhotonEnergyMax = this->LambdaToEnergyConversion(fBorosilicateGlass_LambdaMin);

	  // Number of bins for material properties table
	  const G4int NEntries_BorosilicateGlass = 100;

      //--------------------------------------------------------------------------------------
      // Borosilicate glass dispersion constants (regarding wavelength given in micrometers)
      // See http://refractiveindex.info/?shelf=glass&book=SCHOTT-BK&page=N-BK7
      //--------------------------------------------------------------------------------------
      G4double fBorosilicateGlass_Sellmeier_A  =   1.0;
      G4double fBorosilicateGlass_Sellmeier_B1 =   1.03961212;
      G4double fBorosilicateGlass_Sellmeier_B2 =   0.231792344;
      G4double fBorosilicateGlass_Sellmeier_B3 =   1.01046945;
      G4double fBorosilicateGlass_Sellmeier_C1 =   TMath::Sqrt(0.00600069867);
      G4double fBorosilicateGlass_Sellmeier_C2 =   TMath::Sqrt(0.00200179144);
      G4double fBorosilicateGlass_Sellmeier_C3 =   TMath::Sqrt(103.560653);

      G4double fBorosilicateGlass_SellmeierCoefficients[7] = { fBorosilicateGlass_Sellmeier_A,
   	    		                                               fBorosilicateGlass_Sellmeier_B1, fBorosilicateGlass_Sellmeier_B2, fBorosilicateGlass_Sellmeier_B3,
   	    		                                               fBorosilicateGlass_Sellmeier_C1, fBorosilicateGlass_Sellmeier_C2, fBorosilicateGlass_Sellmeier_C3 };


      // now loop over wavelength and calculate/lookup the index of refraction and absorption length
	  // and store the results into vectors

      //-----------------------------------------------------------------------------------------
      //               Store file bases material properties in these vectors
      //-----------------------------------------------------------------------------------------
      G4MaterialPropertyVector* pMPV_RINDEX           = new G4MaterialPropertyVector();
      G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
      //-------------------------------------------------------------------------------------------

      G4double fCurrent_PhotonEnergy = 0.0;
      G4double fCurrent_WaveLength   = 0.0;

      for (G4int i=0; i<NEntries_BorosilicateGlass; i++)
      {
    	  fCurrent_PhotonEnergy = fBorosilicateGlass_PhotonEnergyMin + (fBorosilicateGlass_PhotonEnergyMax - fBorosilicateGlass_PhotonEnergyMin) / (NEntries_BorosilicateGlass - 1) * i ;
    	  fCurrent_WaveLength   = this->EnergyToLambdaConversion(fCurrent_PhotonEnergy);

    	  pMPV_RINDEX_ABSLENGTH ->InsertValues( fCurrent_PhotonEnergy, this->GetBorosilicateGlass_AbsorptionLength ( fCurrent_WaveLength ));
    	  pMPV_RINDEX           ->InsertValues( fCurrent_PhotonEnergy, this->GetIndexOfRefraction_SellmeierEquation( fCurrent_WaveLength,
    			                                                                                                     fBorosilicateGlass_Sellmeier_A,
    			                                                                                                     fBorosilicateGlass_Sellmeier_B1, fBorosilicateGlass_Sellmeier_B2, fBorosilicateGlass_Sellmeier_B3,
																								                     fBorosilicateGlass_Sellmeier_C1, fBorosilicateGlass_Sellmeier_C2, fBorosilicateGlass_Sellmeier_C3 ));
      } // end for

      //---------------------
	  // Define new material
      //---------------------

	  // Stolen from: http://hypernews.slac.stanford.edu/HyperNews/geant4/get/AUX/2013/03/11/12.39-85121-chDetectorConstruction.cc

	  G4int ncomponents;

      pBorosilicateGlass_MD = nullptr;
	  pBorosilicateGlass_MD = new G4Material( sName,
			                                  fDensity,
											  ncomponents = 6,
											  kStateSolid );

	  //----------------------------
	  // Assembling the ingredients
	  //----------------------------
	  G4double fFractionOfMass;

	  pBorosilicateGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_B"),  fFractionOfMass =   4.0062*CLHEP::perCent);
	  pBorosilicateGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_O"),  fFractionOfMass =  53.9562*CLHEP::perCent);
	  pBorosilicateGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Na"), fFractionOfMass =   2.8191*CLHEP::perCent);
	  pBorosilicateGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Al"), fFractionOfMass =   1.1644*CLHEP::perCent);
	  pBorosilicateGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_Si"), fFractionOfMass =  37.7220*CLHEP::perCent);
	  pBorosilicateGlass_MD->AddMaterial( pNistMaterialManager->FindOrBuildMaterial("G4_K"),  fFractionOfMass =   0.3321*CLHEP::perCent);

	  //----------------------------------
      // Define material properties table
	  //----------------------------------
	  pBorosilicateGlass_MPT = nullptr;
	  pBorosilicateGlass_MPT = new G4MaterialPropertiesTable();

	  pBorosilicateGlass_MPT->AddProperty("RINDEX"    , pMPV_RINDEX           );
	  pBorosilicateGlass_MPT->AddProperty("ABSLENGTH" , pMPV_RINDEX_ABSLENGTH );

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pBorosilicateGlass_MD->SetMaterialPropertiesTable(pBorosilicateGlass_MPT);

	} // end if (pMat == nullptr)

	return pBorosilicateGlass_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



//#####################################################################################################################
//#####################################################################################################################
//                      Materials for SiPMs
//#####################################################################################################################
//#####################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_BoronCarbide_Ceramics()
{
	  G4String sName = "BoronCarbide_Ceramics";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //===============================
	  //     BoronCarbide == B4C
	  //===============================
	  // Used as SiPM ceramics housing
	  //-------------------------------

	  pBoronCarbide_Ceramics_MD = nullptr;
	  pBoronCarbide_Ceramics_MD = pNistMaterialManager->FindOrBuildMaterial("G4_BORON_CARBIDE");
	  pBoronCarbide_Ceramics_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pBoronCarbide_Ceramics_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_Silicon()
{
	G4String sName = "Silicon";

	//--------------------------------------------------------------
	// Check whether material exists already in the materials table
	//--------------------------------------------------------------
	G4Material* pMat = nullptr;
	            pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //            Silicon
	  //==============================
	  //
      // Stolen from: https://forge.physik.rwth-aachen.de/projects/g4sipm/repository/revisions/344877aa956f7b4d58ba25f02c0e845abf80705d/entry/g4sipm/src/MaterialFactory.cc

	  pSilicon_MD = nullptr;
	  pSilicon_MD = pNistMaterialManager->FindOrBuildMaterial("G4_Si");
	  pSilicon_MD->SetName(sName);

	  //--------------------------------------------------------------------------------------------------------------------
	  //               Store file bases material properties in these vectors
	  //--------------------------------------------------------------------------------------------------------------------
	  G4MaterialPropertyVector* pMPV_REALRINDEX       = nullptr; pMPV_REALRINDEX       = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX  = nullptr; pMPV_IMAGINARYRINDEX  = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX           = nullptr; pMPV_RINDEX           = new G4MaterialPropertyVector();
	  G4MaterialPropertyVector* pMPV_RINDEX_ABSLENGTH = nullptr; pMPV_RINDEX_ABSLENGTH = new G4MaterialPropertyVector();
	  //--------------------------------------------------------------------------------------------------------------------

	  //--------------------------------------------------------------------------------------------------------------------
	  // Read index of refraction from file
		//------------------------------------------------------------------------------------------------------------------
	  this->Initialize_IndexOfRefraction("/Silicon/Silicon_IndexOfRefraction.dat",
				                          pMPV_REALRINDEX,
				                          pMPV_IMAGINARYRINDEX,
				                          pMPV_RINDEX,
				                          pMPV_RINDEX_ABSLENGTH);

	  std::cout << "Silicon_IndexOfRefraction entries = " << pMPV_RINDEX->GetVectorLength() << std::endl;

	  // ==================================================================================================
	  // Snell's law: real part.
	  // Fresnel formulas: absolute value since imaginary part is not zero.
	  // Geant4 calculates both, refraction angle and the Fresnel formulas in case of dielectric_dielectric
	  // by using the real number "RINDEX".
	  // ===================================================================================================

      //----------------------------------
	  // Define material properties table
	  //----------------------------------
	  G4MaterialPropertiesTable* pSilicon_MPT = nullptr;
	                             pSilicon_MPT = new G4MaterialPropertiesTable();

	  //-----------------------------------------------------------------------
	  pSilicon_MPT->AddProperty("RINDEX"            , pMPV_RINDEX           );
	  pSilicon_MPT->AddProperty("REALRINDEX"        , pMPV_IMAGINARYRINDEX  );
	  pSilicon_MPT->AddProperty("IMAGINARYRINDEX"   , pMPV_RINDEX           );
	  pSilicon_MPT->AddProperty("ABSLENGTH"         , pMPV_RINDEX_ABSLENGTH );
	  //-----------------------------------------------------------------------

	  //----------------------------------------------
	  // Attach material properties table to material
	  //----------------------------------------------
	  pSilicon_MD->SetMaterialPropertiesTable(pSilicon_MPT);

	} // end if (pMat == nullptr)

	return pSilicon_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Material* BTSimCommon_MaterialParameters::GetMaterial_SiliconDioxide()
{
	  G4String sName = "SiliconDioxide";

	  //--------------------------------------------------------------
	  // Check whether material exists already in the materials table
	  //--------------------------------------------------------------
	  G4Material* pMat = nullptr;
	              pMat = G4Material::GetMaterial(sName, false);

	if (pMat == nullptr)
	{
	  //==============================
	  //     SiliconDioxide == SiO2
	  //==============================

	  pSiliconDioxide_MD = nullptr;
	  pSiliconDioxide_MD = pNistMaterialManager->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
	  pSiliconDioxide_MD->SetName(sName);

	} // end if (pMat == nullptr)

	return pSiliconDioxide_MD;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#############################################################################################################################################################
//#############################################################################################################################################################
//#############################################################################################################################################################
//#############################################################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::Initialize_EmissionSpectrum( TString                   sfilename_scintmaterial,
		                                                          G4MaterialPropertyVector* pMPV_EmissionSpectrum)
{
	TString DataBuffer;

    Double_t                 value_wavelength;
    Double_t                 value_lightyield;

	// assemble full file name
	TString sSpectrum_FileName     = sfilename_scintmaterial;
	TString sSpectrum_FullFileName = sMaterialDefinition_FilePath + sSpectrum_FileName;

	 std::cout << "%%%%%%%%%%%%%%%  Reading Spectrum file :  " << sSpectrum_FullFileName << std::endl;

	 std::ifstream DataFile;

	 DataFile.open(sSpectrum_FullFileName.Data() );

	 // Keep reading while the End Of File character is NOT reached
	   while (!DataFile.eof())
	   {
		   // read in first token
		   DataFile >> DataBuffer;

		   //Search for comment Symbol % or empty lines
		   if ( DataBuffer.BeginsWith("#") ||  DataBuffer.IsWhitespace() )
		   {   // comment symbol found, skipping this line completely
			   DataFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
			   std::cout << "===============>>>> Ignoring whiteline or commented line " << std::endl;
		   }
		   else
		   {
			   // Now convert first string into Double_t
			   value_wavelength = DataBuffer.Atof();

			   // Parse the following values
			   DataFile >> value_lightyield;

				// Sanity check: print out the first n lines with some read-in values
//				if (pMPV_EmissionSpectrum->GetVectorLength() < 10)
//				{
					std::cout << " Light Yield = " << value_lightyield*100 << " % @ wavelength of " << value_wavelength << " nm" << std::endl;
//				}

				//--------------
				// fill vector
				//--------------

				pMPV_EmissionSpectrum->InsertValues(this->LambdaToEnergyConversion(value_wavelength*CLHEP::nanometer) , value_lightyield );

		   } // end else

		   // abort if file is broken
		   if (!DataFile.good()) break;

	   }; // end while

	   std::cout << "Array_EmissionSpectrum entries = " << pMPV_EmissionSpectrum->GetVectorLength() << std::endl;


	DataFile.close();

	//----------------------------------------------

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::Initialize_EnergyResponse(
		TString                   filename_scintmat,
		G4double                  fLightYield,                          // scint photons per keV
		G4MaterialPropertyVector* pMPV_ALPHASCINTILLATIONYIELD,
		G4MaterialPropertyVector* pMPV_ELECTRONSCINTILLATIONYIELD )
{

	TString DataBuffer;

    Double_t                 value_energy;
    Double_t                 value_lightyieldrelative_alpha;
    Double_t                 value_lightyieldrelative_beta;
    Double_t                 value_lightyieldabsolute_alpha;
    Double_t                 value_lightyieldabsolute_beta;

	// assemble full file name
	TString sEnergyResponse_FileName     = filename_scintmat;
	TString sEnergyResponse_FullFileName = sMaterialDefinition_FilePath + sEnergyResponse_FileName;

	 std::cout << "%%%%%%%%%%%%%%%  Reading EnergyResponse file :  " << sEnergyResponse_FullFileName << std::endl;

	 std::ifstream DataFile;

	 DataFile.open(sEnergyResponse_FullFileName.Data() );


	 // Keep reading while the End Of File character is NOT reached
	   while (!DataFile.eof())
	   {
		   // read in first token
		   DataFile >> DataBuffer;

		   //Search for comment Symbol % or empty lines
		   if ( DataBuffer.BeginsWith("#") ||  DataBuffer.IsWhitespace() )
		   {   // comment symbol found, skipping this line completely
			   DataFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
			   std::cout << "===============>>>> Ignoring whiteline or commented line " << std::endl;
		   }
		   else
		   {
			   // Convert string into Double_t
			   value_energy = DataBuffer.Atof();

			   // Now parse the other values
			   DataFile >> value_lightyieldrelative_alpha;
			   DataFile >> value_lightyieldrelative_beta;

//				// Sanity check: print out the first n lines with some read-in values
//				if (pMPV_ELECTRONSCINTILLATIONYIELD->GetVectorLength() < 10)
//				{
					std::cout << " LightYieldRelative Alpha = " << value_lightyieldrelative_alpha *100 << " % @ energy of " << value_energy << " keV" << std::endl;
					std::cout << " LightYieldRelative Beta  = " << value_lightyieldrelative_beta  *100 << " % @ energy of " << value_energy << " keV" << std::endl;
//				}

				//--------------
				// fill vectors
				//--------------

				// Taken from: https://github.com/ruphy/crystal/blob/master/src/MyMaterials.cc
				// Calculate *absolute* number of scintillation photons depending on energy

				value_lightyieldabsolute_alpha  = fLightYield*(value_energy*CLHEP::keV)*value_lightyieldrelative_alpha;
				value_lightyieldabsolute_beta   = fLightYield*(value_energy*CLHEP::keV)*value_lightyieldrelative_beta;

				pMPV_ALPHASCINTILLATIONYIELD    ->InsertValues(value_energy*CLHEP::keV, value_lightyieldabsolute_alpha);
				pMPV_ELECTRONSCINTILLATIONYIELD ->InsertValues(value_energy*CLHEP::keV, value_lightyieldabsolute_beta);

		   } // end else

		   // abort if file is broken
		   if (!DataFile.good()) break;

	   }; // end while

	   std::cout << "Array_EnergyResponse entries = " << pMPV_ELECTRONSCINTILLATIONYIELD->GetVectorLength() << std::endl;


	DataFile.close();

	//----------------------------------------------

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OO

void BTSimCommon_MaterialParameters::Initialize_AbsorptionLength( TString                   sfilename_scintmat,
		                                                          G4MaterialPropertyVector* pMPV_AbsorptionLength )
{

	TString DataBuffer;

    Double_t  value_wavelength;
    Double_t  value_absorptionlength ;

	// assemble full file name
	TString sAbsorptionLength_FileName     = sfilename_scintmat;
	TString sAbsorptionLength_FullFileName = sMaterialDefinition_FilePath + sAbsorptionLength_FileName;

	 std::cout << "%%%%%%%%%%%%%%%  Reading AbsorptionLength file :  " << sAbsorptionLength_FullFileName << std::endl;

	 std::ifstream DataFile;

	 DataFile.open(sAbsorptionLength_FullFileName.Data() );

	 // Keep reading while the End Of File character is NOT reached
	   while (!DataFile.eof())
	   {
		   // read in first token
		   DataFile >> DataBuffer;

		   //Search for comment Symbol % or empty lines
		   if ( DataBuffer.BeginsWith("#") ||  DataBuffer.IsWhitespace() )
		   {   // comment symbol found, skipping this line completely
			   DataFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
			   std::cout << "===============>>>> Ignoring whiteline or commented line " << std::endl;
		   }
		   else
		   {
			   // Now convert string into Double_t
			   value_wavelength = DataBuffer.Atof();

			   // Now parse the other values
			   DataFile >> value_absorptionlength; // provided in millimeters

//				// Sanity check: print out the first n lines with some read-in values
//				if (pMPV_AbsorptionLength->GetVectorLength() < 10)
//				{
					std::cout << " Absorption Length = " << value_absorptionlength << " mm @ wavelength of " << value_wavelength << " nm" << std::endl;
//				}

				//--------------
				// fill vectors
				//--------------

				pMPV_AbsorptionLength->InsertValues(this->LambdaToEnergyConversion(value_wavelength*CLHEP::nanometer) , value_absorptionlength*CLHEP::millimeter);

		   } // end else

		   // abort if file is broken
		   if (!DataFile.good()) break;

	   };

	   std::cout << "Array_AbsorptionLength entries = " << pMPV_AbsorptionLength->GetVectorLength() << std::endl;

	DataFile.close();

	//----------------------------------------------

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::Initialize_AbsorptionLengthThroughTransmission_Sellmeier(
		TString                   filename_transmission,
		G4bool                    bFlagCalculateInternalTransmission,
		G4double                  fSellmeierCofficient[7],
		G4double                  thickness_mm,
		G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength )
{
	TString DataBuffer;

    G4double  value_wavelength;
    G4double  value_transmission_percentage;
    G4double  value_abslength;                 // with units

    G4double finternaltransmission_percentage;

	// assemble full file name
	TString sTransmission_FileName     = filename_transmission;
	TString sTransmission_FullFileName = sMaterialDefinition_FilePath + sTransmission_FileName;

	 std::cout << "%%%%%%%%%%%%%%%  Reading Transmission file :  " << sTransmission_FullFileName << std::endl;

	 std::ifstream DataFile;

	 DataFile.open(sTransmission_FullFileName.Data() );

	 // Keep reading while the End Of File character is NOT reached
	   while (!DataFile.eof())
	   {
		   // read in first token
		   DataFile >> DataBuffer;

		   //Search for comment Symbol % or empty lines
		   if ( DataBuffer.BeginsWith("#") ||  DataBuffer.IsWhitespace() )
		   {   // comment symbol found, skipping this line completely
			   DataFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
			   std::cout << "===============>>>> Ignoring whiteline or commented line " << std::endl;
		   }
		   else
		   {
			   // Now parse the current line.
			   value_wavelength = DataBuffer.Atof()*CLHEP::nanometer;  // convert string into Double_t

			   // Now parse the current line.
			   DataFile >> value_transmission_percentage;

//				// Sanity check: print out the first 5 lines with some read-in values
//				if (pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() < 100)
//				{
					std::cout << " Optical Transmission = " << value_transmission_percentage << " % @ wavelength of " << value_wavelength/CLHEP::nanometer << " nm" << std::endl;
//				}

				// Depending on flag the provided transmission is either total transmission (including surface reflections)
				// or internal internal transmission ( corrected for surface reflections)

				if (bFlagCalculateInternalTransmission == true)
				{
				   // calculate index of refraction based on Sellmeier formula
				   G4double frindex   = this->GetIndexOfRefraction_SellmeierEquation( value_wavelength,
						                                                              fSellmeierCofficient[0],
						                                                              fSellmeierCofficient[1], fSellmeierCofficient[2], fSellmeierCofficient[3],
						                                                              fSellmeierCofficient[4], fSellmeierCofficient[5], fSellmeierCofficient[6]
						                                                            );

				   //=================================================================
				   // calculate the corresponding reflection factor (Fresnel Equation)
				   //=================================================================

				   // Considers the reflection of one boundary
//				   G4double freflectionfactor =(1+(frindex*frindex))/ (2*frindex);

				   //--------------------------------------------------------------------------------------
				   // Considers the reflection of front and back surface
				   // Assuming measurement with light perpendicular to surface
				   //--------------------------------------------------------------------------------------
				   //
				   // T_measured = [4*n1*n2/(n1+n2)^2] * exp(-d/AbsorptionLength) * [4*n1*n2/(n1+n2)^2]
				   //
				   // Solving for AbsorptionLength gives:
				   //
				   // AbsorptionLength = -d / ln(T_measured * (n1+n2)^4/(16*n1^2*n2^2))
				   //
				   // Assuming measurements performed in air (i.e. n1=1.0)
				   //--------------------------------------------------------------------------------------

				   G4double freflectionfactor = TMath::Power(1.0 + frindex,4) / (16*frindex*frindex);

				   // correct the total transmission for surface reflections
				   finternaltransmission_percentage =  value_transmission_percentage * freflectionfactor;

				}
				else
				{ 	// The file already contains internal transmission
					finternaltransmission_percentage =  value_transmission_percentage;
				}

				//------------------------------------------------------------------------
				// Calculate absorption length based on thickness and optical transmission
				//
				//   Probability_Tranmission = exp(-1*optical_thickness/absorption_length)
				//
				// Note: Probability for transmission has to be measured for given optical thickness
				//------------------------------------------------------------------------

				if (value_transmission_percentage <= 0.0)
				{
					value_abslength = 1.0/kInfinity; // why not setting it to zero ?!
				}
				else
				{
					value_abslength = -1.0*thickness_mm/(TMath::Log(finternaltransmission_percentage/100.0)) ; // division by 100 because of percentage value
				}

				pMPV_OpticalTransmission_AbsorptionLength->InsertValues(this->LambdaToEnergyConversion(value_wavelength), value_abslength );

		 		// Sanity check: print out the first n lines with some read-in values
//		 		if (pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() < 100)
//		 		{
		 			std::cout << " Optical Internal Transmission = " << finternaltransmission_percentage   << " %  @ wavelength of " << value_wavelength/CLHEP::nanometer << " nm" << std::endl;
		 			std::cout << " Optical Absorption Length     = " << value_abslength/CLHEP::millimeter  << " mm @ wavelength of " << value_wavelength/CLHEP::nanometer << " nm" << std::endl;
//		 		}

		   } // end else

		   // abort if file is broken
		   if (!DataFile.good()) break;

	   };  // end of while loop

	   std::cout << "Array_Transmission entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;

	   DataFile.close();

	//----------------------------------------------
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::Initialize_AbsorptionLengthThroughTransmission_Cauchy(
		TString                   filename_transmission,
		G4bool                    bFlagCalculateInternalTransmission,
		G4double                  fCauchyCofficient[3],
		G4double                  thickness_mm,
		G4MaterialPropertyVector* pMPV_OpticalTransmission_AbsorptionLength )
{

	TString DataBuffer;

    G4double  value_wavelength;
    Double_t  value_transmission_percentage;
    G4double  value_abslength;

    G4double finternaltransmission_percentage;

	// assemble full file name
	TString sTransmission_FileName     = filename_transmission;
	TString sTransmission_FullFileName = sMaterialDefinition_FilePath + sTransmission_FileName;

	 std::cout << "%%%%%%%%%%%%%%%  Reading Transmission file :  " << sTransmission_FullFileName << std::endl;

	 std::ifstream DataFile;

	 DataFile.open(sTransmission_FullFileName.Data() );

	 // Keep reading while the End Of File character is NOT reached
	   while (!DataFile.eof())
	   {
		   // read in first token
		   DataFile >> DataBuffer;

		   //Search for comment Symbol % or empty lines
		   if ( DataBuffer.BeginsWith("#") ||  DataBuffer.IsWhitespace() )
		   {   // comment symbol found, skipping this line completely
			   DataFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
			   std::cout << "===============>>>> Ignoring whiteline or commented line " << std::endl;
		   }
		   else
		   {
			   // Now parse the current line.
			   value_wavelength = DataBuffer.Atof()*CLHEP::nanometer;  // convert string into Double_t

			   // Now parse the current line.
			   DataFile >> value_transmission_percentage;

//				// Sanity check: print out the first 5 lines with some read-in values
//				if (pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() < 100)
//				{
					std::cout << " Optical Transmission = " << value_transmission_percentage << " % @ wavelength of " << value_wavelength/CLHEP::nanometer << " nm" << std::endl;
//				}

				// Depending on flag the provided transmission is either total transmission (including surface reflections)
				// or internal internal transmission ( corrected for surface reflections)

				if (bFlagCalculateInternalTransmission == true)
				{
				   // calculate index of refraction based on Sellmeier formula
				   G4double frindex   = this->GetIndexOfRefraction_CauchyEquation(    value_wavelength,
						                                                              fCauchyCofficient[0],
						                                                              fCauchyCofficient[1], fCauchyCofficient[2]
						                                                         );

				   //=================================================================
				   // calculate the corresponding reflection factor (Fresnel Equation)
				   //=================================================================

				   // Considers the reflection of one boundary
//				   G4double freflectionfactor =(1+(frindex*frindex))/ (2*frindex);

				   //--------------------------------------------------------------------------------------
				   // Considers the reflection of front and back surface
				   // Assuming measurement with light perpendicular to surface
				   //--------------------------------------------------------------------------------------
				   //
				   // T_measured = [4*n1*n2/(n1+n2)^2] * exp(-d/AbsorptionLength) * [4*n1*n2/(n1+n2)^2]
				   //
				   // Solving for AbsorptionLength gives:
				   //
				   // AbsorptionLength = -d / ln(T_measured * (n1+n2)^4/(16*n1^2*n2^2))
				   //
				   // Assuming measurements performed in air (i.e. n1=1.0)
				   //--------------------------------------------------------------------------------------

				   G4double freflectionfactor = TMath::Power(1.0 + frindex,4) / (16*frindex*frindex);

				   // correct the total transmission for surface reflections
				   finternaltransmission_percentage =  value_transmission_percentage * freflectionfactor;

				}
				else
				{   // The file already contains internal transmission
					finternaltransmission_percentage =  value_transmission_percentage;
				}

				//------------------------------------------------------------------------
				// Calculate absorption length based on thickness and optical transmission
				//
				//   Probability_Tranmission = exp(-1*optical_thickness/absorption_length)
				//
				// Note: Probability for transmission has to be measured for given optical thickness
				//------------------------------------------------------------------------

				if (value_transmission_percentage <= 0.0)
				{
					value_abslength = 0.0 ;
				}
				else
				{
					value_abslength = -1.0*thickness_mm/(TMath::Log(finternaltransmission_percentage/100.0)) ; // division by 100 because of percentage value
				}

				pMPV_OpticalTransmission_AbsorptionLength->InsertValues(this->LambdaToEnergyConversion(value_wavelength*CLHEP::nanometer), value_abslength );

		 		// Sanity check: print out the first n lines with some read-in values
//		 		if (pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() < 100)
//		 		{
		 			std::cout << " Optical Internal Transmission = " << finternaltransmission_percentage << " %  @ wavelength of " << value_wavelength << " nm" << std::endl;
		 			std::cout << " Optical Absorption Length     = " << value_abslength/CLHEP::mm        << " mm @ wavelength of " << value_wavelength << " nm" << std::endl;
//		 		}

		   } // end else

		   // abort if file is broken
		   if (!DataFile.good()) break;

	   };  // end of while loop

	   std::cout << "Array_Transmission entries = " << pMPV_OpticalTransmission_AbsorptionLength->GetVectorLength() << std::endl;

	   DataFile.close();

	//----------------------------------------------

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::Initialize_IndexOfRefraction(
		TString filename_scintmat,
		G4MaterialPropertyVector* pMPV_REALRINDEX,
		G4MaterialPropertyVector* pMPV_IMAGINARYRINDEX,
		G4MaterialPropertyVector* pMPV_RINDEX,
		G4MaterialPropertyVector* pMPV_ABSLENGTH)
{

	TString DataBuffer;

    Double_t  value_wavelength;
    Double_t  value_realindexofrefraction;
    Double_t  value_imaginaryindexofrefraction;
    Double_t  value_absoluteindexofrefraction;
    Double_t  value_abslength;

	// assemble full file name
	TString sIndexOfRefraction_FileName     = filename_scintmat;
	TString sIndexOfRefraction_FullFileName = sMaterialDefinition_FilePath + sIndexOfRefraction_FileName;

	 std::cout << "%%%%%%%%%%%%%%%  Reading IndexOfRefraction file :  " << sIndexOfRefraction_FullFileName << std::endl;

	 std::ifstream DataFile;
	 //in.open(theFilename.Data(), ios::in);
	   DataFile.open( sIndexOfRefraction_FullFileName.Data() );

	 // Keep reading while the End Of File character is NOT reached
	   while (!DataFile.eof())
	   {
		   // read in first token
		   DataFile >> DataBuffer;

		   //Search for comment Symbol % or empty lines
		   if ( DataBuffer.BeginsWith("#") ||  DataBuffer.IsWhitespace() )
		   {
			   // comment symbol found, skipping this line completely
			   DataFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
			   std::cout << "===============>>>> Ignoring whiteline or commented line " << std::endl;
		   }
		   else
		   {
			   // convert string into Double_t
			   value_wavelength = DataBuffer.Atof();

			   // Now parse the other values
			   DataFile >> value_realindexofrefraction;
			   DataFile >> value_imaginaryindexofrefraction;

			   //------------------
			   // calculate values
			   //------------------

			   value_absoluteindexofrefraction  = std::sqrt( (value_realindexofrefraction*value_realindexofrefraction) + (value_imaginaryindexofrefraction*value_imaginaryindexofrefraction) );
			   value_abslength                  = (value_wavelength*CLHEP::nanometer) / (4*CLHEP::pi* value_imaginaryindexofrefraction) ;

			   //--------------
			   // fill vectors
			   //--------------

			   pMPV_REALRINDEX      ->InsertValues(this->LambdaToEnergyConversion(value_wavelength*CLHEP::nanometer), value_realindexofrefraction);
			   pMPV_IMAGINARYRINDEX ->InsertValues(this->LambdaToEnergyConversion(value_wavelength*CLHEP::nanometer), value_imaginaryindexofrefraction);

			   pMPV_RINDEX          ->InsertValues(this->LambdaToEnergyConversion(value_wavelength*CLHEP::nanometer), value_absoluteindexofrefraction);
			   pMPV_ABSLENGTH       ->InsertValues(this->LambdaToEnergyConversion(value_wavelength*CLHEP::nanometer), value_abslength);

			   // Sanity check: print out the first n lines with some read-in values
//			   if (pMPV_REALRINDEX->GetVectorLength() < 50)
//			   {
				   std::cout << " Real      index of refraction = " << value_realindexofrefraction      << "   @ wavelength of " << value_wavelength << " nm" << std::endl;
				   std::cout << " Imaginary index of refraction = " << value_imaginaryindexofrefraction << "   @ wavelength of " << value_wavelength << " nm" << std::endl;
				   std::cout << " Absolute  index of refraction = " << value_absoluteindexofrefraction  << "   @ wavelength of " << value_wavelength << " nm" << std::endl;
				   std::cout << " Absorption Length             = " << value_abslength/CLHEP::nm        << "nm @ wavelength of " << value_wavelength << " nm" << std::endl;
//			   }

		   } // end else

		   // abort if file is broken
		   if (!DataFile.good()) break;

	   }; // end of while

	   std::cout << "Array_IndexOfRefraction entries = " << pMPV_REALRINDEX->GetVectorLength() << std::endl;

	   DataFile.close();

	//----------------------------------------------
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::Initialize_Reflectance( TString                    filename_scintmat,
		                                                     G4MaterialPropertyVector*  pMPV_REFLECTIVITY )
{
	TString DataBuffer;

    Double_t                 value_wavelength;
    Double_t                 value_reflectance;

	// assemble full file name
	TString sReflectance_FileName     = filename_scintmat;
	TString sReflectance_FullFileName = sMaterialDefinition_FilePath + sReflectance_FileName;

	 std::cout << "%%%%%%%%%%%%%%%  Reading Reflectance file :  " << sReflectance_FullFileName << std::endl;

	 std::ifstream DataFile;

	 DataFile.open(sReflectance_FullFileName.Data() );

	 // Keep reading while the End Of File character is NOT reached
	   while (!DataFile.eof())
	   {
		   // read in first token
		   DataFile >> DataBuffer;

		   //Search for comment Symbol % or empty lines
		   if ( DataBuffer.BeginsWith("#") ||  DataBuffer.IsWhitespace() )
		   {   // comment symbol found, skipping this line completely
			   DataFile.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
			   std::cout << "===============>>>> Ignoring whiteline or commented line " << std::endl;
		   }
		   else
		   {
			   // Now convert first string into Double_t
			   value_wavelength = DataBuffer.Atof();

			   // Parse the following values
			   DataFile >> value_reflectance;

//				// Sanity check: print out the first 5 lines with some read-in values
//				if (pMPV_REFLECTIVITY->GetVectorLength() < 10) {
					std::cout << " Reflectance = " << value_reflectance*100 << " % @ wavelength of " << value_wavelength << " nm" << std::endl;
//				}

				//--------------
				// fill vectors
				//--------------

				pMPV_REFLECTIVITY->InsertValues( this->LambdaToEnergyConversion(value_wavelength*CLHEP::nanometer) , value_reflectance);

		   } // end else

		   // abort if file is broken
		   if (!DataFile.good()) break;

	   }; // end while

	   std::cout << "Array_Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

	   DataFile.close();

	   //----------------------------------------------
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4double BTSimCommon_MaterialParameters::GetIndexOfRefraction_SellmeierEquation(G4double wavelength, G4double A, G4double B1, G4double B2, G4double B3, G4double C1, G4double C2, G4double C3)
{
	//-----------------------------------------------------------------------------------
	// Refractive Indices: Use Sellmeier dispersion formula
	//
	// Note: apparently the dispersion constants stated on http://refractiveinfo.info
	//       are used with wavelengths given in micrometers, not nanometers
	//-----------------------------------------------------------------------------------

	G4double x = wavelength/CLHEP::micrometer;

	G4double n1 = B1*x*x/(x*x - (C1*C1));
	G4double n2 = B2*x*x/(x*x - (C2*C2));
	G4double n3 = B3*x*x/(x*x - (C3*C3));

	G4double n = TMath::Sqrt(A+n1+n2+n3);

	return n;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::GetIndexOfRefraction_CauchyEquation(G4double wavelength, G4double A, G4double B1, G4double B2 )
{
	//-----------------------------------------------------
	// Refractive Indices: Use Cauchy dispersion formula
	//
	// Note: used for some MeltMount glues
	//-----------------------------------------------------

	G4double x = wavelength/CLHEP::angstrom;

	G4double n1 = B1/(x*x);
	G4double n2 = B2/(x*x*x*x);


	G4double n = TMath::Sqrt(A+n1+n2);

	return n;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::GetLaBr3_Ce_RefractionIndex(G4double wavelength)
{
	//---------------------------------------
	// Refractive Indices: Sellmeier formula
	//---------------------------------------

	G4double x = wavelength/CLHEP::nanometer;

	G4double B = 2.80;
	G4double C = 0.048; // nm*nm

	G4double s1 = B*x*x/(x*x-C);


	G4double n = sqrt(1+s1);
//	G4double n = 2.0;

	return n;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::GetFusedSilica_AbsorptionLength(G4double wavelength)
{
	// Quartz absorption length: from the Cedar manual (CERN 82-13), p.13

   G4double x = wavelength/CLHEP::nm;
   G4double Transmittance_12cm;

   if      (x>400) Transmittance_12cm = 0.999;
   else if (x>220) Transmittance_12cm = 0.900 + 0.099/(400-220)*(x-220);
   else if (x>180) Transmittance_12cm = 0.700 + 0.200/(220-200)*(x-200);
   else            Transmittance_12cm = 0.500; // non-physical but fool-proof

   G4double AbsLength = - 12*CLHEP::cm / log(Transmittance_12cm);

   return AbsLength;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::GetFusedSilica_WindowTransmittance(G4double Wavelength)
{
   G4double x = Wavelength/CLHEP::nm;
   G4double transmittance = -999;

   if      (x>370) transmittance = 0.900 + 0.00011    * (x-370);
   else if (x>270) transmittance = 0.910 - 0.010/100. * (x-270);
   else if (x>260) transmittance = 0.890 + 0.020/ 10. * (x-260);
   else if (x>250) transmittance = 0.830 + 0.060/ 10. * (x-250);
   else if (x>240) transmittance = 0.790 + 0.040/ 10. * (x-240);
   else if (x>230) transmittance = 0.790;
   else if (x>225) transmittance = 0.740 + 0.050/  5. * (x-225);
   else if (x>220) transmittance = 0.640 + 0.100/  5. * (x-220);
   else if (x>210) transmittance = 0.350 + 0.290/ 10. * (x-210);
   else if (x>205) transmittance = 0.260 + 0.090/  5. * (x-205);
   else            transmittance = 0.235 + 0.025/  5. * (x-200);

   return transmittance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::GetBorosilicateGlass_AbsorptionLength(G4double wavelength)
{
	//----------------------------------------------------------------------------------------------------
	// Literature: http://www.ppl.phys.chiba-u.jp/research/IceCube/DetectorSim/DOMINANT/matprop_glass.dat
	//----------------------------------------------------------------------------------------------------

	// G4double x = wavelength/nm;

	G4double AbsLength = 10.5*CLHEP::cm; // assumed to be constant

	return AbsLength;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::EnergyToLambdaConversion (G4double value)
{
	//------------------------------------------------------
	// Converting photon energy to wavelength and vice versa.
	// The conversion constant is approximately 1240 eV*nm.
	//------------------------------------------------------

  return 1239.84193*CLHEP::eV*CLHEP::nanometer / value;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::LambdaToEnergyConversion (G4double value)
{
	//------------------------------------------------------
	// Converting photon energy to wavelength and vice versa.
	// The conversion constant is approximately 1240 eV*nm.
	//------------------------------------------------------

  return  1239.84193*CLHEP::eV*CLHEP::nanometer / value;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//#############################################################################################################################################################
//#############################################################################################################################################################
//#############################################################################################################################################################
//#############################################################################################################################################################

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//=================================
// Reflection type: dielectric_LUT
//=================================

//polishedair,                 // mechanically polished surface
//polishedlumirrorair,         // mechanically polished surface, with lumirror
//polishedlumirrorglue,        // mechanically polished surface, with lumirror & meltmount
//polishedteflonair,           // mechanically polished surface, with teflon
//polishedtioair,              // mechanically polished surface, with tio paint
//polishedtyvekair,            // mechanically polished surface, with tyvek
//polishedvm2000air,           // mechanically polished surface, with esr film
//polishedvm2000glue,          // mechanically polished surface, with esr film & meltmount
//
//etchedair,                   // chemically etched surface
//etchedlumirrorair,           // chemically etched surface, with lumirror
//etchedlumirrorglue,          // chemically etched surface, with lumirror & meltmount
//etchedteflonair,             // chemically etched surface, with teflon
//etchedtioair,                // chemically etched surface, with tio paint
//etchedtyvekair,              // chemically etched surface, with tyvek
//etchedvm2000air,             // chemically etched surface, with esr film
//etchedvm2000glue,            // chemically etched surface, with esr film & meltmount
//
//groundair,                   // rough-cut surface
//groundlumirrorair,           // rough-cut surface, with lumirror
//groundlumirrorglue,          // rough-cut surface, with lumirror & meltmount
//groundteflonair,             // rough-cut surface, with teflon
//groundtioair,                // rough-cut surface, with tio paint
//groundtyvekair,              // rough-cut surface, with tyvek
//groundvm2000air,             // rough-cut surface, with esr film
//groundvm2000glue             // rough-cut surface, with esr film & meltmount

//--------------------------------------------------------------------------------------------------------------------------------------------------
// Note 1: The SigmaAlpha value matters only for Ground (not Polished) surfaces within Unified model
// Note 2: For a polished surface it does not matter which model to use (Glisur or Unified)
// Note 3: (a) If you want 'groundbackpainted with a polished surface between scintillator and air use: SPECULARSPIKECONSTANT=1, all others=0,
//             for the polished surface and the Lambertian reflector
///        (b) If you want 'polishedbackpainted' with depolished surface between scintillator and air use: SPECULARLOBECONSTANT=1, all others=0,
//             for a depolished surface and a polished reflector
//--------------------------------------------------------------------------------------------------------------------------------------------------
// Taken from:
// "Peculiarities in the Simulation of Optical Physics with Geant4", arXiv:1612.05162v1 [physics.ins-det], 15. December 2016
//
// The refractive index is not a surface property but a material property. Therefore, assigning it to a G4OpticalSurface has no effect.
// However, exceptions are G4OpticalSurfaces with the surface finishes named “-backpainted”.  Here, a refractive index has to be
// defined, which is interpreted as the refractive index of the medium in the gap between the painted volume and the paint.
//
// In contrast, the complex refractive index is not treated as a material property but as a surface property. Therefore, it only has an effect when being specified for
// G4OpticalSurfaces. In particular, assigning a complex refractive index to a material does not result in an implicit specification of the attenuation length spectrum.
// This treatment of the complex refractive index is physically reasonable, as all optical processes besides reflection probability and absorption
// (which is directly specified for materials) only depend on the real part of the refractive index


  //===============================================================================================================================================
  //                                                        BGO polished
  //===============================================================================================================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedAir()
{
	  //--------------------------------------
	  // BGO polished surface, facing Air only
	  //--------------------------------------

	  if (pBGOPolishedAir_OS == nullptr)
	  {
		  G4String name = "BGOPolishedAir";

		  pBGOPolishedAir_OS = new G4OpticalSurface(name);
		  pBGOPolishedAir_OS->SetType(dielectric_LUT);
		  pBGOPolishedAir_OS->SetModel(LUT);
		  pBGOPolishedAir_OS->SetFinish(polishedair);

	  } // end if

	  return pBGOPolishedAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedLumirrorAir()
{
	  //------------------------------------------------
	  // BGO polished surface, wrapped with Lumirror-Air
	  //------------------------------------------------

	  if (pBGOPolishedLumirrorAir_OS == nullptr)
	  {
		  G4String name = "BGOPolishedLumirrorAir";

		  pBGOPolishedLumirrorAir_OS = new G4OpticalSurface(name);
		  pBGOPolishedLumirrorAir_OS->SetType(dielectric_LUT);
		  pBGOPolishedLumirrorAir_OS->SetModel(LUT);
		  pBGOPolishedLumirrorAir_OS->SetFinish(polishedlumirrorair);

		  //---------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();


		  // Read reflectance from file
		  // => Assuming that the reflectance is dominated by Lumirror and not limited by air
		  this->Initialize_Reflectance("/Coating/Lumirror_Reflectance.dat",
				                        pMPV_REFLECTIVITY );

		  std::cout << "Lumirror Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		  // Define optical properties table
		  pCoating_Lumirror_MPT = new G4MaterialPropertiesTable();

		  pCoating_Lumirror_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero );
		  pCoating_Lumirror_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		  // Attach optical properties table to optical surface
		  pBGOPolishedLumirrorAir_OS->SetMaterialPropertiesTable( pCoating_Lumirror_MPT );

	  } // end if

	  return pBGOPolishedLumirrorAir_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedLumirrorGlue()
  {
	  //-------------------------------------
	  // BGO polished surface, glued Lumirror
	  //-------------------------------------

	  if (pBGOPolishedLumirrorGlue_OS == nullptr)
	  {
		  G4String name = "BGOPolishedLumirrorGlue";

		  pBGOPolishedLumirrorGlue_OS = new G4OpticalSurface(name);
		  pBGOPolishedLumirrorGlue_OS->SetType(dielectric_LUT);
		  pBGOPolishedLumirrorGlue_OS->SetModel(LUT);
		  pBGOPolishedLumirrorGlue_OS->SetFinish(polishedlumirrorglue);

		  //---------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		  // Read reflectance from file
		  // => Assuming that the reflectance is dominated by Lumirror and not limited by the glue
		  this->Initialize_Reflectance("/Coating/Lumirror_Reflectance.dat",
                                        pMPV_REFLECTIVITY );

		  std::cout << "Lumirror Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		  // Define optical properties table
		  pCoating_Lumirror_MPT = new G4MaterialPropertiesTable();

		  pCoating_Lumirror_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		  pCoating_Lumirror_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY );


		  // Attach optical properties table to optical surface
		  pBGOPolishedLumirrorGlue_OS->SetMaterialPropertiesTable( pCoating_Lumirror_MPT );

	  } // end if

	  return pBGOPolishedLumirrorGlue_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedSpectraflectAir()
{
	//---------------------------------------------------------------
	// BGO polished surface, coated with TiO2-Air (Titanium Dioxid)
	// but the reflectance of coating equals Spectraflect == BaSO4
	//---------------------------------------------------------------

	if (pBGOPolishedSpectraflectAir_OS == nullptr)
	{
		G4String name = "BGOPolishedSpectraflectAir";

		pBGOPolishedSpectraflectAir_OS = new G4OpticalSurface(name);
		pBGOPolishedSpectraflectAir_OS->SetType(dielectric_LUT);
		pBGOPolishedSpectraflectAir_OS->SetModel(LUT);
		pBGOPolishedSpectraflectAir_OS->SetFinish(polishedtioair); // use measured reflection properties of TiO2 as template

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// retrieve reflectance from data file
		this->Initialize_Reflectance("/Coating/Spectraflect_Reflectance.dat",
                                      pMPV_REFLECTIVITY);

		std::cout << "Spectraflect (BaSO4) Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_Spectraflect_MPT = new G4MaterialPropertiesTable();

		pCoating_Spectraflect_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_Spectraflect_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY );


		// Attach optical properties table to optical surface
		pBGOPolishedSpectraflectAir_OS->SetMaterialPropertiesTable( pCoating_Spectraflect_MPT );

	} // end if

	return pBGOPolishedSpectraflectAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedSpectralonAir()
{
	//---------------------------------------------------------------
	// BGO polished surface, coated with TiO2-Air (Titanium Dioxid)
	// but the reflectance of coating equals Spectralon == PTFE
	//---------------------------------------------------------------

	if (pBGOPolishedSpectralonAir_OS == nullptr)
	{
		G4String name = "BGOPolishedSpectralonAir";

		pBGOPolishedSpectralonAir_OS = new G4OpticalSurface(name);
		pBGOPolishedSpectralonAir_OS->SetType(dielectric_LUT);
		pBGOPolishedSpectralonAir_OS->SetModel(LUT);
		pBGOPolishedSpectralonAir_OS->SetFinish(polishedtioair); // use measured reflection properties of TiO2 as template

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/Spectralon_Reflectance.dat",
                                      pMPV_REFLECTIVITY);

		std::cout << "Spectralon (PTFE) Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_Spectralon_MPT = new G4MaterialPropertiesTable();

		pCoating_Spectralon_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_Spectralon_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOPolishedSpectralonAir_OS->SetMaterialPropertiesTable( pCoating_Spectralon_MPT );

	} // end if

	return pBGOPolishedSpectralonAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedTeflonAir()
  {
	  //----------------------------------------------
	  // BGO polished surface, wrapped with Teflon-Air
	  //----------------------------------------------

	  if (pBGOPolishedTeflonAir_OS == nullptr)
	  {
		  G4String name = "BGOPolishedTeflonAir";

		  pBGOPolishedTeflonAir_OS = new G4OpticalSurface(name);
		  pBGOPolishedTeflonAir_OS->SetType(dielectric_LUT);
		  pBGOPolishedTeflonAir_OS->SetModel(LUT);
		  pBGOPolishedTeflonAir_OS->SetFinish(polishedteflonair);

		  //---------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		  // Read emission spectrum from file
		  this->Initialize_Reflectance("/Coating/Teflon_Reflectance.dat",
                                        pMPV_REFLECTIVITY);

		  std::cout << "Teflon Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		  // Define optical properties table
		  pCoating_Teflon_MPT = new G4MaterialPropertiesTable();

		  pCoating_Teflon_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		  pCoating_Teflon_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		  // Attach optical properties table to optical surface
		  pBGOPolishedTeflonAir_OS->SetMaterialPropertiesTable( pCoating_Teflon_MPT );

	  } // end if

	  return pBGOPolishedTeflonAir_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedTioAir()
  {
	  //----------------------------------------------------------
	  // BGO polished surface, coated with TiO2-Air (Titanium Dioxid)
	  //----------------------------------------------------------

	  if (pBGOPolishedTioAir_OS == nullptr)
	  {
		  G4String name = "BGOPolishedTioAir";

		  pBGOPolishedTioAir_OS = new G4OpticalSurface(name);
		  pBGOPolishedTioAir_OS->SetType(dielectric_LUT);
		  pBGOPolishedTioAir_OS->SetModel(LUT);
		  pBGOPolishedTioAir_OS->SetFinish(polishedtioair);

		  //---------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		  // Read emission spectrum from file
		  this->Initialize_Reflectance("/Coating/TiO2_Reflectance.dat",
                  	  	  	  	  	  	pMPV_REFLECTIVITY);

		  std::cout << "TiO2 Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		  // Define optical properties table
		  pCoating_TiO2_MPT = new G4MaterialPropertiesTable();

		  pCoating_TiO2_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		  pCoating_TiO2_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		  // Attach optical properties table to optical surface
		  pBGOPolishedTioAir_OS->SetMaterialPropertiesTable( pCoating_TiO2_MPT );

	  } // end if

	  return pBGOPolishedTioAir_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedTyvekAir()
  {
	  //---------------------------------------------
	  // BGO polished surface, wrapped with Tyvek-Air
	  //---------------------------------------------

	  if (pBGOPolishedTyvekAir_OS == nullptr)
	  {
		  G4String name = "BGOPolishedTyvekAir";

		  pBGOPolishedTyvekAir_OS = new G4OpticalSurface(name);
		  pBGOPolishedTyvekAir_OS->SetType(dielectric_LUT);
		  pBGOPolishedTyvekAir_OS->SetModel(LUT);
		  pBGOPolishedTyvekAir_OS->SetFinish(polishedtyvekair);

		  //---------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		  // Read emission spectrum from file
		  this->Initialize_Reflectance("/Coating/Tyvek_Reflectance.dat",
                                        pMPV_REFLECTIVITY);

		  std::cout << "Tyvek Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		  // Define optical properties table
		  pCoating_Tyvek_MPT = new G4MaterialPropertiesTable();

		  pCoating_Tyvek_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		  pCoating_Tyvek_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		  // Attach optical properties table to optical surface
		  pBGOPolishedTyvekAir_OS->SetMaterialPropertiesTable( pCoating_Tyvek_MPT );

	  } // end if

	  return pBGOPolishedTyvekAir_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedVM2000Air()
  {
	  //----------------------------------------------
	  // BGO polished surface, wrapped with VM2000-Air
	  //----------------------------------------------

	  if (pBGOPolishedVM2000Air_OS == nullptr)
	  {
		  G4String name = "BGOPolishedVM2000Air";

		  pBGOPolishedVM2000Air_OS = new G4OpticalSurface(name);
		  pBGOPolishedVM2000Air_OS->SetType(dielectric_LUT);
		  pBGOPolishedVM2000Air_OS->SetModel(LUT);
		  pBGOPolishedVM2000Air_OS->SetFinish(polishedvm2000air);

		  //---------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		  // Read emission spectrum from file
		  this->Initialize_Reflectance("/Coating/VM2000Front_Reflectance.dat",
                                        pMPV_REFLECTIVITY );

		  std::cout << "VM2000 (Front Side) Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		  // Define optical properties table
		  pCoating_VM2000_MPT = new G4MaterialPropertiesTable();

		  pCoating_VM2000_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		  pCoating_VM2000_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		  // Attach optical properties table to optical surface
		  pBGOPolishedVM2000Air_OS->SetMaterialPropertiesTable( pCoating_VM2000_MPT );

	  } // end if

	  return pBGOPolishedVM2000Air_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOPolishedVM2000Glue()
{
	//----------------------------------------
	// BGO polished surface, glued VM2000 foil
	//----------------------------------------

	if (pBGOPolishedVM2000Glue_OS == nullptr)
	{
		G4String name = "BGOPolishedVM2000Glue";

		pBGOPolishedVM2000Glue_OS = new G4OpticalSurface(name);
		pBGOPolishedVM2000Glue_OS->SetType(dielectric_LUT);
		pBGOPolishedVM2000Glue_OS->SetModel(LUT);
		pBGOPolishedVM2000Glue_OS->SetFinish(polishedvm2000glue);

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/VM2000Front_Reflectance.dat",
                                      pMPV_REFLECTIVITY );

		std::cout << "VM2000 (Front Side) Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_VM2000_MPT = new G4MaterialPropertiesTable();

		pCoating_VM2000_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_VM2000_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOPolishedVM2000Glue_OS->SetMaterialPropertiesTable( pCoating_VM2000_MPT );

	  } // end if

	return pBGOPolishedVM2000Glue_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  //===============================================================================================================================================
  //                                                        BGO ground = rough-cut
  //===============================================================================================================================================

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundAir()
{
	//------------------------------------
  	// BGO ground surface, facing Air only
  	//------------------------------------

  	if (pBGOGroundAir_OS == nullptr)
  	{
  		G4String name = "BGOGroundAir";

  		pBGOGroundAir_OS = new G4OpticalSurface(name);
  		pBGOGroundAir_OS->SetType(dielectric_LUT);
  		pBGOGroundAir_OS->SetModel(LUT);
  		pBGOGroundAir_OS->SetFinish(groundair);
  	} // end if

  	return pBGOGroundAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundLumirrorAir()
{
	//----------------------------------------------
	// BGO ground surface, wrapped with Lumirror-Air
	//----------------------------------------------

	  if (pBGOGroundLumirrorAir_OS == nullptr)
	  {
		  G4String name = "BGOGroundLumirrorAir";

		  pBGOGroundLumirrorAir_OS = new G4OpticalSurface(name);
		  pBGOGroundLumirrorAir_OS->SetType(dielectric_LUT);
		  pBGOGroundLumirrorAir_OS->SetModel(LUT);
		  pBGOGroundLumirrorAir_OS->SetFinish(groundlumirrorair);

		  //---------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		  // Read reflectance from file
		  // => Assuming that the reflectance is dominated by Lumirror and not limited by the air
		  this->Initialize_Reflectance("/Coating/Lumirror_Reflectance.dat",
                                        pMPV_REFLECTIVITY );

		  std::cout << "Lumirror Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		  // Define optical properties table
		  pCoating_Lumirror_MPT = new G4MaterialPropertiesTable();

		  pCoating_Lumirror_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		  pCoating_Lumirror_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		  // Attach optical properties table to optical surface
		  pBGOGroundLumirrorAir_OS->SetMaterialPropertiesTable( pCoating_Lumirror_MPT );

	  } // end if

	  return pBGOGroundLumirrorAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundLumirrorGlue()
{
	  //-----------------------------------
	  // BGO ground surface, glued Lumirror
	  //-----------------------------------

	  if (pBGOGroundLumirrorGlue_OS == nullptr)
	  {
		  G4String name = "BGOGroundLumirrorGlue";

		  pBGOGroundLumirrorGlue_OS = new G4OpticalSurface(name);
		  pBGOGroundLumirrorGlue_OS->SetType(dielectric_LUT);
		  pBGOGroundLumirrorGlue_OS->SetModel(LUT);
		  pBGOGroundLumirrorGlue_OS->SetFinish(groundlumirrorglue);

		  //---------------------------------------------------------------------------
		  G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		  // Read reflectance from file
		  // => Assuming that the reflectance is dominated by Lumirror and not limited by the glue
		  this->Initialize_Reflectance("/Coating/Lumirror_Reflectance.dat",
                                        pMPV_REFLECTIVITY );

		  std::cout << "Lumirror Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		  // Define optical properties table
		  pCoating_Lumirror_MPT = new G4MaterialPropertiesTable();

		  pCoating_Lumirror_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		  pCoating_Lumirror_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		  // Attach optical properties table to optical surface
		  pBGOGroundLumirrorGlue_OS->SetMaterialPropertiesTable( pCoating_Lumirror_MPT );

	  } // end if

	  return pBGOGroundLumirrorGlue_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundSpectraflectAir()
{
	//---------------------------------------------------------------
	// BGO Ground surface, coated with TiO2-Air (Titanium Dioxid)
	// but the reflectance of coating equals Spectraflect == BaSO4
	//---------------------------------------------------------------

	if (pBGOGroundSpectraflectAir_OS == nullptr)
	{
		G4String name = "BGOGroundSpectraflectAir";

		pBGOGroundSpectraflectAir_OS = new G4OpticalSurface(name);
		pBGOGroundSpectraflectAir_OS->SetType(dielectric_LUT);
		pBGOGroundSpectraflectAir_OS->SetModel(LUT);
		pBGOGroundSpectraflectAir_OS->SetFinish(groundtioair); // use measured reflection properties of TiO2 as template

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/Spectraflect_Reflectance.dat",
                                      pMPV_REFLECTIVITY );

		std::cout << "Spectraflect (BaSO4) Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_Spectraflect_MPT = new G4MaterialPropertiesTable();

		pCoating_Spectraflect_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_Spectraflect_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOGroundSpectraflectAir_OS->SetMaterialPropertiesTable( pCoating_Spectraflect_MPT );

	} // end if

	return pBGOGroundSpectraflectAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundSpectralonAir()
{
	//---------------------------------------------------------------
	// BGO Ground surface, coated with TiO2-Air (Titanium Dioxid)
	// but the reflectance of coating equals Spectralon == PTFE
	//---------------------------------------------------------------

	if (pBGOGroundSpectralonAir_OS == nullptr)
	{
		G4String name = "BGOGroundSpectralonAir";

		pBGOGroundSpectralonAir_OS = new G4OpticalSurface(name);
		pBGOGroundSpectralonAir_OS->SetType(dielectric_LUT);
		pBGOGroundSpectralonAir_OS->SetModel(LUT);
		pBGOGroundSpectralonAir_OS->SetFinish(groundtioair); // use measured reflection properties of TiO2 as template

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/Spectralon_Reflectance.dat",
                                      pMPV_REFLECTIVITY );

		std::cout << "Spectralon (PTFE) Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_Spectralon_MPT = new G4MaterialPropertiesTable();

		pCoating_Spectralon_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_Spectralon_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOGroundSpectralonAir_OS->SetMaterialPropertiesTable( pCoating_Spectralon_MPT );

	} // end if

	return pBGOGroundSpectralonAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundTeflonAir()
{
	//--------------------------------------------
	// BGO ground surface, wrapped with Teflon-Air
	//--------------------------------------------

	if (pBGOGroundTeflonAir_OS == nullptr)
	{
		G4String name = "BGOGroundTeflonAir";

		pBGOGroundTeflonAir_OS = new G4OpticalSurface(name);
		pBGOGroundTeflonAir_OS->SetType(dielectric_LUT);
		pBGOGroundTeflonAir_OS->SetModel(LUT);
		pBGOGroundTeflonAir_OS->SetFinish(groundteflonair);

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/Teflon_Reflectance.dat",
				                      pMPV_REFLECTIVITY );

		std::cout << "Teflon Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_Teflon_MPT = new G4MaterialPropertiesTable();

		pCoating_Teflon_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_Teflon_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOGroundTeflonAir_OS->SetMaterialPropertiesTable( pCoating_Teflon_MPT );

	} // end if

	return pBGOGroundTeflonAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundTioAir()
{
	//----------------------------------------------------------------
	// BGO ground surface, wrapped with TiO-Air (Titanium Oxide: TiO2)
	//----------------------------------------------------------------

	if (pBGOGroundTioAir_OS == nullptr)
	{
		G4String name = "BGOGroundTioAir";

		pBGOGroundTioAir_OS = new G4OpticalSurface(name);
		pBGOGroundTioAir_OS->SetType(dielectric_LUT);
		pBGOGroundTioAir_OS->SetModel(LUT);
		pBGOGroundTioAir_OS->SetFinish(groundtioair);

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/TiO2_Reflectance.dat",
				                      pMPV_REFLECTIVITY );

		std::cout << "TiO2 Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_TiO2_MPT = new G4MaterialPropertiesTable();

		pCoating_TiO2_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_TiO2_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOGroundTioAir_OS->SetMaterialPropertiesTable( pCoating_TiO2_MPT );

	} // end if

	return pBGOGroundTioAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundTyvekAir()
{
	//-------------------------------------------
	// BGO ground surface, wrapped with Tyvek-Air
	//-------------------------------------------

	if (pBGOGroundTyvekAir_OS == nullptr)
	{
		G4String name = "BGOGroundTyvekAir";

		pBGOGroundTyvekAir_OS = new G4OpticalSurface(name);
		pBGOGroundTyvekAir_OS->SetType(dielectric_LUT);
		pBGOGroundTyvekAir_OS->SetModel(LUT);
		pBGOGroundTyvekAir_OS->SetFinish(groundtyvekair);

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/Tyvek_Reflectance.dat",
				                      pMPV_REFLECTIVITY );

		std::cout << "Tyvek Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_Tyvek_MPT = new G4MaterialPropertiesTable();

		pCoating_Tyvek_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_Tyvek_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOGroundTyvekAir_OS->SetMaterialPropertiesTable( pCoating_Tyvek_MPT );

	  } // end if

	return pBGOGroundTyvekAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundVM2000Air()
{
	//--------------------------------------------
	// BGO ground surface, wrapped with VM2000-Air
	//-------------------------------------------

	if (pBGOGroundVM2000Air_OS == nullptr)
	{
		G4String name = "BGOGroundVM2000Air";

		pBGOGroundVM2000Air_OS = new G4OpticalSurface(name);
		pBGOGroundVM2000Air_OS->SetType(dielectric_LUT);
		pBGOGroundVM2000Air_OS->SetModel(LUT);
		pBGOGroundVM2000Air_OS->SetFinish(groundvm2000air);

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/VM2000Front_Reflectance.dat",
				                      pMPV_REFLECTIVITY );

		std::cout << "VM2000 (Front Side) Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_VM2000_MPT = new G4MaterialPropertiesTable();

		pCoating_VM2000_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_VM2000_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOGroundVM2000Air_OS->SetMaterialPropertiesTable( pCoating_VM2000_MPT );

	} // end if

	return pBGOGroundVM2000Air_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BGOGroundVM2000Glue()
{
	//--------------------------------------
	// BGO ground surface, glued VM2000 foil
	//--------------------------------------

	if (pBGOGroundVM2000Glue_OS == nullptr)
	{
		G4String name = "BGOGroundVM2000Glue";

		pBGOGroundVM2000Glue_OS = new G4OpticalSurface(name);
		pBGOGroundVM2000Glue_OS->SetType(dielectric_LUT);
		pBGOGroundVM2000Glue_OS->SetModel(LUT);
		pBGOGroundVM2000Glue_OS->SetFinish(groundvm2000glue);

		//---------------------------------------------------------------------------
		G4MaterialPropertyVector* pMPV_REFLECTIVITY = new G4MaterialPropertyVector();

		// Read emission spectrum from file
		this->Initialize_Reflectance("/Coating/VM2000Front_Reflectance.dat",
                                      pMPV_REFLECTIVITY );

		std::cout << "VM2000 (Front Side) Coating Reflectance entries = " << pMPV_REFLECTIVITY->GetVectorLength() << std::endl;

		// Define optical properties table
		pCoating_VM2000_MPT = new G4MaterialPropertiesTable();

	    pCoating_VM2000_MPT->AddProperty("EFFICIENCY"   , pMPV_DetectionEfficiency_Zero ); // do not account for border surface hits
		pCoating_VM2000_MPT->AddProperty("REFLECTIVITY" , pMPV_REFLECTIVITY             );

		// Attach optical properties table to optical surface
		pBGOGroundVM2000Glue_OS->SetMaterialPropertiesTable( pCoating_VM2000_MPT );

	} // end if

	return pBGOGroundVM2000Glue_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  //===============================================================================================================================================
  //                                             Generic material surface treatments
  //===============================================================================================================================================
  //
  // Note: The SigmaAlpha value matters only for Ground (not Polished) surfaces within Unified model

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_PerfectSmooth()
{

	//------------------------------------
	// Perfect polished smooth surface
	//------------------------------------

	if (pPerfectPolished_OS == nullptr)
	{
		G4String name = "PerfectSmoothSurface";

		pPerfectPolished_OS = new G4OpticalSurface(name);
		pPerfectPolished_OS->SetType(dielectric_dielectric);
		pPerfectPolished_OS->SetModel(unified);
		pPerfectPolished_OS->SetFinish(polished);
		pPerfectPolished_OS->SetSigmaAlpha(0.0*CLHEP::degree); // just perfect

	} // end if

	return pPerfectPolished_OS;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_DiamondMilled()
{

	//------------------------------------
	// Diamond milled surface
	//------------------------------------

	if (pDiamondMilled_OS == nullptr)
	{
		G4String name = "DiamondMilled";

		pDiamondMilled_OS = new G4OpticalSurface(name);
		pDiamondMilled_OS->SetType(dielectric_dielectric);
		pDiamondMilled_OS->SetModel(unified);
		pDiamondMilled_OS->SetFinish(ground);
		pDiamondMilled_OS->SetSigmaAlpha(0.1*CLHEP::degree); // educated guess for diamond milling

	} // end if

	return pDiamondMilled_OS;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_GenericPolishedAir()
{
	//------------------------------------
	// Generic polished surface facing Air
	//------------------------------------

	if (pGenericPolishedAir_OS == nullptr)
	{
		G4String name = "GenericPolishedAir";

		pGenericPolishedAir_OS = new G4OpticalSurface(name);
		pGenericPolishedAir_OS->SetType(dielectric_dielectric);
		pGenericPolishedAir_OS->SetModel(unified);
		pGenericPolishedAir_OS->SetFinish(ground); // necessary even for polished surfaces to enable UNIFIED code
		pGenericPolishedAir_OS->SetSigmaAlpha(1.3 * CLHEP::degree); // Janecek2010
	} // end if

	return pGenericPolishedAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_GenericGroundAir()
{
	//----------------------------------
	// Generic ground surface facing Air
	//----------------------------------

	if (pGenericGroundAir_OS == nullptr)
	{
		G4String name = "GenericGroundAir";

		pGenericGroundAir_OS = new G4OpticalSurface(name);
		pGenericGroundAir_OS->SetType(dielectric_dielectric);
		pGenericGroundAir_OS->SetModel(unified);
		pGenericGroundAir_OS->SetFinish(ground);
		pGenericGroundAir_OS->SetSigmaAlpha(12.0 * CLHEP::degree); // Janecek2010
	} // end if

	return pGenericGroundAir_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_GenericPolishedWhitePainted()
  {
	  //----------------------------------------
	  // Generic polished surface, white painted
	  //----------------------------------------

	  if (pGenericPolishedWhitePainted_OS == nullptr)
	  {
		  G4String name = "GenericPolishedWhitePainted";

		  pGenericPolishedWhitePainted_OS = new G4OpticalSurface(name);
		  pGenericPolishedWhitePainted_OS->SetType(dielectric_dielectric);
		  pGenericPolishedWhitePainted_OS->SetModel(unified);
		  pGenericPolishedWhitePainted_OS->SetFinish(groundbackpainted);
		  pGenericPolishedWhitePainted_OS->SetSigmaAlpha(1.3 * CLHEP::degree); // Janecek2010

		  //========================
		  // common paint attributes
		  //========================

		  //------------------------------------------------------------------------
		  // defining optical range of interest
		  //------------------------------------------------------------------------
		  G4double fLambdaMin         = 200.0*CLHEP::nanometer;
		  G4double fLambdaMax         = 800.0*CLHEP::nanometer;
		  G4double fPhotonEnergyMin   = this->LambdaToEnergyConversion(fLambdaMax);
		  G4double fPhotonEnergyMax   = this->LambdaToEnergyConversion(fLambdaMin);
		  //------------------------------------------------------------------------
		  G4double fEnergy[2]            = { fPhotonEnergyMin, fPhotonEnergyMax }; // Everything from far IR to VUV
		  G4double fRindex[2]            = { 1.61            , 1.61             }; // Janecek2010
		  G4double fReflectance[2]       = { 0.955           , 0.955            }; // Janecek2010
		  //------------------------------------------------------------------------
		  G4double fSpecularLobe[2]      = { 1.00            , 1.00             };
		  G4double fSpecularSpike[2]     = { 0.00            , 0.00             };
		  G4double fBackScatter[2]       = { 0.00            , 0.00             };
         //------------------------------------------------------------------------
		 // Sum must be one              =   1.00            , 1.00
		 //------------------------------------------------------------------------

		  G4MaterialPropertiesTable* pGenericPolishedWhitePainted_MPT = new G4MaterialPropertiesTable();

		  	  	                     //--------------------------------------------------------------------------------------------------
		  	  	  	  	  	  	  	 pGenericPolishedWhitePainted_MPT->AddProperty("RINDEX"                ,fEnergy , fRindex        , 2);
		  	  	  	  	  	         pGenericPolishedWhitePainted_MPT->AddProperty("REFLECTIVITY"          ,fEnergy , fReflectance   , 2);
		  	  	  	  	  	         //--------------------------------------------------------------------------------------------------
		  	  	  	  	  	  	  	 pGenericPolishedWhitePainted_MPT->AddProperty("SPECULARLOBECONSTANT"  ,fEnergy , fSpecularLobe  , 2);
		  	  	  	  	  	  	  	 pGenericPolishedWhitePainted_MPT->AddProperty("SPECULARSPIKECONSTANT" ,fEnergy , fSpecularSpike , 2);
		  	  	  	  	  	  	  	 pGenericPolishedWhitePainted_MPT->AddProperty("BACKSCATTERCONSTANT"   ,fEnergy , fBackScatter   , 2);
		  	  	  	  	  	         //--------------------------------------------------------------------------------------------------


          // Attach optical properties table to optical surface
          pGenericPolishedWhitePainted_OS->SetMaterialPropertiesTable(pGenericPolishedWhitePainted_MPT);

	  } // end if

     return pGenericPolishedWhitePainted_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_GenericGroundWhitePainted()
  {
	  //--------------------------------------
	  // Generic ground surface, white painted
	  //--------------------------------------

	  if (pGenericGroundWhitePainted_OS == nullptr)
	  {
		  G4String name = "GenericGroundWhitePainted";

		  pGenericGroundWhitePainted_OS = new G4OpticalSurface(name);
		  pGenericGroundWhitePainted_OS->SetType(dielectric_dielectric);
		  pGenericGroundWhitePainted_OS->SetModel(unified);
		  pGenericGroundWhitePainted_OS->SetFinish(groundbackpainted);
		  pGenericGroundWhitePainted_OS->SetSigmaAlpha(12.0 * CLHEP::degree); // Janecek2010

		  //========================
		  // common paint attributes
		  //========================

		  //------------------------------------------------------------------------
		  // defining optical range of interest
		  //------------------------------------------------------------------------
		  G4double fLambdaMin         = 200.0*CLHEP::nanometer;
		  G4double fLambdaMax         = 800.0*CLHEP::nanometer;
		  G4double fPhotonEnergyMin   = this->LambdaToEnergyConversion(fLambdaMax);
		  G4double fPhotonEnergyMax   = this->LambdaToEnergyConversion(fLambdaMin);
		  //------------------------------------------------------------------------
		  G4double fEnergy[2]            = { fPhotonEnergyMin, fPhotonEnergyMax }; // Everything from far IR to VUV
		  G4double fRindex[2]            = { 1.61            , 1.61             }; // Janecek2010
		  G4double fReflectance[2]       = { 0.955           , 0.955            }; // Janecek2010
		  //------------------------------------------------------------------------
		  G4double fSpecularLobe[2]      = { 1.00            , 1.00             };
		  G4double fSpecularSpike[2]     = { 0.00            , 0.00             };
		  G4double fBackScatter[2]       = { 0.00            , 0.00             };
         //------------------------------------------------------------------------
		 // Sum must be one              =   1.00            , 1.00
		 //------------------------------------------------------------------------

		  G4MaterialPropertiesTable* pGenericGroundWhitePainted_MPT = new G4MaterialPropertiesTable();

                             	 	 pGenericGroundWhitePainted_MPT->AddProperty("RINDEX"                ,fEnergy , fRindex        ,2);

                             	 	 pGenericGroundWhitePainted_MPT->AddProperty("SPECULARLOBECONSTANT"  ,fEnergy , fSpecularLobe  ,2);
                             	 	 pGenericGroundWhitePainted_MPT->AddProperty("SPECULARSPIKECONSTANT" ,fEnergy , fSpecularSpike ,2);
                             	 	 pGenericGroundWhitePainted_MPT->AddProperty("BACKSCATTERCONSTANT"   ,fEnergy , fBackScatter   ,2);

                             	 	 pGenericGroundWhitePainted_MPT->AddProperty("REFLECTIVITY"          ,fEnergy , fReflectance     ,2);

          // Attach optical properties table to optical surface
          pGenericGroundWhitePainted_OS->SetMaterialPropertiesTable(pGenericGroundWhitePainted_MPT);

	  }  // end if

     return pGenericGroundWhitePainted_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_GenericPolishedBlackPainted()
  {
	  //----------------------------------------
	  // Generic polished surface, black painted
	  //----------------------------------------

	  if (pGenericPolishedBlackPainted_OS == nullptr)
	  {
		  G4String name = "GenericPolishedBlackPainted";

		  pGenericPolishedBlackPainted_OS = new G4OpticalSurface(name);
		  pGenericPolishedBlackPainted_OS->SetType(dielectric_dielectric);
		  pGenericPolishedBlackPainted_OS->SetModel(unified);
		  pGenericPolishedBlackPainted_OS->SetFinish(groundbackpainted);
		  pGenericPolishedBlackPainted_OS->SetSigmaAlpha(1.3 * CLHEP::degree); // Janecek2010

		  //========================
		  // common paint attributes
		  //========================

		  //------------------------------------------------------------------------
		  // defining optical range of interest
		  //------------------------------------------------------------------------
		  G4double fLambdaMin         = 200.0*CLHEP::nanometer;
		  G4double fLambdaMax         = 800.0*CLHEP::nanometer;
		  G4double fPhotonEnergyMin   = this->LambdaToEnergyConversion(fLambdaMax);
		  G4double fPhotonEnergyMax   = this->LambdaToEnergyConversion(fLambdaMin);
		  //------------------------------------------------------------------------
		  G4double fEnergy[2]            = { fPhotonEnergyMin, fPhotonEnergyMax }; // Everything from far IR to VUV
		  G4double fRindex[2]            = { 1.61            , 1.61             }; // Janecek2010
		  G4double fReflectance[2]       = { 0.955           , 0.955            }; // Janecek2010
		  //------------------------------------------------------------------------
		  G4double fSpecularLobe[2]      = { 0.957           , 0.957            };
		  G4double fSpecularSpike[2]     = { 0.00            , 0.00             };
		  G4double fBackScatter[2]       = { 0.043           , 0.043            }; // Dury2006
         //------------------------------------------------------------------------
		 // Sum must be one              =   1.00            , 1.00
		 //------------------------------------------------------------------------

		  G4MaterialPropertiesTable* pGenericPolishedBlackPainted_MPT = new G4MaterialPropertiesTable();

	  	  	  	  	  	  	  	 	 pGenericPolishedBlackPainted_MPT->AddProperty("RINDEX"                ,fEnergy , fRindex        ,2);
	  	  	  	  	  	  	  	 	 pGenericPolishedBlackPainted_MPT->AddProperty("SPECULARLOBECONSTANT"  ,fEnergy , fSpecularLobe  ,2);
	  	  	  	  	  	  	  	 	 pGenericPolishedBlackPainted_MPT->AddProperty("SPECULARSPIKECONSTANT" ,fEnergy , fSpecularSpike ,2);
	  	  	  	  	  	  	  	 	 pGenericPolishedBlackPainted_MPT->AddProperty("BACKSCATTERCONSTANT"   ,fEnergy , fBackScatter   ,2);
	  	  	  	  	  	  	  	 	 pGenericPolishedBlackPainted_MPT->AddProperty("REFLECTIVITY"          ,fEnergy , fReflectance   ,2);

          // Attach optical properties table to optical surface
	      pGenericPolishedBlackPainted_OS->SetMaterialPropertiesTable(pGenericPolishedBlackPainted_MPT);

	  } // end if

	 return pGenericPolishedBlackPainted_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_GenericGroundBlackPainted()
  {
	  //--------------------------------------
	  // Generic ground surface, black painted
	  //--------------------------------------

	  if (pGenericGroundBlackPainted_OS == nullptr)
	  {
		  G4String name = "GenericGroundBlackPainted";

		  pGenericGroundBlackPainted_OS = new G4OpticalSurface(name);
		  pGenericGroundBlackPainted_OS->SetType(dielectric_dielectric);
		  pGenericGroundBlackPainted_OS->SetModel(unified);
		  pGenericGroundBlackPainted_OS->SetFinish(groundbackpainted);
		  pGenericGroundBlackPainted_OS->SetSigmaAlpha(12.0 * CLHEP::degree); // Janecek2010

		  //========================
		  // common paint attributes
		  //========================

		  //------------------------------------------------------------------------
		  // defining optical range of interest
		  //------------------------------------------------------------------------
		  G4double fLambdaMin         = 200.0*CLHEP::nanometer;
		  G4double fLambdaMax         = 800.0*CLHEP::nanometer;
		  G4double fPhotonEnergyMin   = this->LambdaToEnergyConversion(fLambdaMax);
		  G4double fPhotonEnergyMax   = this->LambdaToEnergyConversion(fLambdaMin);
		  //------------------------------------------------------------------------
		  G4double fEnergy[2]            = { fPhotonEnergyMin, fPhotonEnergyMax }; // Everything from far IR to VUV
		  G4double fRindex[2]            = { 1.61            , 1.61             }; // Janecek2010
		  G4double fReflectance[2]       = { 0.955           , 0.955            }; // Janecek2010
		  //------------------------------------------------------------------------
		  G4double fSpecularLobe[2]      = { 0.957           , 0.957            };
		  G4double fSpecularSpike[2]     = { 0.00            , 0.00             };
		  G4double fBackScatter[2]       = { 0.043           , 0.043            }; // Dury2006
         //------------------------------------------------------------------------
		 // Sum must be one              =   1.00            , 1.00
		 //------------------------------------------------------------------------

		  G4MaterialPropertiesTable* pGenericGroundBlackPainted_MPT = new G4MaterialPropertiesTable();

                             	 	 pGenericGroundBlackPainted_MPT->AddProperty("RINDEX"                ,fEnergy , fRindex        ,2);
                             	 	 pGenericGroundBlackPainted_MPT->AddProperty("SPECULARLOBECONSTANT"  ,fEnergy , fSpecularLobe  ,2);
                             	 	 pGenericGroundBlackPainted_MPT->AddProperty("SPECULARSPIKECONSTANT" ,fEnergy , fSpecularSpike ,2);
                             	 	 pGenericGroundBlackPainted_MPT->AddProperty("BACKSCATTERCONSTANT"   ,fEnergy , fBackScatter   ,2);
                             	 	 pGenericGroundBlackPainted_MPT->AddProperty("REFLECTIVITY"          ,fEnergy , fReflectance   ,2);

          // Attach optical properties table to optical surface
          pGenericGroundBlackPainted_OS->SetMaterialPropertiesTable(pGenericGroundBlackPainted_MPT);

	  } // end if

     return pGenericGroundBlackPainted_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_AirGroundAluminum()
  {
	  //-----------------------------------
	  // Air facing ground Aluminum surface
	  //-----------------------------------

	  if (pAirGroundAluminum_OS == nullptr)
	  {
		  G4String name = "AirGroundAluminum";

		  pAirGroundAluminum_OS = new G4OpticalSurface(name);
		  pAirGroundAluminum_OS->SetType(dielectric_metal);
		  pAirGroundAluminum_OS->SetModel(unified);
		  pAirGroundAluminum_OS->SetFinish(ground);
		  pAirGroundAluminum_OS->SetSigmaAlpha(12.0*CLHEP::degree);

		  // Attach optical properties table to optical surface: retrieve complex refraction index from material Aluminum
		  // Since the complex index of refraction is provided, Geant4 can calculate the REFLECTIVITY

		  G4Material* pMyAluminum = this->GetMaterial_Aluminum();

		  // aMaterialPropertiesTable->GetProperty("REFLECTIVITY");
		  pAirGroundAluminum_OS->SetMaterialPropertiesTable( pMyAluminum->GetMaterialPropertiesTable() );

	  } // end if

     return pAirGroundAluminum_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_AirPolishedAluminum()
{
	  //-----------------------------------
	  // Air facing Polished Aluminum surface
	  //-----------------------------------

	  if (pAirPolishedAluminum_OS == nullptr)
	  {
		  G4String name = "AirPolishedAluminum";

		  pAirPolishedAluminum_OS = new G4OpticalSurface(name);
		  pAirPolishedAluminum_OS->SetType(dielectric_metal);
		  pAirPolishedAluminum_OS->SetModel(unified);
		  pAirPolishedAluminum_OS->SetFinish(polished);
		  pAirPolishedAluminum_OS->SetSigmaAlpha(1.3*CLHEP::degree);

		  // Attach optical properties table to optical surface: retrieve complex refraction index from material Aluminum
		  // Since the complex index of refraction is provided, Geant4 can calculate the REFLECTIVITY

		  G4Material* pMyAluminum = this->GetMaterial_Aluminum();

		  pAirPolishedAluminum_OS->SetMaterialPropertiesTable( pMyAluminum->GetMaterialPropertiesTable());

	  } // end if

     return pAirPolishedAluminum_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_AirGroundTitanium()
{
	  //-----------------------------------
	  // Air facing ground Titanium surface
	  //-----------------------------------

	  if (pAirGroundTitanium_OS == nullptr)
	  {
		  G4String name = "AirGroundTitanium";

		  pAirGroundTitanium_OS = new G4OpticalSurface(name);
		  pAirGroundTitanium_OS->SetType(dielectric_metal);
		  pAirGroundTitanium_OS->SetModel(unified);
		  pAirGroundTitanium_OS->SetFinish(ground);
		  pAirGroundTitanium_OS->SetSigmaAlpha(12.0*CLHEP::degree);

		  // Attach optical properties table to optical surface: retrieve complex refraction index from material Titanium
		  // Since the complex index of refraction is provided, Geant4 can calculate the REFLECTIVITY

		  G4Material* pMyTitanium = this->GetMaterial_Titanium();

		  pAirGroundTitanium_OS->SetMaterialPropertiesTable( pMyTitanium->GetMaterialPropertiesTable());

	  } // end if

	  return pAirGroundTitanium_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_AirPolishedTitanium()
{
	  //-----------------------------------
	  // Air facing polished Titanium surface
	  //-----------------------------------

	  if (pAirPolishedTitanium_OS == nullptr)
	  {
		  G4String name = "AirPolishedTitanium";

		  pAirPolishedTitanium_OS = new G4OpticalSurface(name);
		  pAirPolishedTitanium_OS->SetType(dielectric_metal);
		  pAirPolishedTitanium_OS->SetModel(unified);
		  pAirPolishedTitanium_OS->SetFinish(polished);
		  pAirPolishedTitanium_OS->SetSigmaAlpha(1.3*CLHEP::degree);

		  // Attach optical properties table to optical surface: retrieve complex refraction index from material Titanium
		  // Since the complex index of refraction is provided, Geant4 can calculate the REFLECTIVITY

		  G4Material* pMyTitanium = this->GetMaterial_Titanium();

		  pAirPolishedTitanium_OS->SetMaterialPropertiesTable( pMyTitanium->GetMaterialPropertiesTable());

	  } // end if

	  return pAirPolishedTitanium_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_AirGroundSilver()
  {
	  //-----------------------------------
	  // Air facing ground Silver surface
	  //-----------------------------------

	  if (pAirGroundSilver_OS == nullptr)
	  {
		  G4String name = "AirGroundSilver";

		  pAirGroundSilver_OS = new G4OpticalSurface(name);
		  pAirGroundSilver_OS->SetType(dielectric_metal);
		  pAirGroundSilver_OS->SetModel(unified);
		  pAirGroundSilver_OS->SetFinish(ground);
		  pAirGroundSilver_OS->SetSigmaAlpha(12.0*CLHEP::degree);

		  // Attach optical properties table to optical surface: retrieve complex refraction index from material Silver
		  // Since the complex index of refraction is provided, Geant4 can calculate the REFLECTIVITY

		  G4Material* pMySilver = this->GetMaterial_Silver();

		  pAirGroundSilver_OS->SetMaterialPropertiesTable( pMySilver->GetMaterialPropertiesTable());

	  } // end if

	  return pAirGroundSilver_OS;
  }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_AirPolishedSilver()
{
	  //-----------------------------------
	  // Air facing Polished Silver surface
	  //-----------------------------------

	  if (pAirPolishedSilver_OS == nullptr)
	  {
		  G4String name = "AirPolishedSilver";

		  pAirPolishedSilver_OS = new G4OpticalSurface(name);
		  pAirPolishedSilver_OS->SetType(dielectric_metal);
		  pAirPolishedSilver_OS->SetModel(unified);
		  pAirPolishedSilver_OS->SetFinish(polished);
		  pAirPolishedSilver_OS->SetSigmaAlpha(1.3*CLHEP::degree);

		  // Attach optical properties table to optical surface: retrieve complex refraction index from material Silver
		  // Since the complex index of refraction is provided, Geant4 can calculate the REFLECTIVITY

		  G4Material* pMySilver = this->GetMaterial_Silver();

		  pAirPolishedSilver_OS->SetMaterialPropertiesTable( pMySilver->GetMaterialPropertiesTable());

	  } // end if

	  return pAirPolishedSilver_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_AirGroundGold()
{
	//-----------------------------------
	// Air facing ground Gold surface
	//-----------------------------------

	if (pAirGroundGold_OS == nullptr)
	{
		G4String name = "AirGroundGold";

		pAirGroundGold_OS = new G4OpticalSurface(name);
		pAirGroundGold_OS->SetType(dielectric_metal);
		pAirGroundGold_OS->SetModel(unified);
		pAirGroundGold_OS->SetFinish(ground);
		pAirGroundGold_OS->SetSigmaAlpha(12.0*CLHEP::degree);

		// Attach optical properties table to optical surface: retrieve complex refraction index from material Gold
		// Since the complex index of refraction is provided, Geant4 can calculate the REFLECTIVITY

		G4Material* pMyGold = this->GetMaterial_Gold();

		pAirGroundGold_OS->SetMaterialPropertiesTable( pMyGold->GetMaterialPropertiesTable());

	} // end if

	return pAirGroundGold_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_AirPolishedGold()
{
	//-----------------------------------
	// Air facing polished Gold surface
	//-----------------------------------

	if (pAirPolishedGold_OS == nullptr)
	{
		G4String name = "AirPolishedGold";

		pAirPolishedGold_OS = new G4OpticalSurface(name);
		pAirPolishedGold_OS->SetType(dielectric_metal);
		pAirPolishedGold_OS->SetModel(unified);
		pAirPolishedGold_OS->SetFinish(polished);
		pAirPolishedGold_OS->SetSigmaAlpha(1.3*CLHEP::degree);

		// Attach optical properties table to optical surface: retrieve complex refraction index from material Gold
		// Since the complex index of refraction is provided, Geant4 can calculate the REFLECTIVITY
		G4Material* pMyGold = this->GetMaterial_Gold();

		pAirPolishedGold_OS->SetMaterialPropertiesTable( pMyGold->GetMaterialPropertiesTable());

	} // end if

	return pAirPolishedGold_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_PhotoCathode()
{
	//============================================================================
	//                      Optical Surface Properties of Cathode
	//============================================================================

	if (pPhotoCathode_OS == nullptr)
	{

		G4String name = "PhotoCathode";

		pPhotoCathode_OS = new G4OpticalSurface(name);

		// The boundary glass <--> cathode is best described by dielectric (glass) to metal (cathode)
		//PhotoCathode_OS->SetType(dielectric_metal);
		pPhotoCathode_OS->SetType(dielectric_dielectric);
		pPhotoCathode_OS->SetModel(unified);
		pPhotoCathode_OS->SetFinish(ground);

		// However, use setup from Qweak experiment
		//PhotoCathode_OS->SetModel(glisur);
		//PhotoCathode_OS->SetFinish(polished);

		// Not sure what this means and what the impact is
		pPhotoCathode_OS->SetSigmaAlpha(0.0*CLHEP::degree);

		//------------------------------------------------------------------------
		// defining optical range of interest
		//------------------------------------------------------------------------
		G4double fLambdaMin         = 200.0*CLHEP::nanometer;
		G4double fLambdaMax         = 800.0*CLHEP::nanometer;
		G4double fPhotonEnergyMin   = this->LambdaToEnergyConversion(fLambdaMax);
		G4double fPhotonEnergyMax   = this->LambdaToEnergyConversion(fLambdaMin);

		//------------------------------------------------------------------------
		G4double fEnergy[2]       = { fPhotonEnergyMin, fPhotonEnergyMax };
		G4double fReflectivity[2] = { 0.00            , 0.00             }; // Cathode surface does not reflect light (simple, but not true ...)
		G4double fEfficiency[2]   = { 1.00            , 1.00             }; // Perfect detection efficiency. PMT QE will be applied in analysis

		// At this stage assume a 100% efficiency independent of wavelength. The PMT quantum efficiency  will used later on
		// when filling hits (see BTSimEventAction::EndOfEventAction(), BTSimAnalysis::GetNumberOfPhotoElectrons_D753WKB())

		G4MaterialPropertiesTable* pPhotoCathode_MPT = new G4MaterialPropertiesTable();

							   	   pPhotoCathode_MPT->AddProperty("REFLECTIVITY" , fEnergy, fReflectivity, 2);
							   	   pPhotoCathode_MPT->AddProperty("EFFICIENCY"   , fEnergy, fEfficiency  , 2);

       // Attach optical properties table to optical surface
	  pPhotoCathode_OS->SetMaterialPropertiesTable(pPhotoCathode_MPT);

	} // end if

    return pPhotoCathode_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetOpticalSurface_BlackHole()
{
	//============================================================================
	//                   Perfect Black Absorber
	//============================================================================

	if (pBlackHole_OS == nullptr)
	{

		G4String name = "BlackHole";

	//	pBlackHole_OS = new G4OpticalSurface(name, unified);
		pBlackHole_OS = new G4OpticalSurface("BlackHole");

		// The boundary description
		pBlackHole_OS->SetType(dielectric_dielectric);

		pBlackHole_OS->SetModel(glisur);
		pBlackHole_OS->SetFinish(polished);

		// Not sure why is is needed for a full absorption
		pBlackHole_OS->SetSigmaAlpha(0.0*CLHEP::degree);

		//------------------------------------------------------------------------
		// defining optical range of interest
		//------------------------------------------------------------------------
		G4double fLambdaMin         = 200.0*CLHEP::nanometer;
		G4double fLambdaMax         = 800.0*CLHEP::nanometer;
		G4double fPhotonEnergyMin   = this->LambdaToEnergyConversion(fLambdaMax);
		G4double fPhotonEnergyMax   = this->LambdaToEnergyConversion(fLambdaMin);

		//------------------------------------------------------------------------
		G4double fEnergy[2]       = { fPhotonEnergyMin, fPhotonEnergyMax };
		G4double fReflectivity[2] = { 0.00            , 0.00             }; // Does not reflect any light: perfect Back
		G4double fEfficiency[2]   = { 0.00            , 0.00             }; // Perfect detection efficiency, not needed here

		G4MaterialPropertiesTable* pBlackHole_MPT = new G4MaterialPropertiesTable();

                               	   pBlackHole_MPT->AddProperty("REFLECTIVITY" , fEnergy , fReflectivity ,2);
                               	   pBlackHole_MPT->AddProperty("EFFICIENCY"   , fEnergy , fEfficiency   ,2);

        // Attach optical properties table to optical surface
        pBlackHole_OS->SetMaterialPropertiesTable(pBlackHole_MPT);

	}  // end if

    return pBlackHole_OS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4OpticalSurface* BTSimCommon_MaterialParameters::GetSurfaceObjectByName(G4String name)
{
	   if (name == "BGOPolishedAir")               {this->GetOpticalSurface_BGOPolishedAir();                      return pBGOPolishedAir_OS;}
  else if (name == "BGOPolishedLumirrorAir")       {this->GetOpticalSurface_BGOPolishedLumirrorAir();              return pBGOPolishedLumirrorAir_OS;}
  else if (name == "BGOPolishedLumirrorGlue")      {this->GetOpticalSurface_BGOPolishedLumirrorGlue();             return pBGOPolishedLumirrorGlue_OS;}
  else if (name == "BGOPolishedSpectraflectAir")   {this->GetOpticalSurface_BGOPolishedSpectraflectAir();          return pBGOPolishedSpectraflectAir_OS;}
  else if (name == "BGOPolishedSpectralonAir")     {this->GetOpticalSurface_BGOPolishedSpectralonAir();            return pBGOPolishedSpectralonAir_OS;}
  else if (name == "BGOPolishedTeflonAir")         {this->GetOpticalSurface_BGOPolishedTeflonAir();                return pBGOPolishedTeflonAir_OS;}
  else if (name == "BGOPolishedTioAir")            {this->GetOpticalSurface_BGOPolishedTioAir();                   return pBGOPolishedTioAir_OS;}
  else if (name == "BGOPolishedTyvekAir")          {this->GetOpticalSurface_BGOPolishedTyvekAir();                 return pBGOPolishedTyvekAir_OS;}
  else if (name == "BGOPolishedVM2000Air")         {this->GetOpticalSurface_BGOPolishedVM2000Air();                return pBGOPolishedVM2000Air_OS;}
  else if (name == "BGOPolishedVM2000Glue")        {this->GetOpticalSurface_BGOPolishedVM2000Glue();               return pBGOPolishedVM2000Glue_OS;}

  else if (name == "BGOGroundAir")                 {this->GetOpticalSurface_BGOGroundAir();                        return pBGOGroundAir_OS;}
  else if (name == "BGOGroundLumirrorAir")         {this->GetOpticalSurface_BGOGroundLumirrorAir();                return pBGOGroundLumirrorAir_OS;}
  else if (name == "BGOGroundLumirrorGlue")        {this->GetOpticalSurface_BGOGroundLumirrorGlue();               return pBGOGroundLumirrorGlue_OS;}
  else if (name == "BGOGroundSpectraflectAir")     {this->GetOpticalSurface_BGOGroundSpectraflectAir();            return pBGOGroundSpectraflectAir_OS;}
  else if (name == "BGOGroundSpectralonAir")       {this->GetOpticalSurface_BGOGroundSpectralonAir();              return pBGOGroundSpectralonAir_OS;}
  else if (name == "BGOGroundTeflonAir")           {this->GetOpticalSurface_BGOGroundTeflonAir();                  return pBGOGroundTeflonAir_OS;}
  else if (name == "BGOGroundTioAir")              {this->GetOpticalSurface_BGOGroundTioAir();                     return pBGOGroundTioAir_OS;}
  else if (name == "BGOGroundTyvekAir")            {this->GetOpticalSurface_BGOGroundTyvekAir();                   return pBGOGroundTyvekAir_OS;}
  else if (name == "BGOGroundVM2000Air")           {this->GetOpticalSurface_BGOGroundVM2000Air();                  return pBGOGroundVM2000Air_OS;}
  else if (name == "BGOGroundVM2000Glue")          {this->GetOpticalSurface_BGOGroundVM2000Glue();                 return pBGOGroundVM2000Glue_OS;}

  else if (name == "PerfectSmoothSurface")         {this->GetOpticalSurface_PerfectSmooth();                       return pPerfectPolished_OS;}
  else if (name == "DiamondMilled")                {this->GetOpticalSurface_DiamondMilled();                       return pDiamondMilled_OS;}

  else if (name == "GenericGroundAir")             {this->GetOpticalSurface_GenericGroundAir();                    return pGenericGroundAir_OS;}
  else if (name == "GenericPolishedAir")           {this->GetOpticalSurface_GenericPolishedAir();                  return pGenericPolishedAir_OS;}
  else if (name == "GenericPolishedWhitePainted")  {this->GetOpticalSurface_GenericPolishedWhitePainted();         return pGenericPolishedWhitePainted_OS;}
  else if (name == "GenericGroundWhitePainted")    {this->GetOpticalSurface_GenericGroundWhitePainted();           return pGenericGroundWhitePainted_OS;}
  else if (name == "GenericPolishedBlackPainted")  {this->GetOpticalSurface_GenericPolishedBlackPainted();         return pGenericPolishedBlackPainted_OS;}
  else if (name == "GenericGroundBlackPainted")    {this->GetOpticalSurface_GenericGroundBlackPainted();           return pGenericGroundBlackPainted_OS;}

  else if (name == "AirGroundAluminum")            {this->GetOpticalSurface_AirGroundAluminum();                   return pAirGroundAluminum_OS;}
  else if (name == "AirGroundTitanium")            {this->GetOpticalSurface_AirGroundTitanium();                   return pAirGroundTitanium_OS;}
  else if (name == "AirGroundSilver")              {this->GetOpticalSurface_AirGroundSilver();                     return pAirGroundSilver_OS;}
  else if (name == "AirGroundGold")                {this->GetOpticalSurface_AirGroundGold();                       return pAirGroundGold_OS;}

  else if (name == "AirPolishedAluminum")          {this->GetOpticalSurface_AirPolishedAluminum();                 return pAirPolishedAluminum_OS;}
  else if (name == "AirPolishedTitanium")          {this->GetOpticalSurface_AirPolishedTitanium();                 return pAirPolishedTitanium_OS;}
  else if (name == "AirPolishedSilver")            {this->GetOpticalSurface_AirPolishedSilver();                   return pAirPolishedSilver_OS;}
  else if (name == "AirPolishedGold")              {this->GetOpticalSurface_AirPolishedGold();                     return pAirPolishedGold_OS;}

  else if (name == "PhotoCathode")                 {this->GetOpticalSurface_PhotoCathode();                        return pPhotoCathode_OS;}
  else if (name == "BlackHole")                    {this->GetOpticalSurface_BlackHole();                           return pBlackHole_OS;}

  else
  {
	  G4cerr << "======>>> BTSimCommon_MaterialParameters::GetSurfaceObjectByName() : Could not find object " << name << G4endl;
	  return 0;
  }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::GetDensityOfAir(G4double pressure_Pa, G4double temperature_celsius, G4double humidity)
{

	G4double term1 = (1/ ((temperature_celsius+273.15)*287.05));
	G4double term2 = pressure_Pa - (1-(287.05/461.495))*humidity*611.213*TMath::Exp( 17.5043*temperature_celsius/(241.2+temperature_celsius));

	G4double airdensity = term1*term2*CLHEP::mg/CLHEP::cm3;

	std::cout << "PTB Air density = " << airdensity/ (CLHEP::mg/CLHEP::cm3)      << " mg/cm3 @" << humidity*100.0 <<"% Humidity and " << temperature_celsius << "°C"<< std::endl;


	return airdensity;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::DefineIsotopes_Boron()
{
	//---------------------------------
	// Define stable isotopes of Boron
	//---------------------------------
	//
	// Atomic masses taken from: http://www.tracesciences.com/b.htm

	pB10_ID = new G4Isotope( "B10",                               // its name
			                    5,                                // atomic number (Z)
							   10,                                // number of nucleons (A=N+Z)
							   10.0129369*CLHEP::g/CLHEP::mole    // mass of mole
						    );

	pB11_ID = new G4Isotope( "B11",
			                    5,
							   11,
							   11.0093054*CLHEP::g/CLHEP::mole
							);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::DefineIsotopes_Gadolinium()
{
	//-------------------------------
	// Define isotopes of Gadolinium
	//-------------------------------
	//
	// Atomic masses taken from: http://www.tracesciences.com/gd.htm

	pGd152_ID = new G4Isotope("Gd152", 64, 152, 151.919786*CLHEP::g/CLHEP::mole);
	pGd154_ID = new G4Isotope("Gd154", 64, 154, 153.920861*CLHEP::g/CLHEP::mole);
	pGd155_ID = new G4Isotope("Gd155", 64, 155, 154.922618*CLHEP::g/CLHEP::mole);
	pGd156_ID = new G4Isotope("Gd156", 64, 156, 155.922118*CLHEP::g/CLHEP::mole);
	pGd157_ID = new G4Isotope("Gd157", 64, 157, 156.923956*CLHEP::g/CLHEP::mole);
	pGd158_ID = new G4Isotope("Gd158", 64, 158, 157.924019*CLHEP::g/CLHEP::mole);
	pGd160_ID = new G4Isotope("Gd160", 64, 160, 159.927049*CLHEP::g/CLHEP::mole);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::DefineIsotopes_Helium()
{
	//---------------------------
	// Define isotopes of Helium
	//---------------------------
	//
	// Atomic masses taken from: http://www.tracesciences.com/gd.htm

	G4int    nProtons    = 2;
	G4int    nNeutrons   = 1;
	G4int    nNucleons   = nProtons + nNeutrons;

	G4double fAtomicMass = 3.016*CLHEP::g/CLHEP::mole;

	pHe3_ID = new G4Isotope("He3", 2, 3, 3.016*CLHEP::g/CLHEP::mole);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::DefineIsotopes_Lithium()
{
	//-----------------------------------
	// Define stable isotopes of Lithium
	//-----------------------------------
	//
	// Atomic masses taken from: https://en.wikipedia.org/wiki/Isotopes_of_lithium#Lithium-6

	pLi6_ID = new G4Isotope("Li6", 3, 6, 6.015123*CLHEP::g/CLHEP::mole);
	pLi7_ID = new G4Isotope("Li7", 3, 7, 7.016005*CLHEP::g/CLHEP::mole);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::SetOpticalAbsorptionLength_ZnS_Ag (G4double value)
{
	std::cout << "Calling BTSimCommon_MaterialParameters::SetOpticalAbsorptionLength_ZnS_Ag()" << std::endl;

		fAbsorptionLength_ZnS_Ag = value;

	std::cout << "Leaving BTSimCommon_MaterialParameters::SetOpticalAbsorptionLength_ZnS_Ag()" << std::endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::GetOpticalAbsorptionLength_ZnS_Ag(G4double wavelength)
{
	std::cout << "Calling BTSimCommon_MaterialParameters::GetOpticalAbsorptionLength_ZnS_Ag()" << std::endl;

	std::cout << "################# The optical absorption length of ZnS:Ag is set to " << fAbsorptionLength_ZnS_Ag/CLHEP::micrometer << " [um]" << std::endl;

	return fAbsorptionLength_ZnS_Ag;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BTSimCommon_MaterialParameters::SetIndexOfRefraction_MagicOpticalBond (G4double value)
{
	std::cout << "Calling BTSimCommon_MaterialParameters::SetIndexOfRefraction_MagicOpticalBond()" << std::endl;

		fIndexOfRefraction_MagicOpticalBond = value;

	std::cout << "Leaving BTSimCommon_MaterialParameters::SetIndexOfRefraction_MagicOpticalBond()" << std::endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double BTSimCommon_MaterialParameters::GetIndexOfRefraction_MagicOpticalBond()
{
	std::cout << "Calling BTSimCommon_MaterialParameters::GetIndexOfRefraction_MagicOpticalBond()" << std::endl;

	std::cout << "################# The index of refraction of non-existing MagicOpticalBond is set to " << fIndexOfRefraction_MagicOpticalBond << std::endl;

	return fIndexOfRefraction_MagicOpticalBond;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

std::vector<G4double>  BTSimCommon_MaterialParameters::ConvertVolumeToMassFraction( std::vector<G4double> vVolumeFraction, std::vector<G4double> vMolarWeight )
{
	// Extract number of elements in mixture
	G4int nParts = vVolumeFraction.size();

	// calculate total mass for 100 kmol of mixture
	G4double fTotalMass = 0.0;

	for ( G4int i=0; i<nParts; i++)
	{
		fTotalMass = fTotalMass + (vVolumeFraction.at(i)*vMolarWeight.at(i));
	}

	std::cout << "####### Mixture total mass = " << fTotalMass << std::endl;

	// Define and initialize
	std::vector<G4double> vMassFraction;
	                      vMassFraction.clear();

	// Calculate mass fractions
	G4double fMassFraction = 0.0;

	for ( G4int i=0; i<nParts; i++)
	{
		fMassFraction = (vVolumeFraction.at(i)*vMolarWeight.at(i)) / fTotalMass;

		vMassFraction.push_back( fMassFraction );

		std::cout << "####### Mixture mass fraction [%] = " << fMassFraction*100.0 << std::endl;
	}

	return vMassFraction;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

std::vector<G4double>  BTSimCommon_MaterialParameters::ConvertPartialPressureToMassFraction( std::vector<G4double> vPartialPressure, std::vector<G4double> vMolarWeight )
{
	//---------------------------------------
	// Extract number of elements in mixture
	//---------------------------------------
	G4int nParts = vPartialPressure.size();

	//-------------------------------------
	// Calculate total pressure of mixture
	//-------------------------------------
	G4double fTotalPressure = 0.0;

	for ( G4int i=0; i<nParts; i++)
	{
		fTotalPressure = fTotalPressure +  vPartialPressure.at(i);
	}

	std::cout << "####### Mixture total pressure [mbar] = " << fTotalPressure << std::endl;

	//----------------------------
	// Calculate volume fractions
	//----------------------------
	std::vector<G4double> vVolumeFraction; vVolumeFraction.clear();

	G4double fVolumeFraction = 0.0;

	for ( G4int i=0; i<nParts; i++)
	{
		fVolumeFraction = vPartialPressure.at(i) / fTotalPressure;

		vVolumeFraction.push_back( fVolumeFraction );

		std::cout << "####### Mixture volume fraction [%] = " << fVolumeFraction*100.0 << std::endl;
	}

	//----------------------------------------------
	// Calculate total mass for 100 kmol of mixture
	//----------------------------------------------
	G4double fTotalMass = 0.0;

	for ( G4int i=0; i<nParts; i++)
	{
		fTotalMass = fTotalMass + (vVolumeFraction.at(i)*vMolarWeight.at(i));
	}

	std::cout << "####### Mixture total mass [kg]= " << fTotalMass << std::endl;

	//--------------------------
	// Calculate mass fractions
	//--------------------------
	std::vector<G4double> vMassFraction; vMassFraction.clear();

	G4double fMassFraction = 0.0;

	for ( G4int i=0; i<nParts; i++)
	{
		fMassFraction = (vVolumeFraction.at(i)*vMolarWeight.at(i)) / fTotalMass;

		vMassFraction.push_back( fMassFraction );

		std::cout << "####### Mixture mass fraction [%] = " << fMassFraction*100.0 << std::endl;
	}

	return vMassFraction;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

