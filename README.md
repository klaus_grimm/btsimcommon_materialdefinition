### README BTSimCommon_MaterialDefinition ###

This C++ class represents an extended list of materials and optical surfaces to be used in Geant4 simulation. Some materials have optical properties included, i.e. scintillators, glasses, and some metals. 
The optical properties include:

* Index of refraction
* Absorption length
* Emission spectrum (in case of scintillators)

### How to use it in your Geant4 code
Part 1/2: Write in your MyDetectorConstruction.hh  
>   
```C++  
#include "BTSimCommon_MaterialParameters.hh"

class MyDetectorConstruction
{
...
private:

BTSimCommon_MaterialParameters*	pMaterial;

G4Material*	pMasterContainer_Material;
G4Material*	pObjectA_Material;
G4Material*	pObjectB_Material;
G4Material*	pObjectC_Material;
...
}
```
Part 2/2: Write in your MyDetectorConstruction.cc constructor
>  
```C++
MyDetectorConstruction::MyDetectorConstruction()
{
...
pMaterial = BTSimCommon_MaterialParameters::GetInstance();

pMasterContainer_Material	= pMaterial->GetMaterialDefinitionByName("Air");
pObjectA_Material	= pMaterial->GetMaterialDefinitionByName("Al2007");
pObjectB_Material	= pMaterial->GetMaterialDefinitionByName("Nickel");
pObjectC_Material	= pMaterial->GetMaterialDefinitionByName("ArCH4Gas_9010_858mbar");
...
}
```

And later on in e.g.
>  

```C++
void MyDetectorConstruction::ConstructDetector(G4VPhysicalVolume* pMotherVolume)
{
...
pMasterContainer_Logical  = nullptr;
pMasterContainer_Logical  = new G4LogicalVolume( pMasterContainer_Solid,
											     pMasterContainer_Material,
										              "MasterContainer_Logical",
											      nullptr,
										   	      nullptr,
											      nullptr,
											      true );
...												  
}
```

